Welcome to RiskCat, a package for probabilistic seismic risk assessment.

Building RiskCat
================

From the toplevel directory, go to the Source sub-directory and run make:
```
>> cd Source
>> make
```
The executable, "riskcat", is installed in the master directory.

Getting Started Example
=======================

From the toplevel directory, go to the EqSimRisk sub-directory
```
>> cd EqSimRisk
```
This directory contains an example run menu file, KingIs.menu. You can run RiskCat as:
```
>> ../riskcat  KingIs.menu
```
See the User Guide for further details.

