
! Generate a random number distributed [0,1]
! Current working algorithm using Tao Pang's method published and    
! copyrighted by Cambridge University Press in 1997.  
             
    real (4) function  rnunf ()

!******************************************************************************
    use GenParam
    implicit none
    INTEGER :: IA, IC, IH, IL, IQ, IR, IT

    DATA IA/16807/,IC/2147483647/,IQ/127773/,IR/2836/
    
        IH = IDSEED/IQ
        IL = MOD(IDSEED,IQ)
        IT = IA*IL-IR*IH
        IF(IT.GT.0) THEN
          IDSEED = IT
        ELSE
          IDSEED = IC+IT
        END IF
        rnunf = FLOAT(IDSEED) / FLOAT(IC)     
    
    return 
    end function rnunf
    
