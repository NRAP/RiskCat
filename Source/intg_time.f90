      subroutine intg_time(tra,np,dt)

      IMPLICIT NONE

      INTEGER :: i, np
      REAL    :: dt, tintg, totint
      REAL, dimension(np+1) ::  tra
! time domain integration
      totint=0.0
      do 2 i=1,np-1
      tintg=0.5*dt*(tra(i)+tra(i+1))
      totint=totint+tintg
      tra(i)=totint
 2    continue
      tra(np) = tra(np-1)
      do 3, i=1,np
      tra(i)=tra(i)-tra(1)
 3    continue
      return
      end





