!cssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
        subroutine  xyznext  (a1,a2,dip,d1,d2,b1,b2)
!
!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
!		This routine calculates the coordinates of the points B1 and B2
!		for a flat element of fault with top trace defined by points
!		A1 and A2, at depth D1. The fault is dipping with angle = dip
!		dip = 0 for a vertical fault.
!			  Positive when rotated positively (counterclockwise) when  
!               stationed at A1 and looking towards A2. 
!			  Negative otherwise.
!		B1 and B2 are at depth D2.
!		The points B1 and B2  are used to determine where two contiguous
!		segments of the fault intersect at the lower boundary at depth
!		D2, in order to avoid the overlap, (or gap) when segments are
!		not in the same plane.
!		Input:
!         *****
!		Coordinates of the points A1, A2, dip, depths D1 and D2.
!		Output:
!         ******
!		Coordinates of point B1 and B2:
!		B1(1) and B2(1) = longitude
!		B1(2) and B2(2) = latitude  decimal degrees
!		B1(3) and B2(3) = D2, both at depth D2

!c

implicit none
real :: alfa1,alfa2,a1(3),a2(3),b1(3),b2(3),aa2(3),dip,d1,d2
real :: rx,ry,signx,signy,xm,ym

 
!	Transform all coordinates into cartesian, center A1, all measures
!	in km.
ry = (6371.2 * 6.2831853/360)   ! length of 1 degree latitude
rx = ry * cos(a1(2)/57.2957795) ! length of 1 degree longitude at A1

aa2(1) = (a2(1)-a1(1)) * rx
aa2(2) = (a2(2)-a1(2)) * ry

signx = 1.
signy = -1.
if (aa2(2).lt.0.)  signx = -1.
if (aa2(1).lt.0.)  signy = 1.

if (aa2(1).ne.0.)  then
    alfa1 = aa2(2) / aa2(1)
    if (alfa1 .ne. 0.)  then
        alfa2 = -1./alfa1
        xm = (d2-d1) * tan(dip/57.2957795) / sqrt(1.+ alfa2*alfa2)
        xm = xm * signx
        ym = alfa2 * xm
    else
        xm = 0.
        ym = (d2-d1)*tan(dip/57.2957795)*signy
    end if
else
    xm = (d2-d1) * tan(dip/57.2957795) * signx
    ym = 0.
end if

!	Transform back to longitude, latitude and depth.
xm = xm / rx
ym = ym / ry


b1(1) = a1(1) + xm
b1(2) = a1(2) + ym
b1(3) = d2
b2(1) = a2(1) + xm
b2(2) = a2(2) + ym
b2(3) = d2


return

end subroutine xyznext
