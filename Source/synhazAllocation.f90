!       Called from GMHR2
!
!*******************************************************************************
!
        subroutine synhazAllocation (k, Ndim, Negf, Nel, Nev, Nipfl )
!
!*******************************************************************************
! 
!       Pupose is to allocate memory to run synhaz, and deallocate when finished
!       k = 1 allocate necessary memory for the specific case
!       k = 2 deallocate memory 
! 
!       Ndim  = (maxdim) maximum spectral dimensions for synthesis and EGF's
!               (ex: maxdim=10000) 
!       Negf  = (maxegf) number of points in sac files of empirical Green's functions,
!               must be large enough to allow fixtim to shift start of
!               record (tegf). must be larger than the NPTS of the SAC files
!               for EGF's (ex: =50000) 
!       Nel   = (maxel) number of elemental areas 
!       Nev   = (maxev) number of events used for empirical Greens functions    
!       Nipfl = (ipfl) number of fault perimeter points to specify, must be .ge. ip 
!               for dimensions 
!
!-------------------------------------------------------------------------------
!
        use Modulesynhaz   

        implicit none

        INTEGER :: k, Ndim, Negf, Nel, Nev, Nipfl

        If (k .eq. 1)  then

            !ALLOCATE ARRAYS
            ALLOCATE ( cspt(Nev), evid(Nev), pathname(Nev) ) 
 
            ALLOCATE ( blat(Nev) ,blon(Nev), bh(Nev), bm(Nev),  bm1(Nev),    &
                       bm2(Nev), spt(Nev),  bstr(Nev), bdp(Nev), bsv(Nev),   &
                       rstd(Nev), ffc(Nev), ffc2(Nev), pkkcc(Nev),  ma1(Nev) )

            ALLOCATE ( inel(Nel),ll(Nel),lla(Nel),mm(Nel),llb(Nel),pkid(Nel) )

            ALLOCATE ( eazm(Nel), rdip(Nel), rdis(Nel), ramo(Nel), rlat(Nel), &
                       rlon(Nel), rh(Nel), rlat1(Nel), rlon1(Nel), rh1(Nel),  &
                       rela(Nel), rstk(Nel), rsv(Nel), razm(Nel), ristm(Nel), &
                       eltim(Nel), rslp(Nel), rslp1(Nel), dtimp(Nel), ristm1(Nel), &
                       ict(Nel),rgd1(Nel), eltim1(Nel),xx(Nel),yy(Nel),       &
                       slptim(Nel),slpvl(Nel) )
            ALLOCATE ( fl(Nipfl), fla(Nipfl) )
    
            ALLOCATE ( trar(Negf), trat(Negf), traz(Negf), traaz(Negf),      &
                       traar(Negf), traat(Negf)                              )

            ALLOCATE ( trcz(Ndim), trcr(Ndim), trct(Ndim), ttim(Ndim),       &
                       fx(Ndim), fy(Ndim), aarray(Ndim), barray(Ndim),       &
                       ttt(Ndim), tra_tss(Ndim), tra_tds(Ndim),              &
                       tra_rss(Ndim), tra_rds(Ndim),  tra_rdd(Ndim),         &
                       tra_zss(Ndim), tra_zds(Ndim), tra_zdd(Ndim),          &
                       tra_rex(Ndim), tra_zex(Ndim)                          )

            ALLOCATE ( tzpz(Ndim), tzpr(Ndim), tzpt(Ndim), tzsz(Ndim),       &
                       tzsr(Ndim),tzst(Ndim), ttsp(Ndim),ttsr(Ndim),         &
                       ttst(Ndim),etrz(Ndim), etrr(Ndim),etrt(Ndim),         &
                       trz(Ndim), trr(Ndim),trt(Ndim), trazz(Ndim),          &
                       trazr(Ndim),trazt(Ndim)                               )

        else

           !DEALLOCATE ARRAYS:
           DEALLOCATE ( cspt, evid, pathname )

           DEALLOCATE ( inel, ll, lla, mm, llb, pkid )

           DEALLOCATE ( blat, blon, bh, bm, bm1, bm2, spt,bstr, bdp, bsv,    &
                        rstd, ffc, ffc2, pkkcc,  ma1                         )

           DEALLOCATE ( eazm, rdip, rdis, ramo,  rlat, rlon, rh, rlat1,      &
                        rlon1, rh1, rela, rstk, rsv, razm, ristm, eltim,     &
                        rslp, rslp1, slptim,   &
                        slpvl,dtimp, ristm1, ict, rgd1,  eltim1, xx, yy      )
                                 
           DEALLOCATE ( fl, fla )

           DEALLOCATE ( trar, trat, traz, traaz, traar, traat )

           DEALLOCATE ( trcz, trcr, trct, ttim, fx, fy, aarray, barray,      &
                        ttt, tra_tss, tra_tds, tra_rss, tra_rds, tra_rdd,    &
                        tra_zss, tra_zds, tra_zdd, tra_rex, tra_zex          )
                                  

           DEALLOCATE ( tzpz, tzpr, tzpt, tzsz, tzsr, tzst, ttsp, ttsr,      &
                        ttst, etrz, etrr, etrt, trz, trr, trt, trazz,        &
                        trazr, trazt                                         )


        end if

        return

        end subroutine synhazAllocation
