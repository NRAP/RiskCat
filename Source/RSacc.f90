! Calculates the velocity Response Spectrum for a Velocity time-series
!
!********************************************************************************
!
        subroutine RSacc (dzeta1, freq, Ndata, nfreq, PARS, ts1, vit1)
!
!********************************************************************************
!
! Input:
!       dzeta : Reduced damping factor. Ex: dzeta = 0.05 = 5% Damping of 1 DOF
!       Freq  : Array of frequencies for which  spectra are calculated (Hz)
!       Ndata : Number of data points in input velocity time-history
!       Nfreq : Dimension of array Freq
!       ts    : time step (sec.) of velocity time-series
!       vit   : Array of dimension Ndata, of velocity values (cm/sec)
!
! Output:
!       PARS  ; Pseudo-Acceleration-Response-Spectra array of dimension Nfreq
!               (cm/sec/sec)
!
!********************************************************************************
 
        IMPLICIT NONE

        INTEGER :: i, j, Ndata, Nfreq
        REAL    :: dzeta1, ts1
        REAL, dimension(Nfreq) :: freq, PARS
        REAL, dimension(Ndata) :: vit1

        REAL (KIND=8) :: dzeta, F0, PGA, ts
        REAL (KIND=8), dimension(Ndata) :: vit


        dzeta = DBLE (dzeta1) 
        ts    = DBLE(ts1)
        do j  = 1, Ndata
            vit(j) = DBLE(vit1(j))
        end do

        do i = 1, Nfreq
            F0 = DBLE (freq(i))
            call OneDOF (dzeta, F0, Ndata, PGA, ts, vit)
            PARS(i) = REAL (PGA)
        end do

        return

        end subroutine RSacc
!
!*******************************************************************************

  !************************************************************
  !* This procedure calculates the acceleration of the sismic *
  !* mass of an elementary oscillator (1 degree of freedom),  *
  !* the basis of which is submitted to a given acceleration, *
  !* x"(t).                                                   *
  !*  The input signal x" is digitalized with a constant time *
  !* step, Sampling_Incr. The table signal contains N accele- *
  !* ration values.                                           *
  !* -------------------------------------------------------- *
  !* INPUTS:                                                  *
  !*         f............: eigen frequency of oscillator     *
  !*         signal.......: input acceleration signal x"(t)   *
  !*         ts...........: time step of signal (constant)    *
  !*         dzeta........: reduced damping factor            *
  !*         ndata........: number of points of signal        *
  !* OUTPUT:                                                  *
  !*         response.....: table containing acceleration     *
  !*                        response of oscillator (N points) *
  !*                                                          *
  !*                        F90 version by J-P Moreau, Paris  *
  !************************************************************
Subroutine OneDOF (dzeta,f,ndata,PGA,ts,signal)

    IMPLICIT NONE

    INTEGER :: ii, ndata

    REAL (KIND=8) :: dzeta,f,ts
    REAL (KIND=8) :: arg,cosA,delta,dQ2,omega,p0,p1,PGA,Pi,Q,q1,q2,sq2
    REAL (KIND=8) :: xn,xnm1,yn,ynm1,ynm2
    REAL (KIND=8), dimension(ndata) :: signal,response


    Pi=3.1415926535d0
    omega=2.d0*Pi*f
    IF (dzeta<1.d-6)  dzeta=1.d-6
    Q=1.d0/(2.d0*dzeta)
    dQ2=2.d0*Q*Q
    delta=DSQRT(2.d0*dQ2-1.d0)
    p0=omega*ts/Q
    q2=DEXP(-p0)
    sq2=DSQRT(q2)
    arg=0.5d0*p0*delta
    cosA=DCOS(arg)
    q1=-2.d0*sq2*cosA
    p1=p0*sq2*((dQ2-1.d0)*DSIN(arg)/delta-cosA)
    ynm1=0.d0
    xn=signal(1)
    yn=0.d0
    response(1)=yn

    PGA = DABS(yn)
    do ii=2, ndata
      ynm2=ynm1; ynm1=yn; xnm1=xn
      xn=signal(ii)
      yn=p0*xn+p1*xnm1-q1*ynm1-q2*ynm2
      response(ii)=yn
      if (DABS(response(ii)) .gt. PGA)  then
          PGA = DABS(response(ii))
      end if
    end do

    return

    end subroutine OneDOF 
