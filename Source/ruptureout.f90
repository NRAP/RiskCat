      subroutine ruptureout(aspf,cplta,asprstm,aspslpvel,aspmo,    &
                            asparea,aspstress,nfsp)

      IMPLICIT NONE

      INTEGER :: i, ierr, j, nfsp
      REAL, dimension(20) :: asprstm, aspslpvel, aspmo, asparea,   &
                             aspstress, cplta
      CHARACTER (LEN=80), dimension(20) :: aspf, pmodel

!      real asprstm(20),aspslpvel(20),aspmo(20),asparea(20),
!     *aspstress(20),cplta(20)
!      character*80 aspf(20),pmodel(1000)

      open(unit=10,file='../../table.asperities')
      read(10,100,end=105)

      do  i = 1,1000
          read(10,102,iostat=IERR) pmodel(i)
          if (IERR .ne. 0)  then
              exit
          end if
      end do

105   rewind(10)

      write(10,106)
      write(11,106)
      write(10,102) (pmodel(j), j = 1,i-1)
      write(10,101) aspf(nfsp+1),cplta(nfsp+1),asprstm(nfsp+1),           &
          aspslpvel(nfsp+1),aspmo(nfsp+1),asparea(nfsp+1),aspstress(nfsp+1)
      write(11,101) aspf(nfsp+1),cplta(nfsp+1),asprstm(nfsp+1),           &
          aspslpvel(nfsp+1),aspmo(nfsp+1),asparea(nfsp+1),aspstress(nfsp+1)
      write(10,101) (aspf(j),cplta(j),asprstm(j),aspslpvel(j),            &
          aspmo(j),asparea(j),aspstress(j),j=1,nfsp)
      write(10,103)
      write(11,101) (aspf(j),cplta(j),asprstm(j),aspslpvel(j),            &
         aspmo(j),asparea(j),aspstress(j),j=1,nfsp)

 100  format(' '/' ')
 101  format(a18,1x,f5.0,5x,f4.1,3x,f5.0,8x,e9.3,1x,f7.0,4x,f5.0)
 102  format(a80)
 103  format(' ')
 106  format(5x,'MODEL  values at element with maximum slip   moment   ', &
      'rea   stress'/,12x,'      max slip  ristm  slip-vel      ',        &
      '(dyne-cm) (km**2) (bars)')


      close(10)

      stop
      end


