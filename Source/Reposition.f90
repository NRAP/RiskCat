!       Reposition pointer in data file
!       called from riskcat
!*******************************************************************************
!
        subroutine reposition ( fileread, string1, unitread )
!
!*******************************************************************************
!
!       reposition read pointer to line following "string1" in file attached to
!       unit number "unitread"
!
        implicit none
       
        integer            :: i, unitread
        character (len=20) :: fileread
        character (len=18) :: string1, string2
!
!-------------------------------------------------------------------------------

       !Re-position pointer after flag "string1"
!        open (unit=unitread,file=fileread,status="old",iostat=IERR)
!        call CantFindFile (fileread,IERR)
        rewind (unitread)

        do i = 1, 100000
            Read (unitread,*,END= 100) string2
            if (string1 .eq. string2) exit
        end do
        go to 200
        
 100    continue
        write (*,*) 'String:            ', string1
        write (*,*) 'Not found in file: ',fileread
        write (*,*) 'Check content of file'
        write (*,*) '---------------------  '
        write (*,*) 'STOP EXECUTION'
        STOP
        
200     return
        end subroutine reposition
