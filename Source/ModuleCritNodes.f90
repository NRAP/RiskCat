!
!...........Module to store the coordinates and misc info on critical points
!
        Module CritNodes
        
        use GenParam
        use ModuleEQ
        
        INTEGER, dimension(MaxCritypes)     :: CritnumNodes

        REAL, dimension(MaxDams)   :: xDam, yDam
        REAL, dimension(Maxcritpt) :: accelcritRD                  
        REAL, dimension(Maxcritpt) :: shortcritRD,shortd,shortd1
 	real, dimension(Maxcritpt) :: xlarge,xlarge1,xlargest,xshortest
        REAL, dimension(Maxsites)  :: Xsites, Ysites
        
	REAL, dimension(Maxcritpt,2)        :: CritXY
	REAL, dimension(Maxcritpt,MaxGMbins):: cumzzk
        REAL, dimension(Maxnode,2)          :: xynodeRD
        REAL, dimension(MaxOverUnderRD,2)   :: xyOVURD
        
        REAL, dimension(Maxcritpt,MaxGMbins,MaxProbExceed) :: perctl
        REAL, dimension(Maxlink,Maxsublk,2)                :: xysegRD
        
!        REAL, ALLOCATABLE,dimension(:,:) :: CritXY

                
        end Module CritNodes
