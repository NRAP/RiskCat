      subroutine disbaz(azm,baz,delta,elat,elon,slat,slon)
!
!        '** elipitical earth distance - azimuth calculation **'
!     longitude 180 > > -179 positive for west
!     latitude 90 > >  -90  positive north
!
      implicit none

      REAL :: azm, baz, const, cos2, delta, dr, elat, elon, gc, pi2
      REAL :: rlam, rlat, rlo, rphi, rrphi, sin1, slat, slon
      
      pi2 = 6.28318530717958
      dr = pi2/360.0
      gc=(1.-1./297.)**2
      const=57.2957795
      rlat=dr*slat
      rlat=gc*tan(rlat)
      rlat=atan(rlat)
      rlo=slon*dr
      rphi=dr*elat
      rphi=gc*tan(rphi)
      rphi=atan(rphi)
      rlam=elon*dr
      rrphi=sin(rphi)*sin(rlat)+cos(rphi)*cos(rlat)*cos(rlam-rlo)
      if(rrphi.gt.1.0) rrphi=1.0
      delta=acos(rrphi)
      sin1=cos(rphi)*sin(rlo-rlam)/sin(delta)
      cos2=(sin(rphi)-cos(delta)*sin(rlat))/(sin(delta)*cos(rlat))
      baz = atan2(sin1,cos2)
      baz=baz/dr
      if(baz.lt.0.0) baz = 360. + baz
      delta=delta*111.12/dr
      azm = 180. - (360. - baz)
      if(azm.lt.0.0) azm = 360. + azm
      return
      end
