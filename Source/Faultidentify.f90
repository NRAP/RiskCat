!
!       Routine called from GMHR2.f90
!
!*******************************************************************************
        subroutine Faultidentify ( iSimsource, xe, ye, zee )
!*******************************************************************************

!       Identify the fault index for an element loaded from RSQSim

        !Loop over the number of faults and number of segments in each fault to 
        !find which is closest to the center of the RSQSim subelement
        
        !Input:
        !xe, ye, zee = coordinates of center of sub-element
        !              zee is negative downwar. Must be changed to ze = - zee
        !x1, y1, z1  = Node N1 of segment  , stored in ModuleEQ
        !x2, y2, z2  = Node N2 of segment  , stored in ModuleEQ
        !Dip         = dip of segment      , stored in ModuleEQ
        !Units: x,y Long, Lat in degrees
        !       z in km, positive towards earth center (Downward)
      
        !Output:
        !iSimsource  = fault index in original source file indexing
        !iseg        = segment index in the selected iSimsource
!-------------------------------------------------------------------------------
       
        use ModuleEQ

        implicit none

        INTEGER :: iflt, is, iSimsource, iseg, is1
        INTEGER :: ndx, nd1, nd2, Nseg, nsegtot, Nss, tracenodes

        REAL :: a1, a2, b1, b2, dl1
        REAL :: xe, xM1, x1, x2
        REAL :: ye, yM1, y1, y2, ze, zee, z1, z2

        INTEGER, ALLOCATABLE, dimension(:) :: storeflt, storeseg
        REAL   , ALLOCATABLE, dimension(:) :: distx

!-------------------------------------------------------------------------------

        !Transform depth to common axis
        ze = - zee
        !Retrieve number of faults, Ns.
        Nss = NzMap(kzMap)

        !Allocate arrays for storing map of fault and segment indeces
        nsegtot = 0
        do is = 1, Nss
            !Absolute fault index
            iflt = Mapzid(kzMap,is)
            !Number of nodes
            tracenodes = scrnodnum(iflt)
            !Number of segments
            Nseg       = tracenodes - 1 
            nsegtot    = nsegtot + Nseg           
        end do
        ALLOCATE ( distx(nsegtot), storeflt(nsegtot), storeseg(nsegtot) )

        !Re-Initialize total number of segments
        nsegtot = 0
        Do is = 1, Nss
            iflt = Mapzid(kzMap,is)
            tracenodes = scrnodnum(iflt)
            Nseg       = tracenodes - 1 
 
!***************************************************************
            !For time being, assume only one depth strip
!***************************************************************

            do iseg = 1, Nseg

                !Cumulate total number of segments
                nsegtot           = nsegtot +  1
                distx(nsegtot)    = 1.e20
                !Store absolute fault index
                storeflt(nsegtot) = iflt
                !Srore segment index within source
                storeseg(nsegtot) = iseg

                !Retrieve coordinates of trace's nodes N1 and N2
                !nd1 is original index of node in input file
                ndx = (iseg-1)*2 + iseg
                nd1 = scrnodes(iflt,ndx)
                !Coordinates ser in Long, Lat, depth for use in synhaz
                x1 = snLong(nd1)
                y1 = snLat(nd1)
                z1 = Wtop(iflt,1)
                nd2 = nd1 + 1
                x2 = snLong(nd2)
                y2 = snLat(nd2)
                z2 = z1

                !Trace is parallel to surface. Only need to reason in 2-D
                !Equation of Line 1 passing by trace nodes N1 and N2: y = a1.x + b1
                a1 = ABS (x2 - x1)
                if (a1 .gt. 0.)  then
                    a1 = (y2-y1) / (x2-x1)
                    b1 = y1 - x1*a1
                    !Eq of Line 2  perp. to Line 1 +  surface proj of Elt center P
                    !y = a2.x + b2
                    a2 = -1./a1
                    b2 = ye - a2*xe
                    !M1 = Line 1 intersect. Line 2
                    xM1 = (b2-b1) / (a1-a2)
                    yM1 = b1 + a1*xM1
                else  !a1 = 0. Trace is parallele to y axis (Latitude axis)
                    xM1 = x1
                    yM1 = ye
                end if


                !Distance norm from center of sub-element to segment plane
                dl1 = SQRT (                                      &
                (xM1-xe)*(xM1-xe) + (yM1-ye)*(yM1-ye) + (z1-ze)*(z1-ze) )
                if (distx(nsegtot) .gt. dl1)  then
                    distx(nsegtot) =  dl1 
                end if

            end do

        end do

        !determine which is the closest segment
        dl1  = 1.e20 
        is1  = 1  
        do is = 1, Nsegtot
            if (distx(is) .lt. dl1)  then
               dl1 = distx(is)
               is1 = is
            end if
        end do

        !Absolute fault index
        iSimsource = storeflt(is1)
        !Segment index in the selected source
        iseg       = storeseg(is1)

        DEALLOCATE ( distx, storeflt, storeseg )

        return

        end subroutine Faultidentify
    
                

        
