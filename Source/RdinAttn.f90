!
!..........This routine reads:
!          1. 'GM-Array'
!             The definition of the array of Ground Motion values for which to
!             calculate the seismic hazard
!          2. 'GM-Models'
!             The  parameters of the Attenuation relationships
!          3. 'GM-Assignments'
!             The assignment of attenuation models to source types
!          4. 'SIGMA-Assignments'
!             The assignment of standard deviation on the log the Ground Motion
!             mean value model to source type
!          5. 'Lowest-Exeedence'
!             Lowest Probability of exceedence (of the ground motion parameter)
!             to be considered
!   
!        
!
!          Routine Called by:  simulperil
!
!*******************************************************************************
!
        subroutine RdinAttn (N,unit1,unit2)
        
        use ModuleEQ
        
        implicit none
        
        integer :: i, IERR, j, j2, k
        integer :: N, nGMtype, unit1, unit2
        
        CHARACTER (LEN=6) :: DUM
        character (len=18):: string1, string2
        
        character (len=30), dimension(MaxAttnMods) :: GMname
         
!*******************************************************************************

        if (GMfile .eq. ' ')  then
            print *,'No GM file was specified. Default "GMmodels" will be used'
            GMfile = 'GMmodels'
        end if
        print *, '   GM-file:  ', GMfile
        open (unit=unit1,file=GMfile,status="old",iostat=IERR)
        call CantFindFile (GMfile,IERR)
                
!.......Read Attenuation models parameters:
!       Read the total number of Models stored in the file whose name is 
!       stored in variable: GMfile.
!       Default Name for the file is: 'GMmodels'

        !First re-position the file
        !Section on GM models parameters is flagged by string "GM-Models"
        rewind (unit1)
        string1 = 'GM-Models'
        do i = 1, 100000
            read (unit1,*) string2
            if (string1 .eq. string2)  exit
        end do
        if (i .gt.10000)  then
            write (*,*) 'Section GM-Models not found, check:',GMfile
            write (*,*) 'STOP EXECUTION'
            STOP
        end if

        !read Number of Models
        READ (unit1,*) N
        do i = 1,N
             read (unit1,2) dum,GMname(i),attn(i,9),attn(i,10),  &
             attn(i,19),attn(i,20)
             read (unit1,12) (attn(i,j), j=1,8)
             read (unit1,12) (attn(i,j), j=11,18)
             read (unit1,12) (attn(i,j), j=21,28)
        end do
        close (unit1)

        
!.......Read Models Assignments to seismic source types:

!       Read the assignments of GM models to sources types:
!       Re-open source input file with name stored in variable: afile
!       Default name for afile = 'source.txt'
!       
        if (afile .eq. ' ')  then
            afile = 'source.txt'
        end if
        !This also rewinds the file
        open (unit=unit2,file=afile,status="old",iostat=IERR)
        
        !Skip to the desired section of the file, identified by the
        !string: "GM-Assign"
        string1 = 'GM-Assignments'
        do i = 1, 100000
        
            read (unit2,*) string2
            
            if (string2 .eq. string1)  then
                !read the number of GM classes. 
                !Each class contains a set of GM models with weights
                !Each class represents epistemic uncertainty in GM prediction
                !A source can be assigned to any GM class , that way each 
                !seismic source can be fitted with the appropriate set of 
                !GM models
                
                !The GM type index for a source i is: iSourceGM(i)
                !nattn(j)   = number of attenuation models for source
                !             of type j. 
                !             nattn(j) = 1,...,Kj
                !wattn(j,k) = weight of k-th attenuation model 
                !             of type j
                !iattn(j,k) = Index (in the table of all attenuation
                !             models) of the k-th model for source
                !             of type j
                !Max value for nGMtype = MaxGMsrcType (in ModuleEQ)
                !Max value for K       = MaxGMTypeMods
                
                !The GM type-set of the seismic source must be read as input in the
                !properties of the source (in file 'source.txt'), as
                !variable iSourceGM(i)
                
                read (unit2,*) nGMtype
                do j = 1, nGMType
                    read (unit2,*) j2
                    nattn(j) = j2
                    read (unit2,*) (iattn(j,k), k=1,j2)
                    read (unit2,*) (wattn(j,k), k=1,j2)                
                end do
                exit
            end if
        
        end do
        
!.......Read GM Standard deviation model assignment to seismic source type

        !Re-position file.
        ! This section MUST be preceeded by the section 'GM-Assignments'
        
        rewind (unit2)
        !Skip to the desired section of the file, identified by the
        !string: "SIGMA-Assignments"
        string1 = 'SIGMA-Assignments'

        do i = 1, 100000
        
            read (unit2,*) string2
            
            if (string2 .eq. string1)  then
                !Number of sigma for integration
                read (unit2,*) xnSIGMA
                !Default sigma value for each GM model type
                read (unit2,*) (defGMSIG(j), j=1,nGMtype)
                exit
            end if
            
        end do
        
2       format (a6,a30,4x,4f10.3)
12      format (8f10.3)

        return
        
        end subroutine RdinAttn
        
