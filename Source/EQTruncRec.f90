!
!......Truncated Exponential Recurrence.
!      Calculate the probability density function of magnitude for magnitudes
!      given by the array xMag and the probability of ocurrence of at least 
!      one event greater than M=M0
!Input:
!******
!      Alfa  = G-R origin ordinate on natural log
!      Beta  = slope of G-R in natural log
!      dt    = the size of the time step considered (in years). Default=1.
!      M0    = Minimum magnitude considered in the calculation
!      M1    = Lower magnitude bin value 
!      M2    = High magnitude bin value
!      Mmax  = Maximum magnitude possible for this source
!      nMag  = dimension of the Magnitude array
!      NaZero= Number of events greater than M=0, per year
!      xMag()= Magnitude array
!
!Output:
!*******
!      PM0   = Annual Probability of ocurrence of at least one EQ greater or 
!              equal to M0 (=Lambda at M0)
!      Pm()  = Probability density of ocurrence of EQ of magnitude m. (rather
!              it is the probability of ocurrence of EQs in the bin of 
!              magnitude centered on the magnitude value m 
!      Ninbin= Expected value of number of events in the magnitude bin [M1-M2]     
!
!-------------------------------------------------------------------------------
!
        subroutine  EQTruncRec (alfa,beta,dt,M0,Mmax,M1,M2,Ninbin,PM0)
     
        use ModuleEQ
             
        implicit none
        
        integer :: i
        
        real :: alfa,beta,dt,K,M0,M1,M2,Mmax,NaZero,Ninbin,PM0
        real :: x,xm,xm1,xm2,ym1,ym2
        
        
!-------------------------------------------------------------------------------
!
!       alfa    = a of G-R relationship
!       beta    = b    "   "
!       M0      = Minimum magnitude of interest (usually M4. or M4.5)
!       Mmax    = Maximum possible magnitude achievable on this source

        Pm = 0.
        !Number of events greater than M0
        x = alfa - beta*M0
        x = exp (x)
        !rate per window of time dt (dt in units of year)
        x = x * dt
        !x = Lamda*t, of exp(Lamda.t) = rate per step of time dt
        PM0 = 1. - exp (-x)
!
!       Number of events in magnitude bin [M1`-M2]
        NaZero = exp (alfa)
        Ninbin = 0.
        if ((M2.gt.M0) .and. (M1.lt.Mmax))  then
            xm1 = exp (-beta*M0)
            xm2 = exp (-beta*Mmax)
            if (M1 .gt. M0)   then; xm1 = exp(-beta*M1);  end if            
            if (M2 .lt. Mmax) then; xm2 = exp(-beta*M2);  end if
            Ninbin = NaZero * (xm1-xm2)
        end if
       

!       Probability density function of magnitudes: Pm()         
        K      = 0.
        Pm     = 0.
        do i = 1, nMagbins
            ym1 = xMagLBnd(i)
            ym2 = xMagHBnd(i)
            if ((ym2.gt.M0) .and. (ym1.lt.Mmax))  then
                xm1 = exp (-beta*M0)
                xm2 = exp (-beta*Mmax)
                if (ym1.gt.M0)   then; xm1 = exp(-beta*ym1);  end if
                if (ym2.lt.Mmax) then; xm2 = exp(-beta*ym2);  end if
                Pm(i) = exp(-beta*ym1) - exp(-beta*ym2)
                K = K + Pm(i)
            end if
        end do
        !Normalize
        do i = 1, nMagbins
            Pm(i) = Pm(i) / K
        end do
        
        !Check on Pm:
        xm = 0.
        do i = 1,nMagbins
            xm = xm + Pm(i)
        end do
        
        return
        
        end subroutine EQTruncRec