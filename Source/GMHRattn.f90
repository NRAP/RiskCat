
!
!*******************************************************************************
!
!       This routine uses "standard" attenuation curves to calculate the seismic
!       hazard for one epistemic simulation, including all aleatory simulations

!       It takes all the catalogs of earthquakes generated in an epistemic
!       simulation (NMispl catalogs).
!       The hazard curve for each catalog is calculated and combined over all 
!       catalogs.
!
!*******************************************************************************
!
        subroutine  GMHR  (Catdepth1, Catdepth2, CSTR1, CSTR2, iATN_Larry,      &
                          iepis, MAxEqINPUT, maxnodl, maxnodw, Mxstrp, nbin,    &
                          NcritRD, nGMbins, nMspl, Nscreen, pagenumber1,        &
                          PlotHazEpis, TrackNumber, xLmax, xM0,YearsCat)

!******************************************************************************
!       The consequences, in terms of loss or other metric, are calculated 
!       in riscat for each set of epistemic catalog and weighted 
!       accordingly.
!      
!       Once the source has been selected, we need to
!       select the parameters (M,Dip, Depth, etc,) associated with the
!       parameters used to calculate the probability of occurrence of 
!       events >= M0. 
!       these parameters are stored in Arec(i,*)which is updated after 
!       an event

!       Input:
!       ------
!       iepis   = index of current epistemic cycle
!       NcritRD = number of sites for which to calculate hazard
!       nGMbins = number of bins of GM values
!       nMspl   = number of catalogs simulated per epistemic simulation
!       rateM0  = occurrence rate at M0
!       tlife   = array of time since starting. Defines hazard timewindows
!       WgEpis  = Weight of the current Epistemic simulation cycle
!       YearsCat= Length of catalogs in years.
!
!******************************************************************************

   !Internal custom modules
        use CritNodes
        use ModuleEQ 
        use GENparam 
        use ModuleRSQSim                   
      
        IMPLICIT NONE

        CHARACTER (LEN=41):: CSTR1
        CHARACTER (LEN=20):: CSTR2          
        CHARACTER (LEN=40):: faultname
      
        INTEGER :: i,i1,i2,iATN_Larry,icat,icrt,iepis
        INTEGER :: ierrorgm,ierrsig,ii,inev
        INTEGER :: iSimAttn,iSimsource,ispl,istrip
        INTEGER :: itp,ixM0,j,jcrit
        INTEGER :: ks,MaxEqINPUT,maxnodl,maxnodw,Model,Mxstrp
        INTEGER :: n1,nbin,NcritRD,Nd,Nev
        INTEGER :: nGMbins,nl,nLHbins,nLHSamples
        INTEGER :: nMspl,npltf,Nscreen,nsimul,NsubTot
        INTEGER :: pagenumber1,PlotHazEpis,iSrcType,tracenodes,TrackNumber   
               
        REAL    :: Catdepth1,Catdepth2,corsite,d,depi,gmxsig
        REAL    :: rnunf
        REAL    :: sigma,simdepth1,Simlat1,SimLong1,SimMag1
        REAL    :: timeCat,timewindow,tp1
        REAL    :: wmspl
        REAL    :: x,xc,xLmax,xM0
        REAL    :: xshift1,xWeight,xWmax
        REAL    :: yc,YearsCat,yshift1,zzk
        
        REAL, dimension(MaxAttnCoeff)  :: atn
        REAL, dimension(Mxstrp)        :: depth2        
        REAL, dimension(Maxseg,Mxstrp) :: dips
        REAL, dimension(Maxnodl,3)     :: trace
        
!Variable set for plotting
        INTEGER :: ifault,ipfault,iplotnum,mgridyes
        REAL    :: cigar, radeg, rearth, rdegree, rw
        REAL    :: xf,xh,vux,vuy,vuz,xmpltf1,xmpltf2
      
!*******************************************************************************                
!Constants:
      rearth  = 6371200.                   ! Earth radius in meters
      rdegree = rearth * 6.28318531 / 360. ! Length of 1 degre at equator
      radeg   = 57.2957795                 ! Conversion degree-radian
!*******************************************************************************

!   There are NMspl catalogs for ONE epistemic simulation.
!   Each catalog has a weight xWeight (Set in riskcat)

!Loop over the number of Hazard time windows:
!--------------------------------------------
      Time_Window: Do itp = 1, Ntp

          !Reset values in work4() for each Hazard Time window
          do i = 1, NGMbins
              do j = 1, NcritRD
                  work4(j,i) = 0.
              end do
          end do
      
            
          !Assignment of weights  for each epistemic set 
          !---------------------------------------------
          xWeight  = Weightepi(iepis)
          if (iStd_Dieterich .eq. 3)  then
              xWeight = 1.
          end if
!

          !Loop over the number of aleatory simulations:
          !--------------------------------------------       
          Loop2_Sampling: Do ispl = 1, NMspl
             
              !Number of events in catalog:
              Nev = NCatEvents(ispl)
              !Re-set event counter within catalog 
              ixM0 = 0  

              if (iStd_Dieterich .eq. 2)  then
                 ii = (iepis-1)*NMspl + ispl                     
              end if


              !Duration of time window for which Hazard is calculated (yrs)
              timewindow = tlife(1)
              if (itp .gt.1)  then
                  timewindow = tlife(itp) - tlife(itp-1)
              endif
              tp1 = 0.
              if (itp .gt. 1)  then
                  tp1 = tlife(itp-1)
              end if  
              TimeCat = tlife(itp) - tp1
              tp1     = YearsCat   - tp1
              if (tp1 .lt. TimeCat)  then
                  TimeCat = tp1
              end if




              !Loop over the number of Eqs. in the catalog:
              !-------------------------------------------- 
              i1 = IstartEq(ispl,itp)
              i2 = IendEq(ispl,itp)
              print *,'I1,I2:',i1,i2

              Loop3_Number_of_Events_in_Catalog:   Do inev = i1, i2

                  !There are 4 possible alternatives:
                  ! 1. iStd_Dieterich = 1
                  !    Use Standard PSHA Maps+seismicity catalog
                  !    - Area Sources with simulated locations of point sources
                  !
                  ! 2. iStd_Dieterich = 1
                  !    Use Standard PSHA Maps+seismicity catalog
                  !    - Finite fault sources with trace, dip, etc.
                  !
                  ! 3. iStd_Dieterich = 2
                  !    Use RSQSim generated catalog
                  !    - Main events, not sub-elements - used as point sources
                  !
                  ! 4. iStd_Dieterich = 3
                  !    Standard, simple or statistically simulated catalog
                  !    - Events used as point sources
                  !
                  !    Magnitude  : SimMag1
                  !    Longitude  : SimLong1
                  !    Latitude   : SimLat1
                  !    Depth      : Simdepth1
                     
!                 Event parameters: Depend on Type of source
                  !----Index of the source (from 1 to Ns) : iSimsource
                  !----For an Area source we have:
                  !      Magnitude  : SimMag1
                  !      Longitude  : SimLong1
                  !      Latitude   : SimLat1
                  !      Depth      : Simdepth1
                  !----For a finite fault (type 2 source)we have only: 
                  !      Magnitude  : SimMag1
                  !    (Other parameters are simulated in "Faultband"
                  !    including length, width, strike, dip, depth, etc.)

                
!                 Event Magnitude. For all cases:
                  !------------------------------
                  SimMag1  = CatEqMag(ispl,inev)
                  !Process only events with magnitude M > xM0, 
                  !and event index < MaxEqINPUT
                  if (SimMag1 .gt. xM0)  then
                      ixM0 = ixM0 + 1
                      if (ixM0.gt.MaxEqINPUT) then
                          exit
                      end if
                  end if
 
                  !Event Source Index and type:
                  !---------------------------- 
                  !(point source iSrcType =1, finite fault iSrcType =2)
                  if (iStd_Dieterich .eq. 1)  then
                      !Alternative 1: Point sources   
                      !Alternative 2: Finite fault sources
                      iSimsource = iCateQ(ispl,inev)
                      iSrcType   = SourceType(iSimSource)
                      print *,'iSimsource, iSrcType:', iSimsource, iSrcType
                  else
                      !Other catalogs use point source events,and
                      !are assumed to be fault number 1
                      !Alternative 3: Point sources  
                      !Alternative 4: Point sources 
                      iSimsource = 1
                      iSrcType   = 1
                  end if
                   
!                 Ground-motion model index, coefficients and default sigma :  
                  !----------------------------------------------------------
                  iSimAttn = XSimAttn(iSimsource)
                  gmxsig   = Xgmxsig(iSimsource)
                  do jcrit = 1,MaxAttnCoeff
                      atn(jcrit) = Attn(iSIMAttn,jcrit)
                  end do
                  corsite = 1.

                  !Location of Hypocenter for point sources:
                  !-----------------------------------------
                  if (iSrcType .eq. 1)  then
                      !Only need Long, Lat and Depth of event                   
                      SimLong1  =   Xrsq(ispl,inev)     
                      SimLat1   =   Yrsq(ispl,inev) 
                      if (iStd_Dieterich .eq. 3)  then  
                          !Simdepth1 is simulated from a distribution of depths
                          !for the Earthquakes in the catalog.
                          !       min depth  = CatDepth1
                          !       max depth  = CatDepth2
                          !CatDepth1 and 2 are read from input file Menu.
                          !For now use Uniform Distribution of depths.
                          x = rnunf ()
                          Simdepth1 = Catdepth1 + (Catdepth2-Catdepth1)*x 
                      else
                          Simdepth1 = - Zrsq(ispl,inev)
                      end if
                   end if


!                 Distance from critical points/sites:
                  !-----------------------------------                       
                  !The index of the selected source in the simulation is: 
                  !iSimsource in the indexing of retained sources (1 to Ns)
                  !iSrcType is the source type for the considered event
                      
                  SELECT CASE ( iSrcType )
                      
                      CASE (1)      !Area and Point Sources (iSrctype=1)
                      
                          !Use SimLong1, SimLat1, SimDepth1, and SimMag1
                          !GM prediction model selected has index SimAttn
                          
                          !Distance for all critical points.                     
                          do icrt = 1, NcritRD                      
                              xc = CritXY(icrt,1)
                              yc = CritXY(icrt,2)                             
                              !Epicentral distances                           
                              call distsimple  (Simlong1,Simlat1,xc,yc,d)
                              !depi = Epicentral distance (km)
                              depi = d
                              !d = Hypo distance (km)
                              d    = sqrt (d*d + SimDepth1*SimDepth1)  
                              !Store distance in array shortCritRD(icrit)
                              shortCritRD(icrt) = d
                          end do


                      CASE (2)       !Finite Fault Sources (iSrcType=2)
                                    
                          !Earthquake definition of source:                      
                          !--------------------------------
                          faultname = SourceName(iSimsource)
                                              
                          xwmax = 20.                          
                          !Number of nodes
                          tracenodes = scrnodnum(iSimsource)
        !print *,'GMRHAttn-l287-iSimsource,tracenodes:',iSimsource,tracenodes
                          !Number of segments
                          nl = tracenodes - 1                         
                          !Number of strips fixed to 1 presently (nd=1)
                          nd = 1 
                          nsimul = 1                 
                          !Load 'trace' vector of trace coordinates
                          do jcrit = 1, tracenodes
                              n1  = scrnodes(iSimsource,jcrit)
                              trace(jcrit,1) = snLong(n1)
                              trace(jcrit,2) = snLat(n1)
                              trace(jcrit,3) = Wtop(nl,1)  !Depth of top of fault
                              if (jcrit .ne. tracenodes)  then
                                  dips(jcrit,1)  = 0.      !Vertical   
                              end if                       
                          end do                          
                          !Depth of bottom of strips in segments
                          do istrip = 1, nd
                             depth2(istrip) = Wbot(iSimsource,istrip)
                          end do                          
                          ifault   = iSimsource
                          ipfault  = 0
                          iplotnum = 0
                          mgridyes = 0
                          rw = 0.5
                          cigar = 0.5
                          xf = 0.5
                          xh = 0.125
                          vux = 0.
                          vuy = 0.
                          vuz = 0.
                          xshift1 = 0.
                          yshift1 = 0.
                          xmpltf1 = 4.5
                          xmpltf2 = 9.                 
                                  
                          call faultband (cigar,dips,depth2,&
                          iATN_Larry,iSimsource,ipfault,maxnodl,    &
                          maxnodw,Mxstrp,NcritRD,  &
                          nl,nd,npltf,nsimul,nSubTot,rw,trace,  &
                          xf,xh,SimMag1,  &
                          xLmax,xmpltf1,xmpltf2,xwmax)
                          !Output distance is stored in array shortCritRD(icrit)
                   
                      CASE DEFAULT
            
                  END SELECT !Type of seismic source


                  !Ground-motion estimates with attenuation relations:
                  !---------------------------------------------------
                  !Loop on number of sites
                  do jcrit = 1,NcritRD 
                    
                      !Distance: d (km)
                      d  = ShortcritRD(jcrit)

                      !Median g.m. value: zzk
                      icat = 0
                      model = INT ( atn(19) )  
                      call  grmdlib  (atn,MaxAttnCoeff,SimMag1, &
                           d,SimMag1,zzk,icat,ierrorgm,model)
                      print *,'Distance d=',d
                                                                           
                      ! 1. Calculate the value of the GM parameter
!                     zzk1 = exp (zzk)
!		      Standard deviation on Log(GM)
!                     model  = model index
!		      xm     = magnitude value
!	              xi     = intensity value
!                     r	     = actual arithmetic distance value
!                     attn   = 1D- Array containing the coefficients of the 
!	                       ground motion model with index as above: "model"
!                     zzk    = the Neperian (base-e) logarithm of the 
!            	               ground motion calculated output from grdmlib.
!                     gmxsig = Default value of sigma, read from the GMX file
!                     corsite= Site correction factor for this frequency, 
!                              applied on the log of gropund motion estimat
!                              corsite is stored, for each frequency (ifre),
!                              into array corfrq(ifre) that is calculated in
!                              subroutine SETCOR
!                     sigma  = the standard error on the natural log of the 
!                                  ground motion estimate (zzk)

                      !All models used must be in same units
                      !sigma on Natural log of ground-motion
                      call  gmsiglib  (SimMag1,atn, &
                      zzk,ierrsig,corsite,gmxsig,sigma,MaxAttnCoeff)

                      !For Douglas, PGA is converted into cm/s/s
                      if (model .eq. 26)  then
                          !Case of Douglas model that is in MKS, others are in cgs
                          zzk = zzk + alog(100.)
                      end if

                      !Retrieve magnitude bin sampling weight
                      !Which bin does xMSc fall into?
                      do i = 1, nbin
                          if (Smag(i) .lt. SimMag1+0.001)  then
                              ks = i
                          else
                              exit
                          end if
                      end do 
                      wmspl = wMagbin(ks,itp)
                                                                
                      ! 2. Latin-HyperCube sampling the distribution of GM
                      !----------------------------------------------- 
                      !with nLHbins in each bin, and using 10 bins
                      !Limit the excursion to nSIGMA (Read from input)
!                     xnSIGMA    =      Max number of sigmas for sampling
!                                       read from source file (source.txt)
!                                       Stored in module ModuleEq
                      nLHbins    = 10   !L_H number of sampled bins
                      nLHSamples =  5   !L-H number of samples per bin

                      call LHCGM ( jcrit, nGMbins, nLHbins, nLHSamples,    &
                                   sigma, wmspl, xWeight, zzk)
                  
                  end do     !End loop on critical points  jcrit
                    
              End Do Loop3_Number_of_Events_in_Catalog    ! inev
  
          End Do Loop2_Sampling                           ! ispl



!***************************************************************************
 
       !---------------------------------------------------------------------

       !Calculate hazard for this set of catalogs and this hazard time window

       !---------------------------------------------------------------------

       !TimeCat is the smallest of itp-th time-window and portion of catalog 
       !in this time-window.
       !if there is only one time-window, then TimeCat = smallest(Time-window,
       ! and Catalog duration)


       CALL HazEpis (CSTR1, CSTR2, iepis, itp, nCritRD, nGMbins, NMspl,    &
                    Nscreen,  pagenumber1, PlotHazEpis,TrackNumber,timeCat) 


     End Do Time_Window            !Ends loop on number of Hazard time windows


  
! COMBINATION of ALL THE HAZARD CURVES RESULTING FROM THE NEPIS EPISTEMIC 
! SIMULATIONS IS DONE IN MAIN: RISKCAT
!       Use the weight of each epistemic realization for linear combination
!       of the curves.
!       Obtain the histogram of hazard for each GM bin
!       Calculate peercentile values of Hazard for each GM bin
!       Plot resulting percentile curves



        return
        end subroutine GMHR
