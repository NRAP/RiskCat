!
! Modifies the occurrence relationships of each source affected by the 
! occurrence of a magnitude MSc on source NextSc.
!
!   Input:
!       MSc         Magnitude of event that just occurred on source NextSc
!       NextSc      Source ID on which the last earthquake occurred
!       TimeSc      Time at which last Eq occrurred
!   
!   Output:
!       New set of parameters for occurrence relatiohnships
!

!*******************************************************************************
        SUBROUTINE UpdateScRates  (NextSc,TimeSc,xMSc)
  
        use ModuleEQ                   
      
        IMPLICIT NONE
      
        INTEGER :: i, NextSc    
      
        REAL :: x, xMSc, TimeSc
      
!*******************************************************************************
!
        i = NextSc
        x = xMSc
        x = TimeSc
          
        return
        end subroutine UpdateScRates
