!    Calculate the Strike of an element to be consistent with Dip notation
!
!*******************************************************************************
!
        subroutine Aki_Richards (dip1, strike, x1, x2, y1, y2)
!
!*******************************************************************************
!
! Input:
!        Coordinates of ends of oriented 1>>2 trace of the fault
!        x1, x2   : Longitude of end nodes, in decimal degrees.
!        y1, y2   : Latitude of end nodes,  "  "  "  "  "  "  "
!        Dip1     : Dip of the fault segment, in a orthogonal system (Jean's)
!
! Output:
!        Strike   : Strike of the segment in the Aki & Richards, Dip consistent
!                   notation.
!                   That is: Dip is always between 0 and +90 degrees.
!                            Fault is pending on the right hand side of trace
!                            in the direction of the strike going from 
!                            point 1 to point 2.
!
!********************************************************************************
!
        IMPLICIT NONE

        REAL :: dip1, dip2, dt, dx, dy, pi2, strike, strike1, x1, x2, y1, y2

       pi2 = 3.14159265 * 2.

       !Strike1 is strike of (Non-Oriented) staight line X1-X2
       dx = x2 - x1
       dy = y2 - y1
       if (dy .ne. 0.)  then
           dt = dy / dx
           strike1 = 90. - ATAN (dt)*(360./pi2) !Strike1 is between 0. and 180. degrees.
       else
           strike1 = 90.
       end if

       !Aki_Richards Dip is between 0 and +90. degrees
       if (Dip1 .ge. 0.)  then
           Dip2 = 90. - Dip1
       else
           Dip2 = 90. + Dip1
       end if

       !Four cases

       ! 1. dx > 0, dy > 0
       ! 2. dx > 0, dy < 0.
       if (dx .ge. 0.)  then
            strike = strike1
       end if

       ! 3. dx < 0., dy < 0.
       ! 4. dx < 0., dy > 0.
       if (dx .lt. 0.)  then
            strike = strike1 + 180.
       end if


       return

       end subroutine Aki_Richards
 



            
  

        
