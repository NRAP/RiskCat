!  Called from SampleEpisFlt
!
!*******************************************************************************
!
        subroutine DistribSampling ( m, r, xbin, xs, x1in, x2in )
!
!*******************************************************************************
!
!Inputs:
!*******
        !m  = integer, type of distribution that is being sampled.
        !      m = 1, uniform
        !      m = 2 triangular
        !      ect...
        ! r   = simulated uniformly distributed [0.,1.] random value
        ! x1  = minimum values of the parameter sampled
        ! x2  = maximum
        ! xb  = mode of the distribution (sometimes called best estimate)

!Ouput:
!******
        ! xs  = sample of the selected distribution
!
!*******************************************************************************
!
        implicit none

        INTEGER :: m, n

        INTEGER, dimension(3) :: ir

        REAL    :: a1, r, xb, xbin, xs, x1, x1in, x2, x2in, x3, yb
        
        REAL, dimension(3) :: v, v1

!-------------------------------------------------------------------------------
        !Ensure that x1 <= xb <= x2
        v(1) = x1in
        v(2) = x2in
        v(3) = xbin
        n     = 3
        !Sort values
        call SortJBS ( ir, n, v, v1 )
        x1 = v1(1)
        xb = v1(2)
        x2 = v1(3)

        !Got to the selected type of distribution
        SELECT CASE ( m )
        
           !Uniform distribution
            CASE (1)
                xs = x1 + (x2-x1)*r

            !Triangular distribution
            CASE (2)
                yb = 2./(x2-x1)
                a1 = (xb-x1) / (x2-x1)
                if (r .le. a1)  then
                    x3 = r * (x2-x1) * (xb-x1)
                    if (x3 .gt. 0.)  then
                        xs = x1 + sqrt ( x3 )
                    else
                        xs = x1
                    end if
                else
                    x3 = (r-a1) * (x2-x1) * (x2-xb)
                    if (x3 .gt. 0.)  then
                        xs =  xb + sqrt ( x3 )
                    else
                        xs = xb
                    end if
                end if

            !Case Default
            CASE DEFAULT

        END SELECT

        return

        END subroutine DistribSampling
