!Select one of the Earthquake events
!Return: EQ ID
!        EQ geometry  :  some vector
!        EQ Magnitude : Meq
!        Ns  is the number of seismic sources in a given zonation map
!        Nmagbins is the number of bins of magnitude to describe the density
!        function on magnitude, assuming that the smallest value is M0 and the 
!        highest value is Mmax. 
!

!*******************************************************************************
        SUBROUTINE EQselect (iSimSource,M0,SimDepth1,SimLat1,   &
        SimLong1,SimMag1)
                             
          use ModuleEQ                   
          
          IMPLICIT NONE
          
          INTEGER :: i
          INTEGER :: iSimsource
          INTEGER :: nodes         
          
          REAL :: M0
          REAL :: rnunf,r1,sum,SimDepth1,SimLat1,SimLong1,SimMag1
                    
!          REAL, ALLOCATABLE,dimension(:,:,:) :: binrates
          
!*******************************************************************************

!........1. Select the seismic source that breaks
            r1 = rnunf()
            sum = 0.
            do i = 1, Ns
            sum = sum + eqrates(i)
            end do
            do i = 1, ns
            eqrates(i) = eqrates(i) / sum
            end do
            sum = 0.
            do i = 1, Ns
            iSimsource = i
            if (r1 .le. sum)  exit
            sum = sum + eqrates(i)
            end do

!........2. Given the seismic source, select the parameters
            !Type of source (Area/Fault/...)
            SELECT CASE (SourceType(i))
            
                !2.1 Areas
                CASE (1)
                
                    !Reccurrence Relation type
                    SELECT CASE (SeisRectype(i))
                        !2.1.1 Truncated Exponential
                        CASE (1)    
                            !Select a Magnitude
                            call TruncSelect (i,M0,SimMag1)
                            !Select EQ location
                            nodes = scrnodnum(i)
                            call AreaEQselect &
                            (i,nodes,simdepth1,Simlat1,SimLong1)
                            
                        !2.1.2 Characteristic EQ (Youngs-Coppersmith)
                        CASE (2)
                            
                        !2.1.3 Foxall model segmented fault
                        CASE (3)  
                          
                        !2.1.4 Empirical non-parametric
                        CASE (4)    
               
                        !2.1.5 Others
                        CASE DEFAULT
                    END SELECT
                    
                !2.2 Faults
                CASE (2)
                   !Reccurrence Relation type
                    SELECT CASE (SeisRectype(i))
                        !2.1.1 Truncated Exponential
                        CASE (1)    
                            !Select a Magnitude
                            call TruncSelect (i,M0,SimMag1)
                            
                        !2.1.2 Characteristic EQ (Youngs-Coppersmith)
                        CASE (2)
                            
                        !2.1.3 Foxall model segmented fault
                        CASE (3)  
                          
                        !2.1.4 Empirical non-parametric
                        CASE (4)    
               
                        !2.1.5 Others
                        CASE DEFAULT
                    END SELECT

                !2.3 Others                                                                                                                     
                CASE Default
                
            END SELECT
            


          !
          !So now we have, for this simulation:
          !  internal index of the source (from 1 to Maxigarde): iSimsource
          !  index of the source as used in CA analysis        : iSimEvent
          !  Event ID                                          : IDsimevent
          !  magnitude of the simulated event/source           : xmagsim
          !  magnitude bin of the simulated event              : iSimbin
           
!          DEALLOCATE (binrates, STAT=IERR)
    !      if (ierr /= 0) then
   !           !Could not deallocate
  !            write (6,*) 'Could not deallocate array binrates, STOP'
 !             STOP
!          end if                        
          
          
        return
        end subroutine EQselect
