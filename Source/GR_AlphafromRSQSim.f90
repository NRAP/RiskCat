! This routine calculates the alpha value of the G-R reccurrence curve
! from the slip rate read in the input file of RSQSim.
!
! uses: 
!         For calculating seismic moment:
!         M     = 2/3 Log10(Mo) - 16.05  (M0 in dyne.cm)
!                                        (Hanks & Kanamori, 1979)
!         M0    = 10 ** (1.5 m + 9.05)   (M0 in N.m)    
!
!         For calculating Area:
!         A     = 10 ** (-3.42 + .9 Mw)  (km2)  
!                                        (Wells & Coppersmith 1994)
!               = 10 ** ( 2.58 + .9 Mw)  (m2) 
!
! Input:  beta  = slope of G-R curve (on Natural log scale)
!         xM0   = minimum magnitude of integration
!         xMmax = maximum magnitude possible on the fault. 
!                 Also total fault rupture area.
!         xmu   = earth shear modulus    (N/m2)

! Output: alpha = Natural Log of number of events greater than M = 0
!
!******************************************************************************
!
 subroutine GR_AlphafromRSQSim (alpha, beta, slip, xMmax, xM0, xmu)
 
 implicit none
 
 real :: A1, A2, alpha, beta, Ce
 real :: slip, sum1, sum2, T1, T2, xMmax, xmu, xM0
 
 A1   = 2.58
 A2   = 0.90
 T1   = 9.05
 T2   = 1.50
 Ce   = alog(10.)
 
 sum1 = alog( xmu*slip*(Ce*T2-beta) ) + Ce*(A1+A2*xMmax) 
 sum2 = exp ( (Ce*T2-beta)*xMmax )    - exp( (Ce*T2-beta)*xm0 )
 sum2 = alog( beta*sum2 ) + Ce*T1
 
! Alpha in Number of evenets/year
 alpha = sum1 / sum2
 
 return
 end subroutine GR_AlphafromRSQSim
