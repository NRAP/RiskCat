!
!        This routine is called by GMHRattn
!
!******************************************************************************
!
        SUBROUTINE LHCGM ( jcrit, nGMbins, nLHbins, nLHSamples,      &
                           sigma, wmspl, xWeight, zzk)
!
!******************************************************************************
!       Latin Hyper Cube sampling for nLHbins with nLHSamples each
!       The distribution is assumed to be Gaussian N(zzk,sigma)
!
!       Input:
!             nLHbins    = number of bins sampled
!             nLHSamples = number of samples per bin
!             nsigma     = maximum number of standard deviations sampled
!             sigma      = standard deviation
!             wmspl      = weight to be applied for magnitude sampling weighting
!             xWeight    = weight of epistemic*aleatory catalog  simulations
!             zzk        = mean log-values of calculated ground-motion
!
!       Output:
!             nLHbins*nLHSamples values of ground-motion (log) generated and 
!             stored in array work4 (declared in ModuleEq) at the respective
!             location of GM bin and site index, to create the histogram of
!             ground motion values, and later the Hazard.
!
!*******************************************************************************
!
        use ModuleEq

        IMPLICIT NONE

        INTEGER :: ii, iGM, isimSIG, jcrit, kGM, nGMbins
        INTEGER :: nLHbins, nLHSamples

        REAL    :: fx0, psigma1, r1, rnunf, sigma, wgmt, wmspl, wzzk
        REAL    :: xNormal, xWeight, xxx
        REAL    :: zzk, zzkMin, zzkMax, zzksize, zzk2, zzk3, zzk4, zzk5, zzk6

        REAL (KIND=8) :: fx1, fx3
!
!*******************************************************************************
!
        !If nSigma or sigma = 0, then skip LHC sampling
        if (xnSIGMA*sigma .lt. 1.e-6)  then
            !Take the mean values calculated and apply epistemic and aleatory 
            !sampling weights

            !Index of GMarray bins for  estimated GM
            do iGM = 1, nGMbins
                kGM = iGM                              
                if (GMarrayLog(iGM) .gt. zzk)  exit
            end do
            !Accumulate weights to create GM histogram
            xxx = xWeight*wmspl
            work4(jcrit,kGM) = work4(jcrit,kGM)+ xxx  
            
            return
        end if

        !Bounds of the distribution
        zzkMin = zzk - xnSIGMA*sigma
        zzkMax = zzk + xnSIGMA*sigma

        !Size of bins sampled
        zzksize = (zzkMax - zzkMin) / Float (nLHbins)

        !Normalization factor
        fx1     = real ( xnSIGMA, kind = 8)
        call XNordf ( fx1, fx3 )
        pSIGMA1 =  real ( fx3, kind = 4)
        xNormal = 2.*pSIGMA1 - 1.
                          
        !Loop on number of LHC-sampled bins (Typically 10 bins)
        zzk5 = (zzkMin-zzk)/sigma
        fx1 = real ( zzk5, kind = 8 )
        call XNordf ( fx1, fx3 )
        zzk5 = real ( fx3, kind = 4 )

        do isimSIG = 1, nLHbins

            zzk3 = zzkMin + zzksize*(isimSIG-1)
            zzk4 = zzkMin + (isimSIG*zzksize)
            !Weight of the LH sampling bin
            fx0  = (zzk4-zzk)/sigma
            fx1  = real ( fx0, kind = 8 )
            call XNordf ( fx1, fx3 )
            zzk6 = real ( fx3, kind = 4 )
            wzzk = (zzk6 - zzk5) / xNormal
            zzk5 = zzk6 
            !So..., 
            !now we have the bounds and weight of the bin

            !Do the Uniform sampling within a bin
            wgmt = wzzk / Float(nLHSamples) 
            do ii = 1, nLHSamples
                r1 = rnunf()
                zzk2 = zzk3 + r1*(zzk4-zzk3)
                !Index of GMarray bins for  estimated GM
                do iGM = 1, nGMbins
                    kGM = iGM                              
                    if (GMarrayLog(iGM) .gt. zzk2)  exit
                end do
                !Accumulate weights to create GM histogram
                xxx = xWeight*wgmt*wmspl
                work4(jcrit,kGM) = work4(jcrit,kGM)+ xxx     
            end do

        end do

        return

        end subroutine LHCGM
