!sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
        subroutine  faultPoint                                         & 
                    (Az1, Az2, cigar, D1, D2, DipPoint1, DipPoint2,    &
                    Nsub, Rake1Point, Rake2Point, rigidity, StrMax,    &
                    StrMin, VsMax, VsMin, Xi, xLmax, xMagPoint, Yi)       
                                                 
!
!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
!          This routine is called from GMHR2 to create fault geometries 
!          to be used by synhaz.
!
!
!       this routine generates the geometry of a fictitious one-segment fault
!       based on the earthquake epicentral location and its magnitude.
!       The output is an array of properties for a set of Nsub sub-elements
!       that the fault is divided into, for use in synhaz.

!Approach:
!       -- Total rupture surface area is calculated from empirical function
!       -- Aspect ratio is assumed equal to 1. as default, except when
!          dimensions of fault require otherwise
!       -- The fault is built around the location of hypocenter of event
!       -- First, overall geometry of fault is determined using simulated
!          values of Dip and Azimuth, from range of values given in input
!       -- Then, location of initiation point whithin the fault, is simulated
!       -- Then, overall fault is shifted (Long and Lat of nodes) to bring
!          initiation point at the location of the earthquake.

!Input:
!       Az1, Az2  : Range of azinuths for the on e segment simulated fault
!                   Angle from geographical North going clockwise dec. degrees
!       D1, D2    : Range of depth for location of hypocenter
!       DipPoint1,  DipPoint2 : 
!                   Range of dip for the one segment fault (decimal degrees)
!                   Standard orthogonal axis system, NOT Aki & Richard's system.
!       rigidity  : Earth rigidity
!       xLmax      : Default length of sub-elements input to synhaz
!       Xi, Yi    : Initiation point Coordinates (epicenter) decimal degrees
!       xMagPoint : Magnitude of earthquake
!
!Output: 
!       Nsub      : Number of sub-elements created to cover the entire rupture
!       Csub(i,j) : Coordinates of centers of sub-elements  
!                   j=1 Longitude (deg.), 2 Latitude (deg.), 3 depth (km).
!       Asub(i)   : Area of the sub-elements (km^2)
! 
!*******************************************************************************
!       
        use CritNodes
        use ModuleSYNHAZ
        use ModuleRSQSim
        use ModuleEQ
        use GenParam
        use CritFacilities
!
        IMPLICIT NONE

        INTEGER :: i, iflag, initEQ, j, k, nL2, nStore, Nsub, nW2

        REAL    :: area, AzimAR, Azim, Az1, Az2, cigar, dd, Di, DipAR
        REAL    :: DipPoint, DipPoint1, DipPoint2
        REAL    :: Drad, dx, dy, dz, D1, D2, pi
        REAL    :: radx, RakePoint, Rake1Point, Rake2Point
        REAL    :: rdegree, rearth, rigidity, rnunf, rupta, rx
        REAL    :: siminitEQ, Str, StrMax, StrMin
        REAL    :: tau, Vrup, Vs, VsMax, VsMin
        REAL    :: xcos, Xi, xL, xleffective, xl1, xLmax, xM0, xM1
        REAL    :: xMagPoint, xM0log, xsin, xW, xWeffective
        REAL    :: x1, x2, x3 
        REAL    :: Yi, y1, zza1, zza2

        REAL    ( kind = 4 ) r4_normal_01_cdf_inverse

        REAL, dimension(4,3) :: apex
        REAL, dimension(1,4) :: dumar2

        REAL, allocatable, dimension(:,:) :: store1
        
        


!*******************************************************************************  
!Constants and misc. initiations:
        rearth  = 6371200.                   ! Earth radius in meters
        rdegree = rearth * 6.28318531 / 360. ! Length of 1 degre at equator
        radx    = 57.2957795                 ! Conversion degree-radian
        pi      = 3.14159265                 ! Rhubarb 

!*******************************************************************************

! Total area of fictitious fault: Use Young and CopperSmith
!       Use Larry's relation
!       A(km^2)= (2.23/10^15)* (M0^(2/3))
!       with M0  (dyne.cm) = 10^(1.5M+16.1)
        xM0log = (1.5*xMagPoint+16.1)
        xM0    = 10.**xM0log
        rupta  = 0.3483 -15. + (xM0log*0.667)

	!uncertainty in rupture area
        zza1   = rnunf ()            !random number [0,1]
        zza2   = r4_normal_01_cdf_inverse ( zza1 )
        rupta  = 10.** (rupta + cigar*zza2)

! Length and Width of fictitious fault
        xL     = sqrt (rupta)
        xW     = xL

! Hypocenter depth
        zza1   = rnunf ()            !random number [0,1]
        Di     = D1 + (D2-D1)*zza1

! Start with vertical fault apexes with input catalog initiation point at center
!
!                     C1--------------------------------C4 --------> North
!                      |                                |
!                      |                                |
!                      |                                |
!                      |..xl1...........+....xl2........|
!                      |                ^               |
!                      |            Initiation          |
!                      |             (xi,yi)            |
!                      |            at center           |
!                     C2--------------------------------C3
!                      |
!                      |
!                      |
!                      V
!                Local element Y-axis
!
!       Fault vertical and North azimuth (azim = 0.)
        xl1 = xl*1000. / rdegree      !Lentgh in degrees
        ! Node C1
        apex(1,1)     = xi            !Longitude
        apex(1,2)     = yi - xl1/2.    !Latitude
        apex(1,3)     = Di + xW/2.    !Depth
        ! Node C2
        apex(2,1)     = xi
        apex(2,2)     = yi - xl1/2.
        apex(2,3)     = Di - xW/2.
        ! Node C3
        apex(3,1)     = xi
        apex(3,2)     = yi + xl1/2.
        apex(3,3)     = Di - xW/2.
        ! Node C4
        apex(4,1)     = xi
        apex(4,2)     = yi + xl1/2.
        apex(4,3)     = Di + xW/2.
!
! Determine number of sub-elements in length and width: 
        !nL2 and nW2 (iflag=1)
        iflag  = 1
        nstore = 1
        call EMPSYNcontour (apex,iflag,nL2,nStore,Nsub,nW2,dumar2,xLmax)
        Nsub = nL2 * nW2
!
! Sub-elements center coordinates in in vertical fault
        !North azimuth = 0.
        iflag  = 2
        nStore = Nsub
        ALLOCATE ( store1(nStore, 4) )
        call EMPSYNcontour (apex,iflag,nL2,nStore,Nsub,nW2,store1,xLmax)
        !Transfer values in store1 to XYZsub():
        do i = 1, Nsub
            !Mapping for synhaz is identity
            SynEqSubMap(i) = i
            do j = 1,3
               !SynXYZsub(i,j) = store1(i,j)
               XYZsub(i,j) = store1(i,j)
            end do
        end do
        !Effective dimensions of su-elements
        xleffective = xl / Float(nL2)
        xWeffective = xW / Float(nW2)

! Simulation of azimuth (O at North. Positive clockwise)
        rx = rnunf () 
        Azim = Az1 + (Az2-Az1)*rx 

! Simulation of Dip (Orthogonal system notation)
        rx = rnunf () 
        DipPoint = DipPoint1 + (DipPoint2-DipPoint1)*rx
        !in radians
        Drad = DipPoint / radx  

! Dip and Azimuth in Aki and Richard's notation
        if (DipPoint .ge. 0.)  then
            DipAR  = DipPoint
            AzimAR = Azim
        else
            DipAR  = 90. + DipPoint
            AzimAR = MOD (Azim+180., 360.)
        end if

! Select initiation sub-element randomly among the Nsub sub-elements
        if (Nsub.eq.0) then
           initEQ = 1
        end if   
        if (Nsub .eq. 1)  then
           initEQ = 1
        else
           siminitEQ = rnunf ()
           siminitEQ = siminitEQ * Float(Nsub)
           initEQ = INT(siminitEQ) + 1
           if (initEQ .gt. Nsub)  then
             initEQ = Nsub
           end if
        end if
        !Load coordinates of Initiation point sub-element
        do j = 1,3
            synXYZinit(j) = store1(initEQ,j)
        end do 

        deallocate ( store1 )

! Construct final fault by moving down, translating and rotating to be such that
! initiation center is located at coordinates of input catalog event, that Dip
! and azimuth are as simulated, and that top of fault is not at negative depth.
        !1. Vertical move to correct for depth of initiation point
        !2. Horizontal translation to bring epicenter to that of EQ in catalog
        dx = Xi - synXYZinit(1)
        dy = Yi - synXYZinit(2)
        dz = Di - synXYZinit(3)
        do i = 1, Nsub
            !synXYZsub(i,1) = synXYZsub(i,1) - dx
            !synXYZsub(i,2) = synXYZsub(i,2) - dy
            !synXYZsub(i,3) = synXYZsub(i,3) - dz
            XYZsub(i,1)    = XYZsub(i,1) - dx
            XYZsub(i,2)    = XYZsub(i,2) - dy
            XYZsub(i,3)    = XYZsub(i,3) - dz
        end do
        !3.Rotate around initiation point, from vertical to simulated Dip
        do i = 1, Nsub
            if (i .ne. initEQ)  then
                !dd = synXYZsub(i,3) - synXYZsub(initEQ,3)
                dd = XYZsub(i,3) - XYZsub(initEQ,3)
                dx = dd * cos(Drad)
                dz = dd * sin(Drad)
                !synXYZsub(i,1) = synXYZsub(i,1) - dx
                XYZsub(i,1) = XYZsub(i,1) - dx
                if (DipPoint .le. 0.)  then
                   !synXYZsub(i,3) = synXYZsub(i,3) + dz
                   XYZsub(i,3) = XYZsub(i,3) + dz
                else
                   !synXYZsub(i,3) = synXYZsub(i,3) - dz
                   XYZsub(i,3) = XYZsub(i,3) - dz
                end if
            end if
        end do
        !4. Check that fault is not in the air
        !dz = synXYZsub(1,3) - 0.5*xWeffective*cos(Drad)
        dz = XYZsub(1,3) - 0.5*xWeffective*cos(Drad)
        !If dz < 0, move all points down
        if (dz .lt. 0.)  then
            do i = 1, Nsub
               !synXYZsub(i,3) = synXYZsub(i,3) + dz
               XYZsub(i,3) = XYZsub(i,3) + dz
            end do
        end if
        !5. Rotate of angle Azim around vertical axis at initiation Point
        xSin = sin(Azim/radx)
        xCos = cos(Azim/radx)
        do i = 1, Nsub
            !x1             =  synXYZsub(i,1) - synXYZinit(1)
            !y1             =  synXYZsub(i,2) - synXYZinit(2)
            !synXYZsub(i,1) =  synXYZinit(1) + ( x1*xCos + y1*xSin)
            !synXYZsub(i,2) =  synXYZinit(2) + (-x1*xSin + y1*xCos)
            x1          = XYZsub(i,1) - synXYZinit(1)
            y1          = XYZsub(i,2) - synXYZinit(2)
            XYZsub(i,1) = synXYZinit(1) + ( x1*xcos + y1*xsin)
            XYZsub(i,2) = synXYZinit(2) + (-x1*xsin + y1*xcos)
        end do
  

!*******************************************************************************

! Stress-Drop is sampled from Uniform distribution StrMin-StrMax
!           StrMin and StrMax are in bars. Read from input Menu file
            rx = rnunf ()
            str = strMin + (StrMax-StrMin)*rx

! Rise-time, tau,  is estimated with Brune source model
!           where Vs is shear wave velocity in km/sec
!           Vs is given a range and is simulated with Uniform distribution
!           VsMin and VsMax are read from Menu file 
!           M0 is in dynes.cm
            rx = rnunf ()
            Vs = VsMin + (VsMax-VsMin)*rx
                
!Slip is assumed to be same for all sub-elements
            !rigidity = earth rigidity (Pa)
            !rupta    = area (km^2) 
            !Slip here in cm 
            x2 = 1.e-11 * xM0 / (rigidity*rupta) 
 
! Load info for synhaz 
            NSynSubElt = Nsub
            do k = 1, Nsub

                !Sub-elements areas are all equal (in km^2 for input to synhaz)
                !area = 1.e06 *rupta / Float(Nsub)
                area = rupta / Float(Nsub)
                SynAreaSub(k) = area

                !Sub-Element seismic moment
                xM1 = xM0 / Float(Nsub)
                SynSUbM0(k) = xM1

                !Sub-element Rise time               
                call Point_rise_time ( Str, tau, Vs, xM1 )
                Synrise(k) = tau

                !Sub-element Slip, assumed to be constant for entire rupture
                !M0 = mu * Slip * Area
                !Slip = M0 / (mu*Area)
                SynSubSlip(k) = x2

                !Sub-element Stress-Drop,in Mpa to be converted in bars in synhaz 
                SynStdp(k) = Str / 10.19

                !Rigidity - was read in Pa. 1.pa = 10 dynes/cm2 as needed in synhaz
                !SynRgd(k) = rigidity 

                !Slip-time (sec): slptim (i) time of initiation of elt 
                !Distance from initiation point divided by rupture velocity
                Vrup = Vs * SynVr
                !dx   = SynXYZsub(k,1)
                !dy   = SynXYZsub(k,2)
                !dz   = SynXYZsub(k,3)
                dx   = XYZsub(k,1)
                dy   = XYZsub(k,2)
                dz   = XYZsub(k,3)
                call  distsimple (Xi,Yi,dx,dy,dd)
                dd = sqrt (dd*dd + (dz-Di)*(dz-Di)) 
                !SynSubTim(k) = dd / Vrup
                x3 = sqrt((area/3.14156)) / Vrup
                SynSubTim(k) = x3

                !Percentage of Vs for rupture velocity 
                !Rupture velocity, Percentage of Vs
                SynRsv(k) = SynVr

                !Percentage of Vr for healing for rupture velocity 
                !Rupture velocity, Percentage of Vs
                SynRvh(k) = SynVh

                !Dip of Sub-element. SynSubDip
                SynsubDip(k) = DipPoint

                !Strike of Sub-element. Use standard convention.
                SynSubStk(k) = Azim

                !Rake in sub-element
                !Simulate a Rake, valid for the entire fault 
                    !Lower bound angle  = Rake1Point  - usual notation
                    !Best estimate      = RakePoint   
                    !Upper bound        = Rake2Point  
                    rx = rnunf ()
                    !For now, use a uniform distribution of rake between min-max
                    RakePoint = Rake1Point + (Rake2Point-Rake1Point)*rx
                SynsubRake(k)= RakePoint

            end do  

        !Change sign of depth for use in Synhaz (Useless now synhaz test for   < 0)
        do i = 1, Nsub
           !SynXYZsub(i,3) = - SynXYZsub(i,3)
           XYZsub(i,3) = - XYZsub(i,3)
        end do
   

        return
        end subroutine faultPoint


