!This subroutine reads the information on sesismic source, faults, areas and  
!volumes of diffuse seismicity.
!It gets the characterization of the epistemic uncertainty as a set of zonation 
!maps with weights

!      Called by subroutine "simulperil"
!
!*******************************************************************************
!
        subroutine EQsourcesinfo (CSTR1,CSTR2,dt1,Nscreen,PlotRecSimul,unit) 
                   
                   
         use ModuleEQ
         use GENparam
         
         IMPLICIT NONE

        INTEGER :: i,irec
        INTEGER :: j,jwdt,k
        INTEGER :: N,Nc,Nf,Nportrait,Nscreen
        INTEGER :: nsimrec,Ns1,Nview,Nwidth1,Nwidth2
        INTEGER :: pagenumber,PlotRecSimul
        INTEGER :: unit
               
        REAL :: alfa,beta,CumPM0
        REAL :: delbeta,dt1
        REAL :: Lam1,Lam2,Lambda
        REAL :: PM0,PM1  
        REAL :: M0,M1,M2,Ninbin
        REAL :: r1,rnunf,xmax,xm1,xm2,xsimrec
        
        REAL, dimension(MaxMagbins)   :: CumPM1
        
        REAL, dimension(4,Maxmagbins) :: XX,YY
        
        CHARACTER (LEN=41) :: CSTR1
        CHARACTER (LEN=20) :: CSTR2
        CHARACTER (LEN=60) :: CTIT1
        CHARACTER (LEN=27) :: CTIT2        
        CHARACTER (LEN=20) :: LegendTIT
        CHARACTER (LEN=5)  :: NameEXT
        CHARACTER (LEN=80) :: Nameroot
        CHARACTER (len=256):: source  
        
        CHARACTER (len=20),dimension(6) :: Legperc


!*******************************************************************************
        
!.....Read Info in sources
       if (afile .eq. '  ')  then
           afile = 'sources.txt'
       end if

       CALL RdinEQsources(M0,nsimrec,Ns1,unit)
       
#ifdef USE_DISLIN
!
!************** THIS PART ONLY FOR PLOTTING RECCURRENCE CURVES *****************
       Plot_Rec: if (iplotrec .ge. 1)  then
       
!......Generate the mean, 15th and 85th percentile reccurrence curves
!      Perform nsimrec simulations of random curves and do statistices
       CumPm  = 0.
       CumPM0 = 0.
       CumPM1 = 0.
       CumPM2 = 0.
       pagenumber = 1

       call random_seed
          !Loop over all sources
          do i = 1, Ns1
               Plot_Source_Specific: if (iplotrec1(i) .eq. 1)  then
               do irec = 1,nsimrec
               
               !simulate occurrence parameters for this simulation:
               SELECT CASE (SeisRectype(i))
               
               !Truncated exponential model
               CASE (1)
                   Lam1 = Arec(i,3) * (1.+ Arec(i,5))
                   Lam2 = Arec(i,3) / (1.+ Arec(i,5))
                   delbeta = Arec(i,6)
                   !Mmax. 
                   r1 = rnunf()
                   !uniform distribution on [MmaxLower,MmaxUpper]
                   xMax = Arec(i,7) + r1*(Arec(i,9)-Arec(i,8))
                   
                   !beta.
                   r1 = rnunf()
                   !uniform distribution on [beta-delbeta,beta+delbeta]
                   beta = Arec(i,4)-delbeta + r1*2.*delbeta
                   
                   !alfa.
                   r1 = rnunf()
                   !uniform distr. of Lambda(MLam) 
                   !with +/-  delalfa % increase/decrease
                   Lambda = lam2 + r1*(Lam1-Lam2)
                   alfa = alog(Lambda) + beta*Arec(i,1)
                          
                   !Truncated Exponential: Input G-R parameters and more
                   
                   !Real variables:
                   !Arec(i,1)= MLam Magnitude for which Lambda is given 
                   !Arec(i,2)= MG (Center of Gravity of the seis data, if known, or 0)
                   !Arec(i,3)= Lambda at MLam (annual rate of ocurrence)
                   !Arec(i,4)= Beta (Slope of G-R in natural log)
                   !Arec(i,5)= Std dev on  Lambda at MLam, in %
                   !Arec(i,6)= Std dev on Beta
                   !Arec(i,7)= Best Estimate of Max Magnitude for the source
                   !Arec(i,8)= Lower bound MaxMag. If 0, use  + or - delM 
                   !Arec(i,9)= Upper bound MaxMag. If 0, use  + or - delM
                   !Arec(i,10)= delM, uncertainty on Magmax
                     
                   !Integer Variables:             
                     
                   !Brec(i,1)= Type of probability distribution for Max Magnitude
                   !           1: Uniform between MmaxLower and MmaxUpper
                   !           2: Trapeze. 0 at Mlow, uniform on [Mbest-Mupper]
                   !           3: Trapeze. Uniform on [Mlower-Mbest], 0 at Mupper
                   !           4: Triangular. 0 at Mlower, Mode at Mbest, 0 at Mupper

               
                   call  EQTruncRec (alfa,beta,dt1,M0,xMax,M1,M2,Ninbin,PM0)
                   
                   
               !Characteristic model (Youngs-Coppersmith)
               CASE (2)
               
               !Segmented fault model (Foxall Model)
               CASE (3)
               
               !Empirical model manually entered
               CASE (4)
               
               CASE DEFAULT
               
               END SELECT
               
                    
                   !Construct the complementary cumulative for M
                   PM1 = exp (alfa - (beta*M0))
                   CumPM1(MaxMagbins) = PM1 * PM(MaxMagbins)
                   do k = MaxMagbins-1, 1, -1
                       CumPM1(k) = CumPM1(k+1) + PM1*Pm(k)
                   end do    
                       
                   !Cumulate values in array CumPm and PM0 
                   CumPM0 = CumPM0 + PM0
                   do k = 1, MaxMagbins
                       CumPm(k)  = CumPm(k)  + CumPM1(k)
                       CumPM2(k) = CumPM2(k) + CumPM1(k)*CumPM1(k)
                   end do
                             
               end do
           
          !Means and standard deviation on probability density
          xsimrec = Float(nsimrec)
          CumPM0  = CumPM0 / xsimrec
          do k = 1, MaxMagbins
               CumPM(k)  = CumPM(k ) / xsimrec
               CumPM2(k) = CumPM2(k) / xsimrec
               r1 = CumPM2(k)- CumPM(k)*CumPM(k)
               r1 = AMAX1 (0.,r1)
               sigPM(k)  = sqrt(r1)
               !mean complementary cumulative function
               PMperc(k,1) = CumPM(k)
               !mean + sigma
               PMperc(k,2) = CumPM(k) + sigPM(k)
               !mean - sigma
               PMperc(k,3) = CumPM(k) - sigPM(k)
          end do
           
          !Cumulative distribution (G-R) with uncertainties (+/- Sig)
          do k = 1, MaxMagbins
              do j = 1,3
                  CumYY(k,j) = PMperc(k,j)
              end do
          end do

       
!.........Plot Reccurrence curves: Mean, Mean-1sigma, Mean+1sigma, Best Estimate
!         Assignement values
!       CSTR1,  = For the identification title at top of the page    
!       CSTR2     which includes:
!                    "string CSTR1"/Date and Time of the Run/"String CSTR2"      
!       jwdt    = Selection of Date and Time page header, and page number
!               = 1, YES
!               = 0, NO Date and time page header, and NO page number
!       LegendTIT   = Legend Title
!       Maxmag  = Max number of magnitude points on X axis
!       N       = Number of points in each curve to be plotted (same for all)
!       NameModels  = Name of the models plotted
!       Nc      = Number of curves
!       Nf      = number of points in each curve
!       Nportrait   = 1 plot in portrait layout
!                     2 plot in landscape layout
!       Nscreen = For Nview=1 only (Screen Display), counts number of frames 
!                 displayed. This number runs through the entire application
!       Nwidth1 = Width of the major lines (Nwidth=15 default)
!       Nwidth2 = Width of other lines (good number is Nwidth2=10)
!       Nview   = Selects the screen or file format (See above note)
!       pagenumber  = For (Nview.NE.1 (storing on file'NamePlotRec'), keeps
!                 a counter of number of pages.
!       SOURCE  = Name of Seismic SOURCE
!       TEXTREC = Root name for the Recurrence plot file (to which we add
!                 the selected extension, .png, .pdf, ...)
!       XMAG1   = Second part of the second line in the figure title
!       X(I,J)  = Array of max dimension (Nf,NcMax) of X values for Nc curves
!       Y(i,j)  = Array of max dimension (Nf,NcMax) of Y values for Nc curves
     
!         Output graphic file format and default settings
          jwdt = 1
          NameEXT    = 'CONS'
          Nameroot   = 'EQrec'
          Nportrait  = 1
          Nview      = 1
          NWIDTH1    = 15
          NWIDTH2    = 15
                
!       Header title for plots    
!		  CSTR1 = 'RiskNIEQ-SRC 10.1 Compiled:6/26/10 ---- Run:'
!         CSTR2 = '---- Jean Savy, SRC'
          CTIT1 = 'Cummulative Annual Rates'
          CTIT2 = 'Source:'
        
          !Legend labels for percentile hazard curves
          xm1 = Arec(i,7)
          xm2 = xm1 + Arec(i,10)
          write (LegendTIT,"(a7,f4.2,a3,f4.2)") 'Mmax = ',   &
          xm1,' - ',xm2 
          Legperc(1) = 'Mean Truncated Exp'
          Legperc(2) = 'Mean + Sig'
          Legperc(3) = 'Mean - Sig'
          Legperc(4) = 'Gutemberg-Richter'
          !  	 
          N     = iMagStop(i) - iMagStart(i) + 1
          Nc    = 4
          Nf    = N
          source   = SourceName(i)
            
          beta = Arec(i,4)
          alfa = alog(Arec(i,3)) + beta*Arec(i,1)
          do j = 1, N
              k = j + iMagStart(i) - 1
              XX(1,j) = xMagBins(k)
              XX(2,j) = xMagBins(k)
              XX(3,j) = xMagBins(k)
              XX(4,j) = xMagBins(k)
              YY(1,j) = CumYY(k,1)
              YY(2,j) = CumYY(k,2)
              YY(3,j) = CumYY(k,3)
              YY(4,j) = exp (alfa - xMagbins(k)*beta)
          end do
   
!.........Plot  Recurrence curves for the izf-th source---------------------------
          if ((PlotRecSimul.gt.0) .and. (PlotScreen.eq."Display-Screen"))  then
              CALL PlotRecurrence (CSTR1,CSTR2,CTIT1,CTIT2,jwdt,      &
              LegendTIT,N,N,NameEXT,Legperc,Nameroot,i,source,  &
              Nc,Nportrait,Nscreen,NVIEW,NWIDTH1,pagenumber,XX,YY)
          end if
!       END Plot for the i-th Source------------------


          end if Plot_Source_Specific
          !End Loop on number of Sources
          end do
           
       end if Plot_rec
#endif
          
          return
          end subroutine EQsourcesinfo
