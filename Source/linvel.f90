      subroutine linvel(h,dis,rp,angi,ainp,ag,vsp,vm,zm,tt)

      implicit none
      
      INTEGER :: i, idis, nang
      REAL    :: aangi, aangii, adis, ag, ainp,  angi, angi1, axc
      REAL    :: daio, dis, dp, dpp1, h, pi
      REAL    :: raio, ric, rp, rp1, rp2, rt1, rt2, rx, rzif
      REAL    :: spr, tandai, td, tr, tt, x1, x2, xc
      REAL    :: vm, vmx, vmx1, vsp1, vzf, vzf1, vsp, vsrp
      REAL    :: zdp, zif, zm
      

! 08/22/94 roundoff accuracy protection added to asin( ).
! 08/20/00 modified to allow hypoceenter to be in mantle
!
! computed values
!
! ainp:  incident angle in radians (from zero down)
!      incident angle for s wave from source same as for p from source
! angi:  takeoff angle from zero down
!        takeoff angle for s wave same as for p
!   rp:  ray parameter for
!        ray parameter for s at source same as for p
!  vsp:  p velocity at surface
! compute velocity at focus and base of crust
      pi = 3.141593
      vzf=vsp+ag*h
      vmx=vsp+ag*zm
!-LJH 10/05/93: for direct up, station coordinates same as hypocenter
      if(dis.eq.0.0) then
      tt = (log(vsp+ag*h)-log(vsp+ag*0.0))/ag
      angi = pi
      ainp = 0.0
      rp = 0.0
      go to 51
      endif
!-LJH 08/20/00: for event in the half-space
      if(h.gt.zm) then
!-LJH grid search at 0.1 km interval, look for ray parameter match
!-don't add one to idis because x2 goes to zero.
      idis = ifix(dis/0.1) + 1
      if(idis.lt.2) idis = 2
      adis = dis/float(idis-1)
      spr = 100.0
      vzf=vm
      do 10 i = 1,idis-1
      x1 = float(i-1)*adis
      x2 = dis-x1
      if(x2.lt..000001) x2 = .000001
!-modify xc=dis/2.0-(vsp/ag+h/2.0)*h/dis
      xc=x2/2.0-(vsp/ag+zm/2.0)*zm/x2
      axc=abs(xc)
      if(axc.lt..000001) axc = .000001
      zif=atan((zm+vsp/ag)/axc)
      dp=sin(zif)/vzf
      dpp1 = dp*vsp
      if(dpp1.gt.1.0) dpp1 = 1.0
      daio=asin(dpp1)
      aangii = x1/sqrt(x1**2+(h-zm)**2)
      if(aangi.gt.1.0) aangi = 1.0
      angi1 = pi-asin(aangi)
      rp1= sin(angi1)/vm
      rp2=sin(zif)/vzf
      if(abs(rp1-rp2).lt.spr) then
      spr = abs(rp1-rp2)
      tandai = tan(daio/2.0)
      if(tandai.eq.0.0) tandai = 0.000001
      tt=(log(tan(zif/2.0)/tandai))/ag + sqrt(x1**2+(h-zm)**2)/vm
      rp=rp1
      angi=angi1
      vsrp = vsp*rp 
      if(vsrp.gt.1.0) vsrp = 1.0
      ainp = asin(vsrp)
      endif
 10   continue
      return
      endif 
!
! compute variables for refracted ray
      rp=1.0/vm
      vzf1 = vzf/vm
      if(vzf1.gt.1.0) vzf1 = 1.0
      rzif=asin(vzf1)
      vsp1 = vsp/vm
      if(vsp1.gt.1.0) vsp1 = 1.0
      raio=asin(vsp1)
      vmx1 = vmx/vm
      if(vmx1.gt.1.0) vmx1 = 1.0
      ric=asin(vmx1)
      rt1=(cos(raio)+cos(rzif)-2.0*cos(ric))/(ag*rp)
      rt2=(log(atan(ric/2.0)/tan(raio/2.0))+log(tan(ric/2.0)/    &
      tan(rzif/2.0)))/ag
! direct ray
      xc=dis/2.0-(vsp/ag+h/2.0)*h/dis
      axc=abs(xc)
      zif=atan((h+vsp/ag)/axc)
      dp=sin(zif)/vzf
      dpp1 = dp*vsp
      if(dpp1.gt.1.0) dpp1 = 1.0
      daio=asin(dpp1)

      if(xc .gt. 0.)  then 
          rx=1.0/(ag*dp)
          zdp=rx-vsp/ag
          if(zdp.ge.zm) then
!             LJH insert here for refracted arrival
              tr=(log((1.0/tan(zif/2.0)))+log((1.0/tan(daio/2.0))))/ag
              go to 80
          endif 
          td=(log((1.0/tan(zif/2.0)))+log((1.0/tan(daio/2.0))))/ag
          nang=2
      else
!         upward ray
          td=(log(tan(zif/2.0)/tan(daio/2.0)))/ag
          nang=1
      end if

! refracted ray time
      tr=(dis-rt1)/vm+rt2
      if(dis.lt.rt1)go to 62
! compare travel times
      if(td.le.abs(tr))go to 62
   80 angi=rzif
      nang=3
      tt = tr
      go to 50
   62 angi=zif
      tt = td
   50 continue
      rp=sin(angi)/vzf
      vsrp=vsp*rp 
      if(vsrp.gt.1.0) vsrp = 1.0
      ainp=asin(vsrp)
 51   return
      end
