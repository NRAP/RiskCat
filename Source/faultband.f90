!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
        subroutine  faultband  (cigar,dips,depth2, &
                    iATN_Larry,ifault,ipfault,maxnodl, &
                    maxnodw,Mxstrp,NcritRD,  &
                    nl,nd,npltf,nsimul,nSubTot,rw,trace,  &
                    xf,xh,xmagsim,  &
                    xLmax,xmpltf1,xmpltf2,xwmax)    
                                            
!
!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
!          This routine is called from GMHR and from GMHR2
!
!
!       this routine calculates the probability distribution of the
!       shortest distance from the site to the fault rupture as a
!       function of the magnitude.
!		The 3-D fault composed of segments (along the strike/length)
!		and strips (along the width/depth). The strips are obtained 
!		by cutting the 3-D surface of the fault by a series of parallel
!		planes at increasing depths
!       cigar      =
!       faultname  =
!       idseed     =
!       ifault     = Fault identification number 
!       ipfault    = 1 to request a plot of the fault, 0 otherwise
!       iplotnum   = Not used yet (some plot control parameter)
!       mgridyes   = Not used yet (some plot control parameter)
!       nfault     = some fault ID for putting on plots, not used
!       
!		nl	= number of segments along length (strike) of fault
!		nd	= number of strips along the width
!		trace(i,j) = 3-D coordinates of the (nl+1) points that define
!					 the upper fault boundary. 
!                                        If trace(ji,3)=0, the 
!					 upper boundary is the surface trace. 
!					 trace(i,1), trace(i,2) = long., lat. 
!		dips(i,j)  = dip of the i-th segment in the j-th strip
!		depth2(j)  = bottom depth (elevation in km) of the j-th strip
!		xs,xy	   = longitude, latitude (decimal) of the site
!		nsimul	   = number of simulations for each magnitude
!		xmagsim    = magnitude value

!       maxseg = max. number of segments the program can handle
!                   per fault
!		mxstrp = maximum number of strips.
!		nsimul = number of simulations
!       vux, vuy, vuz = coordinates of the Observers 3D-location
!                    point of view, for plotting the 3-D fault planes

!		xf, xh, cigar define the size of a rupture for magnitude M
!                    Ln(Area-km**2) = xf*M + xh + epsilon 
!		     (Base 10)	  
!                     epsilon = (Normal deviate N(0,cigar))

!       xmpltf1,xmpltf2  = when plotting is requested, these two values
!                    determine a window of magnitude in which plots 
!                    will be done. Fault ruptures for magnitudes
!                    outside of this window will not be plotted.
!       xshift1, yshift1 = shift positionning parameters for plotting 
!       
!*******************************************************************************
!       Include the IMSL Dynamic Library
!        include 'link_fnl_shared.h'
        
        use CritNodes
        use ModuleRSQSim
        use ModuleSYNHAZ
        use ModuleEQ
        use GenParam
        use CritFacilities
!
        IMPLICIT NONE
        
        INTEGER :: i,iATN_Larry,icrit,icrit1,icrit2
        INTEGER :: ifault,iflag,ifullrup,ils,ilt,initEQ,initI,initJ
        INTEGER :: iws,iwt,ipfault,ipfault1,isml
        INTEGER :: j,jpt,k
        INTEGER :: maxnodl,maxnodw,mshift,mxstrp,n,NcritRD
        INTEGER :: nd,nfull,niter,nl,nL2,nnp2,npltf,nprecis
        INTEGER :: nsegment,nsegment2,nsimul,nStore,Nsub,NsubTot,nW2
        
        INTEGER, dimension(nl*nd) :: NsegSub
  
        REAL :: abc,arup,b1b2,cigar,correc,correc1,dd,dd1,ddd,delar
        REAL :: dell1,dell2,delw1,delw2,d1,d2
        REAL :: fdz,flx,frx,fuz
        REAL :: precis, precmax,radx,ratioH,ratioZ,rb
        REAL :: rnunf,rr3,rupta,rupta1,rw,r1,r2,r3,r4,rx
        REAL :: siminitEQ,strike,suml,suml1,sumw,sumw1,SynDip1
        REAL :: sx,sy,totarea,totarup
        REAL :: xad1,xad2,xbottom,xf,xh,xinit,xl,xleft,xLmax,xlrange
        REAL :: xlstart,xlstop,xltot,xm,xmagsim,xMoment
        REAL :: xmpltf1,xmpltf2,xM0log,xright,xupper
        real :: xw,xwmax,xwrange,xwstart,xwstop,xwtot,xw2,xxx,x1,x2,x3,x4
        REAL :: y1,y2,y3,y4,ybottom,yinit,yleft,yright,yupper
        REAL :: z1, z2,z3,z4,zbottom,zinit,zleft,zright,zupper
        REAL :: zza1, zza2, zzl, zzw

        REAL    ( kind = 4 ) r4_normal_01_cdf_inverse
  
        REAL, dimension(3) :: a1,a2,b1,b2,b3,b4,b5
        REAL, dimension(4) :: dumar
        REAL, dimension(1,4) :: dumar2
        REAL, dimension(4,3) :: apex
        REAL, dimension(4,maxseg,mxstrp,3) :: c

        REAL, dimension(maxseg,mxstrp) :: elta,shd,shd2
        REAL, dimension(maxnodl) ::  xwd
        REAL, dimension(maxnodw) ::  xld
        REAL, dimension(10) ::  prec
    
  !Common blocs
        REAL ::  dips(maxseg,Mxstrp),depth2(Mxstrp)
        REAL ::  trace(maxnodl,3),crup(4,maxseg,mxstrp,3)
        REAL ::  xlseg(maxseg,Mxstrp),xwseg(maxnodl,Mxstrp)
  
 !Allocatables:
        INTEGER, allocatable, dimension(:,:)   :: Mapseg
        INTEGER, allocatable, dimension(:,:)   :: MapsegIJ, Nsub2
        REAL,    allocatable, dimension(:,:)   :: Rake2, store1, store2
        REAL,    allocatable, dimension(:,:,:) :: store3
  
!*******************************************************************************
      
!Intiations of variables
      radx = 57.2957795
      prec(1) = 0.01
      do i = 1,9
          prec(i+1) = prec(i) * 1.5
      end do
      precmax = 1.0

                    sx = CritXY(1,1)
                    sy = CritXY(1,2)
                    !print *,'faultband-line136-site X,Y:',sx,sy

!c	Calculate the coordinates of the quadrilaterals apexes
!c	to account for the direction change at the trace (avoid
!c	overlap or gap between elements lower boundaries).

        do i = 1,nl
        !print *,'faultband-line140-trace x,y,z:',trace(i,1),trace(i,2),trace(i,3)
            do j = 1,3
                c(1,i,1,j) = trace (i,j)   !longitude of corner 1(top left)
                c(4,i,1,j) = trace (i+1,j) !longitude of corner 4(top right)
            end do
        end do

        loop_1:  do  j = 1,nd

            !if nl=1 (only one segment in the length), then only one flap
            One_flap:  if (nl.eq.1)  then
                 i = 1
                 do  k = 1,3
                     a1(k) = c(1,i,j,k)
                     a2(k) = c(4,i,j,k)
                 end do
                 !d1 = depth of the upper trace of the element
                 if (j .eq. 1)  then
                     d1 = trace(i,3)
                 else
                     d1 = depth2(j-1)
                 end if
                 !d2 = depth of the bottom trace of the element
                 d2 = depth2(j)
                 call  xyznext (a1,a2,dips(i,j),d1,d2,b1,b2)
                 do  k = 1,3
                     c(3,i,j,k) = b2(k)
                     c(2,i,j,k) = b1(k)
                 end do

            else One_flap

                loop_11:  do i = 1,nl-1
                    do  k = 1,3
                        a1(k) = c(1,i,j,k)
                        a2(k) = c(4,i,j,k)
                    end do
                    !d1 = depth of the upper trace of the element
                    if (j .eq. 1)  then
                        d1 = trace(i,3)
                    else
                        d1 = depth2(j-1)
                    end if
                    !d2 = depth of the bottom trace of the element
                    d2 = depth2(j)

                   !calculate coordinate of bottom points, b1 and b2, on
                   !perpandicular to upper trace. Not avoiding gaps or overlaps
                   !segment B1-B2 of element i will intersect B1-B2 of element
                   ! i+1 at point B3.

                   call  xyznext (a1,a2,dips(i,j),d1,d2,b1,b2)

                   do  k = 1,3
                       a1(k) = c(1,i+1,j,k)
                       a2(k) = c(4,i+1,j,k)
                   end do
                   !Calculate B1 and B2 for the next element. (named B4 and B5)
                   !D1 & D2 same as previous element
                   call  xyznext (a1,a2,dips(i+1,j),d1,d2,b4,b5)

                   !B1-B2 intersects B4-B5 at B3, in horizontal plane at depth D2
                   call  intersect  (b1,b2,b3,b4,b5,d2)
                   do  k = 1,3
                       if (i.eq.1) c(2,i,j,k)   = b1(k)
                       c(3,i,j,k)   = b3(k)
                       c(2,i+1,j,k) = b3(k)
                       c(3,i+1,j,k) = b5(k)
                   end do

                end do loop_11
            end if One_flap


	!set coordinates of top nodes of next strip
                if (j .lt. nd)  then
                        do  k = 1,3
                                c(1,i,j+1,k) = c(2,i,j,k)
                                c(4,i,j+1,k) = c(3,i,j,k)
                        end do
                end if

        end do loop_1

        !length along the steepest descent
        do  i = 1,nl
                do  j = 1,nd
                        dd = c(2,i,j,3) - c(1,i,j,3)
                        shd(i,j) = dd / cos(dips(i,j)/radx)
                end do
        end do

	
        !total length in each strip is the sum of each segment length
        do j=1,nd+1; xld(j)=0.; end do
        do j = 1,nd+1
                do i = 1,nl
                        if (j .le. nd)  then
                                x1 = c(1,i,j,1); y1=c(1,i,j,2)
                                x2 = c(4,i,j,1); y2=c(4,i,j,2)
                        else
                                x1 = c(2,i,j-1,1); y1=c(2,i,j-1,2)
                                x2 = c(3,i,j-1,1); y2=c(3,i,j-1,2)
                        end if
                        call  distsimple  (x1,y1,x2,y2,dd)
                        xlseg(i,j) = dd
                        xld(j) = xld(j) + dd
                end do
        end do

        !total width in each segment is sum strips width in each segment
        do i=1,nl+1; xwd(i)=0.; end do
        do i = 1,nl+1
                do j = 1,nd
                        if (i .le. nl)  then
                                x1 = c(1,i,j,1); y1=c(1,i,j,2)
                                x2 = c(2,i,j,1); y2=c(2,i,j,2)
                                x3 = c(2,i,j,3) - c(1,i,j,3)
                        else
                                x1 = c(4,i-1,j,1); y1=c(4,i-1,j,2)
                                x2 = c(3,i-1,j,1); y2=c(3,i-1,j,2)
                                x3 = c(3,i-1,j,3) - c(4,i-1,j,3)
                        end if
                        call  distsimple  (x1,y1,x2,y2,dd)
                        ddd = sqrt (dd*dd + x3*x3)
                        xwseg(i,j) = ddd
                        xwd(i) = xwd(i) + ddd
                end do
        end do

!	Calculate the area of the elements that make up the 3-D fault
        totarea = 0.
        do  i = 1,nl
                do  j = 1,nd

                        do  n = 1,4
                                do  k = 1,3
                                        apex(n,k) = c(n,i,j,k)
                                end do
                        end do

                        call  area3D  (apex,elta(i,j))
                        abc = elta(i,j)
                        totarea = totarea + abc
                end do
        end do


!	SIMULATIONS FOR THE SPECIFIED MAGNITUDE VALUE
!	*************************************************
!	Perform the simulations on the rupture area size and initiation location 
!	within the total rupture area. 

        do icrit1 = 1,NcritRD
                xlargest(icrit1)  = 0.
                xshortest(icrit1) = 1.e10
        end do 
        nfull = 0
        jpt   = 1

        Loop_Simulation: do isml = 1,nsimul                  !SIMULATION LOOP **

           niter = 0
           precis = prec(1)
           do icrit2 = 1,NcritRD
                xlarge(icrit2) = 0.
                shortD(icrit2) = 1.e10
           end do
	   
           xm = xmagsim

           ipfault1 = 0
           if ((xm.le.(xmpltf2+1.E-6)) .and. (xm.ge.(xmpltf1-1.E-6))) then
                if (ipfault.eq.1)  then
                        nnp2 = npltf*(jpt-1) + 1
                        if (nnp2.eq.isml)  then
                                ipfault1 = 1
                                jpt = jpt + 1
                        end if
                        if (isml.eq.nsimul)  ipfault1 = 1
                end if
           end if

           !rupture area, median
           rupta1 = xf*xm + xh        !log median

!          For debugging purposes use Larry's relation
!          A(km^2)= (2.23/10^15)* (M0^(2/3))
!          with M0  (dyne.cm) = 10^(1.5M+16.1)
           xM0log = (1.5*xm+16.1)
           rupta1 = 0.3483 -15. + (xM0log*0.667)

	   !uncertainty
           zza1 = rnunf ()            !random number [0,1]
           zza2 = r4_normal_01_cdf_inverse ( zza1 )
           rupta = 10.** (rupta1 + cigar*zza2)

           ifullrup = 0
           if (rupta .ge. totarea)  ifullrup = 1
                xl = sqrt (rupta / rw)
                if (xl .le. xld(1))  then
                        xw = xl * rw
                else 
                        xl = xld(1)
                        xw = rupta / xl
                end if
                xw2 = xwmax
                if (xwd(1) .lt. xw2)  xw2 = xwd(1)
                if (xw .gt. xw2)  then
                        xw = xw2
                        xl = rupta / xw
                end if
                correc1 = 1.
!	
!	        if rupture area >= totarea, every single element is entirely
!	        ruptured.
!               scan for the smallest distance through all the elements.
!	        Otherwise, determine which elements, or part of elements, will 
!	        rupture and calculate the smallest distance.


    
        !Initialize Nsub2(,)  to 0
        ALLOCATE ( Nsub2(nl,nd) )
        do k = 1, nl
            do n = 1, nd
                Nsub2(k,n) = 0
            end do
        end do 
 
500     Full_rupture:  if (ifullrup .eq. 1)  then

        !Case of full rupture (ifullrup =1) 
        !**********************************
	 

        if (iATN_Larry .eq. 1)  then     
           !iATN_Larry = 1 use attenuation relationships
           !Case of using standard Atenuation models for GM calculation

           if (nfull .lt. 1)  then
                do icrit = 1,NcritRD

                   shortd1(icrit) = 1.e10
                   xlarge1(icrit) = 0. 

                   do  i =1,nl
                       do  j = 1,nd
                          do  n = 1,4
                             do  k = 1,3
                                apex(n,k) = c(n,i,j,k)
                             end do
                          end do
                          sx = CritXY(icrit,1)
                          sy = CritXY(icrit,2)
                          call  shortest  (sx,sy,apex,dd)
                          if (dd .lt. shortd1(icrit))   shortd1(icrit) = dd
                          if (dd .gt. xlarge1(icrit))   xlarge1(icrit) = dd
                       end do
                   end do

                   shortd(icrit) = shortd1(icrit)
                   xlarge(icrit) = xlarge1(icrit)

                end do
                nfull = nfull + 1

           else 
   
                !Case of nfull .ge. 1
                do icrit = 1,NcritRD
                        shortd(icrit) = shortd1(icrit)
                        xlarge(icrit) = xlarge1(icrit)
                end do

           end if
	    
        else               
        ! use Larry's code SYNHAZ with full rupture case

           !Total number of flaps (segments on all strips levels)
           nsegment = 0
           !Total number of sub-elements in a rupture, for SYNHAZ case
           NsubTot  = 0   
  
           !Determine the number of sub-elements that rupture
           i = nl*nd
           ALLOCATE ( store3( i, 4, 3) )
           ALLOCATE ( Mapseg(nl,nd) )
           ALLOCATE ( MapsegIJ(MaxSynsubs,2) )
           iflag = MaxSynsubs
           iflag = 1
           nStore = 1
           nL2 = 1
           nW2 = 1
           do  i =1,nl
              do  j = 1,nd
                 nsegment = nsegment + 1
                 do  n = 1,4
                    do  k = 1,3
                       apex(n,k) = c(n,i,j,k)
                       store3(nsegment,n,k) = apex(n,k)
                    end do
                 end do

                 ALLOCATE ( store1(nStore, 4) )
                 !Determine nL2 and nW2 (iflag=1)
                 call EMPSYNcontour (apex, iflag, nL2,  &
                 nStore,Nsub, nW2, store1, xLmax)
                 Nsub = nL2 * nW2
                 Nsub2(i,j) = Nsub

                 !Update total number of sub-elements 
                 NsubTot = NsubTot + Nsub

                 !MapsegIJ gives the flap indexes that sub-element belongs to.
                 do k = (NsubTot-Nsub+1), NsubTot
                     MapsegIJ(k,1)= i
                     MapsegIJ(k,2)= j
                 end do
                
                 !Map Flap indeces with segment index
                 !Mapseg = index of segment that is included in Flap (i,j)
                     Mapseg(i,j)= nsegment

              end do
           end do 
           deallocate ( store1 )

           ALLOCATE ( store2(NsubTot,4) )
		
	   !Calculate coordinates of sub-elements centers and areas  
           !Create the grid of center points and areas of 
           !sub-elements for use in SYNHAZ simulations (ifalg=2)

           nL2   = 1
           nW2   = 1
           nsegment = 0
           NsubTot  = 0
           do  i =1,nl

              do  j = 1,nd
          
                 nsegment = nsegment + 1
                 do n = 1,4
                    do k = 1,3
                       store3(nsegment,n,k) = apex(n,k)
                    end do
                 end do

                 !Determine nL2 and nW2 (iflag=1)
                 iflag  = 1
                 nstore = 1
                 call EMPSYNcontour (apex, iflag, nL2,  &
                 nStore,Nsub, nW2, dumar2, xLmax)
                 Nsub = nL2 * nW2

                 !Calculate and load sub-elements center coordinates
                 iflag  = 2
                 nStore = Nsub
                 allocate ( store1(nStore, 4) )
                 call EMPSYNcontour (apex,iflag,nL2,nStore,  &
                 Nsub,nW2,store1,xLmax)

                 !Load center coordinates in store2 for this segment/flap
                 do n = 1,Nsub
                    do k = 1,4
                       dumar(k) = store1(n,k)
                       store2(NsubTot+n,k) = store1(n,k)
                    end do
                 end do
                 !Update number of sub-elements
                 NsubTot = NsubTot + Nsub
                
                 !Update total number of segments
                 NsegSub(nsegment) = Nsub  
                 deallocate ( store1 )  

              end do

           end do

           ils = 1
           ilt = nl
           iws = 1
           iwt = nd

!          if (ipfault1 .eq. 1)  then
!          call  plotsource  (faultname,nl,nd,c,sx,sy,isml,xm,&
!             ils,ilt,iws,iwt,c,vux,vuy,vuz,&
!             iplotnum,mgridyes,xshift1,yshift1,&
!             niter,precis,nsimul,ifault,nfault)
!          end if


        end if !end case of SYNHAZ		
        !end case of full rupture


        !Case of partial rupture. (ifullrup = 0)
        !*************************
        else Full_rupture  
		
        !determine the location of boundaries of the rupture by simulating
        ! a location of the left corner of the rupture in terms of its
        !distance from the left corner of the total fault surface.
        !these distances are xlstart and xwstart
        zzl = rnunf()                !uniform [0,1]
        zzw = rnunf()                !uniform [0,1]


!	iteration loop: Because the fault surface can be non-planar, the
        !actual resulting surface of the rupture canvas might be different
        !from the target area. We first located the upper left corner of the
        !rupture area, now we lay down the canvas of the rupture onto the
        !actual 3-D fault.  Some elements might be entirely ruptured and some
        !only partially. The irregular shape of the quadrilaterals can  introduce
        !distorsions that will lead to a total area of the canvas different from
        !that of the target.
        !we correct by iteration. calculate the resulting area and modifying
        !the size of the canvas, conserving its shape, untill it matches
        !the target.
        xltot = xld(1)
        xwtot = xwd(1)
        xlrange = xltot - xl
        xwrange = xwtot - xw
        if (xlrange .lt. 0.) xlrange = 0.
        if (xwrange .lt. 0.) xwrange = 0.
        !start along length and width
        xlstart = zzl * xlrange
        xwstart = zzw * xwrange

        !which is the starting element ?
        ils = 1
        iws = 1
        suml = 0.
        sumw = 0.

        !start is in element ils,iws
        do  j = 1,nd
                if (xwstart.ge.sumw)  then
                        iws = j
                        sumw1 = sumw
                end if
                sumw = sumw + xwseg(1,j)
        end do
        do i = 1,nl
                if (xlstart.ge.suml)  then
                        ils = i
                        suml1 = suml
                end if
                suml = suml + xlseg(i,iws)
        end do
        dell1 =  xlstart - suml1
        delw1 =  xwstart - sumw1


!test
        if ((ils.gt.nl) .or. (iws.gt.nd))  then
                abc = 1.
        end if
        if ((ils.le.0) .or. (iws.le.0)) then
                abc = 1.
        end if


        !end of rupture is in element ilt, iwt

                sumw = 0.
                iwt = 1
		!Determination of iwt (last strip in depth)
                xwstop = xwstart + xw
                do  j = 1,nd
                        if (xwstop .ge. sumw)  then
                                iwt = j
                                sumw1 = sumw
                        end if
                        sumw = sumw + xwseg(ils,j)
                end do

                suml = 0.
                !Determination of ilt (last element in length)
                xlstop = xlstart + xl

             if (xlstop .gt. xld(iwt)) xlstop = xld(iwt)
!
                xad1 = delw1 / xwseg(ils,iws)
                do  i = 1,nl
                        if (xlstop .ge. suml)  then
                                ilt = i
                                suml1 = suml
                                mshift = 2
                                j = iws
                                do n = 1,4
                                        do k = 1,3
                                                apex(n,k) = c(n,i,j,k)
                                        end do
                                end do
                                call  quadscale  (apex,xad1,b1,b2,xad2,mshift)
                                suml = suml + xad2
                        end if
                end do

        sumw1 = 0.
        if (iwt .gt. 1)  then
                i = ils
                xad1 = dell1 / xlseg(ils,iws)
                mshift = 1
                do j = 1,iwt-1
                        do  n = 1,4
                                do  k = 1,3
                                    apex(n,k) = c(n,i,j,k)
                                end do
                        end do
                call quadscale  (apex,xad1,b1,b2,xad2,mshift)
                        sumw1 = sumw1 + xad2
                end do
        end if

        dell2 = xlstop - suml1
        delw2 = xwstop - sumw1

!	Calculate the coordinates of all elements and portions of, that
!	make up the rupture area for this magnitude and this simulation.
!	Store the coordinates in array crup(n,i,j,k)
!	first load the coordinates of the full elements, then will modify
!	those that are only partially ruptured
!        print *,'faultband-676, ils,ilt,iws,iwt=',ils,ilt,iws,iwt
        do  i = ils,ilt
                do  j = iws,iwt
                        do  n = 1,4
                                do  k = 1,3
                                        crup(n,i,j,k) = c(n,i,j,k)
                                        shd2(i,j) = shd(i,j)
                                end do
                        end do
                end do
        end do

!	the only elements that are partially ruptured are at the periphery
!	of the rupture. Thus we calculate the portion of each of these
!	elements that is ruptured in term of a ratio r1 for the portion of
!	segment C1-C4, r2 for C1-C2, r3 for C2-C3, and r4 for C3-C4.
!
!                     C1--------------C4 --------> Local element X-axis
!                      |              |
!                      |              |
!                      |              |
!                      |              |
!                      |              |
!                      |              |
!                     C2--------------C3
!                      |
!                      |
!                      |
!                      V
!                Local element Y-axis
!
        r1 = dell1 / xlseg(ils,iws)        !left,for C1-C2
        r4 = delw1 / xwseg(ils,iws)        !top, for C1-C4
!c	r3 = dell2 / xlseg(ilt,iws)        !right
!c	r2 = delw2 / xwseg(ils,iwt)        !bottom
        r3 = dell2 / xlseg(ilt,iwt)        !right, for C3-C4
        r2 = delw2 / xwseg(ilt,iwt)        !bottom, for C2-C3
        if (ils .eq. ilt)  r3 = xl / (xlseg(ils,iws)-dell1)
        if (iws .eq. iwt)  r4 = delw1 / delw2
!	case of left side of the rupture (affects points C1's and C2's) use r1
        mshift = 1
        i = ils
        do  j = iws,iwt
                do  n = 1,4
                        do  k = 1,3
                                apex(n,k) = crup(n,i,j,k)
                        end do
                end do
                call  quadscale  (apex,r1,b1,b2,b1b2,mshift)
                do  k = 1,3
                        crup(1,i,j,k) = b1(k)
                        crup(2,i,j,k) = b2(k)
                end do
        end do

!	case of bottom side of the rupture (affects points C2's and C3's), use r2
        mshift = 2
        j = iwt
        do  i = ils,ilt
            do  n = 1,4
                do  k = 1,3
                    apex(n,k) = crup(n,i,j,k)
                end do
            end do
            call  quadscale  (apex,r2,b1,b2,b1b2,mshift)
            do  k = 1,3
                crup(2,i,j,k) = b1(k)
                crup(3,i,j,k) = b2(k)
            end do
        end do
	
!	case of right side of the rupture (affects points C3's and C4's), use r3
        mshift = 1
        i = ilt
        do  j = iws,iwt
            do  n = 1,4
                 do k = 1,3
                 apex(n,k) = crup(n,i,j,k)
            end do;  end do
            rr3 = r3
           ! if (isml .eq. nsimul)  rr3 = 1.
!            print *,'faultband-753, r3,rr3=',r3,rr3
            call  quadscale  (apex,rr3,b1,b2,b1b2,mshift)
            do  k = 1,3
                crup(4,i,j,k) = b1(k)
                crup(3,i,j,k) = b2(k)
            end do
        end do

!	case of top side of the rupture (affects points C1's and C4's), use r4
        mshift = 2
        j = iws
        do  i = ils,ilt
                do  n = 1,4
                        do k = 1,3
                                apex(n,k) = crup(n,i,j,k)
                        end do
                end do
                call  quadscale  (apex,r4,b1,b2,b1b2,mshift)
                do  k = 1,3
                        crup(1,i,j,k) = b1(k)
                        crup(4,i,j,k) = b2(k)
                end do
        end do


!	calculate rupture areas of each element/sub-element, and
!	the total area of the rupture, to be compared with the target area.
        totarup = 0.
        do  i = ils,ilt
        do  j = iws,iwt
            do  n = 1,4
                do  k = 1,3
                    apex(n,k) = crup(n,i,j,k)
                end do
            end do
            call  area3D  (apex,arup)
            totarup = totarup + arup
        end do
        end do

	! test if our rupture area is close to the target value	
        delar = (abs(totarup-rupta) / rupta)
!        print *,'faultband-792, totarup,rupta,precis,delar=',  &
!        totarup,rupta,precis,delar

        Delarlabel: if (delar .gt. precis)  then
           niter = niter + 1
           nprecis = 1 + (niter / 10)
           if (nprecis .gt. 10) then
               precis = precmax
           else
               precis = prec(nprecis)
           end if
	 
           !the area of the rupture is different from that of the target
           correc = sqrt(totarup/rupta)
           xl = xl / correc
           xw = xw / correc
           if (xw .gt. xw2)  then
              xw = xw2
              xl = correc1*rupta / xw
              correc1 = correc1 / correc
           end if
           if (xl .gt. xld(1)) then
              xl = xld(1)
           end if
           if ((abs(xl-xld(1)).le. 0.01) .and. &
           (abs(xw-xw2).le. 0.01)) then
              ifullrup = 1
        end if

        !Area not close enough to target: continue iteration
       go to 500               !RUPTURE AREA ITERATION LOOP

        else Delarlabel
!
	!The area of the rupture is close enough to target
!       Plot the source rupture for this simulation
!       if (ipfault1 .eq. 1)  then
!         call  plotsource  (faultname,nl,nd,c,sx,sy,isml,xm, &
!           ils,ilt,iws,iwt,crup,vux,vuy,vuz, &
!           iplotnum,mgridyes,xshift1,yshift1,&
!           niter,precis,nsimul,ifault,nfault)
!       end if
!
        Larryflette:  if (iATN_Larry .eq. 1)  then
        !Case of using attenuation relationships
        !---------------------------------------
           !iATN_Larry = 1
 	   !calculate the shortest distances
           do  i = ils,ilt
              do  j = iws,iwt
                 do  n = 1,4
                    do  k = 1,3
                       apex(n,k) = crup(n,i,j,k)
!                       print *,'faultband-841,apex(n,k)=',apex(n,k)
                    end do
                 end do
                 do icrit = 1,NcritRD
                    sx = CritXY(icrit,1)
                    sy = CritXY(icrit,2)        
                    call shortest  (sx,sy,apex,dd)
                    if (dd .le. shortd(icrit))  shortd(icrit) = dd
                    if (dd .gt. xlarge(icrit))  xlarge(icrit) = dd
                 end do
              end do  
           end do

        else Larryflette 
        !Case of using SYNHAZ.  iATN_Larry = 2
        !-------------------------------------           
 
           !Total number of flaps (segments on all strips levels)
           nsegment = 0
           !Total number of sub-elements in a rupture, for SYNHAZ case
           NsubTot  = 0 
           nStore   = 1  
  
           !Determine the number of sub-elements that rupture
           i = (ilt-ils+1)*(iwt-iws+1)
           ALLOCATE (store3( i, 4, 3))
           ALLOCATE ( Mapseg(nl,nd) )
           ALLOCATE ( MapsegIJ(MaxSynsubs,2) )
           iflag = 1
           nStore = 1
           nL2 = 1
           nW2 = 1

           do  i = ils,ilt
              do  j = iWs,iWt
                 SynDip1 = Dips(i,j)
                 nsegment = nsegment + 1
                 do  n = 1,4
                    do  k = 1,3
                       !Here the indexing of corners goes counter-clockwise
                       apex(n,k) = crup(n,i,j,k)
     !                  print *,'faultband-882,apex(n,k)=',apex(n,k)
                       store3(nsegment,n,k) = apex(n,k)
                    end do
                 end do

                 allocate ( store1(nStore, 4) )
                 !Determine nL2 and nW2 (iflag=1)
                 call EMPSYNcontour (apex, iflag, nL2,  &
                 nStore,Nsub, nW2, store1, xLmax)
                 Nsub = nL2 * nW2
                 Nsub2(i,j) = Nsub

                 !Update total number of sub-elements 
                 NsubTot = NsubTot + Nsub

                 !MapsegIJ gives the indexes that the sub-element belongs to.
                 do k = (NsubTot-Nsub+1), NsubTot
                     MapsegIJ(k,1)= i
                     MapsegIJ(k,2)= j
                 end do
                
                 !Map Flap indeces with segment index
                 !Mapseg = index of segment that is contained in flap (i,j)
                 Mapseg(i,j)= nsegment
        
                 !Assign same Dip to all synhaz sub-elements of this flap
                 !do k = NsubTot+1, NsubTot+Nsub
                 !    SynSubDip(k) = Syndip1
                 !end do

                 !Update total number of sub-elements 
                 NsubTot = NsubTot + Nsub
              end do
           end do 


           deallocate ( store1 )
           allocate   ( store2(NsubTot,4) )
		
	   !Calculate coordinates of sub-elements centers and areas  
           !Create the grid of center points and areas of 
           !sub-elements for use in SYNHAZ simulations (ifalg=2)
 
           nL2   = 1
           nW2   = 1
           nsegment = 0
           NsubTot  = 0
           do  i = ils,ilt
              do  j = iws,iwt          
                 nsegment = nsegment + 1
                 do n = 1,4
                    do k = 1,3
                       store3(nsegment,n,k) = apex(n,k)
                    end do
                 end do

                 !Determine nL2 and nW2 (iflag=1)
                 iflag  = 1
                 nstore = 1
                 call EMPSYNcontour (apex, iflag, nL2,  &
                 nStore,Nsub, nW2, dumar2, xLmax)
                 Nsub = nL2 * nW2

                 !Calculate and load sub-elements center coordinates
                 iflag  = 2
                 nStore = Nsub
                 allocate ( store1(nStore, 4) )
                 call EMPSYNcontour (apex,iflag,nL2,nStore,  &
                 Nsub,nW2,store1,xLmax)

                 !Load center coordinates in store2 for this segment/flap
                 do n = 1,Nsub
                    do k = 1,4
                       dumar(k) = store1(n,k)
                       store2(NsubTot+n,k) = store1(n,k)
                    end do
                 end do
                 !Update number of sub-elements
                 NsubTot = NsubTot + Nsub
                
                 !Update total number of segments
                 NsegSub(nsegment) = Nsub  
                 deallocate ( store1 )  

                end do
            end do

        end if Larryflette        
        end if Delarlabel
        end if full_rupture

 !****************************************************************************** 	
!.......Summarize and store

        if (iATN_Larry .eq. 1)  then      
        !Attenuation models case
           !Store distances for attenuation models
           do icrit = 1, NcritRD
              if (xlargest(icrit) .lt.  xlarge(icrit))  &
                  xlargest(icrit)  = xlarge(icrit)
              if (xshortest(icrit) .gt. shortd(icrit))  &
                  xshortest(icrit) = shortd(icrit)
           end do
           do icrit = 1,NcritRD
               shortCritRD(icrit) = xshortest(icrit)
           end do 
       
        else                              
        !Synhaz case: Initiation point
           !Store  sub-elements centers and areas  
           if (NsubTot.eq.0) then
              initEQ = 1
           end if
           !Select an initiation point randomly among all sub-elements centers
           if (NsubTot .eq. 1)  then
              initEQ = 1
           else
              siminitEQ = rnunf ()
              siminitEQ = siminitEQ * Float(NsubTot)
              initEQ = INT(siminitEQ) + 1
              if (initEQ .gt. NsubTot)  then
                initEQ = NsubTot
              end if
           end if  
           !Find in which segment the initiation is located. (initI, initJ)
            k = 0
            nsegment2 = 0
            do i = 1, nl
                do j = 1, nd
                    nsegment2 = nsegment2 + 1
                    k = k + Nsub2(i,j)
                    if (k .ge. initEQ)  then
                        initI = i
                        initJ = j
                        exit
                    end if
                end do
                if (k .ge. initEQ)  then
                    exit
                end if  
            end do       

           !Calculate seimic Moment from simulated magnitude "xmagsim". 
           !Use Scholz, Christopher (2010) relation.
           !Add 7. for Moment in dyne.cm 
           xMoment = 10 ** (1.5*xmagsim + 16.1)
           do j = 1,3
               synXYZinit(j) = store2(initEQ,j)
           end do
!******************************************************************************

           !Total number of corners
           Ncorners = icorner

           !Load Sub-elements properties for use in synhaz
           !Coordinates of sub-elements center points and areas

           NsynSubElt = NsubTot
           do i = 1, NsubTot
               !Mapping for synhaz. Here ID of sub-event = ID of sub-element
               !because there is no repetition of sub-elt rupture
               SynEqSubMap(i) = i
               !Transfer values in store2 to XYZsub():
               do j = 1,3
                  !SynXYZsub(i,j) = store2(i,j)
                  XYZsub(i,j) = store2(i,j)
               end do
               !The seismic Moment of each Sub-element is scaled according to 
               !its are. The total seismic moment is xMoment (in dynes.cm)
               !The total area is rupta (km**2).
               !At this point Sub area, stored in store2(i,4), is in km**2
               xxx = store2(i,4)                
               SynSubM0(i) = (xxx/rupta) * xMoment
               !and do not convert AreaSub (km**2)  to m**2:
               !SynAreaSub(i) = xxx * 1.e6
               SynAreaSub(i) = xxx 
               
           end do

           deallocate ( store2 )

           !Print check one flap
!           do i = 1, nsegment
 !          do j = 1,4
  !            print *,'j,syncorners(j,k),k=1,3=',  &
   !           j,(store3(i,j,k),k=1,3)
    !          !j,(syncorners(j,k),k=1,3)
     !      end do
      !     end do

    end if
	

	
        end do Loop_Simulation        !end the loop on simulations *************
!
!
!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
       
!.......Remove duplicates from syncorners(:,:), calculate rise-time for synhaz
!       
        if (iATN_Larry .eq. 2)  then

            !Unfold the rupture area and calculate the flx,frx,fuz and fdz
!
!      1 0------------x---------0 4
!        |            |fuz      |       For the entire unfolded rupture area:
!        |            |         |       X   = initiation point of main event
!        |   flx      |  frx    |       flx = distance from X to left edge
!        |------------X---------|       frx = distance from X to right edge
!        |            |         |       fuz = distance from X to upper edge
!        |            |fdz      |       fdz = distance from X to lower edge
!        |            |         |
!      2 0------------x---------0 3
           
    
        !Coordinates of the flap containing the initiation
        nsegment2 = Mapseg(initI,initJ)
        x1 = store3(nsegment2,1,1) 
        y1 = store3(nsegment2,1,2)
        z1 = store3(nsegment2,1,3)
        x2 = store3(nsegment2,2,1) 
        y2 = store3(nsegment2,2,2)
        z2 = store3(nsegment2,2,3)
        x3 = store3(nsegment2,3,1) 
        y3 = store3(nsegment2,3,2)
        z3 = store3(nsegment2,3,3)
        x4 = store3(nsegment2,4,1) 
        y4 = store3(nsegment2,4,2)
        z4 = store3(nsegment2,4,3)
        xinit  = synXYZinit(1)
        yinit  = synXYZinit(2)
        zinit  = synXYZinit(3)
        ratioZ = ABS ( (z2-zinit) / (z2-z1) )

        !Point on left edge of initiation flap
        xleft  = x2 + (x1-x2)*ratioZ
        yleft  = y2 + (y1-y2)*ratioZ
        zleft  = z2 + (z1-z2)*ratioZ

        !Point on right edge of initiation flap
        xright = x3 + (x4-x3)*ratioZ
        yright = y3 + (y4-y3)*ratioZ
        zright = z3 + (z4-z3)*ratioZ

        !Horizontal Distance between left and right edges
        call  distsimple  (xleft,yleft,xright,yright,dd1)

        !Horizontal Distance between initiation point and right edge
        call  distsimple  (xinit,yinit,xright,yright,dd)
        frx = dd
        flx = dd1 - frx
        ratioH = frx / dd1

        !Point on top edge of initiation flap
        xupper = x1 + (x1-x4)*ratioH
        yupper = y1 + (y1-y4)*ratioH
        zupper = z1 + (z1-z4)*ratioH

        !Point on bottom edge of initiation flap
        xbottom = x2 + (x2-x3)*ratioH
        ybottom = y2 + (y2-y3)*ratioH
        zbottom = z2 + (z2-z3)*ratioH 

        !Along dip Distance between upper and lower edges
        call  distsimple  (xupper,yupper,xbottom,ybottom,dd)
        dd1 = sqrt (dd*dd + (zupper-zbottom)*(zupper-zbottom))

        !Along dip Distance between initiation point and top and bottom edges
        !Initiation-Top
        fuz = dd * (1.-ratioZ)
        !Initiation-top
        fdz = dd * ratioZ   

        !Row and column of ruptured flaps are initj 
        !Row   :Add horizontal length of each flap at Initiation depth
        do i = 1, nl
            if (Nsub2(i,initJ) .gt. 0)  then
                !Calculate horizontal length of flap
                nsegment2 = Mapseg(i,initJ) !Index of segment included in this flap
                x1 = store3(nsegment2,1,1) 
                y1 = store3(nsegment2,1,2)
                z1 = store3(nsegment2,1,3)
                x2 = store3(nsegment2,2,1) 
                y2 = store3(nsegment2,2,2)
                z2 = store3(nsegment2,2,3)
                x3 = store3(nsegment2,3,1) 
                y3 = store3(nsegment2,3,2)
                z3 = store3(nsegment2,3,3)
                x4 = store3(nsegment2,4,1) 
                y4 = store3(nsegment2,4,2)
                z4 = store3(nsegment2,4,3)
                call distsimple (x1,y1,x4,y4,dd)
                call distsimple (x2,y2,x3,y3,dd1)
                dd = dd*ratioZ + dd1*(1.-ratioZ)
                if (i .gt. initI)  then
                    !Add to the left
                    flx = flx + dd
                elseif (i .lt. initI)  then
                    !Add to the right
                    frx = frx + dd
                end if
            end if
        end do
        !Column: Add along-dip length  at initiation point proportion
         do j = 1, nd
            if (Nsub2(j,initI) .gt. 0)  then
                !Calculate vertical length of flap
                nsegment2 = Mapseg(j,initI) !Index of segment included in this flap
                x1 = store3(nsegment2,1,1) 
                y1 = store3(nsegment2,1,2)
                z1 = store3(nsegment2,1,3)
                x2 = store3(nsegment2,2,1) 
                y2 = store3(nsegment2,2,2)
                z2 = store3(nsegment2,2,3)
                x3 = store3(nsegment2,3,1) 
                y3 = store3(nsegment2,3,2)
                z3 = store3(nsegment2,3,3)
                x4 = store3(nsegment2,4,1) 
                y4 = store3(nsegment2,4,2)
                z4 = store3(nsegment2,4,3)
                call distsimple (x1,y1,x2,y2,dd)
                call distsimple (x4,y4,x3,y3,dd1)
                dd = dd*(1.-ratioH) + dd1
                if (j .lt. initJ)  then
                    !Add above
                    fuz = fuz + dd
                elseif (j .gt. initJ)  then
                    !Add below
                    fdz = fdz + dd
                end if
            end if
        end do


!*******************************************************************************

            !for debugging make rise-time  x1 = 0.15, slip=5cm, stressdrop= 100.
            x1 = 0.15
            x2 = 5.
            x3 = 100.
            !Simulate a Rake for each flap of the fault
            ALLOCATE ( Rake2(nl,nd) )
            do i = 1, nl
                do j = 1, nd
                    r1 = Rake1Rd(ifault,j,i)  !Lower bound angle - usual notation
                    rb = RakeRd(ifault,j,i)   !Best estimate
                    r2 = Rake2Rd(ifault,j,i)  !Upper bound
                    rx = rnunf ()
                    !For now, use a uniform distribution of rake between r1 and r2
                    Rake2(i,j) = r1 + (r2-r1)*rx
                end do
            end do


            !Load info for synhaz and assign default values
            do k = 1, NsubTot
!               do j = 1,Nsplit
                !call ristm ( , , , x1, , ,)
                !Sub-element Rise time
                Synrise(k) = x1

                !Sub-element Slip   
                SynSubSlip(k) = 5.

                !Sub-element Stress-Drop 
                !(given from RSQSIM in MPa (10^6 N/m^2)
                !synhaz needs it in bars (10^5 Pa)
                !actually,  1.MPa = 10.19 bars
                !Conversion to bars is made in synhaz.
                SynStdp(k) = 100./10.19

                !Rigidity - Value used in ESQSim: Read from menu file in Pa
                !Needs to be converted in dynes/cm2 for synhaz.
                !SynRgd(k) = rigidity

                !Slip-time (sec): slptim (i) time of initiation of elt   
                SynSubTim(k) = 0.15 * Float(k)

                !Percentage of Vs for rupture velocity 
                !Rupture velocity, Percentage of Vs
                SynRsv(k) = SynVr

                !Percentage of Vr for healing for rupture velocity 
                !Rupture velocity, Percentage of Vs
                SynRvh(k) = SynVh

                !Dip of Sub-element. SynSubDip
                !Flap indeces:
                i = MapsegIJ(k,1)
                j = MapsegIJ(k,2)
                SynsubDip(k) = Dips(i,j)

                !Strike of Sub-element. Use standard convention.
                !It is also the strike of the segment contained in flap i,j
                !Segment index:
                n = Mapseg(i,j)
                x1 = store3(n,1,1) 
                y1 = store3(n,1,2)
                x4 = store3(n,4,1) 
                y4 = store3(n,4,2)
                z1 = store3(n,1,3)
                z4 = store3(n,4,3)
                call Aki_Richards (SynSubDip(k), strike, x1, x2, y1, y2)
                SynSubStk(k) = strike

                !Rake in sub-element
                r1= Rake2(i,j)
                SynsubRake(k)= r1



            end do
    
            deallocate ( Mapseg, MapsegIJ ) 
            deallocate ( Rake2, store3 )


        
        end if


!******************************************************************************        

        return
        end subroutine faultband


