!
!        This subroutine is called from GMHR and GMHR2
!
!        Takes an array of values XB(),(time of occurrence of earthquakes)
!        and an array of upper bounds values XA()
!        to determine the start and end indexes of the values in XB that
!        correpond to XA, to produce starting time and ending times of windows
!        of time for which we calculate the hazard.
!
!        Input:
!        nA      : number of windows of time
!        XA()    : upper bound (in years) of windows
!        nB      : number of earthquakes in the catalog
!        XB()    : times of occurrence of earthquakes  in the catalog (in years)
!
!        Output:
!        I1(i)   : Earthquake index,from catalog,at start of i-th time-window
!        I2(i)   : Earthquake index,from catalog,at end of i-th time-window
!        n(i)    : Number of earthquakes in time window i
!  
!*******************************************************************************
!
        subroutine FindStartEnd (I1,I2,nA,nB,XA,XB)
!
!*******************************************************************************
!
        implicit none

        integer                :: i,j,jn1,nA,nB,ntot
        integer, dimension(nA) :: I1,I2,n1
        real                   :: xT1,xT2
        real, dimension(nA)    :: XA
        real, dimension(nB)    :: XB
!
        I1(1) = 1
        xT1   = 0.
       
        do j = 1, nA
            xT2   = XA(j)
            n1(j) = 0
            do i = 1, nB
                if ( (XB(i).gt.xT1) .and. (XB(i).le.xT2) )   then
                    n1(j) = n1(j) + 1
                end if
            end do
            xT1 = xT2
        end do

        do i = 1,nA
            jn1 = n1(i)
        end do
        
        I1(1) = 1
        I2(1) = n1(1) 
        ntot  = n1(1)
        do j = 2, nA
            jn1 = n1(j)
            I1(j) = ntot + 1
            I2(j) = ntot + jn1
            if (n1(j) .eq. 0)  then
                I1(j) = ntot
                I2(j) = ntot
            end if
            ntot = ntot + n1(j)
        end do

        return

        end subroutine FindStartEnd                   
