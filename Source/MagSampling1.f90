!    This routine updates the number of events in sampling magnitude bins on
!    the fly, only for the case of PSHA zonation catalog generation.
!    To later calculate the weights to be applied to each bin of magnitude
!    for the construction of the histogram of GM values.
!                              Called from Riskcat
!
!*******************************************************************************
!
        subroutine MagSampling1 ( Maxspl, nbin, ispl, SamplingFlag, time, xMSc )
!
!*******************************************************************************

        USE ModuleEq
        USE GenParam


        IMPLICIT NONE

        INTEGER :: i, ispl, itp, k, Maxspl, n, n1, nbin, SamplingFlag

        REAL    :: time, time1, time2, xMSc

!-------------------------------------------------------------------------------

        !Bounds of Magnitudes in magnitudes sampling bin "i" are:
            ! Lower bound = Smag(i)
            ! Upper bound = Smag(i+1)  
        !Bounds of time for definition of Time-Window "itp":
            ! Lower bound = tlife(itp-1) (0. for first one)
            ! Upper bound = tlife(itp)

        !SamplingFlag = flag for skipping the storage of events when the number
        !of events in a mag bin is sufficient for statistical convergence.
        !i.e. for n .gt. Maxspl
        !For SamplinFlag = 0, storage will be skipped
        SamplingFlag = 0
        
        !Which bin does xM0 fall into?
        do i = 1, nbin
            if (Smag(i) .lt. xMSc)  then
                k = i
            else
                exit
            end if
        end do 


        if (time .le. tlife(Ntp))  then
           !Which time-window does this event fall into?
           itp = 1
           do i = 2 , Ntp
              if ((time.gt.tlife(i-1)) .and. (time.lt.tlife(i)))  then
                 itp = i
                 exit
              end if
           end do

!           PRINT *,'nbin = ',nbin
 !          print *,'Smag = ',(Smag(i),i=1,nbin)
  !         print *,'tlife =' ,(tlife(i),i=1,Ntp)
   !        print *,'xMSc, time = ',xMSC,time
    !       print *,'k, itp =', k,itp
   
           !Set the time of observation equal to the length of the time-window
           !if  number of events sufficient to obtain statistical convergence
           !is reached, then set time to actual time of occurrence of event
           ! since start of time-window.
           time2  = tlife(1)
           time1  = 0.
           if (itp .gt. 1)  then
               time2 = tlife(itp)
               time1 = tlife(itp-1)
           end if
          
           n = Nbtm(ispl,itp,k)
  !         print *,'Nbtm,ispl,itp,k:',n,ispl,itp,k
          
           if (n .lt. Maxspl)  then
               !Update number of events in mag bin k, and time-window, itp
               n1                  = n + 1
               Nbtm(ispl,itp,k)    = n1
               SamplingFlag        = 1
               if (n1 .eq. Maxspl)  then
                   SamplingTime(itp,k) = time - time1
                end if
                
 !        print *,'Samplingflag,n1:',Samplingflag,n1
  !       if(n1.eq.Maxspl) then
   !          stop
    !     endif
           end if
               
        end if

        return

        end subroutine MagSampling1




  
        
        
