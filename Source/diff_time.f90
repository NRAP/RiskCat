      subroutine diff_time(tra,np,dt)

      IMPLICIT NONE
      INTEGER :: i, np
      REAL    :: diff, dt
      REAL, dimension(np+1) :: tra

! time domain differentiation
      do  i=1,np-1
         diff=(tra(i+1)-tra(i))/dt
         tra(i)=diff
      end do

      tra(np) = tra(np-1)
      return
      end
