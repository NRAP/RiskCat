!csssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
        subroutine  quadscale  (p,r,q1,q2,q1q2,m)
!
!sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
! Calculates the coordinates of nodes of segment Q1-Q2, obtained by
! moving the side of a quadrilateral in a ratio r.
!two cases: either horizontal translation    m=1
!		  or vertical translation		   m=2
!The sides of the quadrilateral do not have to be parallel

!						Q1
!			P1  o********o*******************o P4
!				*	    +				   *
!				*..r...+ 				 *
!				*     + 			   *
!				*    +			     *
!				*   +			   *
!			P2  o**o*************o P3
!				  Q2
!					
!			P1  o****************************o P4
!				*	  .  				   *
!				*     .r 				 *
!				*     .  			   *
!			Q1  o++++++++++++++++++++o Q2
!				*    			   *
!			P2  o****************o P3
!				  Q2

        implicit none 
        integer :: k,m                                     
        real ::  r,q1q2
        real ::  p(4,3), q1(3),q2(3)

    if (m .eq. 1)  then
	!Horizontal Translation of P1-P2 towards P4-P3, 
        do  k = 1,3
            q1(k) = (1-r)*p(1,k) + r*p(4,k)
            q2(k) = (1-r)*p(2,k) + r*p(3,k)
        end do

    else
	!Vertical translation of P1-P4 towards P2-P3
        do  k = 1,3
            q1(k) = (1-r)*p(1,k) + r*p(2,k)
            q2(k) = (1-r)*p(4,k) + r*p(3,k)
        end do 

    end if

        call dist (q1(1), q1(2), q2(1), q2(2), q1q2)

        q1q2 = sqrt (q1q2*q1q2 + (q1(3)-q2(3))**2)
!
        return
        end subroutine quadscale
