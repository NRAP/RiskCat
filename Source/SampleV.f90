!
!Program to sample uniformly an ordered set of N1 integer numbers
! among a set of N2 integer numbers
!                                SAMPLECat
!
!****************************************************************************
!
!
!*******************************************************************************
!
        SUBROUTINE SampleV ( n1, MaxSample )
!
!*******************************************************************************
!       
        USE GenParam
       
        implicit none

        CHARACTER (len=2),dimension(2):: ac
        INTEGER :: i, iteration, j, k, MaxSample
        INTEGER :: ns, ns1, ns2, nz1, nz2, n1, n2, n3, reverse

!*******************************************************************************
!
        IDSEED = 59237547
        ac(1)  = '.'
        ac(2)  = 'S'
        n2     = MaxSample


        !n1 = size of the set of ordered integers from which sampling is done
        !n2 = size of the sample set
        !Condition:  0 < n2 < n1


!If n2 > n1/2, then reverse by sampling n1-n2, and at the end reset values in V
        reverse = 0
        n3 = INT(REAL(n1)/2.)
        if (n2 .gt. n3)  then
            reverse = 1
            n2 = n1 - n2
        end if 

        !Load zeros in V and V1
        ALLOCATE ( V1(n1) )
        VZ = 0
        V1 = 0

!Generate first set of switches
        !Frst iteration
        iteration = 1
 
        !Generate as many as n2 switches, save for multiple selections,
        !ns  = actual number of switches (number of distinct samples) 
        !nz1 = remaining number of zeros in original array V1. nz=n1-ns
        !V1  = input array of zeros and output switches (Values>=0)
        call SampleV2 (ns, nz1, n1, n2, V1)
        !n3 = Number of distinct switching remaining to generate
        n3 = n2 - ns
        !Update switches in V
        Do i = 1, n1
            VZ(i) = MIN (V1(i), 1)
        end do
        DEALLOCATE ( V1 )

        do k = 1, 1000
            !Iterate until remaining number of switches to generate is zero
              
            !Print results
            !print *, "n1 =", n1
            !print *, "n2 =", n2
            !print *, "Number of iteration(s) =", iteration
            !print *, "ordered switches:"

            if (n3.eq. 0)  then  
               !Sampling is finished. n1 switches locations have been generated
               
               if (reverse .eq. 0) then
                  do i = 1, n1
                     if (VZ(i) .gt. 1)  then
                        VZ(i) = 1
                     end if
                  end do
                  !print *,'VZ(i)=', (VZ(i),i=1,n1)
                  !write (*,"(200a1)") (ac(VZ(i)+1),i=1,n1)
               
               else
                  !reverse = 1, V(:) is the complementary of the V(:)
                  ! determined above              
                  do i = 1, n1
                     if (VZ(i) .gt. 0)  then
                         VZ(i) = 0
                     else
                         VZ(i) = 1
                     end if
                  end do
                  !print *, "Reversed switches:"
                  !write (*,"(200a1)") (ac(VZ(i)+1),i=1,n1)
               end if

               exit

            else  
               !One more iteration:
               iteration = iteration + 1             
               !Less switches than n1. Iterate until n3 = 0
               !Create array V1 (dim=nz1)  of ordered integers
               ALLOCATE ( V1(nz1) )
               !Load zeros in V1
               V1 = 0
               !Generate as many as n3=n2-ns switches among nz1 blanks
               call SampleV2 (ns1, nz2, nz1, n3, V1)
               !Update number of distinct switches
               ns  = ns + ns1
               !Update number of remaining zeros in V
               nz1 = nz2
               !Update number of samples remaining to generate
               n3  = n2 - ns

               !Update vector of switches in V. 
               !Add n3 1's to the nz1 slots in V1
               ns2 = 0
               j   = 0
               do i = 1, n1
                  if (VZ(i) .eq. 0)  then
                     j = j + 1

                     if (V1(j) .gt. 0)  then
                         ns2 = ns2 + 1
                         VZ(i) = V1(j)
                     end if
                     if (ns2 .eq. ns1)  then
                         exit
                     end if

                  end if
               end do
               DEALLOCATE ( V1 )
            end if

        end do
        j = 0
        do i = 1, n1
            j = j + VZ(i)
        end do
        !print *,' Total number of Switches =',j
        !print *, '   '
!   
        END subroutine SampleV
!
