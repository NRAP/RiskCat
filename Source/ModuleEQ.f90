       MODULE ModuleEQ

!**********************************************************************

        use GenParam
        
!      parameters assignments for RiskNIEQ1: 

 !......Earthquakes:
        INTEGER, parameter :: MaxAttnMods   = 200
        INTEGER, parameter :: MaxAttnCoeff  = 28
        INTEGER, PARAMETER :: MaxbinCalib   = 1
        INTEGER, PARAMETER :: MaxCatStandard= 20 !Number of Standard Catalogs
        INTEGER, PARAMETER :: MaxEqMAPS     = 20
        INTEGER, PARAMETER :: MaxEpis       = 450
        INTEGER, PARAMETER :: Maxeqnodes    = 110
        INTEGER, PARAMETER :: MaxEQsources  = 10
        INTEGER, PARAMETER :: MaxEq         = 35000        
        INTEGER, parameter :: MaxFltseg     = 10
        INTEGER, PARAMETER :: MaxGMbins     = 100 !Dim of array GMarray 
        INTEGER, parameter :: MaxGMTypeMods = 5
        INTEGER, parameter :: MaxGMsrcType  = 5
        INTEGER, parameter :: MaxHazardVal  = 30
        INTEGER, PARAMETER :: Maxmag        = 50 
        INTEGER, PARAMETER :: Maxmagbins    = 50 
        INTEGER, parameter :: MaxMagrel     = 5
        INTEGER, parameter :: MaxNodesource = 40
        INTEGER, parameter :: MaxProbExceed = 9
        INTEGER, PARAMETER :: Maxreadnodes  = 110
        INTEGER, PARAMETER :: Maxsampling   = 5                                
        INTEGER, PARAMETER :: Maxseg        = 1                                
        INTEGER, PARAMETER :: MaxSeisINT    = 2
        INTEGER, parameter :: MaxSeisREAL   = 13
        INTEGER, parameter :: MaxSourceType = 2
        INTEGER, parameter :: MaxSubElt     = 100000
        INTEGER, parameter :: MaxWstrips    = 1

        !Hanks and Kanamori Magnitude Moment conversion coefficients
        !Base 10  M0 in dyne.cm
        REAL,    PARAMETER :: HK10c         = 16.1
        REAL,    PARAMETER :: HK10d         = 1.50
        !Base e   M0 in dyne.cm
        REAL,    PARAMETER :: HKec          = HK10c * alog(10.)
        REAL,    PARAMETER :: HKed          = HK10d * alog(10.)
        
        INTEGER :: iMsplStart,iMsplStop,iStd_Dieterich,iStd_Dieterich1
        INTEGER :: iplotrec,kZMap
        INTEGER :: NsimEMP,nGMmod,Nmagbins,Nmaps,nProbExc,Ns,UTM

        INTEGER, dimension(MaxNtp)       :: I1tp,I2tp
        INTEGER, dimension(Maxeqsources) :: iMagStart,iMagStop
        INTEGER, dimension(Maxeqsources) :: iplotrec1,iSourceGM
        INTEGER, dimension(MaxGMsrcType) :: nAttn
        INTEGER, dimension(MaxSampling)  :: NCatevents
        INTEGER, dimension(Maxeqsources) :: neqrates,neqseg
        INTEGER, dimension(MaxNodesource):: nodeID,scrnodnum
        INTEGER, dimension(MaxEQsources) :: nsegFault
        INTEGER, dimension(MaxEQsources) :: nwstrips
        INTEGER, dimension(MaxNtp)       :: Nonzerotp1
        INTEGER, dimension(MaxEQsources) :: SeisRectype
        INTEGER, dimension(MaxMagbins)   :: ScMag
        INTEGER, dimension(MaxEQsources) :: sourceType,XSimAttn
        INTEGER, dimension(MaxEQmaps)    :: nzMap
        
        INTEGER, dimension(MaxEQsources,MaxseisINT)   :: Brec,BrecRd,BrecSim
        INTEGER, dimension(MaxGMsrcType,MaxGMTypeMods):: iAttn
        INTEGER, dimension(MaxEQsources,MaxNodesource):: scrnodes
        
        real :: delM,GMend,GMstart
        REAL :: LongOriginCat,LatOriginCat
        REAL :: Meqmin,Meqmax,xMLTlow,xMLThigh,xnSIGMA,xVh,xVr
        
        real, dimension(MaxEQsources)   :: areaD1,areaD2,deldepth,RSQslip
        real, dimension(MaxMagbins)     :: CumLTS,CumPm,CUmPM2
        real, dimension(MaxGMsrctype)   :: defGMSIG
        REAL, dimension(Maxeqsources)   :: CDFbin,eqdepth,eqdip
        REAL, dimension(Maxeqsources)   :: eqrates
        real, dimension(MaxGMbins)      :: GMarray,GMarrayLog
        real, dimension(Maxmagbins)     :: dteq,LTSReg
        real, dimension(MaxProbExceed)  :: perc
        REAL, dimension(MaxProbExceed-4):: aperc
        real, dimension(Maxmagbins)     :: Pm
        real, dimension(2)              :: Porigin           
        real, dimension(Maxmagbins)     :: sigPM
        real, dimension(MaxNodesource)  :: snlat,snlong
        real, dimension(MaxEqsources)   :: Xgmxsig
        real, dimension(Maxmag)         :: xMag 
        real, dimension(Maxmagbins)     :: xMagbins,xMagLBnd,xMagHBnd 
        real, dimension(Maxmagbins+1)   :: VMagBnd
        real, dimension(MaxEQmaps)      :: wMap

        
        real, dimension(MaxEQsources,MaxseisREAL)  :: Arec,ArecRd
        real, dimension(MaxEQsources,MaxseisREAL)  :: ArecSim,ArecUpdt
        real, dimension(MaxAttnMods,MaxAttnCoeff)  :: Attn
        real, dimension(Maxmagbins,4)              :: Cum,CumYY
        real, dimension(MaxMagbins,3)              :: Pmperc
        REAL, dimension(Maxepis,MaxMagBins)        :: SimLTS
        real, dimension(MaxGMsrcType,MaxGMTypeMods):: wAttn
        real, dimension(Maxcritpt,MaxGMbins)       :: work4
        real, dimension(MaxEQsources,MaxWstrips)   :: Wtop,Wbot

        INTEGER, dimension(MaxSampling)            :: nRelax

        INTEGER, dimension(MaxSampling,MaxEq)      :: ICatEQ
        INTEGER, dimension(MaxEQmaps,MaxEQsources) :: MapZid
        INTEGER, dimension(MaxSampling,MaxNtp)     :: IendEq,IstartEq
        INTEGER, dimension(MaxSubElt)              :: SubEltNumber
        
        REAL, dimension(MaxEpis)                   :: Weightepi, wrsqepi
       
        REAL, dimension(MaxSampling,MaxEq)         :: CatEqMag,CatEqM0,CatEqtime 
        REAL, dimension(MaxEpis,MaxSampling)       :: WgtCat
        REAL, dimension(MaxSampling,MaxEq)         :: Xrsq,Yrsq,Zrsq
        REAL, dimension(MaxEQsources,MaxWstrips,MaxFltseg):: DIP,DIPRd
        REAL, dimension(MaxEQsources,MaxWstrips,MaxFltseg):: DIP1,DIP1Rd
        REAL, dimension(MaxEQsources,MaxWstrips,MaxFltseg):: DIP2,DIP2Rd
        REAL, dimension(MaxEQsources,MaxWstrips,MaxFltseg):: Rake,RakeRd
        REAL, dimension(MaxEQsources,MaxWstrips,MaxFltseg):: Rake1Rd,Rake2Rd
        REAL, dimension(MaxEq,4,3)                        :: SubEltCorner
        REAL, dimension(MaxSampling,MaxbinCalib,MaxNtp)   :: tpwin
        
        CHARACTER  (LEN=3) :: hazparam
        character (len=20) :: afile,catalogfile,GMfile,RSQSimIn
        CHARACTER (LEN=14) :: Plotscreen
        CHARACTER (LEN=20),dimension(MaxCatStandard)  :: catalogf
        CHARACTER (len=40),dimension(Maxeqsources)    :: eqname 
        CHARACTER (len=20),dimension(MaxEQsources)    :: sourceName
        
 !
 !Allocatables:

        INTEGER, ALLOCATABLE, dimension (:)      :: CatSubN
        INTEGER, ALLOCATABLE, dimension (:,:,:)  :: Nbtm
        INTEGER, ALLOCATABLE, dimension (:,:)    :: NEqtot
        INTEGER, ALLOCATABLE, dimension (:,:)    :: Nonzerotp2

        REAL, ALLOCATABLE, dimension(:,:,:,:)    :: Haz1
        REAL, ALLOCATABLE, dimension (:,:,:,:,:) :: Haz1SPEC
        REAL, ALLOCATABLE, dimension (:,:)       :: SamplingTime
        REAL, ALLOCATABLE, dimension (:)         :: Smag
        REAL, ALLOCATABLE, dimension (:,:)       :: wMagbin
        REAL, ALLOCATABLE, dimension (:,:,:)     :: work4SPEC 

        
 !******************************************************************************
 !
 !      DEFAULT AND TEMPORARY WORKING VALUES:
 !      ************************************* 
        integer :: defnsimrec = 1000 
        
        real    :: defM0      = 4.5 
        real    :: pp_theta   !Rotation to accomodate pp axis system 
                              !set in riskcat
							 
                                    
!**********************************************************************

      end MODULE ModuleEQ
