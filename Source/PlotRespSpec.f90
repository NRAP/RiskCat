#ifdef USE_DISLIN 
!****************************************************************************** 
!   
      SUBROUTINE PlotRespSpec (CTIT,Ix,IxSCL,Iy,NameLegends,Nc,Nf,XA,Y)
!
!******************************************************************************
!
!   Plots Hazards Nc Curves, the first one being the major curve
!
!       Plot is either displayed on screen or stored in file PLOTFILENAME
!       PLOTFILENAME must include the extension (i.e .pdf, .png, etc.)
!       Nview   = 1 Display on screen
!                 2 Stores graphics on .PNG, .PDF, .BMP, .WMF or .PS file
!       NOTE on stored files:
!       *********************
!       DISLIN allows creation of new pages only for PDF and PS formats
!       Therefore, for all other formats (PNG, BMP and WMF), a new file
!       is created for each new figure. The name of the file is then the
!       original Root name followed by the figure number, and the extension
!       CTIT    = Main figure title    
!       Ix      = Index for the X axis:(=1) Frequency (Hz)
!                                      (=2) Periods (s)
!       IxSCL   = Index for X-axis scaling:
!                                      (=1) X-axis is LINear
!                                      (=0) X-axis is LOGarithmic
!       Iy      = Index for the Y axis:(=1) Acceleration Response Spect. (cm/s/s)
!                                      (=2) Velocity Response Spectra (cm/s)
!       Nc      = Number of curves
!       Nf      = Max number of points on X axis
!       Nview   = Selects the screen or file format (See above note)
!       XA(j)  = Array of max dimension (NcMax) of X values for Nc curves
!       Y(i,j)  = Array of max dimension (Nf,NcMax) of Y values for Nc curves     
!                                      
      USE DISLIN
      IMPLICIT NONE
      
      INTEGER :: I,Ix,IxSCL,iY,j,k,kXmax,kXmin
      INTEGER :: Nc,Nf,Nnonzero,NWIDTH1,NX,NYA,NYLABEL
      integer :: pagenumber !Counter for the pages being plotted
      CHARACTER (LEN=256):: CBUF
      CHARACTER (LEN=13) :: CTIT
      CHARACTER (LEN=60) :: CTIT4
      CHARACTER (LEN=5), DIMENSION(3) :: CLAB = (/'LOG  ','FLOAT','ELOG '/)
      CHARACTER (LEN=7), DIMENSION(8) :: KOLOR
      CHARACTER (len=16),DIMENSION(Nc):: NameLegends
      CHARACTER (LEN=21),DIMENSION(2) :: TitleX
      CHARACTER (LEN=25),DIMENSION(2) :: TitleY
      REAL    :: Xmax,Xmax0,Xmax1,Xmin,Xmin0,Xmin1,XNX,Xstep
      REAL    :: Ymax,Ymax1,Ymin,Ymin1
      REAL    :: z1,z2
      REAL, DIMENSION(Nf)          :: XA,YA
      REAL, DIMENSION(Nf,Nc)       :: Y
!
      pagenumber = 1
      Nwidth1    = 15
      CTIT4      = '   '

      KOLOR(1)  = 'RED'
      KOLOR(3)  = 'GREEN'
      KOLOR(7)  = 'BLUE'
      KOLOR(4)  = 'CYAN'
      KOLOR(5)  = 'ORANGE'
      KOLOR(6)  = 'MAGENTA'
      KOLOR(2)  = 'WHITE'
      KOLOR(8)  = 'YELLOW'

      TitleX(1) = '   Frequency (Hz)    '
      TitleX(2) = '     Period (s)      '
      TitleY(1) = '  Acceleration (cm/s/s)  '
      TitleY(2) = '     Velocity (cm/s  )   '

!Determine the min and max of each axis
!   Both axes are logarithmic:
      Xmin0 = 1.e10
      Xmax0 = 0.
      Ymin = 1.
      Ymax = 0.

      do i = 1,Nf
        Xmin0 = AMIN1 (Xmin0,XA(i))
        Xmax0 = AMAX1 (Xmax0,XA(i))
        do j = 1,Nc
            !print *,'Y(i,j)',Y(i,j)
            if (Y(i,j).gt. 0.)  then
                Ymin = AMIN1 (Ymin,Y(i,j))
                Ymax = AMAX1 (Ymax,Y(i,j))
                kXmax = i
            end if
        end do

      end do

      kXmin = 1
      do i = Nf, 1, -1
           do j = 1,Nc
                if ((Y(i,j).gt. 0.) .and. (Y(i,j).lt.0.999999999)) then
                    kXmin = i
                end if
            end do
      end do

      if (kXmin .gt. 1)  kXmin = kXmin - 1
      
      Xmin0 = XA(kXmin)
      Xmax0 = XA(kXmax)

      if (IxSCL .eq. 1)  then
          !X-axis is LINear
          Xmin = 0.
          Xmax = Xmax0
      else
          !X-axis is LOGarithmic
          Xmin = ALOG10 (Xmin0)
          Xmax = ALOG10 (Xmax0)
      end if

      Xmin1= INT (Xmin)
      Xmax1= INT (Xmax)

      if (Xmin1 .ne. Xmin)  then
        if (Xmin.lt.0.) then
            Xmin = Xmin1 - 1
        end if            
      end if

      if (Xmax1 .ne. Xmax) then
        Xmax = Xmax1 + 1.
      end if 
      !Xmax = Xmax1
           
      Ymin = ALOG10 (Ymin)
      Ymax = ALOG10 (Ymax)
      Ymin1= INT (Ymin)
      Ymax1= INT (Ymax)

      if (Ymin1 .ne. Ymin) then
        Ymin = Ymin1 - 1.
      end if      
      if (Ymax1 .ne. Ymax) then
        if (Ymax.lt.0.) then
            Ymax = Ymax1
        else
            Ymax = Ymax1 + 1.
        end if
      end if
!--------------------------------------------------------------------------
!Plotting initializations:
        
      !Display on screen
      CALL METAFL('CONS') 
      CALL SCRMOD('REVERS')
      CALL SETPAG('DA4P')   !Portrait    
      CALL DISINI()
      CALL PAGERA
 
      CALL SETCLR (255)
      CALL PAGERA()
!Fonts
      CALL HWFONT()

      CALL COLOR('FORE')

!Define Axes 1
      CALL AXSLEN(1400,1700)
      CALL NAME(TitleX(iX),'X')
      CALL NAME(TitleY(iY),'Y')
      if (IxSCL .eq. 0)  then
          CALL AXSSCL('LOG','X')
        else
          CALL AXSSCL('LIN','X')
      end if 
      CALL AXSSCL('LOG','Y')

!Figure Title
      CALL TITLIN (CTIT,2)
      CALL TITLIN (CTIT4,4)

!Define Axes 2
        NYA=2400
        NYLABEL = 1000
        CALL LABDIG(-1,'XY')
        CALL AXSPOS(500,NYA)
        CALL TICPOS ('REVERS','XY')
       
        if (IxSCL .eq. 0)  then
            CALL LABELS(CLAB(1),'XY')
            CALL GRAF(Xmin,Xmax,Xmin,1.,Ymin,Ymax,Ymin,1.)
          else
            CALL LABELS(CLAB(2),'X')
            CALL LABELS(CLAB(1),'Y')
            z1 = INT(ALOG10(Xmax))
            z2 = 10.**z1
            Xstep = z2 / 2.
            if (z1 .gt. 5.)  then
            Xstep = z2
            end if
            CALL GRAF(Xmin,Xmax,Xmin,Xstep,Ymin,Ymax,Ymin,1.)
        end if
        CALL GRID (2,2)

          CALL HEIGHT(60)
          CALL TITLE()
          CALL HEIGHT(30)
          
 !Plot Nc RS Curves
 
 !      First curve in RED (for MEAN or MEDIAN)
        Nnonzero = 1
        do j = 1, Nc
            do i = 1,Nf
                 YA(i) = Y(i,j)
            end do
                k = 1 + modulo(j-1,8)
                CALL COLOR (KOLOR(k))
                CALL LINWID (NWIDTH1)
            if (j.GT.8) then 
                CALL DASHL    
            endif
!            print *,  ""
!            print *, '    i     XA(i)       YA(i)'
 !           do i = 1, Nf
  !              print *, i,XA(i),YA(i)
   !         end do
            CALL CURVE (XA,YA,Nf)
        end do
        CALL SOLID ()
        
!Skip Legends
   
!Legend statement
        CALL HEIGHT (40)
        CALL COLOR ('FORE')
        !Nc lines of Legend:
        CALL LEGINI(CBUF,Nc,20)   
!        NX = 1269
        XNX = Xmin + 0.4*(Xmax-Xmin)
        NX  = NXposn(10**XNX)
        do i = 1,Nc
            CALL LEGLIN (CBUF,NameLegends(i),i)
        end do
!        CALL LEGTIT (Legtitle)
        CALL LEGEND(CBUF,7)
        CALL LINWID (1)

        
      CALL ENDGRF()
             
      CALL DISFIN()
      

      RETURN
    
      END SUBROUTINE PlotRespSpec
#endif
