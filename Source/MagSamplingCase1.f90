!
!     Called by riskcat for case of PSHA zone-map generated catalogs: Case(1)
!
!*******************************************************************************
!
        subroutine MagSamplingCase1 ( ispl, MaxSample, nbin, Neq, Neq2, Ntp1, &
                                   unitcat1, unitcat2, xMagMax2, xMagMin2, xM0)
                                 
!
!*******************************************************************************
!    This routine reads the catalog and throws away events in a magnitude bin
!    that are beyond the number of MaxMagSampling.
!    Creates a new catalog with no more than MaxMagSampling events in each M-bin
!    Determines the number of events in each magnitude bin and calculates the relative
!    weight of the corresponding M-bin as:
!
!     wmagbin(i) = Ntotal(i) / Nsample(i)
!
!     where:    Ntotal(i) = Number of events in magnitude bin i from full catalog
!               Nsample(i)= Number of events retained in magnitude bin i
!
!    The weights are used later to reconstruct the final ground-motion histogram
!    before calculating the hazard.
!
!    Input: 
!           Catalog of events stored in temporary file CatTEMP1 as unit 17
!           ispl           = index of aleatory catalogs cycle
!           MaxMagSample   = max number of events in each magnitude sampling bin
!           nbin           = number of magnitude bins sampled
!           Neq2           = Number of events in the original catalog
!           Ntp1           = Number of time-windows
!           unitcat1       = unit number fororiginal catalog file "CatTEMP1"
!           unitcat2       = unit number for new culled catalog file "CatTEMP2"
!           xMagmax2       = Largest magnitude event in original catalg
!           xMagmin2       = Smallest magnitude event in original catalog
!           xM0            = minimum magnitude considered in the problem
!      
!    Output:
!           Culled Catalog stored in temporay file CatTEMP2 as unit 7
!           Neq            = Number of events in the culled catalog
!           Mag, Time, M0, X,Y,Z of each evnt in the culled catalog
!           wMagbin(i)     = Normalized weight of magnitude bin i.
!
!*******************************************************************************
!
        USE ModuleEq
        USE MOduleRSQSim
        USE GenParam


        IMPLICIT NONE

        !Declarations for RSQSim externally generated catalog
        CHARACTER (LEN=9) :: Xevent
        
        INTEGER :: i, ispl, iSource, itp
        INTEGER :: j, j1, k, MaxSample, nTMW, nTotal
        INTEGER :: n, nbin, Neq, Neq2, n1, Ntp1,unitcat1, unitcat2

        REAL    :: delem
        REAL    :: sectoyear, time, time1, time2
        REAL    :: xcretM0, xMagMin2, xMagMax2, xMSc, xM0, x1, x2, x3

        INTEGER, dimension(nbin,Ntp1)      :: nEqMTW, NevMag

        INTEGER,ALLOCATABLE,dimension(:,:)   :: VSDummy
        INTEGER,ALLOCATABLE,dimension(:,:,:) :: CatMap, CatSwitch

        !-------------------------------------------------------------------------------

        sectoyear= 3600. * 24. * 365.   ! Number of seconds in a year

        !Reset the number of events in each M-bin. in full un-culled catalog
        NevMag   = 0
        !Reset number of events in culled catalog
        Neq      = 0
        !Reset total number of events for culled catalog in time window itp
        nEqtot   = 0
        !Reset Number of event per Mag,Time-window bin for full catalog
        nEqMTW   = 0
        !Number of events with time <= end of last time-window in full catalog
        nTotal   = 0
        Xevent = 'Main '

!Redefine Magnitude bins for sampling:
!-------------------------------------
!       We want bins such that larger Mag events are definitely sampled
!       Smallest magnitude event in catalog is: MagMin2
!       Largest magnitude event is : MagMax2
!       Number of magnitude bins is nbin (nbin=10)
!                 starting magnitude in bin i is Smag(i)
!                 upper magnitude in bin i is Smag(i+1)
!                 therefore Snag is dim nbin+1
        Smag(1)      = xMagMin2
        Smag(nbin+1) = xMagMax2
        delem        = (xMagMax2-xMagMin2) / REAL(nbin)
        do i = 2, nbin
            Smag(i) = Smag(i-1) + delem
        end do
        print *,'  '
        print *,'Magnitude Sampling binning:'
        print *,'Bin NUmber  Min bound Mag    Max bound Mag'
        do i = 1, nbin
            print *, i,Smag(i), Smag(i+1)
        end do

        !Catalog of original events stored in temporary file CatTEMP1
        !as unit = unitcat1, unformatted
        rewind (unitcat1)


  
        !Bounds of Magnitudes in magnitudes sampling bin "i" are:
            ! Lower bound = Smag(i)
            ! Upper bound = Smag(i+1)  
        !Bounds of time for definition of Time-Window "itp":
            ! Lower bound = tlife(itp-1) (0. for first one)
            ! Upper bound = tlife(itp)

!Preliminary indexes mappings:
!****************************
!       Preliminary stuff
!       1. Count number of events in each bin (Magnitude,Time Window), 
!          before culling the catalog, and 
!       2. create mappimg of index in full catalog versus index in each bin

        ALLOCATE ( VSDummy(Neq2,3) )

        Preliminary: Do j = 1, Neq2

           !Retreive  info for this event j in full catalog
           read (unitcat1) time,xMSc,iSource,xcretM0

            !Which Mag bin does xMSc fall into?
            !Which time-window bin does this event fall into 
            call IndexMagTmw ( k, itp, xMSc, nbin, Ntp1, time)
            !Number of events in Mag-TWM bin for full un-culled catalog
            !NevMag(k,itp)  = NevMag(k,itp) +1

            if ( (time.le.tlife(Ntp1)) .and. (xMSc .ge. xM0) )  then

                !Update number of events in each bin (Mag,Time Window)
                NevMag(k,itp)  = NevMag(k,itp) +1
                !Update total number of events with time <= tlife(Ntp1)
                nTotal = nTotal + 1

                !Number of events , per itp and ispl: nEqtot(ispl,itp)
                !Number of events , per Mag and itp bin : nEqMTW(k,itp)
                nEqtot(ispl,itp) = nEqtot(ispl,itp) + 1
                nEqMTW(k,itp)    = nEqMTW(k,itp)    + 1
    
                !Set the time of observation equal to the length of the time-window
                !if  number of events sufficient to obtain statistical convergence
                !is reached, then set time to actual time of occurrence of event
                ! since start of time-window.
                time2  = tlife(1)
                time1  = 0.
                if (itp .gt. 1)  then
                    time2 = tlife(itp)
                    time1 = tlife(itp-1)
                end if

                !Load working array of mapping indexes:
                ! j = index of the event in the full catalog
                !VSDummy(j,1) = Magnitude sampling bin index of the event
                VSdummy(j,1) = k
                !VSDummy(j,2) = Time window index of the event
                VSdummy(j,2) = itp
                !VSDummy(j,3) = Index of event for only those in (k,itp) bin
                n            = nEqMTW(k,itp)
                VSdummy(j,3) = n

            end if

        end do Preliminary


        !Determine nTMW: largest number of events in any mag-TW bin
        nTMW = 0
        print *, '  '
        !print *, 'Number of events per each cell (Mag,TWM):'
        do  k = 1, nbin
            !print *, 'k=',k,('  itp=',is,'  N(k,itp)=',nEqMTW(k,is),is=1,Ntp1)
            do itp = 1, Ntp1
                if (nEqMTW(k,itp) .gt. nTMW)  then
                    nTMW = nEqMTW(k,itp)
                end if
            end do
        end do

        ALLOCATE ( CatMap(nbin,Ntp1,nTMW) )

        !Move info from VSDummy to CatMap()
        !CatMap() then contains mapping between an event in (mag,TW,eq) bin to 
        !original event in full catalog
        do j = 1, Neq2
            k   = VSDummy(j,1)
            itp = VSDummy(j,2)
            n   = VSDummy(j,3)
            if (n .eq. 0) then
                n = 1
            end if
            CatMap(k,itp,n) = j
        end do      

        deallocate ( VSDummy)
 
        !Create the array of switches for each mag-TW bin
        ALLOCATE ( CatSwitch(nbin,Ntp1,Neq2) )
        CatSwitch = 0
        do  k = 1, nbin
            do itp = 1, Ntp1
                !Number of events in bin
                n1 = nEqMTW(k,itp)
                if ( (n1.gt.MaxSample) .and. (n1.gt.0) )  then

!**************************************************************************
                    !Print info for debugging:
 !                   print *,'   '
  !                  print *,'Mag bins =',nbin,'  Time-windows=',Ntp1
   !                 print *,' Magg bin:',k,'   Time-Window bin:',itp
    !                print *,' Events in cell:',n1,'   MaxSample=',MaxSample
!***************************************************************************

                    !Max number of samples is MaxSample
                    ALLOCATE ( VZ(n1) )
                    call SampleV (n1, MaxSample)
                    !Load switches in CatSwitch
                    do j1 = 1,n1
                        j = CatMap(k,itp,j1)
                        CatSwitch(k,itp,j) = VZ(j1)
                    end do
                    deallocate ( VZ )
                elseif ((n1.gt.0) .and. (n1.lt.MaxSample))  then
                    !Less events in M-TW bin than MaxSample: Switch is ON
                    do j1 = 1, n1
                        j = CatMap(k,itp,j1)
                        CatSwitch(k,itp,j) = 1
                    end do 
                End if
            end do
        end do

        deallocate ( CatMap )
        
 

        !Loop over all events in original catalog
        rewind (unitcat1)
        rewind (unitcat2)
        Do j = 1, Neq2         

            !Read event info from full catalog
            read (unitcat1) time,xMSc,iSource,xcretM0

            !Determine which bin of mag and Time-window this event belongs to:
            call IndexMagTmw ( k, itp, xMSc, nbin, Ntp1, time)

            !Recover the switch status for this event:
            n1 = CatSwitch(k,itp,j)

            !Determine if event will be retained in culled catalog "CatTEMP2"
            !To be part of the newly created culled catalog the conditions are:
            ! time <= end of last time-window
            ! Magnitude <= minimum magnitude considered in the analysis: xM0
            ! Switch ON from the sampling switches, (n1=1)
            
            if ((time.le.tlife(Ntp1)) .and.  &
                (xMSc.ge.xM0)         .and.  &
                (n1 .gt. 0)                  )  then    
                !Update number of events in culled catalog
                Neq = Neq + 1
                write (unitcat2) time, xMSc, iSource, xcretM0                
            end if

        end do

        DEALLOCATE ( CatSwitch )
        close (unitcat1)

!       Calculate weights
        do j = 1, Ntp1
            do i = 1, nbin
                x1 = REAL (nEqMTW(i,j))
                x2 = REAL (MaxSample)
                x3 = 1.
                if ((x1.ge.MaxSample) .and. (x2.gt.0.)) then                
                    x3 =  x1 / x2 
                end if
                wMagbin(i,j) = x3
            end do
        end do

!        print *, 'Min Mag   :', xMagMin2
 !       print *, 'Max Mag   :', xMagMax2
  !      print *, 'Mag bins  :', (Smag(i),i=1,nbin+1)
   !     do j = 1, Ntp1
    !        print *, 'N/Mag Bin:', (NevMag(i,j),i=1,nbin)
     !       print *, 'Weights  :', (wMagbin(i,j),i=1,nbin)
      !  end do


        return

        end subroutine MagSamplingCase1




  
        
        
