!
!........This routine reads the information on critical facilities, such as NPP
!
!        Called by "simulperil"
!
!*******************************************************************************
!
        subroutine InCritical (Infile, Nsites, unit1)
        
        use CritFacilities
        use CritNodes
        use ModuleEq
        
        implicit none
        
        integer :: i, IERR, j, Nexpected, Nsites, Nstring, unit1
        
        INTEGER, DIMENSION(80) ::Ndumread
              
        character (len=20) :: Infile, string1, string2
        
        CHARACTER (LEN=80),DIMENSION(80) :: DUMREAD

!
!*******************************************************************************

        open (unit=unit1,file=Infile,status="old",iostat=IERR)
        call CantFindFile (Infile,IERR)

        rewind (unit1)
        
!.......Position the record reading location in the file to start after the 
!       string: "POrigin-Sites-Coord", in Menu file
        string1 = 'POrigin-Sites-Coord' 
        do i = 1,100000
            read (unit1,*)  string2
            if (string1 .eq. string2) exit 
        end do

!.......Read coordinates of Point of Origin:  Long, Lat (Dec. degrees)
!       The point of origin is origin of the coordinate system in which
!       the x-y coordinates of the sub-elements are given
!       if the coordinates are given in UTM (meters) then they are transformed
!       into Long, Lat (x,y).
!       The switch is parameter "UTM"
!       UTM = 0 no transformation, coordintaes entered are in Long-Lat degree
!             1 x-y coordintes entered are in meters, from Point of origin
        Read (unit1,*) string2  !Line of explanations. Not used in code
        Read (unit1,*) (Porigin(i), i=1,2),UTM   
     
!.......Read Name of sites and coordinates
        !Number of sites
        read (unit1,*) Nsites
        !^^^^^^^^^^^^^^^^^^^^
        if (Nsites .le. 0)  then
            close (unit1)
            return
        end if
        
        do i = 1, Nsites
        
            read (unit1,2) SiteName(i)
            !^^^^^^^^^^^^^^^^^^^^^^^^^
            
            !READ (Xsites, Ysite)
            !^^^^^^^^^^^^^^^^^^^^
            call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
            Nexpected = 2
            if (Nexpected .ne. Nstring)  then
                write (*,*) 'ERROR coordinates for site:',i,SiteName(i)
                Write (*,*) 'Values read:'
                do j = 1, Nstring
                    write (*,*) DUMREAD(j)
                end do
                write (*,*) 'STOP Execution, Check Inputs in MenuIn...'
                STOP
            end if
            
            read (DUMREAD(1),*,ERR=500) Xsites(i)!Longitude in decimal degrees
            read (DUMREAD(2),*,ERR=500) Ysites(i)!Latitude
            
            go to 600
500         write (*,*) 'WRONG format coodinates site:',SiteName(i)
            write (*,*) 'Values read:'
            do j = 1, Nstring
                write (*,*) DUMREAD(j)
            end do
            write (*,*) 'STOP Execution, Check Inputs in MenuIn...'
            STOP
600      end do

        close (unit1)
        
2       format (a20)
        
        return
        
        end subroutine Incritical
        
