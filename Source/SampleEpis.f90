!
! This subroutine is called by simulperil
!
!*******************************************************************************
!
        subroutine SampleEpis ( iflt,nj,nk )
!
!*******************************************************************************
!
!This routine develops the sample set of epistemic random variables:
!        it samples the epistemic uncertainty space of:
!            * the faults' geometry
!            * the option parameters used in RSQSim
!
!
!Faults geometry and seismicity:
!********************************
!Takes input parameters from input file *sources.txt:
!        Dip, Dip1, Dip2 (iflt,j,k) iflt=fault,j=depth strip,k=segment
!        Rake, Rake1, Rake2(iflt,j,k)
!        Arec(iflt,l)               l=1-10
!        Brec(iflt,n)               n=1-2
!        Input:  DipRd,Dip1Rd,Dip2Rd,ArecRd,BrecRd,RakeRd, 
!        ------  (remain unchanged by routine)
!
!        Output: Dip, Dip1, Dip2, Arec, Brec, Rake,RSQslip 
!        ------- (one simulated sample of params)
!
!RSQSim Option parameters:
!*************************
!        fA with uncertainty range [fAMin, fAMax]
!        stressOvershootFactor range [stressOvershootFactorMin, ....Max]
!
!-------------------------------------------------------------------------------
!
        use ModuleEQ
        use ModuleRSQSim

        implicit none

        INTEGER :: Distribtype,i,iflt,j,k,nj,nk

        REAL    :: r, rnunf, xb, xs, x1, x2



!-------------------------------------------------------------------------------


             !Input main variables: (REAL)
             !---------------------
             !i = fault index

             !Arec(i,1) = MLam Magnitude for which Lambda is given 
             !Arec(i,2) = MG (Center of Gravity of seis data, if known, or 0)
             !Arec(i,3) = Lambda at MLam (annual rate of ocurrence)
             !Arec(i,4) = Beta (Slope of G-R in natural log)
             !Arec(i,5) = Std dev on log10 of Lambda
             !Arec(i,6) = Std dev on Beta
             !Arec(i,7) = Best Estimate of Max Magnitude for the source
             !Arec(i,8) = Lower bound MaxMag. If 0, use  + or - delM
             !Arec(i,9) = Upper bound MaxMag. If 0, use  + or - delM
             !Arec(i,10)= delM, uncertainty on Magmax
             !Arec(i,11)= Slip, lower bound (mm/yr)
             !Arec(i,12)= Slip, Best Estimate (B.E.)/Mode (mm/yr)
             !Arec(i,13)= Slip, Upper bound (mm/yr)

             !Variables for uncertainty description: (REAL)
             !--------------------------------------
             !Brec(i,j)= Type of probability distribution
             !           1.: Uniform between MmaxLower and MmaxUpper
             !           2.: Triangular. 0 at Mlower, Mode at Mbest, 0 at Mupper
             !           3.: Trapeze. Uniform on [Mlower-Mbest], 0 at Mupper
             !           4.: Trapeze. 0 at Mlow, uniform on [Mbest-Mupper]
             !           5.: Gaussian, limited by +/- 1-sigma (70%)
             !           6.: Gaussian, limited by +/- 2-sigma (90%)
             !           7.: Gaussian, limited by +/- 3-sigma (98%)
             !          -n.: Discrete, where n is the number of points (n<=10)
             !           ----------------------------------------------------
             !           j = 1 for Max Magnitude distribution
             !               2 for slip distribution
!
!-------------------------------------------------------------------------------

        do i = 1, MaxseisREAL
            Arec(iflt,i) =ArecRd(iflt,i)
        end do

        do i = 1, MaxseisINT
            Brec(iflt,i) =BrecRd(iflt,i)
        end do

        do j = 1,nj
            do k = 1, nk

                !Dip
                !Default distributions type Uniform
                Distribtype = 1
                xb = DipRd(iflt,j,k)
                x1 = Dip1Rd(iflt,j,k)
                x2 = Dip2Rd(iflt,j,k)
                r  = rnunf()
                call DistribSampling (Distribtype, r, xb, xs, x1, x2)
                Dip(iflt,j,k)  = xs

                !Rake
                !Default distributions type Uniform
                Distribtype = 1
                xb = RakeRd(iflt,j,k)
                x1 = Rake1Rd(iflt,j,k)
                x2 = Rake2Rd(iflt,j,k)
                r  = rnunf()
                call DistribSampling (Distribtype, r, xb, xs, x1, x2)
                Rake(iflt,j,k) = xs

            end do
        end do

        !Slip 
        distribtype = BrecRd(iflt,2) 
        x1 = ArecRd(iflt,11)
        xb = ArecRd(iflt,12)
        x2 = ArecRd(iflt,13) 
        call DistribSampling (Distribtype, r, xb, xs, x1, x2)  
        RSQslip(iflt) = xs

        return

        END subroutine SampleEpis
