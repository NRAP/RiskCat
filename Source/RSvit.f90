! Calculates the velocity Response Spectrum for a Velocity time-series
!
!********************************************************************************
!
        subroutine RSvit (dzeta1, freq, Ndata, nfreq, PARS, ts1, vit1)
!
!********************************************************************************
!
! Input:
!       dzeta : Reduced damping factor. Ex: dzeta = 0.05 = 5% Damping of 1 DOF
!       Freq  : Array of frequencies for which  spectra are calculated (Hz)
!       Ndata : Number of data points in input velocity time-history
!       Nfreq : Dimension of array Freq
!       ts    : time step (sec.) of velocity time-series
!       vit   : Array of dimension Ndata, of velocity values (cm/sec)
!
! Output:
!       PARS  ; Pseudo-Acceleration-Response-Spectra array of dimension Nfreq
!               (cm/sec/sec)
!
!********************************************************************************
 
        IMPLICIT NONE

        INTEGER :: i, j, Ndata, Nfreq
        REAL    :: dzeta1, ts1
        REAL, dimension(Nfreq) :: freq, PARS
        REAL, dimension(Ndata) :: vit1

        REAL (KIND=8) ::dzeta, F0,  PGV, ts
        REAL (KIND=8), dimension(Ndata) :: vit


        dzeta = DBLE (dzeta1) 
        ts    = DBLE(ts1)
        do j  = 1, Ndata
            vit(j) = DBLE(vit1(j))
        end do

        do i = 1, Nfreq
            F0 = DBLE (freq(i))
            call OSCIL (dzeta, F0, Ndata, PGV, ts, vit)
            PARS(i) = REAL ( PGV )
        end do

        return

        end subroutine RSvit
!
!*******************************************************************************
  
    !*************************************************************
    !* This subroutine calculates the acceleration of the sismic *
    !* mass of an elementary oscillator (1 degree of freedom),   *
    !* the basis of which is submitted to a given speed, VIT(t). *
    !* The input signal VIT is digitalized with a constant time  *
    !* step, TS. The table VIT(I) contains NDATA speed values.   *
    !* --------------------------------------------------------- *
    !* INPUTS:                                                   *
    !*         FREQU........: eigen frequency of oscillator      *
    !*         VIT..........: input speed signal of basis VIT(t) *
    !*         TS...........: time step of signal (constant)     *
    !*         DZETA........: reduced damping factor             *
    !*         NDATA........: number of points of signal         *
    !* OUTPUT:
    !*         PGA..........: Peak of the response acceleration  *
    !*                        for the 1 DOF freqency of F0       *
    !*                        (Added JBS 4/14/14)                *        
    !*         ACC..........: (Not passed in call statement)     *
    !*                        table containing acceleration res- *
    !*                        ponse of oscillator (NDATA points) *
    !*                                                           *
    !*                         F90 Version By J-P Moreau, Paris. *
    !*************************************************************                                                                                 
      SUBROUTINE OSCIL(DZETA,FREQU,NDATA,PGV,T,VIT)  

      IMPLICIT NONE

      INTEGER       :: I, Ndata
      REAL (KIND=8) :: ARG, COSARG, DELTA, DZETA, EXPA, FREQU 
      REAL (KIND=8) :: GOMEGA, OMEGA, OMEGA2, PI, PGV
      REAL (KIND=8) :: Q0, Q1, Q2, QSI, R0, R1, R2, R3, SINARG, T 
      REAL (KIND=8) :: XPN, XPNP1, YPN, YPNP1, YPPN, YPPNP1
      REAL (KIND=8),   dimension(Ndata) :: ACC, VIT
                          
                                        
      PI=4.D+00*DATAN(1.D+00)                                                   
      OMEGA=2.D+00*PI*FREQU                                                     
      OMEGA2=OMEGA*OMEGA                                                        
      DELTA=DSQRT(1.D+00-DZETA*DZETA)                                           
      EXPA=DEXP(-OMEGA*DZETA*T)                                                 
      GOMEGA=OMEGA*DELTA                                                        
      ARG=GOMEGA*T                                                              
      SINARG=DSIN(ARG)                                                          
      COSARG=DCOS(ARG)                                                          
      QSI=DZETA/DELTA                                                           
      Q0=(COSARG-QSI*SINARG)*EXPA                                               
      Q1=OMEGA2*SINARG/GOMEGA*EXPA                                              
      Q2=(1.D+00+(QSI*SINARG-COSARG)*EXPA)/T                                    
      R1=SINARG/GOMEGA*EXPA                                                     
      R0=COSARG*EXPA+DZETA*OMEGA*R1                                             
      R2=(1.D+00-DZETA*OMEGA*T)*R1/T-COSARG*EXPA                                
      R3=1.D+00-R1/T                                                            
      XPNP1=VIT(1)                                                              
      YPPNP1=0.D+00                                                             
      YPNP1=0.D+00                                                              
      ACC(1)=Q2*XPNP1                                                           
        
      PGV = DABS( ACC(1) )

      DO I=2,NDATA                                                           
        YPN=YPNP1                                                                 
        YPPN=YPPNP1                                                               
        XPN=XPNP1                                                                 
        XPNP1=VIT(I)                                                              
        YPPNP1=Q0*YPPN+Q1*(XPN-YPN)+Q2*(XPNP1-XPN)                                
        ACC(I)=YPPNP1    
        if (DABS(ACC(I)) .gt. PGV)  then
            PGV = ACC(I) 
        end if
        YPNP1=R0*YPN+R1*YPPN+R2*XPN+R3*XPNP1                                      
      ENDDO  

                                                                        
      RETURN
                                                                    
      END subroutine OSCIL           

