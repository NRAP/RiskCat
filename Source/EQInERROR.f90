!
!.....This routine writes a message indicating the type and location
!     of input errors for the Earthquake sources input

        subroutine EQinERROR (DUMREAD,i,Message, Nexpected, &
                   Nread, SourceName)
        
        implicit none
        
        integer i, j, Nexpected, Nread
        
        character (len=40) :: Message
        character (len=20) :: SourceName
        character (len=80), dimension(80) :: DUMREAD
        
        Write (*,*) 'ERROR Source :',i,':',SourceName
        write (*,*) 'ERROR reading parameters :', Message
        write (*,*) 'Number of values expected:', Nexpected
        write (*,*) 'Number of values read    :', Nread
        write (*,*) 'Parameter values that were read:'
        do j = 1,Nread
        write (*,"(a80)") DUMREAD(j)
        end do
 
        write (*,*) '..............STOP Execution'
        STOP
        
        return 
        end subroutine EQinERROR
