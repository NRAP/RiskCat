!
!        This routine calculates the total seismic moment release for one source
!        for  'Years'
!
!*******************************************************************************
!
         subroutine BudgetM0Stationary    &
         ( c,d,isc,irec,Momentsum,xMagMax2,xMagMin2,xM0,Years )
!
!*******************************************************************************
!
! INPUT:
!-------
!        alfa      = a of G-R reccurence vurve alfa = Ln[ N[E>=xM0]/year }
!        beta      = b coefficient of G-R reccurence curve in natural log
!        isc       = source index
!        irec      = ID of type of reccurenc curve for the source
!                    Truc. Exp. irec = 1
!                    Characteristic    2
!                    Segmented Foxall  3
!                    Input by points   4
!        lamda0    = annual number of events greater than xM0
!        Trelax    = time (years)  begins long-term, perturbation free period
!        xM0       = Minimum magnitude
!        xMagMin2,xMagMax2 = Range of magnitudes for which the budget is needed
!                    if either value is zero, it is set to the properties of
!                    the source:
!                    if xMagMin2 = 0 then min mag range is xM0
!                    if xMagMax2 = 0 then max mag range is xMax
!        xMmax     = Maximum magnitude of the distribution for the source 
!        Years     = Total lentgh of the catalog.
!
! OUTPUT:
!--------
!        Momentsum = Cumulative annual moment release for the source for 
!                    the periods of 'Years'.
!-------------------------------------------------------------------------------
!
         use ModuleEq

         implicit none

         INTEGER :: irec,isc

         REAL    :: alfa,beta,c,d,Dm,K,lamda,lamda0
         REAL    :: Momentsum,x,xMagMax2,xMagMin2,xM0,xMmax,xMmin,Years

!-------------------------------------------------------------------------------

         SELECT CASE (irec)
         
             CASE (1)
                   !Mmax. 
                   xMmax  = Arec(isc,7)
                   if (xMagMax2 .gt. 0.)  then
                       xMmax = xMagMax2
                   end if

                   !xMin.
                   xMmin = xM0
                   if (xMagMin2 .gt. 0.)  then
                       xMmin = xMagMin2
                   end if
                   
                   !beta.
                   beta   = Arec(isc,4)

                   !lamda (at MLam)
                   lamda  = Arec(isc,3)
                   !alfa.
                   alfa   = alog(lamda) + beta*Arec(isc,1)

                   !lamda0 (at xM0)
                   lamda0 = alfa - beta*xM0
                   lamda0 = exp ( lamda0 ) 

                   !Total M0 release annually
                   Dm = xMmax - xMmin
                   K  = 1. / (1. - exp(-beta*Dm))
                   x  = lamda0 * beta * exp(c+d*xM0) * (exp((d-beta)*Dm)-1.)
                   Momentsum = x / ((1.-exp(-beta*Dm))*(d-beta))
                   Momentsum = Momentsum * Years
                   
              !Characteristic model (Youngs-Coppersmith)
              CASE (2)
                   
              !Segmented fault model (Foxall Model)
              CASE (3)

              !Empirical model manually entered
              CASE (4)

              CASE DEFAULT

         END SELECT

         return

         end subroutine BudgetM0Stationary
