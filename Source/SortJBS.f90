!
! This routine is for sorting arrays
!
! JBS 7/20/12
!
! input:
!*******
!       V()  = array to be sorted, real numbers
!              array V is unchanged by routine
!       n    = number of elements in array V, integer number

!Output:
!*******
!       ir() = array index mapping. Gives the location (index) 
!              of ranked number in original array V
!       V1() = sorted array [V1(i) = V(ir(i))]
!
        subroutine SortJBS (ir, n, V, V1)
!
        implicit none
        integer :: i, isn, j1, j2, k, kmin, kmax, n, n2
        integer, dimension(n) :: ir, ir2
		
        real vmax, vmin
        real, dimension(n) :: V, V1
!
!******************************************************************************

        !Initialize ir() and ir2()
        do i = 1,n
            ir(i)  = 0
            ir2(i) = 0
        end do
		
	!Is n odd or even?
        isn = n - (n/2)*2
	!if isn =1, n is odd
	!n2 is number of loops for extracting min and max in each cycle
        if (isn .eq. 1)  then
            n2 = (n+1) / 2
        else
            n2 = n / 2
        end if
!	print *, '*************************************  n2=', n2
	
	!Perform sorting looping over n2 cycles 
	!Extract Min and Max of remaining part of array V, in each cycle
        do i = 1, n2
            j1 = i 
            j2 = n - i + 1
            vmax = -1.e20
            vmin =  1.e20
            kmin = j1
            kmax = j2
		
            do k = 1, n
                if ((ir2(k).eq.0) .and. (V(k).lt.vmin))  then
                    vmin   = V(k)
                    kmin   = k
                end if

                if ((ir2(k).eq.0).and.(V(k).gt.vmax)) then
                    vmax   = V(k)
                    kmax   = k
                end if
            end do

            ir(j1)    = kmin
            ir(j2)    = kmax
            ir2(kmin) = 1
            ir2(kmax) = 1
			
            V1(j1) = V(kmin)
            V1(j2) = V(kmax)
         end do
		
        return
		
        end subroutine SortJBS
		
