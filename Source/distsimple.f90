!csssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
        subroutine  distsimple (x1,y1,x2,y2,d)
!
!sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
!

        implicit none
        real :: ab,d,del,r,radeg,sindel
        real :: xa,xb,x1,x2,ya,yb,y1,y2,za,zb                          
!
!	Simple Method with spherical coordinates. Assumes a constant average 
!	radius for the earth of 6371.2 km

        r = 6371.2                               ! Earth radius in km
        radeg = 57.2957795                       ! Conversion degree-radian
!       Point A
        xa = r * cos(x1/radeg) * cos(y1/radeg)
        ya = r * sin(x1/radeg) * cos(y1/radeg)
        za = r * sin(y1/radeg)
!       Point B
        xb = r * cos(x2/radeg) * cos(y2/radeg)
        yb = r * sin(x2/radeg) * cos(y2/radeg)
        zb = r * sin(y2/radeg)
!       Straight line distance between A and B
        ab = sqrt((xa-xb)**2+(ya-yb)**2+(za-zb)**2)
!       Half the angle AOB
        sindel = 0.5 * ab / r
        del = asin (sindel)                      ! Angle in radians
!       Length of the arc AB.
        ab = 2. * r * del
        d = ab
!
!
        return

        end subroutine distsimple
