!       This routine is called from Riskcat
!
!*******************************************************************************
!
        SUBROUTINE Scaled_Catalog ( Flag_Mag_Moment, ispl, MaxEqINPUT,      &
                                    MinMag, Nev1, Nev2, scale,              &
                                    xMagMax, xMagMin, Years )
!*******************************************************************************

!       Generate an array of indexes mapping the original order of the events
!       in the full caqtalog, to the sequencial order of events in the new
!       cropped catalog with scaled magnitudes and moments.
!       The cropping is performed on the scaled magnitude to retain only events 
!       greater than magnitude MinMag and maximum number of events MaxEqINPUT.
!
!       Input:
!       ------
!       Full Catalog of events, including:
!           -  Flag_Mag_Moment = 1 XX is array of Magnitudes
!                                2 XX is array of Seismic Moments
!           -  ispl       = Aleatory sample index ispl
!           -  MaxEqINPUT = Maximum number of events to retain.
!           -  MinMag     = Minimum magnitude of interest
!           -  Nev1       = Number of events in the original full catalog 
!           -  scale      = scaling factorof Moment
!           -  XMagMax    = Max magnitude in cropped catalog
!           -  xMagMin    = Min magnitude in cropped catalog
!           -  XX         = Array of unscaled Magnitudes if available, or
!           -  XX         = Array of unscaled Seismic Moments, if available
!
!       Output:
!       -------
!       Cropped and scaled catalog, including:
!           -  Nev2       = Number of events in cropped and scaled catalog
!           -  NewCrop    = Sequential Array of indexes of events in the
!                           original catalog
!                           ID = NewCrop(i) 
!                           means ID is index in original catalog of i-th 
!                           event in new.
!
!********************************************************************************      
   
        use ModuleEq

        implicit none

        INTEGER :: Flag_Mag_Moment, i, ispl, ixM0, j, Nev1, Nev2, MaxEqINPUT

        REAL    :: MinMag, scale, xm, xMagMax, xMagMin, xMom, Years

        INTEGER, allocatable, dimension( : ) :: ID_NewCrop

        REAL,    allocatable, dimension( : ) :: TT, XX

!******************************************************************************* 

        ALLOCATE ( ID_NewCrop(Nev1), TT(Nev1), XX(Nev1) )

        ixM0 = 0
        xMagMax = -1000.
        xMagMin =  1000.

        Do i = 1, Nev1 

            !Scaling
            if (Flag_Mag_Moment .eq. 1)  then
                !Array is in Magnitude
                xm   = CatEqMag(ispl,i)
            else
                xmom = CatEqM0(ispl,i) * scale ! in dynes
                xm   = .666*log10(xmom) - 10.7
            end if

            !Load array of scaled magnitudes into XX, and time into TT
            xm    = xm +  0.666*log10(scale)
            XX(i) = xm 
            TT(i) = CatEqTime(ispl,i)

        end do     
 

        !Perform filtering (Magnitude, number of years, and max number of events)
        ixM0 = 0
        print *,'  '
        print *,'In Routine Scaled_Catalog:'
        print *,'Nev1 =',nev1
        do i = 1, Nev1

            if ((TT(i) .gt. Years)  .or. (ixM0  .ge. MaxEqINPUT))  then
                exit
            end if

            if (XX(i) .ge. MinMag) then
                ixM0 = ixM0 + 1
                ID_NewCrop(ixM0) = i

                !Min and Max magnitudes in cropped catalog
                xm = XX(i)
                if (xm .gt. xMagMax)  then
                    xMagMax = xm
                end if
                if (xm .lt. xMagMin)  then
                    xMagMin = xm
                end if
            end if
        end do

        !Number of events in cropped catalog
        Nev2 = ixM0

        !Re-load original arrays with new sorted catalog
        do i = 1, Nev2
            !j is Index in original catalog
            j = ID_NewCrop(i)
            NCatevents(ispl)  =  Nev2 
            CatEqMag(ispl,i)  =  CatEqMag(ispl,j)
            CatEqM0(ispl,i)   =  CatEqM0(ispl,j)
            CatEqTime(ispl,i) =  CatEqTime(ispl,j)
            if (istd_Dieterich .eq. 3)  then
                Xrsq(ispl,i)  =  Xrsq(ispl,j)
                Yrsq(ispl,i)  =  Yrsq(ispl,j)       
                Zrsq(ispl,i)  =  Zrsq(ispl,j)             
            end if
        end do

        deallocate ( ID_NewCrop, TT, XX )

        return

        END SUBROUTINE Scaled_Catalog
                



