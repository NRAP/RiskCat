!
!........This module defines the general parameters for running RSQSim
!
        Module ModuleRSQSim
        
        implicit none

!Declarations for info needed to load RSQSim externally generated catalogs
        integer :: Nrsqcat, Nrsqepis, Nrsqrand
        character (len=10)  :: RSQcatROOT, RSQfltROOT
        character (len=14)  :: stringcat, stringflt

        character (len=14), allocatable, dimension(:) :: Catfile, Faultfile

        REAL   , ALLOCATABLE, dimension (:,:) :: XYZsub     

        end Module ModuleRSQSim
