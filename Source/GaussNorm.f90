!
!*******************************************************************************
!
subroutine GaussNorm (x2, p2)
!
!*******************************************************************************

! Normal Gaussian of x2
! Gives the CDF of Normal Gaussian x2
! gives p2 = 0.5 for x2 = 0.

implicit none

real (kind = 4) x2, p2
real (kind = 8) erfc2, p, sqrt2, x , x1

x = real ( x2, kind = 8 )
sqrt2 = SQRT ( 2. )

x1 = x / sqrt2
p = 1. - (0.5*erfc2( x1 ))

p2 = real ( p, kind = 4 )

return
end subroutine GaussNorm
