!
!..........This routine selects a location for an earthquake to occurr, 
!          random within an Area (or Volume) source zone.
!          It assumes a constant probability through the entire zone.
!
!          This routine is called by: routines EQselect and from GMHR2
!
!*******************************************************************************
!
        subroutine AreaEQselect (iSource,nodes,SimDepth,SimLat,SimLong)
        
        use ModuleEQ
        use GENparam
        
        implicit none
        
        integer :: INOUT, iSource, j, n1, nodes
        
        real :: D1, D2, delX, delY
        real :: rnunf, rx, ry, SimDepth, SimLat, SimLong
        real :: Xmax, Xmin, Ymax, Ymin
        
        real, dimension(nodes) :: x, y
        
!*******************************************************************************
        
!.......Get coordinates of Polygon / Volume
        D1    = areaD1(isource)
        D2    = areaD2(isource)
        do j = 1, nodes
            n1   = scrnodes(isource,j)
            x(j) = snlong(n1)
            y(j) = snlat(n1)
        end do

!.......Get coordinates of box around the polygon: Xmin, Xmax, Ymin, Ymax
!       in units of degrees.
        Xmin = 400.
        Ymin = 400.
        Xmax = -400.
        Ymax = -400.
        do j = 1, nodes
            if (Xmin .gt. x(j)) Xmin = x(j)
            if (Xmax .lt. x(j)) Xmax = x(j)
            if (Ymin .gt. y(j)) Ymin = y(j)
            if (Ymax .lt. y(j)) Ymax = y(j)
        end do

!.......Pick a random sample point in the box, with uniform distribution
!       untill it falls inside the polygon. That will be the location of the
!       event:
        delX = Xmax - Xmin
        delY = Ymax - Ymin
        do j = 1,100000
            rx = rnunf()
            ry = rnunf()
            SimLong = Xmin + delX*rx
            SimLat  = Ymin + delY*ry
        
!...........Test whether point is inside polygon
            !INOUT = -1 > Point is outside
            !         0 > Point is on edge or on a vertex
            !         1 > Point is inside the polygon
            call PNPOLY (SimLong, SimLat, x, y, nodes, INOUT )
            
            if (INOUT .eq. 1)  exit
        end do

!.......Sample depth from uniform distribution
        rx = rnunf()
        SimDepth = D1 + rx*(D2-D1)

        return
        
        end subroutine AreaEQselect
