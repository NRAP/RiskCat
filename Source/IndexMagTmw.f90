!      This routine called by MagSamplings and others
!
!*******************************************************************************
!
        SUBROUTINE IndexMagTmw ( kmag, ktmw, xMag3, nbin, Ntp1, time1 ) 
!
!*******************************************************************************
!
!       Given a magnitude and a time of occurrence of an event, this routine
!       returns the indeces of bith, the magnitude calibration bin, and
!       of the time-window.
!
!       Input:
!             nbin  = Number of magnitude calibration bins
!             Ntp1  = Number of time-windows
!             time1 = Time of occurrence of the event (years)
!             xMag3 = Magnitude of the event
!
!       Output:
!             kmag  = index of the magnitude calibration bin of the magnitude
!             ktmw  = index of the time-window of occurrence of the event
!
!*******************************************************************************
!
        USE ModuleEq

        IMPLICIT NONE

        INTEGER :: i, itp, k, kmag, ktmw, nbin, Ntp1

        REAL    :: time1, xMag3, x1, x2

!*******************************************************************************

 
        !Bounds of Magnitudes in magnitudes sampling bin "i" are:
            ! Lower bound = Smag(i)
            ! Upper bound = Smag(i+1)  
        !Bounds of time for definition of Time-Window "itp":
            ! Lower bound = tlife(itp-1) (0. for first one)
            ! Upper bound = tlife(itp)
            !Which Mag bin does xMag3 fall into?
            do i = 1, nbin
                if (Smag(i) .lt. xMag3+0.001)  then
                    k = i
                else
                    exit
                end if
            end do 

            !Which time-window bin does this event fall into?
            if ((time1.le.tlife(1)) .or. (Ntp1.eq.1))  then
                itp = 1
            else
                do i = 2, Ntp1
                    x1 = tlife(i-1)
                    x2 = tlife(i)
                    if ((time1.gt.x1) .and. (time1.le.x2))  then
                        itp = i
                        exit
                    end if                    
                end do
            end if

        kmag = k
        kTmw = itp

        return

        end subroutine IndexMagTmw
