      subroutine cool(n,x,sgn)
      parameter (maxdim=80000) 
c      implicit real*8 (a-h,o-z)
c discrete fourier transform for complex time series of
c 2**n points
c
c after forward transform mulitply by time increment, and after reverse
c transform multiply by frequency increment.
c
c
c   sgn=-1, forward transform
c   sgn= 1, inverse transform
c nmax= largest value of n to be processed with this routine --
c        can be modified to suit
c
      dimension m(25),x(maxdim)
      complex x,wk,hold,q
      lx=2**n
      do  i=1,n
         m(i)=2**(n-i)
      end do
      
      do  l=1,n       
         nblock=2**(l-1)        
         lblock=lx/nblock         
         lbhalf=lblock/2         
         k=0         
         do  iblock=1,nblock            
            fk=k            
            flx=lx           
            v=sgn*6.2831853* fk/flx            
            wk=cmplx(cos(v),sin(v))            
            istart=lblock*(iblock-1)                 
            do  i=1,lbhalf               
               j=istart+i               
               jh=j+lbhalf               
               q=x(jh)*wk               
               x(jh)=x(j)-q               
               x(j)=x(j)+q               
            end do            
            do  i = 2,n
               ii=i
               if (k.lt.m(i)) then
                  k=k+m(ii)
                  exit                       
               else                        
                  k=k-m(i)
               end if
            end do               
         end do            
      end do

      
      k=0
      do  j=1,lx
         if(k.ge.j) then
            hold=x(j)
            x(j)=x(k+1)
            x(k+1)=hold
         end if
         do i = 1,n
            ii = i
            if (k.lt.m(i)) then
               k = k + m(ii)
               exit
            else
               k = k - m(i)
            end if
         end do
      end do
      
      return
      end
