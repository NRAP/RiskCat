!   imsl routine name   - merrc=erfc
!
!-----------------------------------------------------------------------
!
!   computer            - cray/single
!
!   latest revision     - august 1, 1981
!
!   purpose             - evaluate the complemented error function
!
!   usage               - result = erfc(y)
!
!   arguments    y      - input argument of the complemented error
!                           function.
!                erfc   - output value of the complemented error
!                           function.
!
!   precision/hardware  - single/all
!                         note - erfc may not be supplied by imsl if it
!                           resides in the mathematical subprogram
!                           library supplied by the manufacturer.
!
!   reqd. imsl routines - none required
!
!   notation            - information on special notation and
!                           conventions is available in the manual
!                           introduction or through imsl routine uhelp
!
!   copyright           - 1978 by imsl, inc. all rights reserved.
!
!   warranty            - imsl warrants only that imsl testing has been
!                           applied to this code. no other warranty,
!                           expressed or implied, is applicable.
!
!-----------------------------------------------------------------------
!
      REAL (KIND=8) function erfc2(y)
      
      IMPLICIT NONE

!                        specifications for arguments
      REAL (KIND=8)      :: res,ssqpi,x,xbig,xden,xi,xlarge,xmin,xnum,xsq,y

!                        specifications for local variables
      INTEGER            :: i,isw
      REAL, dimension(5) :: p, p2
      REAL, dimension(8) :: p1
      REAL, dimension(3) :: q
      REAL, dimension(7) :: q1
      REAL, dimension(4) :: q2


!*******************************************************************************            
!                                  coefficients for 0.0 .le. y .lt.
!                                  .477
      data              p(1)/-.44422647396874/,   &
                        p(2)/10.731707253648/,   &
                        p(3)/15.915606197771/,   &
                        p(4)/374.81624081284/,   &
                        p(5)/2.5612422994823e-02/   
      data              q(1)/17.903143558843/,   &   
                        q(2)/124.82892031581/,   &
                        q(3)/332.17224470532/   
!                                  coefficients for .477 .le. y
!                                  .le. 4.0
      data              p1(1)/7.2117582508831/,   &
                        p1(2)/43.162227222057/,   &
                        p1(3)/152.98928504694/,   &
                        p1(4)/339.32081673434/,   &
                        p1(5)/451.91895371187/,   &
                        p1(6)/300.45926102016/,   &
                        p1(7)/-1.3686485738272e-07/,   &
                        p1(8)/.56419551747897/   
      data              q1(1)/77.000152935229/,   &
                        q1(2)/277.58544474399/,   &
                        q1(3)/638.98026446563/,   &
                        q1(4)/931.35409485061/,   &
                        q1(5)/790.95092532790/,   &
                        q1(6)/300.45926095698/,   &
                        q1(7)/12.782727319629/   
!                                  coefficients for 4.0 .lt. y
      data              p2(1)/-.22695659353969/,   &
                        p2(2)/-4.9473091062325e-02/,   &
                        p2(3)/-2.9961070770354e-03/,   &
                        p2(4)/-2.2319245973418e-02/,   &
                        p2(5)/-2.7866130860965e-01/   
      data              q2(1)/1.0516751070679/,   &
                        q2(2)/.19130892610783/,   &
                        q2(3)/1.0620923052847e-02/,   &
                        q2(4)/1.9873320181714/   
!                                  constants
      data               xmin/1.0e-8/,xlarge/5.6875e0/   
!                                  erfc(xbig) .approx. setap
      data               xbig/25.90625/
      data               ssqpi/.56418958354776/
!                                  first executable statement
      x = y
      isw = 1
      if (x.ge.0.0e0) go to 5
      isw = -1
      x = -x
    5 if (x.lt..477e0) go to 10
      if (x.le.4.0e0) go to 30
      if (isw .gt. 0) go to 40
      if (x.lt.xlarge) go to 45
      res = 2.0e0
      go to 65
!                                  abs(y) .lt. .477, evaluate
!                                  approximation for erfc
   10 if (x.lt.xmin) go to 20
      xsq = x*x
      xnum = p(5)
      do 15 i = 1,4
         xnum = xnum*xsq+p(i)
   15 continue
      xden = ((q(1)+xsq)*xsq+q(2))*xsq+q(3)
      res = x*xnum/xden
      go to 25
   20 res = x*p(4)/q(3)
   25 if (isw.eq.-1) res = -res
      res = 1.0e0-res
      go to 65
!                                  .477 .le. abs(y) .le. 4.0
!                                  evaluate approximation for erfc
   30 xsq = x*x
      xnum = p1(7)*x+p1(8)
      xden = x+q1(7)
      do 35 i=1,6
         xnum = xnum*x+p1(i)
         xden = xden*x+q1(i)
   35 continue
      res = xnum/xden
      go to 55
!                                  4.0 .lt. abs(y), evaluate
!                                  minimax approximation for erfc
   40 if (x.gt.xbig) go to 60
   45 xsq = x*x
      xi = 1.0e0/xsq
      xnum = p2(4)*xi+p2(5)
      xden = xi+q2(4)
      do 50 i = 1,3
         xnum = xnum*xi+p2(i)
         xden = xden*xi+q2(i)
   50 continue
      res = (ssqpi+xi*xnum/xden)/x
   55 res = res*dexp(-xsq)
      if (isw.eq.-1) res = 2.0e0-res
      go to 65
   60 res = 0.0e0
   65 erfc2 = res


      return
      end
