#ifdef USE_DISLIN 
!******************************************************************************    
       SUBROUTINE PlotNuisance (CSTR1,CSTR2,IxSCL,jwdt,LegendTitle,  &
       MAINTITLE,MAINTITLE2,Nameplotfile,NameLegends,Nc,Nf,NVIEW,       &
       pagenumber,TitleX,TitleY,XA,Y)
!******************************************************************************
!
!   Plots Hazards Nc Curves, the first one being the major curve
!
!       Plot is either displayed on screen or stored in file PLOTFILENAME
!       PLOTFILENAME must include the extension (i.e .pdf, .png, etc.)
!       Nview   = 1 Display on screen
!                 2 Stores graphics on .PNG, .PDF, .BMP, .WMF or .PS file
!       NOTE on stored files:
!       *********************
!       DISLIN allows creation of new pages only for PDF and PS formats
!  
!       CSTR1,  = For the identification title at top of the page 
!       CSTR2     which includes:
!                    "string CSTR1"/Date and Time of the Run/"String CSTR2"    
!       IxSCL   = Index for X-axis scaling:
!                                      (=1) X-axis is LINear
!                                      (=0) X-axis is LOGarithmic
!       jwdt    Selection of Date and Time page header, and page number
!               = 1, YES
!               = 0, NO Date and time page header, and NO page number
!       MAINTITLE = Main figure title 
!       Mainline2 = Second line of title
!       Nc      = Number of curves
!       Nf      = Max number of points on X axis
!       Nportrait   = 1 plot in portrait layout
!                     2 plot in landscape layout
!       Nwidth1 = Width of the major lines (Nwidth=15 default)
!       Nwidth2 = Width of other lines (good number is Nwidth2=10)
!       Nview   = Selects the screen or file format (See above note)
!       pagenumber  = Counter of number of pages for printout files
!       XA(j)   = Array of dimension (Nc) of X values for Nc curves
!       Y(i,j)  = Array of dimension (Nf,Nc) of Y values for Nc curves    
! 
!*******************************************************************************
!                                      
      USE DISLIN
      IMPLICIT NONE
      
      integer :: IxSCL,jwdt,k,kXmax,kXmin,Nc,Nc1
      integer :: Nf 
      integer :: Nnonzero 
      integer :: pagenumber !Counter for the pages being plotted
  
      INTEGER, dimension(NC) :: iflagplot

      CHARACTER (LEN=256):: CBUF
      CHARACTER (LEN=41) :: CSTR1
      CHARACTER (LEN=20) :: CSTR2
      CHARACTER (LEN=13) :: LegendTitle
      CHARACTER (LEN=55) :: MAINTITLE,MAINTITLE2
      CHARACTER (LEN=5)  :: NameEXT
      CHARACTER (LEN=80) :: Nameplotfile
      CHARACTER (LEN=32) :: TitleX
      CHARACTER (LEN=25) :: TitleY
      CHARACTER (LEN=5), DIMENSION(3) :: CLAB = (/'LOG  ','FLOAT','ELOG '/)
      CHARACTER (LEN=7), DIMENSION(8) :: KOLOR
      CHARACTER (len=16),DIMENSION(Nc):: NameLegends,NameLegends1

      INTEGER :: I,j,Nportrait,Nview,NWIDTH
      INTEGER :: NX,NYA,NYLABEL
      REAL    :: Xmax,Xmax0,Xmax1,Xmin,Xmin0,Xmin1,XNX,XP,Xstep
      REAL    :: Ymax,Ymax1,Ymin,Ymin1
      REAL    :: z1,z2
      REAL, DIMENSION(Nf)    :: XA,YA
      REAL, DIMENSION(Nf,Nc) :: Y,Y2
!
!*******************************************************************************
!
      KOLOR(1)  = 'RED'
      KOLOR(3)  = 'GREEN'
      KOLOR(7)  = 'BLUE'
      KOLOR(4)  = 'CYAN'
      KOLOR(5)  = 'ORANGE'
      KOLOR(6)  = 'MAGENTA'
      KOLOR(2)  = 'WHITE'
      KOLOR(8)  = 'YELLOW' 

      Nportrait = 1
      NWIDTH    = 15
      NameEXT    = 'CONS'
      if (NVIEW .eq. 2)  then
          NameEXT = 'PDF'
      end if
      


!Determine the min and max of each axis
!   Both axes are logarithmic:
      Xmin0 = 1.e10
      Xmax0 = 0.
      Ymin = 1.
      Ymax = 0.

      do i = 1,Nf

        Xmin0 = AMIN1 (Xmin0,XA(i))
        Xmax0 = AMAX1 (Xmax0,XA(i))
        !print *, 'N, Nc =', N,Nc
        do j = 1,Nc
            !print *,'Y(i,j)',Y(i,j)
            if (Y(i,j).gt. 0.)  then
                Ymin = AMIN1 (Ymin,Y(i,j))
                if (Y(i,j).gt.1.)  then
                    Y(i,j) = 1.
                end if
                Ymax = AMAX1 (Ymax,Y(i,j))
                kXmax = i
            end if
        end do
     !stop
      end do

      kXmin = 1
      do i = Nf, 1, -1
           do j = 1,Nc
                if ((Y(i,j).gt. 0.) .and. (Y(i,j).lt.0.999999999)) then
                    kXmin = i
                end if
            end do
      end do
      if (kXmin .gt. 1)  kXmin = kXmin - 1
      !print *, 'kXmin,kXmax=', kXmin,kXmax
      !print *, 'XA(min), XA(max)=',XA(kXmin),XA(kXmax)
!*******************************************************************************
      
      Xmin0 = XA(kXmin)
      Xmax0 = XA(kXmax)
      if (IxSCL .eq. 1)  then
          !X-axis is LINear
          Xmin = 0.
          Xmax = Xmax0
        else
          !X-axis is LOGarithmic
          Xmin = ALOG10 (Xmin0)
          Xmax = ALOG10 (Xmax0)
      end if
      Xmin1= INT (Xmin)
      Xmax1= INT (Xmax)

      if (Xmin1 .ne. Xmin)  then
        if (Xmin.lt.0.) then
            Xmin = Xmin1 - 1
        end if            
      end if

      if (Xmax1 .ne. Xmax) then
        Xmax = Xmax1 + 1.
      end if 
      !Xmax = Xmax1
           
      Ymin = ALOG10 (Ymin)
      Ymax = ALOG10 (Ymax)
      Ymin1= INT (Ymin)
      Ymax1= INT (Ymax)
      if (Ymin1 .ne. Ymin) then
        Ymin = Ymin1 - 1.
      end if      
      if (Ymax1 .ne. Ymax) then
        if (Ymax.lt.0.) then
            Ymax = Ymax1
        else
            Ymax = Ymax1 + 1.
        end if
      end if
!--------------------------------------------------------------------------
!Plotting initializations:
      
      if (Nview .eq. 1) then    
          !Display on screen
          CALL METAFL(NameEXT) 
          CALL SCRMOD('REVERS')
          CALL SETPAG('DA4L')   !Landscape
          if (Nportrait.lt.2) THEN
              CALL SETPAG('DA4P')   !Portrait
          END IF      
          CALL DISINI()
          CALL PAGERA
      else
          if (pagenumber .eq.1) then
              CALL METAFL ('PDF')   
              !Create output file
              CALL SETFIL (NamePlotfile)!Name includes extension (ex: .PDF)
              CALL SCRMOD('REVERS')
              CALL SETPAG('DA4L')   !Landscape
              if (Nportrait.lt.2) THEN
              CALL SETPAG('DA4P')   !Portrait
              CALL DISINI()
              CALL PAGERA
              END IF  
          else
              CALL NEWPAG()
          endif       
      endif
     
!      CALL CLRMOD ('FULL')
      CALL SETCLR (255)
      CALL PAGERA()
!Fonts
      !if (NameExt .eq. 'CONS')  then
          !call X11FNT ('-Adobe-Times-Medium-R-Normal-','timR10-ISO8859-1')
      !else
          CALL HWFONT()
      !end if

       CALL COLOR('FORE')

!Define Axes 1
      CALL AXSLEN(1400,1700)
      CALL NAME(TitleX,'X')
      CALL NAME(TitleY,'Y')
      if (IxSCL .eq. 0)  then
          CALL AXSSCL('LOG','X')
        else
          CALL AXSSCL('LIN','X')
      end if 
      CALL AXSSCL('LOG','Y')
!
!Figure Titles
      CALL TITLIN (MAINTITLE,2)
      CALL TITLIN (MAINTITLE2,4)

!Define Axes 2
        NYA=2400
        NYLABEL = 1000
        CALL LABDIG(-1,'XY')
        CALL AXSPOS(500,NYA)
        CALL TICPOS ('REVERS','XY')
       
        if (IxSCL .eq. 0)  then
            CALL LABELS(CLAB(1),'XY')
            CALL GRAF(Xmin,Xmax,Xmin,1.,Ymin,Ymax,Ymin,1.)
          else
            CALL LABELS(CLAB(2),'X')
            CALL LABELS(CLAB(1),'Y')
            z1 = INT(ALOG10(Xmax))
            z2 = 10.**z1
            Xstep = z2 / 2.
            if (z1 .gt. 5.)  then
            Xstep = z2
            end if
            CALL GRAF(Xmin,Xmax,Xmin,Xstep,Ymin,Ymax,Ymin,1.)
        end if
        CALL GRID (2,2)

!Print Header at top of figure page
          CALL HEIGHT(60)
          CALL TITLE()
          CALL HEIGHT(30)
          if (jwdt.eq.1)  then
              CALL PAGHDR (CSTR1,CSTR2,4,0)
          end if
          
 !Plot Nc Hazard Curves
 !      Get rid of curves with only 1 or less nonzero Y value
        iflagplot = 0
        Nc1       = 0
        do j = 1, Nc
            Nnonzero = 0
            do i = 1,Nf
                if (Y(i,j) .gt. 1.e-10)  then
                    Nnonzero = i
                end if
            end do
            if (Nnonzero .gt. 1)  then
                iflagplot(j) = 1
                Nc1 = Nc1 + 1
            end if
        end do
        !There are only Nc1 non zero curves
        !Shift all non zero curves and legends into first Nc1 slots
        Nnonzero = 0
        do j = 1, Nc
            if (iflagplot(j) .eq. 1)  then
                Nnonzero = Nnonzero + 1
                !Shift curves
                do i = 1,Nf
                    Y2(i,Nnonzero) = Y(i,j)
                end do
                !Shift legends
                NameLegends1(Nnonzero) = NameLegends(j)
            end if
        end do
        
 !      First curve in RED (for MEAN or MEDIAN)
        do j = 1, Nc1
            Nnonzero = 0
            do i = 1,Nf
!                 XA(i) = X(i,j)
                 YA(i) = Y2(i,j)
                 if (YA(i) .gt. 1.e-10)  then
                     Nnonzero = i
                 end if
            end do
                k = 1 + modulo(j-1,8)
                CALL COLOR (KOLOR(k))
                CALL LINWID (NWIDTH)
            if (j.GT.8) then 
                CALL DASHL    
            endif
            CALL CURVE (XA,YA,Nnonzero)
        end do
        CALL SOLID ()
        
!Legend statement
!        CALL LINWID (20)
        CALL HEIGHT (40)
        CALL COLOR ('FORE')
        !Nc lines of Legend:
        CALL LEGINI(CBUF,Nc1,20)   
        !Set title of Legend
        CALL LEGTIT (LegendTitle) !replaced "Legend:" by Legendtitle
!        NX = 1269
        XNX = Xmin + 0.05*(Xmax-Xmin)
        NX  = NXposn(10**XNX)
!        CALL LEGPOS(NX,NYPOSN(10**Ymax))
        do i = 1,Nc1
            CALL LEGLIN (CBUF,NameLegends1(i),i)
        end do
!        CALL LEGTIT (Legtitle)
        CALL LEGEND(CBUF,5)
        CALL LINWID (1)
        
!Add a page number
        CALL HEIGHT (30)
        if (jwdt.eq.1)  then
           CALL MESSAG('Page: ',1700,2800)
           if (Nview .eq. 1)  then
                XP = 1.
                CALL NUMBER(XP,-1,1900,2800)
            else
                XP = FLOAT(pagenumber)
                CALL NUMBER(XP,-1,1900,2800)
                pagenumber = pagenumber + 1
            end if
        end if
        
      CALL ENDGRF()
             
      if (Nview .eq. 1)  then
        CALL DISFIN()
      endif
      

      RETURN
    
      END SUBROUTINE PlotNuisance
#endif
