!
!     Called by riskcat for case of empirical catalogs: Case(3)
!
!*******************************************************************************
!
        subroutine MagSampling2 ( iCaseCat, ispl, MaxSample, nbin,    &
                                  Neq, Neq2, Ntp1, unitcat,           &
                                  xMagMin2, xMagMax2, xM0, YearsHist)
!
!*******************************************************************************
!    This routine reads the catalog and throws away events in a magnitude bin
!    that are beyond the number of MaxMagSampling.
!    Creates a new catalog with no more than MaxMagSampling events in each M-bin
!    Determines the time/date of the last event and calculates the relative
!    weight of the corresponding M-bin as:
!
!     wmagbin(i) = YearHist / t(i)
!
!     where:    YearHist is the total duration of the catalog
!               t(i) is the time of the last event in magnitude bin i
!
!    The weights are used later to reconstruct the final ground-motion histogram
!    before calculating the hazard.
!
!    Input: 
!           Catalog of events
!           iCaseCat       = type of input format for catalog entries
!                          = 1, Corrinne's Xutm,Yutm,Zutm,Magnitude, time faked
!                          = 2, Empirical with date and time Y/M/D/H/M/S
!                          etc, to be updated
!           MaxMagSampling = max number of events in each magnitude sampling bin
!           xM0            = maxmimum magnitude considered in the problem
!           nbin           = number of magnitude bins sampled
!           Neq2           = Number of events in the original catalog
!           Ntp1           = Number of time-windows
!           Smag(i)        = array of lower-bound value of magnitude bin i
!           unitcat        = unit number the file of the catalog is assigned
!
!    Output:
!           Neq            = Number of events in the culled catalog
!           Mag, Time, M0, X,Y,Z of each event in the culled catalog
!           wMagbin(i)     = Normalized weight of magnitude bin i.
!
!*******************************************************************************
!
        USE ModuleEq
        USE GenParam


        IMPLICIT NONE

        INTEGER :: i, iCaseCat, ispl, itp, j, k
        INTEGER :: MaxSample, unitcat
        INTEGER :: n, nbin, Neq, Neq2, n1, Ntp1
        REAL    :: time, time1, time2
        REAL    :: x, xMagMin2, xMagMax2, xMSc, xM0, xt, x1, x2, x3
        REAL    :: y, YearsHist, z

        INTEGER, dimension(nbin,Ntp1) :: nEqMTW, NevMag

        REAL,    dimension(nbin,Ntp1) :: TimeMag

!-------------------------------------------------------------------------------
        !Reset the number of events in each M-bin.
        NevMag   = 0
        !Resest number of events in culled catalog
        Neq      = 0
        !Reset Time till last event in magnitude bin
        TimeMag  = YearsHist
        !Reset total number of events
        nEqtot   = 0
        !Reset Number of event per Mag,Time-window bin
        nEqMTW   = 0

        rewind (unitcat)
        xt = REAL (Neq2)
 
        !Bounds of Magnitudes in magnitudes sampling bin "i" are:
            ! Lower bound = Smag(i)
            ! Upper bound = Smag(i+1)  
        !Bounds of time for definition of Time-Window "itp":
            ! Lower bound = tlife(itp-1) (0. for first one)
            ! Upper bound = tlife(itp)

        Do j = 1, Neq2
     
            read (unitcat,*) x, y, z, xMSc
            time =(YearsHist/xt) * Real(j)

            if (time .le. tlife(Ntp1))  then

                !Which Mag bin does xMSc fall into?
                !Which time-window bin does this event fall into
 
                call IndexMagTmw ( k, itp, xMSc, nbin, Ntp1, time)

                !Store number of events greater then xM0, per itp and ispl
                !Store number of events greater then xM0, per Mag and itp bin
                if (xMSc .ge. xM0)  then
                    nEqtot(ispl,itp) = nEqtot(ispl,itp) + 1
                    nEqMTW(k,itp)    = nEqMTW(k,itp)    + 1
                end if
    
                !Set the time of observation equal to the length of the time-window
                !if  number of events sufficient to obtain statistical convergence
                !is reached, then set time to actual time of occurrence of event
                ! since start of time-window.
                time2  = tlife(1)
                time1  = 0.
                if (itp .gt. 1)  then
                    time2 = tlife(itp)
                    time1 = tlife(itp-1)
                end if
           
                n = NevMag(k,itp)
                if (n .lt. MaxSample)  then
                    !Update number of events in mag bin and time-window
                    n1             = n + 1
                    NevMag(k,itp)  = n1
                    if (n1 .eq. MaxSample)  then
                        TimeMag(k,itp) = time - time1
                    end if
                    !Update number of events in culled catalog
                    Neq = Neq + 1
                    !Load the events parameters in appropriate arrays
                    call LoadCulledCat( Neq, icaseCat, ispl, time, x, xMSc,   &
                                       xMagMin2, xMagMax2, y, z )
                end if
                
            end if

        end do

        !close (unitcat)

!       Calculate weights
        do j = 1, Ntp1
            do i = 1, nbin
                x1 = REAL (nEqMTW(i,j))
                !wMagbin(i,j) = YearsHist / TimeMag(i,j)
                x2 = REAL (NevMag(i,j))
                x3 = 1.
                if ((NevMag(i,j).eq.MaxSample) .and. (x2.gt.0.)) then                
                    x3 =  x1 / x2 
                end if
                wMagbin(i,j) = x3
            end do
        end do

        print *, 'Min Mag   :', xMagMin2
        print *, 'Max Mag   :', xMagMax2
        print *, 'Mag bins  :', (Smag(i),i=1,nbin)
        do j = 1, Ntp1
            print *, 'N/Mag Bin:', (NevMag(i,j),i=1,nbin)
            print *, 'Weights  :', (wMagbin(i,j),i=1,nbin)
        end do


        return

        end subroutine MagSampling2




  
        
        
