!csssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!c
        subroutine  dist (x1,y1,x2,y2,d)
!c
!csssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!c
!c

        implicit none                                      

        integer :: k
        real :: ab,d,del,delamb,delphi,dx,dy
        real :: phi,r,radeg,sindel,xa,xb,x1,x2,ya,yb,y1,y2,za,zb
        
!c
!c
        real, dimension(64) ::  a = (/1.,2.,3.,4.,5.,6.,7.,8.,    &
                   9.,10.,11.,12.,13.,14.,                        &  
                   1.792552,1.783950,1.774808,1.765128,1.754912,  &
                   1.744163,1.732887,1.721083,1.708757,1.695910,  &
                   1.682550,1.668677,1.654297,1.639413,1.624032,  &
                   1.608155,1.591788,1.574937,1.557605,1.539798,  &
                   1.521522,1.502780,1.483580,1.463927,1.443827,  &
                   1.423283,1.402307,1.380900,1.359070,1.336823,  &
                   1.314167,1.291108,1.267653,1.243808,1.219582,  &
                   1.194982,1.170013,1.144685,1.119005,1.092980,  &
                   1.066618,1.039928,1.012918,0.985595,0.957968,  &
                   0.930047,0.901837,0.873348,0.844590,0.815572/)
!c
        real, dimension(64) ::  b = (/  &
                  1.,2.,3.,4.,5.,6.,7.,8.,9.,10.,11.,12.,13.,14.,& 
                  1.844062,1.844230,1.844408,1.844595,1.844792,  &
                  1.844998,1.845213,1.845437,1.845668,1.845907,  &
                  1.846153,1.846408,1.846670,1.846938,1.847213,  &
                  1.847495,1.847781,1.848073,1.848372,1.848673,  &
                  1.848980,1.849290,1.849605,1.849922,1.850242,  &
                  1.850565,1.850890,1.851217,1.851543,1.851873,  &
                  1.852202,1.852531,1.852860,1.853188,1.853515,  &
                  1.853842,1.854165,1.854487,1.854805,1.855122,  &
                  1.855433,1.855742,1.856045,1.856345,1.856640,  &
                  1.856928,1.857212,1.857490,1.857762,1.858025/)
!
!	Simple Method with sperical coordinates. Assume a constant average 
!	radius for the earth of 6371.2 km
        r = 6371.2                                ! Earth radius in km
        radeg = 57.2957795                        ! Conversion degree-radian
!       Point A
        xa = r * cos(x1/radeg) * cos(y1/radeg)
        ya = r * sin(x1/radeg) * cos(y1/radeg)
        za = r * sin(y1/radeg)
!       Point B
        xb = r * cos(x2/radeg) * cos(y2/radeg)
        yb = r * sin(x2/radeg) * cos(y2/radeg)
        zb = r * sin(y2/radeg)
!       Straight line distance between A and B
        ab = sqrt((xa-xb)**2+(ya-yb)**2+(za-zb)**2)
!       Half the angle AOB
        sindel = 0.5 * ab / r
        del = asin (sindel)                        ! Angle in radians
!       Length of the arc AB.
        ab = 2. * r * del
        d = ab

        return

!       THE REST IS DEAD CODE THAT WILL BE ELIMINATED WHEN ALL IS CHECKED
!       distance between two points with coordinates
!       (x1,y1) and (x2,y2) of longitude and latitude.
!       the distance is computed by using the method exposed in richter's
!       book page 701 (appendix 12), 1958 edition.
!       this method is good within less than one (1.) km for all the
!       ranges of distances that are of interest in this study.
!
!
!       mean latitude : phi
        phi = (y1 + y2) / 2.
        k = int(phi)
!       interpolate to find a and b
        del  = phi - float(k)
        xa = a(k) + (a(k+1) - a(k))*del
        xb = b(k) + (b(k+1) - b(k))*del
!
!       delphi and delambda have to be expressed in minutes
        delphi = (y2 - y1) * 60.
        delamb = (x2 - x1) * 60.
        dx = xa * delamb
        dy = xb * delphi
        d = sqrt ((dx*dx) + (dy*dy))
!
!
        return
        end subroutine dist
