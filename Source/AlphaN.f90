!
!******************************************************************************
        subroutine AlphaN  (OutAlpha,N,nAlpha1,nAlpha2,InAlpha)
!******************************************************************************
!
!   This routine takes a alphanumeric string (STRING) of up to 256 characters
!   as input, and spits out the number N of characters between the first and  
!   lastnon-blank characters. It stores the charcaters in array Alpha2(*) in 
!   postions 1 to N 
!   Ex: STRING = '   xy df aed  '  gives,
!       N      = 9
!       Alpha2 = /'x','y',' ','d','f',' ','a','e','d'/
!INPUT:   
!       STRING  = alphanumeric variable up to 256 characters
!
!OUTPUT:
!       Alpha2() = array of characters in STRING
!       N1  = position of first non-blank character
!       N2  = position of last non-blank character
!       N   = length of in-between string (N = N2 - N1 + 1)

    implicit none
    
    integer :: i,N,nalpha1,nalpha2
    
    CHARACTER (LEN=1)  :: a1
    character (len=256):: InAlpha
    CHARACTER (LEN=1), DIMENSION(256) :: ALPHA
    CHARACTER (LEN=1), DIMENSION(256) :: OutAlpha
    
!     Reset ALPHA and Alpha2
      a1 = ' '
      do i = 1,256
          ALPHA(i) = a1
          OutAlpha(i)= a1
      end do

!     Load the characters of 'STRING' in array ALPHA()
      read (InAlpha,"(256a1)",end=10) (ALPHA(i),i=1,256)
      
10    nalpha1 = 256
      nalpha2 =   0
      do i = 1, 256
          if (ALPHA(i).ne.a1)  then
              nalpha1 = i
              exit
          end if
      end do
      
      do i = 256,1,-1
          if (ALPHA(i).ne.a1)  then
              nalpha2 = i
              exit
          end if
      end do
      
      N = nalpha2 - nalpha1 + 1
   
      do i = 1,N
      OutALPHA(i) = ALPHA(nalpha1-1+i)
      end do
      
      RETURN
      
      END SUBROUTINE AlphaN