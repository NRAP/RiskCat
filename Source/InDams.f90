!Read the information on Dams in the region of the study
    !nDams      = number of Dams in the analysis
    !MaxDams    = Maximum nuber of Dams possible in the analysis
    !IdDam      = ID of the Dam
    !DamName    = Dam name
    !xDam & yDam=Coordinates of Dam 
    !FragDam(i,1) = Median Fragiltiy for Dam i (in cm/s/s)
    !..........2..= Beta fragility coefficient for Dam i

        subroutine InDams (DamName,FragDam,infile,MaxDams,nDams,   &
                          unit1,xDam,yDam)
        
      IMPLICIT NONE
      
      INTEGER :: i,IERR,MaxDams,nDams,unit1
      INTEGER, dimension(MAxDams) ::IdDam
      REAL, dimension(MAxdams) :: xDam,yDam
      REAL, dimension(MaxDams,2) :: FragDam
      CHARACTER (LEN=10) :: Dummy
      character (len=20) :: infile
      CHARACTER (LEN=30), dimension(MaxDams) :: DamName
      
      
        open (unit=unit1,file=infile,status="old",iostat=IERR)
        call CantFindFile (infile,IERR)
        rewind (unit=unit1)
        
        
        read (unit1,*) nDams
        if (nDams .le. 0)  then
            close (unit1)
            RETURN
        end if
        
        Read (unit1,*)  Dummy
        do  i = 1,nDams
            Read (unit1,*) IdDam(i),DamName(i),xdam(i),yDam(i), &
            FragDam(i,1),FragDam(i,2)
        end do
       
        close (unit=unit1)
        
        return
        end subroutine InDams
