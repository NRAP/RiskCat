! This routine is called from routine "faultband" and from "faultPoint"
!
! EMPSYNcontour subdivides the fault 3D segments flaps into sub-elements
! for use in EMPSYN earthquake simulations.
!
        subroutine EMPSYNcontour (apex, iflag, nL2, nStore, &
                                  Nsub, nW2, store1, xLmax)
!        
!******************************************************************************
!       Input:
!       apex(i,j)   - coordinates of the 4 corners of quadrilateral segment
!       rLW         - aspect ratio of the fault (default is 0.5)
!       xLmax       - required length of subdivided elements for EMPSYN calcs.
!       xMag        - magnitude of the simulated earthquake
!
!       Output:
!       All stored in Module EMPSYNinput
!       Nsub        - number of sub elements needed for EMPSYN simulation
!       xMag        - magnitude of the simulated earthquake (Moment Magnitude)
!       Pinit(3)    - x,y,z lat,long,depth/km of initiation point
!       SubE(i,j,4) - x,y,z coordinates (lat,long,deptth/km) of center of 
!                     discretized sub element, and
!                     area (km^2) of sub-element.
!******************************************************************************
!       
        use ModuleEQ
        
        implicit none
        
        character (LEN=1):: blankline
        
        integer:: i, iflag, j, k, nL, nL2, nStore, nW, nW2, Nsub
        
        real   :: a1, a2, area, distL, distW
        real   :: ratioL, ratioW, rLW, test, x1, x2, xLmax
        real   :: y1, y2, z1, z2

        real, dimension(3)   :: center
        real, dimension(4,3) :: apex, c
        real, dimension(nStore,4) :: store1
        
        real, allocatable    :: c1(:, :),c2(:, :)
        
!*********************************************************************************
!       Some initializations:
        blankline = ' '
        Nsub      = 0
        rLW       = 1.
!******************************************************************************
!
!              Node order for one segment of fault
!
!   Top trace ........ 1------------4
!                      |            |
!                      |   Fault    |
!           Left side  |  Segment   | Right side
!                      |   Nodes    |
!  Bottom trace ...... 2------------3    
! 
!   
!
!If iflag = 1, ONLY calculate the number of sub-elements and return
    if (iflag .eq. 1)  then
        ! Number of sub-elements in length
        !Top trace of quadrilateral
        x1 = apex(1,1)
        x2 = apex(4,1)
        y1 = apex(1,2)
        y2 = apex(4,2)
        z1 = apex(1,3)
        z2 = apex(4,3)
        call distsimple (x1,y1,x2,y2,distL)
        distL = SQRT (distL*distL + (z2-z1)*(z2-z1))
        nL = INT(distL/xLmax)
        test = (Float(nL)*xLmax) - distL
        if ( ABS(test) .ge. 0.01)  then
            nL = nL + 1
        end if
        !Bottom trace
        x1 = apex(3,1)
        x2 = apex(2,1)
        y1 = apex(3,2)
        y2 = apex(2,2)
        z1 = apex(3,3)
        z2 = apex(2,3)
        call distsimple (x1,y1,x2,y2,distL)
        distL = SQRT (distL*distL + (z2-z1)*(z2-z1))
        nL2= INT(distL/xLmax)
        test = (Float(nL2)*xLmax) - distL
        if ( ABS(test) .ge. 0.01)  then
            nL2 = nL2 + 1
        end if
        nL = MAX0 (nL, nL2)
        
        !Number of sub-elements in width (depth)
        !Left side of quadrilateral
        x1 = apex(1,1)
        x2 = apex(2,1)
        y1 = apex(1,2)
        y2 = apex(2,2)
        z1 = apex(1,3)
        z2 = apex(2,3)
        call distsimple (x1,y1,x2,y2,distW)
        distW = SQRT (distW*distW + (z2-z1)*(z2-z1))
        nW = INT(distW/(xLmax*rLW))
        test = (Float(nW)*(xLmax*rLW)) - distW
        if ( ABS(test) .ge. 0.01)  then
            nW = nW + 1
        end if
        !Right side
        x1 = apex(4,1)
        x2 = apex(3,1)
        y1 = apex(4,2)
        y2 = apex(3,2)
        z1 = apex(4,3)
        z2 = apex(3,3)
        call distsimple (x1,y1,x2,y2,distW)
        distW = SQRT (distW*distW + (z2-z1)*(z2-z1))
        nW2= INT(distW/(xLmax*rLW))
        test = (Float(nW2)*xLmax*rLW) - distW
        if ( ABS(test) .ge. 0.01)  then
            nW2 = nW2 + 1
        end if
        nW = MAX0 (nW, nW2)
        
        nL2 = nL 
        nW2 = nW 
 !       print *,'EMPSYNcontour-128: distL,xLmax,nL2',distL,xLmax,nL2
 !       print *,'EMPSYNcontour-129: distW,xLmax,rLW,nL2',distW,xLmax,rLW,nW2
 !       stop 'EMPSYNcontour-130'
        return
        
    else
        
        !if iflag = 2, calculate the sub-elements center coordinates and area
        allocate (c1(nL2+1, 3))
        allocate (c2(nL2+1, 3))
        
        ! Take one ribbon at a time starting by top ribbon, and subdivide it 
        ! in nW sub-elements, then repeat for each next ribbon going downward
        !First row  at top of ribbon/fault (not necessarily surface)
        do k = 1,3
            c1(1,k) = apex(1,k)
        end do
        do  j = 1, nL2
            ratioL = Float(j) / Float(nL2)
            do  k = 1,3
                c1(j+1,k) = ((apex(4,k)-apex(1,k))*ratioL) + apex(1,k)
            end do
        end do
        !Row i > 1
        do i = 1, nW2
            ratioW = Float(i) / Float(nW2)
            !End points
            do k = 1,3
                a1 = ((apex(2,k)-apex(1,k))*ratioW) + apex(1,k)
                a2 = ((apex(3,k)-apex(4,k))*ratioW) + apex(4,k)
                !Divide in length
                c2(1,k) = a1
                do j = 1, nL2
                    ratioL = Float(j) / Float(nL2)
                    c2(j+1,k) = (a2-a1)*ratioL + a1
                end do
            end do
            !c1 contains the coordinates of points at top of ribbon
            !c2 has coordinates of bottom points
            !There are nL2 sub-elements in the ribbon
            !Calculate coordinates of center and area of sub-elements
            do j = 1, nL2
                nSub = nSub + 1
                do k = 1,3
                    center(k) =(c1(j,k)+c1(j+1,k)+c2(j,k)+c2(j+1,k))/4.
                end do
                !Load coordinates of sub-element apexes in c(,)
                do k = 1,3
                    c(1,k) = c1(j,k)
                    c(2,k) = c2(j,k)
                    c(3,k) = c2(j+1,k)
                    c(4,k) = c1(j+1,k)
                end do
                !Area of sub-element
                call area3D (c,area)
                !Store center coordinates and area in array store2 to write it 
                !later in file EMPsunIn (in routine faultband, unit 3)
                do k = 1,3
                    store1(nSub,k) = center(k)
                end do
                store1(nSub,4) = area
            end do
            do j = 1, nL2+1
                do k = 1, 3
                    c1(j,k) = c2(j,k)
                end do
            end do
        end do
        
    end if

        deallocate ( c1 )
        deallocate ( c2 )      
        
        return

        end subroutine EMPSYNcontour
                    
        
