!*******************************************************************************
!
subroutine LoadGF (componenttype,file1,GFpath,GFstations,ncomp,nsources,   &
                   nSrc1,nSrc2,synflag)
!
!*******************************************************************************
!
!       This routine is called by simulperil
!
!       This routine reads the information on Green's Functions and generates
!       an array of peak values GFpeak, that will be used for small earthquakes
!       assumed to be point sources. The peaks will be scaled linearly according
!       to their seismic moment m0.
!
!       Input:
!       ******
!       file1  = name, including path, of file from which sources info are read
!       file2  = path of where GF files are stored
!
!       Output:
!       *******
!       nsources     = number of intput source locations 
!       ncomp        = number of Green's functions tensor components 
!       GFxyz(i,3)   = coordinates of GF source locations i, (Long,Lat,Depth)
!       GFobsxy(2)   = coordinates of observation point where GF were calculated
!       GFpeak(i,j ) = peak values (ZZZ) of j-th GF simulation for i-th source
!                      
!
!*******************************************************************************

        USE ModuleSYNHAZ

        implicit none

        CHARACTER (LEN=256):: Alpha, GFpath, GFpath1, GFpath2,GFstations
        CHARACTER (len=3)  :: componenttype
        CHARACTER (len=5)  :: dummy, dummy1
        CHARACTER (len=1)  :: dummy2
        CHARACTER (len=80) :: file1, GFformat
        CHARACTER (len=15) :: string
        CHARACTER (len=5)  :: string1
        CHARACTER (len=9)  :: string2

        CHARACTER (LEN=1),  DIMENSION(256)          :: OutGFpath, Outfilename
        CHARACTER (len=80), dimension(MaxGFsources) :: file2

        INTEGER :: i, i1, IERR, j, k, kk, nSrc1, nSrc2
        INTEGER :: N1, N11, N12, N2, N21, N22
        INTEGER :: ncomp, nsources, nt, synflag

        REAL    :: dt, ObsLat, ObsLong, sourcedepth
        REAL    :: stlat, stlon, sM0, stdum, stdum1, tim, xxx

        REAL, allocatable  :: v( : )

 


!*******************************************************************************
!
        print*, ' '
        print *, '  Load GF Sources from file:  ', file1
        print *, '  **************************'
 
!       This part only if GF-Sources are constructed  with one vertical distribution 
!       of sources and radial distances.

        !nSrc1 = Number of sources on one vertical line for each station
        !nSrc2 = Number of stations 
 
       !Total number of sources considering one observation point and many sources 
       nsources = nSrc1 * nSrc2
        !file1 is the file containing the onfo on the nSrc2 sources
        !The origin point is taken as the location of that vertical line
        open (unit=9,file=file1,status="unknown",iostat=IERR)        
        read (9,*) dummy, dummy1, Obslat, Obslong,xxx,GFacmp1,sM0,GFacmp3
        !The unique station name is "Statn". Its depth is Zero.
        GFobsName  = "Statn"
        GFobsxy(1) = Obslong
        GFobsxy(2) = Obslat
        GFsdpzz    = 0.
        rewind (9)
        open (unit=11,file=GFstations,status="unknown",iostat=IERR)

        print *, '  GF Observation point name:  ', GFobsName
        print *, '  Latitude and Longitude of Observation Point:',Obslat,Obslong


        !Create the GF-sources file names:
        k = 0 
        do j = 1, nSrc1    !Read as Sources
            read (9,*) dummy, dummy, Obslat, Obslong, Sourcedepth, GFacmp1, &
            GFacmp2,GFacmp3
                if (j .lt. 10)  then
                    write (string2,"(a6,i1,a2)") 'FKR000',j,'.v'
                end if
                if ((j .gt.9) .and. (j.lt. 100))  then
                    write (string2,"(a5,i2,a2)")  'FKR00',j,'.v'
                end if
                if ((j .gt.99) .and. (j.lt. 1000))  then
                    write (string2,"(a4,i3,a2)")   'FKR0',j,'.v'
                end if
                if ((j .gt.999) .and. (j.lt. 10000))  then
                    write (string2,"(a3,i4,a2)")    'FKR',j,'.v'
                end if

            do i = 1, nSrc2   !Read as Stations
                k = k + 1
                if (i .lt. 10)  then
                    write (string1,"(a4,i1)") 'S000',i
                end if
                if ((i .gt.9) .and. (i.lt. 100))  then
                    write (string1,"(a3,i2)")  'S00',i
                end if
                if ((i .gt.99) .and. (i.lt. 1000))  then
                    write (string1,"(a3,i2)")  'S00',i
                end if
                if ((i .gt.999) .and. (i.lt. 10000))  then
                    write (string1,"(a3,i2)")  'S00',i
                end if           

                write (string,"(a5,a1,a9)")  string1, '_', string2
                write (file2(k),"(a15)") string

                GFfile2(k) = string
                GFfile3(k) = string

                read (11,*) dummy1,stlat,stlon,stdum1,dummy2,   &
                dummy2,dummy2,stdum,stdum,stdum
                !Load parameters describing the sources
                !Name of source file
                GFcspp(k)  = GFfile2(k)  
                GFxyz(k,1) = stlon
                GFxyz(k,2) = stlat
                GFxyz(k,3) = Sourcedepth
                GFma11(k)  = stdum1
                !Seismic moment of the GF source?
                GFm0(k)    = sM0 
                GFfc2(k)   =  1.
                GFbstrr(k) = 10.
                GFbdpp(k)  =  0.
                GFbsvv(k)  =  0.

                !write (*,12)  k,   &
                !GFxyz(k,2), GFxyz(k,1), GFxyz(k,3), GFm0(k),GFfile2(k)

            end do

            rewind (11)

        end do
        
        print *, 'Number of GF sources:', nsources 
        close (9)
        close (11)

!       Read GF sources' info from GFpath
        !Construct the name of the path for reading the GF
        call AlphaN (OutGFpath,N1,N11,N12,GFpath)

        print*, '  '
        print *, '  Load GF time series - PATH/filename: '
        print *, '  ************************************'

        if (componenttype .eq. 'ACC')  then
           print *,'Time Series are read as ACCELERATIONS'
        elseif (componenttype .eq. 'VEL')  then
           print *,'Time Series are read as VELOCITIES'
           print *,'They will be differentiated to accelerations'
        else
           print *,'Do not know what type of component that is. STOP'
        end if

        print *, ' Source number:',   &
                 '   Lat        Long         Depth',   &
                 '       M0','          Source Name'
        
        do i = 1, nsources

            !Construct the name of the path for reading the GF
            Alpha = GFpath
            call AlphaN (OutGFpath,N2,N21,N22,Alpha)
            Alpha    = GFfile2(i)
            call AlphaN (Outfilename,N1,N11,N12,Alpha)
            write (GFpath1,"(256a1)")(OutGFpath(kk),kk=N21,N22),   &
                                     (Outfilename(kk),kk=N11,N12)
            GFfile2(i) = GFpath1
            Alpha =  GFpath1
            call AlphaN (Outfilename,N2,N21,N22,Alpha)
            write (GFpath2,"(256a1)")(Outfilename(kk),kk=N21,N22-1),'a'
            GFfile2(i) = GFpath2

            !Write name of files used
            !************************
            if ((i.eq.1) .or. (i.eq.nsources))  then
               if (componenttype .eq. 'VEL')      then
                  !write (*,2) i, GFpath1
                  write (*,12)  1,   &
                  GFxyz(i,2), GFxyz(i,1), GFxyz(i,3), GFm0(i),GFpath1
               elseif (componenttype .eq. 'ACC')  then
                  !write (*,2) i, GFpath2
                  write (*,12)  i,   &
                  GFxyz(i,2), GFxyz(i,1), GFxyz(i,3), GFm0(i),GFpath2
               end if
            end if

            open (unit=9,file=GFpath1,status="unknown",iostat=IERR)
            open (unit=11,file=GFpath2,status="unknown",iostat=IERR)
            read (9,*)   ncomp
            read (9,*)   GFformat

            !Use unit =  9 to read time histories in Velocity
            !           11 to read them in acceleration

            do j = 1, ncomp
	    
               if (componenttype .eq. 'ACC')  then
	    
                  !The components read are already in acceleration,
                  !No need to differentiate. Just load in vector V()
                  read (11,*)  dummy
                  read (11,*)  nt,dt,tim
                  !Set the number of points in GF time histories ntGF 
                  ntGF = nt                  
                  allocate ( v(nt) )
                  read (11,GFformat) (v(k),k=1,nt)
            
               elseif (componenttype .eq. 'VEL')  then   
    
                  !Components are read as Velocities. Need to be differentiated
                  read (9,*)   dummy
                  read (9,*)   nt,dt,tim
                  !Set the number of points in GF time histories ntGF 
                  ntGF = nt

                  allocate ( v(nt) )
                  read (9,GFformat) (v(k),k=1,nt)

                  write (11,*) dummy
                  write (11,*) nt,dt,tim
	       
                  !GFs are given in Velocity:
                  !**************************
                  if (synflag .eq. 1)  then                  
                     !Input GF are in velocity (cm/s).
                     !Hazard calculations are in acceleration, GFs need
                     !to be differentiated from velocity to accelerations before 
                     !being used in synhaz.
                     !Differentiate the GFs
                     call diff_time (v,ntGF,dt)
                     write (11,GFformat) (v(k),k=1,ntGF) 
                  end if

               else
                  !There was error or no specification of component type
                  !Stop with message
                  print *,'  '
                  print *,'ERROR in Menu file:'
                  print *,'   Type of T/H component not specified.'
                  print *,'   Please specify type of input read: ACC or VEL ?'
                  print *,'STOP Execution'
                  STOP
                  
               end if
                    
               !Load GFs in array GFtensor(i,j,k)
	       !*********************************
               !i = source index
               !j = index of component (1 to 10)
               !k = number of time steps (typically, 1 to 1024)
               !this assumes that there is only one station and multiple sources
               GFnt(i) = ntGF
               do i1 = 1,ntGF
                  GFtensor(i,j,i1) = v(i1)
               end do
                      
               deallocate ( v )

            end do

            close (9)
            close (11)

            SynNp(i) = nt
            Syndt(i) = dt
            Syntim(i)= tim

        end do

!2       format (' File used number: ',i5,'    Full Path name:',a80)
12      format ('      ',i9,3f11.6,3x,e13.6,3x,a40)

        return

        end subroutine LoadGF
