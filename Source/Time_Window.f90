!       This routine is called from RiskCat
!
!*******************************************************************************
!
        SUBROUTINE Time_Window (ispl, Nev1, Ntw )
!
!*******************************************************************************
!       Determines the starting and ending indeces of each time-window
!       in an array given the bound values of the windows.
!
!       Input:
!       ------
!       Nev      : Number of events in XB
!       Ntw      : Number of time windows
!       X        : Array of values to be sgregated in time-window bins
!       TWend    : Array of ending values of bins of time-windows
!
!*******************************************************************************
!
        USE ModuleEQ

        IMPLICIT NONE

        INTEGER :: i, ispl, itp1, itp2, Nev1, Ntw

        REAL    :: xdum

        REAL,ALLOCATABLE, dimension( : ) :: TWend, XB
!
!*******************************************************************************
!
        allocate ( TWend(Ntw), XB(Nev1) )

        !Load array of times of ocurrence
        Nev1 = NCatevents(ispl)
        do i = 1, Nev1
            XB(i) = CatEqTime(ispl,i)
        end do

        !Perform the window sorting:
        !---------------------------
        call FindStartEnd (I1tp,I2tp,Ntw,Nev1,tlife,XB)

        !Check if all time windows have events in them
        !---------------------------------------------
        !if not, reset number of time windows up to the first 
        !non- populated one
        !and pursue calculations for only the (new)Ntp first
        !populated time windows. Subsequent ones are not considered.

        !Event's Starting and Ending indeces for first time window
        IstartEq(ispl,1) = I1tp(1)
        IendEq(ispl,1)   = I2tp(1)
        itp1 = I2tp(1) - I1tp(1) + 1
        if ((I2tp(1).lt.I1tp(1)) .or. (I1tp(1)*I2tp(1).lt.1)) then
           Nonzerotp1(1)  = 0
        end if
        
        do i = 2,Ntw              
            !Event's Starting and Ending indeces for other time windows
            IstartEq(ispl,i) = I1tp(i)
            IendEq(ispl,i)   = I2tp(i)
            !Number of events in each time window
            itp1 = I2tp(i) - I1tp(i) + 1
            
            if (I1tp(i) .le. I2tp(i-1))  then
                !zero events in time period i,
                Nonzerotp1(i) = 0
                print *,'*************--- Zero events in Time Window:',i
             end if            
        end do

               !Print time-windows information:
                !------------------------------
                print *, '  '
                print *,'                                  I1tp    I2tp'
                write (*,552) 
                xdum = tlife(1)
                do i = 1, Ntw
                    if (i .gt. 1)  then
                        xdum = tlife(i) - tlife(i-1)
                    end if
                    if (Nonzerotp1(i) .eq. 1) then
                        itp1 = I1tp(i)
                        itp2 = I2tp(i)
                        write (*,562) i,tlife(i),xdum,   &
                        itp1,itp2,(itp2-itp1+1), &
                        XB(itp1),XB(itp2),nonzerotp1(i)
                    else
                        write (*,572) i,tlife(i),xdum,nonzerotp1(i)  

                    end if
                end do


        deallocate ( TWend , XB )

552     format ('Time Window    Ending   Duration ',   &
        'Index1  Index2 #events  Time 1st', &
        '  Time Last (years) NonZero ')
562     format ( i11,2f10.1,3i8,2f10.2,10x,i2)
572     format ( i11,2f10.1,'     N/A     N/A       0', &
        '       N/A       N/A',10x,i2)

        return

        END SUBROUTINE Time_Window
