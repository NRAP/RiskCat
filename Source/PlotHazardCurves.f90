#ifdef USE_DISLIN 
!******************************************************************************    
      SUBROUTINE PlotHazCurves (CSTR1,CSTR2,CTIT,CTIT2,CTIT3,   &
      Ix,IxSCL,Iy,jwdt,N,NameEXT,NameLegends,NameRoot,Nc,NcMax,Nf, &
      Nportrait,Nscreen,NVIEW,NWIDTH1,pagenumber,XA,Y)
!******************************************************************************
!
!   Plots Hazards Nc Curves, the first one being the major curve
!
!       Plot is either displayed on screen or stored in file PLOTFILENAME
!       PLOTFILENAME must include the extension (i.e .pdf, .png, etc.)
!       Nview   = 1 Display on screen
!                 2 Stores graphics on .PNG, .PDF, .BMP, .WMF or .PS file
!       NOTE on stored files:
!       *********************
!       DISLIN allows creation of new pages only for PDF and PS formats
!       Therefore, for all other formats (PNG, BMP and WMF), a new file
!       is created for each new figure. The name of the file is then the
!       original Root name followed by the figure number, and the extension
!  
!       CSTR1,  = For the identification title at top of the page 
!       CSTR2     which includes:
!                    "string CSTR1"/Date and Time of the Run/"String CSTR2" 
!       CTIT    = Main figure title 
!       CTIT2   = Second line of the main figure title , followed by frequency
!                 value for Hazard curves for each spectral frequency     
!       Ix      = Index for the X axis:(=1)Accelerations )m\s\s)
!                                      (=2)Velocity (m\s)
!                                      (=3)Displacement (m)
!       IxSCL   = Index for X-axis scaling:
!                                      (=1) X-axis is LINear
!                                      (=0) X-axis is LOGarithmic
!       Iy      = Index for the Y axis:(=1)Prob of exceedance
!                                      (=2)Return Period (yr)
!       jwdt    = Selection of Date and Time page header, and page number
!               = 1, YES
!               = 0, NO Date and time page header, and NO page number
!       N       = Number of points in each curve to be plotted (same for all)
!       Nameplothaz = Root name of the plot file, to which is added the
!                 selected extension (.png, .pdf, ...)
!       Nc      = Number of curves
!       NcMax   = Max number of curves
!       Nf      = Max number of points on X axis
!       Nportrait   = 1 plot in portrait layout
!                     2 plot in landscape layout
!       Nscreen = For Nview=1 only (Screen Display), counts number of frames 
!                 displayed. This number runs through the entire application
!       Nwidth1 = Width of the major lines (Nwidth=15 default)
!       Nwidth2 = Width of other lines (good number is Nwidth2=10)
!       Nview   = Selects the screen or file format (See above note)
!       pagenumber  = For (Nview.NE.1 (storing on file'NamePlotHaz'), keeps
!                 a counter of number of pages.
!       XA(j)  = Array of max dimension (NcMax) of X values for Nc curves
!       Y(i,j)  = Array of max dimension (Nf,NcMax) of Y values for Nc curves     
!                                      
      USE DISLIN
      IMPLICIT NONE
      
      integer :: IxSCL,jwdt,k,kXmax,kXmin,Nc,Nc1
      integer :: N1,N11,N12,N2,N21,N22,N3,N31,N32,Nf 
      integer :: NcMax,newfile,Nnonzero 
      integer :: Nscreen
      integer :: pagenumber !Counter for the pages being plotted
                            !This counter is in absolute terms  
      INTEGER, dimension(NC) :: iflagplot
      
      CHARACTER (LEN=256):: CBUF
      CHARACTER (LEN=41) :: CSTR1
      CHARACTER (LEN=20) :: CSTR2
      CHARACTER (LEN=13) :: CTIT, CTIT2,CTIT3 
      CHARACTER (LEN=60) :: CTIT4
      CHARACTER (LEN=5)  :: NameEXT
      CHARACTER (LEN=14) :: NamePlotHaz,NamePlotHaz1
      CHARACTER (LEN=12) :: NameRoot
      CHARACTER (LEN=256):: STRING
      CHARACTER (LEN=1), DIMENSION(256)  :: Alpha1,Alpha2,Alpha3
      CHARACTER (LEN=5), DIMENSION(3) :: CLAB = (/'LOG  ','FLOAT','ELOG '/)
      CHARACTER (LEN=7), DIMENSION(8) :: KOLOR
      CHARACTER (len=16),DIMENSION(NcMax) :: NameLegends,NameLegends1
      CHARACTER (LEN=21),DIMENSION(3) :: TitleX
      CHARACTER (LEN=25),DIMENSION(2) :: TitleY
      INTEGER :: I,iX,iY,j,N,Nportrait,Nview,NWIDTH1
      INTEGER :: NX,NYA,NYLABEL
      REAL    :: Xmax,Xmax0,Xmax1,Xmin,Xmin0,Xmin1,XNX,XP,Xstep
      REAL    :: Ymax,Ymax1,Ymin,Ymin1
      REAL    :: z1,z2
      REAL, DIMENSION(Nf)             :: XA,YA
      REAL, DIMENSION(Nf,NcMax)       :: Y,Y2
!
 

      KOLOR(1)  = 'RED'
      KOLOR(3)  = 'GREEN'
      KOLOR(7)  = 'BLUE'
      KOLOR(4)  = 'CYAN'
      KOLOR(5)  = 'ORANGE'
      KOLOR(6)  = 'MAGENTA'
      KOLOR(2)  = 'WHITE'
      KOLOR(8)  = 'YELLOW'

      TitleX(1) = 'Acceleration (cm/s/s)'
      TitleX(2) = '   Velocity (cm/s)   '
      TitleX(3) = '  Displacement (cm)  '
      TitleY(1) = 'Probability of Exceedance'
      TitleY(2) = '    Return Period (yr)   '

    
      
!Set file creation code
      newfile = 0
      if (NameEXT .eq. 'PNG')  then
          newfile = 1
      end if
      if (NameEXT .eq. 'BMP')  then
          newfile = 1
      end if
      if (NameEXT .eq. 'WMF')  then
          newfile = 1
      end if     
      
!Create the second line figure title
      write (STRING,*) CTIT2
      call AlphaN (Alpha1,N1,N11,N12,STRING)
      write (STRING,*) CTIT3
      call AlphaN (Alpha2,N2,N21,N22,STRING)
      write (CTIT4,"(256a1)") (Alpha1(i),i=1,N1),' ',(Alpha2(j),j=1,N2)
      
!Create the full name of the plot file, including the extension
      if (Nview.gt.1) then
          write (STRING,*) NameRoot
          call AlphaN (Alpha1,N1,N11,N12,STRING)
!          write (STRING,*) Cview(Nview)
          write (STRING,*) NameEXT
          call AlphaN (Alpha2,N2,N21,N22,STRING)
          print *,'PlotHaz-146:NameRoot,NameEXT=',NameRoot,NameEXT
          print *,'Alpha1=',(Alpha1(i),i=1,N1),'  ',N1
          print *,'Alpha2=',(Alpha2(j),j=1,N2),'  ',N2
          if (newfile.eq. 0)  then
              Write (NamePlotHaz,"(256a1)")      &
              (Alpha1(i),i=1,N1),'.',(Alpha2(j),j=1,N2)
          else
              write (STRING,*) pagenumber
              call AlphaN (Alpha3,N3,N31,N32,STRING)
              Write (NamePlotHaz,"(256a1)")      &
              (Alpha1(i),i=1,N1),(Alpha3(k),k=1,N3),  &
              '.',(Alpha2(j),j=1,N2)
          end if
      endif
      
!Remove blank spaces from PDF file name
      call RemoveBlankSpaces ( NamePlotHaz, NamePlotHaz1)

!Determine the min and max of each axis
!   Both axes are logarithmic:
      Xmin0 = 1.e10
      Xmax0 = 0.
      Ymin = 1.
      Ymax = 0.

      do i = 1,N

        Xmin0 = AMIN1 (Xmin0,XA(i))
        Xmax0 = AMAX1 (Xmax0,XA(i))
        !print *, 'N, Nc =', N,Nc
        do j = 1,Nc
            !print *,'Y(i,j)',Y(i,j)
            if (Y(i,j).gt. 0.)  then
                Ymin = AMIN1 (Ymin,Y(i,j))
                Ymax = AMAX1 (Ymax,Y(i,j))
                kXmax = i
            end if
        end do
     !stop
      end do

      kXmin = 1
      do i = N, 1, -1
           do j = 1,Nc
                if ((Y(i,j).gt. 0.) .and. (Y(i,j).lt.0.999999999)) then
                    kXmin = i
                end if
            end do
      end do
      if (kXmin .gt. 1)  kXmin = kXmin - 1      
      Xmin0 = XA(kXmin)
      Xmax0 = XA(kXmax)

      if (IxSCL .eq. 1)  then
          !X-axis is LINear
          Xmin = 0.
          Xmax = Xmax0
        else
          !X-axis is LOGarithmic
          Xmin = ALOG10 (Xmin0)
          Xmax = ALOG10 (Xmax0)
      end if
      Xmin1= INT (Xmin)
      Xmax1= INT (Xmax)

      if (Xmin1 .ne. Xmin)  then
        if (Xmin.lt.0.) then
            Xmin = Xmin1 - 1
        end if            
      end if

      if (Xmax1 .ne. Xmax) then
        Xmax = Xmax1 + 1.
      end if 
      !Xmax = Xmax1
           
      Ymin = ALOG10 (Ymin)
      Ymax = ALOG10 (Ymax)
      Ymin1= INT (Ymin)
      Ymax1= INT (Ymax)
      if (Ymin1 .ne. Ymin) then
        Ymin = Ymin1 - 1.
      end if      
      if (Ymax1 .ne. Ymax) then
        if (Ymax.lt.0.) then
            Ymax = Ymax1
        else
            Ymax = Ymax1 + 1.
        end if
      end if
!--------------------------------------------------------------------------
!Plotting initializations:
      
      if (Nview .eq. 1) then    
          !Display on screen
          CALL METAFL(NameEXT) 
          CALL SCRMOD('REVERS')
          CALL SETPAG('DA4L')   !Landscape
          if (Nportrait.lt.2) THEN
              CALL SETPAG('DA4P')   !Portrait
          END IF      
          CALL DISINI()
          CALL PAGERA
      else
          if (pagenumber .eq.1) then
              CALL METAFL ('PDF')   
              !Choice of output file (.png, .pdf,....)
              CALL SETFIL (NamePlotHaz1)!Name includes extension (ex: .PDF)
              CALL SCRMOD('REVERS')
              CALL SETPAG('DA4L')   !Landscape
              if (Nportrait.lt.2) THEN
              CALL SETPAG('DA4P')   !Portrait
              CALL DISINI()
              CALL PAGERA
              END IF  
          else
              CALL NEWPAG()
          endif       
      endif
     
!      CALL CLRMOD ('FULL')
      CALL SETCLR (255)
      CALL PAGERA()
!Fonts
      CALL HWFONT()

      CALL COLOR('FORE')

!Define Axes 1
      CALL AXSLEN(1400,1700)
      CALL NAME(TitleX(iX),'X')
      CALL NAME(TitleY(iY),'Y')
      if (IxSCL .eq. 0)  then
          CALL AXSSCL('LOG','X')
        else
          CALL AXSSCL('LIN','X')
      end if 
      CALL AXSSCL('LOG','Y')

!Figure Title
      CALL TITLIN (CTIT,2)
      CALL TITLIN (CTIT4,4)

!Define Axes 2
        NYA=2400
        NYLABEL = 1000
        CALL LABDIG(-1,'XY')
        CALL AXSPOS(500,NYA)
        CALL TICPOS ('REVERS','XY')
       
        if (IxSCL .eq. 0)  then
            CALL LABELS(CLAB(1),'XY')
            CALL GRAF(Xmin,Xmax,Xmin,1.,Ymin,Ymax,Ymin,1.)
          else
            CALL LABELS(CLAB(2),'X')
            CALL LABELS(CLAB(1),'Y')
            z1 = INT(ALOG10(Xmax))
            z2 = 10.**z1
            Xstep = z2 / 2.
            if (z1 .gt. 5.)  then
            Xstep = z2
            end if
            CALL GRAF(Xmin,Xmax,Xmin,Xstep,Ymin,Ymax,Ymin,1.)
        end if
        CALL GRID (2,2)

!Print Header at top of figure page
          CALL HEIGHT(60)
          CALL TITLE()
          CALL HEIGHT(30)
          if (jwdt.eq.1)  then
              CALL PAGHDR (CSTR1,CSTR2,4,0)
          end if
          
 !Plot Nc Hazard Curves
 !      Get rid of curves with only 1 or less nonzero Y value
        iflagplot = 0
        Nc1       = 0
        do j = 1, Nc
            Nnonzero = 0
            do i = 1,N
                if (Y(i,j) .gt. 1.e-10)  then
                    Nnonzero = i
                end if
            end do
            if (Nnonzero .gt. 1)  then
                iflagplot(j) = 1
                Nc1 = Nc1 + 1
            end if
        end do
        !There are only Nc1 non zero curves
        !Shift all non zero curves and legends into first Nc1 slots
        Nnonzero = 0
        do j = 1, Nc
            if (iflagplot(j) .eq. 1)  then
                Nnonzero = Nnonzero + 1
                !Shift curves
                do i = 1,N
                    Y2(i,Nnonzero) = Y(i,j)
                end do
                !Shift legends
                NameLegends1(Nnonzero) = NameLegends(j)
            end if
        end do
        
 !      First curve in RED (for MEAN or MEDIAN)
        do j = 1, Nc1
            Nnonzero = 0
            do i = 1,N
!                 XA(i) = X(i,j)
                 YA(i) = Y2(i,j)
                 if (YA(i) .gt. 1.e-10)  then
                     Nnonzero = i
                 end if
            end do
                k = 1 + modulo(j-1,8)
                CALL COLOR (KOLOR(k))
                CALL LINWID (NWIDTH1)
            if (j.GT.8) then 
                CALL DASHL    
            endif
            CALL CURVE (XA,YA,Nnonzero)
        end do
        CALL SOLID ()
        
!Legend statement
!        CALL LINWID (20)
        CALL HEIGHT (40)
        CALL COLOR ('FORE')
        !Nc lines of Legend:
        CALL LEGINI(CBUF,Nc1,20)   
!        NX = 1269
        XNX = Xmin + 0.4*(Xmax-Xmin)
        NX  = NXposn(10**XNX)
!        CALL LEGPOS(NX,NYPOSN(10**Ymax))
        do i = 1,Nc1
            CALL LEGLIN (CBUF,NameLegends1(i),i)
        end do
!        CALL LEGTIT (Legtitle)
        CALL LEGEND(CBUF,7)
        CALL LINWID (1)
        
!Add a page number
        CALL HEIGHT (30)
        if (jwdt.eq.1)  then
           CALL MESSAG('Page: ',1800,2800)
           if (Nview .eq. 1)  then
                XP = FLOAT(Nscreen)
                CALL NUMBER(XP,-1,1900,2800)
                Nscreen = Nscreen + 1
            else
                XP = FLOAT(pagenumber)
                CALL NUMBER(XP,-1,1900,2800)
                pagenumber = pagenumber + 1
            end if
        end if
        
      CALL ENDGRF()
             
      if ((Nview.eq.1) .or. (newfile.eq.1))  then
        CALL DISFIN()
      endif
      

      RETURN
    
      END SUBROUTINE PlotHazCurves
#endif
