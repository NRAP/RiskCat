     subroutine synhaz (damp,freq,INgm,mopass,nfreq,OUTgm,   &
                        PARS,rigidity,synflag,synflagRS,synpeak,SynSwitch)

!     mopass      = the seismic moment of the simulated earthquake
!     synpeak     = the calculated peak of the time-series at the site
!         if synflag= 1 synpeak is PGA - acceleration (cm/s/s)
!         if synflag= 2 synpeak is PGV - velocity       (cm/s)
!         if synflag= 3 synpeak is PGD - displacement     (cm)


!     Hazard Site where motion is synthesized:
!     ****************************************
!     (point where the synthesized ground motion is calculated.)
!     SynStation  = Name of Hazard Site
!     SynStationX = Longitude (degrees) 
!     SynStationY = Latitude. .....
!     SynStationZ = depth in km, positive down
!
!     Observation point where GF are calculated, (from file "sources_M6.0):
!     *********************************************************************
!     GFobsName   = Name of GF recording (or calculated) station
!     GFobsxy(i)  = Location of recording station. Long for i=1, Lat for i=2.
!     Greens Functions will be expected to be time-series in Velocity (cm/s)
!     
       use ModuleRSQSim
       use ModuleSYNHAZ
       
!
! *******************************************************************************
! program modified form EMPSYN (hutchings, 2002), see EMPSYM for details
!
! All parameter dimensions are done in module file "ModulesSYNHAZ.f90"
!
      implicit none

!     Make all variable non-implicit
!     ******************************
      INTEGER :: i1,i,iasp,ichar,icplt,idel,idels
      INTEGER :: ierr,ifreq,ii,ijne,ik,ikk1,ill1
      INTEGER :: INgm,ip,ipa,is,iss,itk,ixy
      INTEGER :: j,j2,j3,ji,jjk,jjkk,jk,jxnp
      INTEGER :: k,k2,kanamori,kasp,ki,kk,kki,kkii,kkiii,kk1,kr,ks
      INTEGER :: l
      INTEGER :: lentemp1,lentemp2,lentemp3,lentemp4,lentemp5
      INTEGER :: llkk,lt,maxsyn,mp,mp1,ms,ms1,m1
      INTEGER :: n,ncomp,nfreq,Nchar,nct,ne,nfsp,np,npGF,npSYN
      INTEGER :: nr,nrr,nt,nt3,nt4,nt1,nt2,OUTgm,synflag,synflagRS

      REAL :: aa,aaa,aainp,aains,aamo,aangi,a1,a2,a3,a4,a5,a6,a7,a8
      REAL :: aarea,absacc,adis,ah,ah1,ainp,ains,ains1,ajk,alat,alat1,alon
      REAL :: alon1,amo,amo1,amo11,angi,angii,ang4,ang44,ang5,ang55
      REAL :: ann,ann1,ann2,ann3,ann4,aphp,aphs,apz,aradius,aradiusmeter,armo
      REAL :: aslp,aspkk
      REAL :: avgslp,azinv,azm,azmm,azm1
      REAL :: bazm,bazm1,bazm2,bb,bbb,bdis,bdp1
      REAL :: bdpp,bhdis,bhh,blatt,blonn,bm22,bstrr,bsv1,bsvv,bzm1,bzmm
      REAL :: cplt,damp,del,delss,den,denom,d1,d2,dipdis11
      REAL :: dis,dishsk,dis1,dis2,diss,dp,dp1,dr,dt,durtim,durtim1
      REAL :: ecc,edis,ediss,ela1,elaa,eladis,elpp,elsh,elsv,ers,ezm
      REAL :: ezm1
      REAL :: facc,facmo,fac4,fc,fc2,ffh11,ffpp,ffsh,ffsv
      REAL :: fh,filGF,filow,filhi,fl1,fl2,fl3,fl4,flat,flat11,flat22
      REAL :: flon,flon11,flon22
      REAL :: gc,gh1,gh2,gh3,gh4,gl1,gl2,gl3,gl4,glat1,glat2,glat3,glat4
      REAL :: glon1,glon2,glon3,glon4
      REAL :: hdis,hdisss,hdis1,hdis2,hdiss
      REAL :: mopass,omega
      REAL :: pi2,synpeak,prctasp,q
      REAL :: rasp,rd,rdis1,rgd,rhdis,rhom,rigidity
      REAL :: r1,r2,r3,rhon,rot2,rot3,rp
      REAL :: rriss,rslpp,rstm,rtmhsk,rv
      REAL :: sazm,sazm1,sazmm,sdpzz,sdur,sdur1,selm,sfo,signi,timm1
      REAL :: slat,slatt,slon,slonn,slpdis,spp,ss,std,std0,mag
      REAL :: stdd,std11,stdrs,stk,stk1,stkr,stkrr,sum,sumdispl,sumristm
      REAL :: sumslipv,sv1
      REAL :: tim,tim0,tim1,titk,trarr,tratt,trig,tt,tt1,tt2,ttvd
      REAL :: totalslp
      REAL :: vh,vp,vpm,vqp,vqs,vr,vrh,vrs,vs,vsp,vss
      REAL :: ws,ww,x0,x1,x2,xdis,xh,xlat,xlat1,xlon,xlon1,xtemp,xxa,xxx
      REAL :: xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, xx12, xx13
      REAL :: y0,y1,y2,y3,z0,zm



!     Old parameter definitons:
!     *************************
 
      character (LEN=3)   :: acmp1, acmp2, acmp3, asp, asyn, avr
      CHARACTER (LEN=3)   :: comp1, decon, GFtype, lsp, mosrc
      CHARACTER (LEN=3)   :: rdc, rec, rsq, slm, slp, SynSwitch
      CHARACTER (LEN=4)   :: cspp
      CHARACTER (LEN=5)   :: asbb
      CHARACTER (LEN=20)  :: asb
      CHARACTER (LEN=40)  :: evnm
      CHARACTER (LEN=80)  :: GFformat,region, alpha, nnsf2
      CHARACTER (len=5)   :: dummy
      CHARACTER (LEN=120) :: GFpath, nnsf1
      CHARACTER (LEN=256) :: GFpath1
      CHARACTER (LEN=80), dimension(20)  :: aspf     
      CHARACTER (LEN=1),  dimension(80)  :: fln

      INTEGER, dimension(20) :: icplta

      REAL                :: mo, mo1, ma11, ma, mb, maxslp, maxslpvel
      REAL                :: po
      REAL                :: Mxx, Myy, Mzz, Mxz, Mxy, Myz
      REAL, dimension(20) :: asptime,asprstm,aspslpvel,aspmo,asparea,aspstress
      REAL, dimension(20) :: cplta
      REAL, dimension(100):: freq, PARS

! MT (next 2 lines) for IRM
      REAL    :: avrdispl, avrristm, avrslipv
      REAL    :: maxdispl, mindispl, maxristm, minristm,minslipv, maxslipv
      REAL    :: rand_max,rand_min

      COMPLEX :: elac1, elac2, eslp, etraz, etrar, etrat
      COMPLEX :: rpfac,rrfac,rtfac
      COMPLEX :: tr1, tr2, tr3, tz1, tz2, tz3, zphp, zphs 



!  added dummy variables for asperity file readins 
 
      CHARACTER (LEN=4) :: cdumm1
      CHARACTER (LEN=3) :: cdumm2, cdumm3
      INTEGER           :: idumm1, idumm2
      INTEGER           :: iseed
      REAL              :: fdumm1,fdumm2,fdumm3,fdumm4,fdumm5,fdumm6,fdumm7
      REAL              :: rand, theta
      REAL (KIND=8)     :: fh1

!
!*******************************************************************************
!
      dtGF = 0.01
      dt = dtGF
      i = SynNp(1)

!     START CODE:
!     ***********
      facmo = 0.
      tim1  = 0.
      theta = 0.
      rand_max = 0.9
      rand_min = 0.1
      iseed = 123456789
      call srand(iseed)
!
! reset idel - delay for synthesized traces
      idel = 0
! input from RsqSim (yes)
      rsq = 'yes'
!
! read moment directly from sources file 
      mosrc = 'yes'
!
! from sources file: ma1 is magnitude, bm2 is moment, bm1 is calculated from magnitude
      !open(unit=11,file='synout')
!
      pi2 = 6.283185308
! seismic quality quotient
      gc = (1.-1./297.)**2
      dr = pi2/360.
      dishsk = 0.0
      maxslp = 1000.0
      maxslpvel = 500.0
      aarea = 0.0
      totalslp = 0.0

      icplt = 1
      cplt = 0.0
      rtmhsk = 1.0
 
! initialization 
      do 29 ii = 1,maxel
      lla(ii) = 0
      llb(ii) = 0
 29   ict(ii) = 0

      do 150 ii = 1,maxdim
      trcz(ii) = 0.0
      trcr(ii) = 0.0
      trct(ii) = 0.0
      trz(ii) = cmplx(0.0,0.0)
      trr(ii) = cmplx(0.0,0.0)
      trt(ii) = cmplx(0.0,0.0)
      tzpt(ii) = cmplx(0.0,0.0)
      tzsz(ii) = cmplx(0.0,0.0)
      tzsr(ii) = cmplx(0.0,0.0)
 150  tzst(ii) = cmplx(0.0,0.0)
!
! filter for convolved trace
! for no filtering of input data filow = 0.0 and filhi = 100.0
      lt = ifix(.5/dt)
      nt1 = lt + 1
      nt2 = maxsyn - lt
      filow = 0.0
      filhi = 100.0
!  301 format(//)

      GFtype = 'FKR'
 
  304 format(1x,a5,1x,a3,1x,a3,1x,f5.3,1x,f5.2,1x,f5.2,1x,f5.2,1x,         &
      f6.2,1x,f5.2,1x,f5.2,1x,i4)
      slp = 'ttm'
      slm = 'ksv'
      asp = 'yes'
      if(slp.ne.'spc'.and.slp.ne.'ttm') then
      print *,'need to specify spc or ttm  STOP'
      stop
      endif
      !write (11,*) 'Rupture Model from SIMHHAZ'
!  following for Region
      region = synregion
!
      call regionall(region,apz,vsp,vpm,zm,q,ma,mb)
!
! Hypocenter of synthesized earthquake. Depth of events positivr downward and in km
      alon = SynXYZinit(1)
      alat = SynXYZinit(2)
      ah   = - SynXYZinit(3)
! Hazard site name and location (Lat, Long)
      asb  = SynStation
      slat = SynStationY
      slon = SynStationX !Depth of Eq must be posistive
! Load center coordinates, and number and area of Sub-elements from Module ModuleSYNHAZ
      kk = NSynSubElt
      do i = 1, kk
         !Constant properties are taken from the fault-file: Index  i1
         !Properties of each small earthquake taken from event file: i
         !Mapping done in routine GMHR
          i1   = SynEqSubMap(i)  ! Index of the element in the fault file
          xx1  =  XYZsub(i1,1)
          xx2  =  XYZsub(i1,2)
          xx3  = -XYZsub(i1,3) !Depth must be positive
          if (xx3 .lt. 0.)  then
             xx3 = -xx3
             end if
          !xx4 is the area of the element in km^2,
          !but passed from GMHR2 in m^2
          !must be converted to km^2
          xx4  = SynAreaSub(i1)/1000000.
          xx5  = SynRise(i)
          xx6  = SynSubSlip(i)/10.
          xx7  = SynSubRake(i1)
          xx8  = SynSubM0(i)
          !xx9  = SynRgd(i)
          xx9  = rigidity
          xx10 = SynSubStk(i1)
          xx11 = SynSubDip(i1)
          ! convert stres-drop from MPa to Bars
          xx12 = SynStdp(i)*10.19
          xx13 = SynSubTim(i)

          rlon(i)  = xx1
          rlat(i)  = xx2
          rh(i)    = xx3
          rela(i)  = xx4
          ristm(i) = xx5
          rslp(i)  = xx6
          rsv(i)   = xx7
          ramo(i)  = xx8
          rgd1(i)  = xx9/1.0e10
          rstk(i)  = xx10
          rdip(i)  = xx11
          rstd(i)  = xx12
          slptim(i)= xx13

          !Element i:
          go to 9999
          print *, '----------------------------'
          print *, 'Sub-Element number:', i,' of ',kk
          print *, 'rlon  =',rlon(i)
          print *, 'rlat  =',rlat(i)
          print *, 'rh    =',rh(i)
          print *, 'rela  =',rela(i)
          print *, 'ristm =',ristm(i)
          print *, 'rslp  =',rslp(i)
          print *, 'rsv   =',rsv(i)
          print *, 'ramo  =',ramo(i)
          print *, 'rgd1  =',rgd1(i)
          print *, 'rstk  =',rstk(i)
          print *, 'rdip  =',rdip(i)
          print *, 'rstd  =',rstd(i)
          print *, 'slptim=',slptim(i)
9999      continue
      end do


!Slip for Sub-element i is "SynSubSlip(i), passed through ModuleSYNHAZ
      if (kk .gt. maxel) then
          write(11,*) 'WARNING! ', kk, 'is too many elemental areas'
          write(11,*) 'maximum allowed is ', maxel
          stop
      endif

! Mo of main event
      mo = mopass

! range of rupture velocity
! fixed rupture velocity?
      avr = 'no'
      vr = 1.0
      vh = 1.0
      vrh = 1.0
      iasp = 3
      rasp = 1.0

      mo1 = mo
      if(slm.eq.'hsk'.and.slp.eq.'spc') rtmhsk = 1.0
      if(slm.eq.'hsk'.and.slp.eq.'ttm') rtmhsk = 0.0
! if slm = ksv, stress drop initially set to 100.0 bars, then adjusted
! to give requested moment.
      if(slm.eq.'ksv') std = 100.0
      if(slm.eq.'hsk') std = 1.0
! records in acceleration (ACC), velocity (VEL), or displacement (DIS)'
      rec = 'ACC'
      if(rec.ne.'ACC'.and.rec.ne.'VEL'.and.rec.ne.'DIS') then
      print *,'ACC, VEL, DIS'
      stop
      endif
      sazm = 90.0
      rdc = 'yes'
      dt = Syndt(1)

!***************************JBS Add****6-19-14********************************
      dt = dtGF
!***********************************************************************
      comp1 = 'upp'
      sazmm = sazm + 90.0
      if(sazmm.ge.360.) sazmm = sazmm - 360.
      maxsyn = ifix(tsyn/dt)
      if (maxsyn.le.32768) n = 15
      if (maxsyn.le.16384) n = 14
      if (maxsyn.le.8192) n = 13
      if (maxsyn.le.4096) n = 12
      if (maxsyn.le.2048) n = 11
      if (maxsyn.le.1024) n = 10
      if (maxsyn.le.512) n = 9
       if (maxsyn.gt.32768) then
       print *,'maxsyn.gt.32768 - stop'
       stop
       endif
      ks = 2**n
       if (ks.gt.maxdim) then
!021313      write(11,*) 'spectral limits maxdim too small for time length of t
!021313     *tsyn and computed ks'
      stop
       endif
   47 format(f8.4,1x,f9.4,1x,f5.2,1x,e8.3,1x,i6,1x,f7.4,1x,f5.1,1x,f4.1,   &
      1x,f6.1,1x,f6.1,1x,a3,1x,i3,1x,a3,1x,i6,1x,f8.3,1x,f5.1,1x,f5.1)
!  106 format(a12,3x,f8.4,2x,f9.4,2x,f5.2,1x,f3.1,1x,e12.4,1x,f4.1,2x,      &
!      f4.0,3x,f3.0,2x,f5.0)
!  107 format(/,'source events       location              mag     moment', &
!      '    fc',2x,'strike  dip    sv')
  108 format(//'**** final values ***********    ',i5,' elements'          &
      /'moment:      ',e10.3,' (dynes.cm)',                               &
      /,'stress drop:    ',f7.1,' (bars)',                                 &
      /,'average slip:   ',f7.3,' (cm)',                                   &
      /,'rupture radius :',f7.1,' (m)',                                    &
      /,'maximum rise time ',f10.3,' (sec)')                                
  109 format('peak acc=        ',e10.3,' (cm/sec/sec)'/)
  110 format('peak vel=        ',e10.3,' (cm/sec)'/)
  111 format('peak dis=        ',e10.3,' (cm)'/)
!  124 format(/,'total number of elemental areas is ',i6,                   &
!      4x,'maximum radius ',f6.2,' km')
!  126 format(/,'background moment, amo=',e13.6,' stress drop ',f7.1/,      &
!      'normalized to mo= ',e13.6,' stress drop ',f7.1/,'displacement nor', &
!      'malized'/)
!  127 format('total faulting duration: ',f7.2//,                           &
!      'hypocenter:',15x,'ristm = ',f6.2,'     disp= ',f7.2,' cm'/,         &
!      'station source pulse duration: ',f7.2)
  147 format('station (e.g.f) instrument orientation for up on traces:',   & 
      /,'traz= UP'/,'trar= N',f5.1,'E'/,'trat= N',f5.1                     &
      ,'E'//,'programs rotates trar(i) into radial component and trat(i)', &
      ' into transverse component (trar + 90 deg)'/)

!  306 format(/'you need to select a density of roughness and the rise ti', & 
!      'me.'/,'the density of roughness is determined by choosing an inte', &
!      'rval'/,'of elements that will have roughness, i.e. 3 means every ', &
!      'third.'/,'this will distribute randomly on the fault because of ',  & 
!      'the radial'/,'arrangement of elements.  the hypocenter is not an',  &
!      'an asperity.'//,'what is the interval?')
!  307 format(//,'rise time for roughness is a random percentage (0.1<0.9', &
!     ')'/,'of neighboring elements.  This is applied so that the full'/,   &
!      'displacement is reached at the same time as neighboring elements.'/)
!  308 format(//,'do you want to include asperities to rupture.  This'/,    &
!      'requires reading a number of files with asperity distributions.',   &
!      //,'do you want to include asperities?')
!  309 format(/'You can choose a Kanamori or Haskell asperity model.'//,    &
!     'Both models add additional slip from a smaller circular rupture'/,   &
!     'to the larger event. The moment of the larger event is maintained.', &
!      /,'The rise times of the larger event are also maintained. Ther',    &
!      'efore'/,'the stress drop is increaed over the asperity.'//,'The ',  &
!      'Hartzell asperity model ruptues at the same ruptue time as the'/,   &
!      'larger event would rupture. The Kanamori asperity model ruptures',  & 
!      'as'/,'a seperate sub-event originating at the center of the ',      & 
!      'asperity and'/,'stops at its edge. It has origin time at what ',    & 
!      'would be the arrival'/,'time of the rupture front'//)
!  310 format(/'You need one contour file for each asperity distribution.', &
!      ' So you'/,'have to run the asperities separetly initially to get ', &
!      'the asperity'/,'files, These files have to have the same element',  &
!      'size as the fault'/,'model used in the program. The names of ',     &
!      'these files are listed in'/,'file: aspfiles'/)
!  414 format(/'two dimensional Haskel rupture model with ramp slip funct', &
!      'ion'//'each element has the same displacement:',f5.1,' cm and ',    &
!      'rise time',f5.2,/'total faulting duration ',f5.2,' seconds.'/,      &
!      'Station source pulse duration ',f5.2)
!  415 format('Haskel rupture model with step slip function'/,              &
!      //'each element has the same displacement:',f5.1/,                   &
!      'total faulting duration ',f5.2,' seconds.'/,'Station',              &
!      ' source pulse duration ',f5.2)
!  416 format(8f9.3)
!  417 format(/,'kostrov slip model with healing'/,'displacement first ',   &
!      i3,' elements (left to right)')
  418 format('ramp displacement function - modified kostrov'/,'slip ampl', &
      'itude and rise time constrained by self-similar kostrov slip')
!  419 format(//'Two dimensional Haskel rupture model with ramp slip func', &
!      'tion'/'each element has same displacement:',f6.1,' cm'/'rise time', &
!      ' =',f5.2,'  total faulting duration ',f5.2/,                        &
!      'station source pulse duration: ',f5.2)
!  499 format(//'Haskel rupture model with step slip function'/,            &
!      'each element has same displacement:',f6.1,' cm, rise time is 0.0',  &
!      'sec'/'total faulting duration ',f5.2/,                              &
!      'station source pulse duration: ',f5.2)

!  422 format('***Synthesis is in time domain****'/,'The slip function is', &
!      ' incremented and fit by delayed stacking step functions'/,'interva',&
!      'l for incrementing slip is dt'/,'synthesized output limited to ',   &
!      i5,' points')  
!  421 format('***Synthesis is in time domain****'/,'Step slip function'/,  & 
!      'synthesis ia incremented by dt'/,'synthesized output limited to',   & 
!      i5,' points')  
!  423 format('***Synthesis is in frequency domain****'/,'The slip functi', &
!      'on is deconvolved with a step function, then convolved with'/,      &
!      'source events')
 ! 426 format('input traces band passed ',f4.1,' to ',f5.1,' Hz'/)
  427 format('input traces not filtered'/)
 ! 431 format('          ***WARNING***'/,                                   &
 !     'spectral dimension is ',i5,' points, and Greens functions are gre', &
 !     'ater than'/,f5.1,' seconds'/'you will get wrap-around in main ',    &
 !     'seismogram')
 
      vrs = 0.75 + rand()*0.25
      vrh = 0.8 + rand()*0.2
!
      !Read shear and healing velocity ratios
      vrs = SynVr
      vrh = SynVh
!
      flat = alat
      flon = alon

      fh = ah
! here, bypass fault description and conversion to Aki & Richards fault attitude
      go to 1115
      ip = 4
       if(ip.gt.ipfl) then
       print*,'Fault file has ip= ',ip,' and SYNHAZ has ipfl= ',ipfl,' STOP'
       stop
       endif

      glon1 =  syncorners(1,1)
      glat1 =  syncorners(1,2)
      gh1   =  syncorners(1,3)

      glon2 =  syncorners(2,1)
      glat2 =  syncorners(2,2)
      gh2   =  syncorners(2,3)

      glon3 =  syncorners(3,1)
      glat3 =  syncorners(3,2)
      gh3   =  syncorners(3,3)

      glon4 =  syncorners(4,1)
      glat4 =  syncorners(4,2)
      gh4   =  syncorners(4,3)

      if(ip.eq.4) go to 117
      if(ip.eq.1) go to 116
      if(ip.eq.3) go to 114
      go to 118
  116 ip = ipfl
      do 115 i = 2,ip
  115 fl(i) = fl(1)
      go to 118
  114 ip = ipfl
      aaa = fl(1)
      aa = fl(1)/2.
      ecc = fl(2)
      ann1 = fl(3)
      ann2 = fl(3)*.01745329
      bb = aa*sqrt(1.0 - ecc**2)
      bbb = 2.0*bb
      do 122 i = 1,ip
      ann = 3.*pi2/4. + ann2 - float(i-1)*pi2/float(ip)
  122 fl(i) = aa*(1.-ecc**2)/(1.+ecc*cos(ann))

!021313      write(11,999) aaa,bbb,ecc,ann1,fl(1),fl(ip/4+1),fl(ip/2+1),
!021313     *fl(ip*3/4+1)
!  999 format('rupture area is ellipitical: major axis ',f4.1,' km, minor',  &
!      ' axis ',f4.1,' km,'/,'eccentricity is ',f4.2,'  angle of perhelion', &
!      ' of major axis clockwise from strike of'/,                           &
!      'fault is ',f5.1,'.  four points on ellipse at 90 deg inteverals ',   &
!      'from 0 deg up dip: '/,f4.1,1x,f4.1,1x,f4.1,1x,f4.1,' km.'/)

      go to 118

  117 ip = ipfl
!     Read the coordinates of segment corners
!     Load 4-corner coordinates from Module ModuleSYNHAZ
!     corners start at the top left and continue counterclockwise
!      g1                                    g4
!        ----------- top of rupture -------->
!       |                                   |
!       |                                   |
!       |                                   |
!       |                                   |
!       |                                   |
!     g2 ___________________________________ g3
!
! convention, RsqSim:
! strike (SynStrike) is from g1 to g4 and and is the vector direction of the of top of
! fault and the x-axis in a left-handed coordinate system, where y is parallel to the surface 
! of the earth, and z is vertically down.  Dip is the rotation angle from vertical 
! to the plane of rupture; positive rotation is counterclockwise looking down the 
! vector (g1,g4). g1, g2, g3, and g4 are always counterclockwise, regardless of the dip.
!
! However, synhaz.f follows the convertion of Aki & Richards: dip is down to the
! right of strike (hanging wall on right looking down strike). If the fault is vertical, 
! then the hanging wall is to the right as you look down strike. 
!
! Also, synhaz works with a coordinae system in the plane of the fault viewed from the
! hanging wall, where x is positive in the direction of the strike and positive y is updip.  
! The origin is at the hypocenter. Sucessive distances from the hypocenter to the edge of 
! the rupture are calculated (or provided) starting direcdtly updip and prgressing 
! clochkwose at equal incremental angles fro 360 degrees.
!
! assuming the fault is a rectangle, calculate fl1,fl2,fl3,fl4, where
!
!      g1                                    g4
!        ----------- top of rupture -------->
!       |       |                           |
!       |      fl1                          |
!       |       |                           |
!       |---fl4-*---------- fl2 ------------|
!       |       |                           |
!       |      fl3                          |
!     g2 _______|__________________________ g3

      call disazm(ezm,azinv,dis,hdis,glat1,glon1,gh1,glat2,glon2)
      gl1 = sqrt(dis**2 + (gh2-gh1)**2)
      call disazm(ezm,azinv,dis,hdis,alat,alon,ah,glat2,glon2)
      gl2 = sqrt(dis**2 + (gh2-ah)**2)
! fl4 = sqrt(gl2**2 - fl3**2)
      call disazm(ezm,azinv,dis,hdis,alat,alon,ah,glat1,glon1)
      gl3 = sqrt(dis**2 + (gh1-ah)**2)
! fl4 = sqrt(gl3**2 - fl1**2)

! gl3**2 - fl1temp =**2 = gl2**2 - fl3**2
! gl1 = fl1 + fl3
! gl3**2 - fl1**2 = gl2**2 - (gl1-fl1)**2
! gl3**2 = gl2**2 - gl1**2 + 2.0*fl1*gl1
      fl1 = (gl3**2 - gl2**2 + gl1**2)/(2.0*gl1)
      fl3 = gl1 - fl1

      call disazm(ezm,azinv,dis,hdis,alat,alon,ah,glat4,glon4)
      gl4 = sqrt(dis**2 + (gh4-ah)**2)
      fl2 = sqrt(gl4**2 - fl1**2)
      fl4 = sqrt(gl3**2 - fl1**2)

      call disazm(ezm,azinv,dis,hdis,glat1,glon1,gh4,glat4,glon4)
      stk = ezm
      dp = 90.0 - rdip(1)
      if(dp.lt.0.0.and.dp.gt.-90.0) then
      call disazm(ezm,azinv,dis,hdis,glat4,glon4,gh4,glat1,glon1)      
      stk = ezm
      dp = 90. + rdip(1)
      xtemp = fl2
      fl2 = fl4
      fl4 = xtemp
      endif

      ann1 = atan2(fl2,fl1)
      ann2 = pi2/4. + atan2(fl3,fl2)
      ann3 = pi2/2. + atan2(fl4,fl3)
      ann4 = .75*pi2 + atan2(fl1,fl4)
      do 119 i = 1,ip
      ann = float(i-1) * pi2/float(ip)
      if(ann.ge.ann4.or.ann.le.ann1) fl(i) = abs(fl1/cos(ann))
      if(ann.gt.ann1.and.ann.le.ann2) fl(i) = abs(fl2/sin(ann))
      if(ann.gt.ann2.and.ann.le.ann3) fl(i) = abs(fl3/cos(ann))
      if(ann.gt.ann3.and.ann.lt.ann4) fl(i) = abs(fl4/sin(ann))
  119 continue
! adjust the fault parameter points to be relative to hypocenter
      fh1 = dble(fh)
  118 call rupsur(flat,flon,fh1,alat,alon,ah,stk,dp,fl,ip,fx,fy,ixy)
      if(ah.eq.0.0) go to 1000
      slpdis = fl(1)
      do 13 i = 2,ip
 13   slpdis = real(min(fl(i),slpdis))
      rh(1)= ah
      rlat(1)= alat
      rlon(1)= alon
 1115 dp1 = rdip(1)*pi2/360.
      stk1 = rstk(1)*pi2/360.
      sv1 = rsv(1)*pi2/360
!
      call disazm(ezm,azinv,dis,hdis,alat,alon,ah,slat,slon)

! velocity model is set in program, vsp=surface p-vel, vpm=velocity at half-
! space, and apz=depth dependence, zm=depth to half-space

      ne = 0

! set parameters for source events

!      print *,'Enter path (only) for input Greens functions data files'
      if(GFtype.eq.'FKR') GFpath = '../Geysers.FKRs/'
      !if(GFtype.eq.'FKR') GFpath = '../FKRs/'
      if(GFtype.eq.'EGF') GFpath = '../EGFs/'

!      print*,'path and file name for sources of Greens functions'
!      SRCpath = '../SOURCES/GF_sources_radial'
!      print *,'component suffixes for the traces for Greens functions ob
!     *tained from sources files'
!      print *,'first component has to be the vertical trace acmp1
!      print *,''
!      print *,'do you want to evenly distribute the source events'
      asyn = 'no'

!     Do not need to open file since we load from arrays read in LoadGF()
!      open(unit=7,file=SRCpath)

! amo_syn is only used for synthetic Green's functions and assumes all were
! calculated for the same moment
!      read(7,*) asbb,slatt,slonn,sdpzz,acmp1,acmp2,acmp3

!     Load the values from values stored in Modulesynhaz. 
!     This describes the Station. Since we have ONE Station and multiple
!     source, its name is always"Station"
      asbb  = GFobsName
      slatt = GFobsxy(2)
      slonn = GFobsxy(1)
      sdpzz = GFsdpzz
      acmp1 =GFacmp1
      acmp2 =GFacmp2
      acmp3 =GFacmp3

! 313  format(/,'moment read in directly from sources file')
      call indexc(GFpath,lentemp1,120)
      call indexc(asbb,lentemp2,5)
      decon = 'no'
      if(GFtype.eq.'EGF') then           !............................1
!021313      write(11,107)
!JBS      print *,''
!JBS      print*,'do you want to deconvolve EGFs with a Brune source'
!      read(5,*) decon
!JBS      print *,''
      do 10 i = 1,maxev                  !............................2
!      read(7,*,end=11) evnm,cspp,blatt,blonn,bhh,ma11,bm22,fc2,bstrr,
!     *bdpp,bsvv

!      Load values from arrays GF...  read in routine LoadGF()

            !Test to make sure that file name + path is less than 20 characters
            read (GFfile2(i), '(80a1)')  (fln(ichar),ichar=1,80)
            do ichar = 1, 80
                if (fln(ichar) .eq. ' ' )  then
                    exit
                end if
                Nchar = ichar 
            end do
            if (Nchar .gt. 40)  then
                print *, '  GF filename is greater than 20 charaters'
                print *, '  STOP'
                stop
            end if
            write (evnm, '(40a1)' )  (fln(ichar),ichar=1,Nchar)

       !evnm   = GFfile2(i)
       cspp   = GFcspp(i)
       blatt  = GFxyz(i,2)
       blonn  = GFxyz(i,1)
       bhh    = GFxyz(i,3)
       ma11   = GFma11(i)
       bm22   = GFm0(i)
       fc2    = GFfc2(i)
       bstrr  = GFbstrr(i)
       bdpp   = GFbdpp(i)
       bsvv   = GFbsvv(i)

      if(evnm.eq.'') go to 11
      call indexc(evnm,lentemp3,20)
!     the s-p split time can be a sac header variable (i.e. t1 or t0) 
!       or entered directly in the sources file
!       To specify a header variable, enter the variable name
!       in the sources file in place of the time.
      if (cspp(1:1) .eq. 't' .or. cspp(1:1) .eq. 'T') then
         cspt(i) = cspp
         spp = 0.0
      else
         read(cspp,*) spp
         cspt(i) = 'xx'
!JBS         if(spp.lt.1..or.spp.gt.90.) print *,'bad s-p split time'
         if(spp.lt.1..or.spp.gt.90.) go to 1000
      endif
!
! calculate moment from magnitude
      ne = ne + 1
      ma1(ne)  = ma11
      bm1(ne)  = 10.**(ma*ma11 + mb)
      bm2(ne)  = bm22
      if(mosrc.eq.'yes'.or.mosrc.eq.'y') then
      bm(ne)   = bm2(ne)
      else
      bm(ne)   = bm1(ne)
      endif
!      evid(ne) = evnm
      spt(ne)  = spp 
      blat(ne) = blatt
      blon(ne) = blonn
      bh(ne)   = bhh
      bstr(ne) = bstrr
      bdp(ne)  = bdpp
      bsv(ne)  = bsvv
      ffc2(ne) = fc2
!021313      write(11,106) evid(ne),blat(ne),blon(ne),bh(ne),ma1(ne),bm(ne),
!021313     *ffc2(ne),bstr(ne),bdp(ne),bsv(ne)
   10 continue                           !............................2
!   11 close(7)
 11   continue
      if(rsq.eq.'y'.or.rsq.eq.'yes') go to 9998
      else                               !............................1
! following is for FKRPROG
!021313      write(11,*)'     EVID            ELAT      ELON     DPT    DIS   S
!021313     *ST    Mw       Mo'
      call disazm(ezm,azinv,dis2,hdis2,alat,alon,ah,slat,slon)
      hdisss = 10000.0
       ne = 0
       do 20, i = 1, nGFsources
!      read(7,*,end=211) FKRinfo(1,1),FKRinfo(1,2),FKRinfo(1,3),
!     *FKRinfo(1,4),FKRinfo(1,5),FKRinfo(1,6),FKRinfo(1,7),FKRinfo(1,8)

!     Retrieve from values read in LoadGF() 
!      read(FKRinfo(1,1),'(a13)') evnm
!      evnm  = GFfile2(i)
      !Test to make sure that file name + path is less than 20 read
!JBS      characters (GFfile2(i), '(80a1)')  (fln(ichar),ichar=1,80)
!JBS        do ichar = 1, 80
!JBS            if (fln(ichar) .eq. ' ' )  then
!JBS                exit
!JBS            end if
!JBS            Nchar = ichar 
 !JBS       end do
 !     if (Nchar .gt. 20)  then
  !        print *, '  GF filename is greater than 20 charaters'
   !       print *, '  STOP'
    !      stop
     ! end if

!JBS        write (evnm, '(40a1)' )  (fln(ichar),ichar=1,Nchar)

      ne = ne + 1

!JBS        call indexc(evnm,lentemp3,20)

!      Retrieve the remaining info from LoadGF()
       !evid(ne)   = evnm
       xx1       = GFxyz(i,2)
       xx2       = GFxyz(i,1)
       xx3       = GFxyz(i,3)
       xx4       = GFma11(i)
       xx5       = GFm0(i)
       xx6       = GFfc2(i)
       blat(ne)  = xx1
       blon(ne)  = xx2
       bh(ne)    = xx3
       ma1(ne)   = xx4
       bm2(ne)   = xx5
       ffc2(ne)  = xx6

! Strike angle, Dip, Strike Vector from modual, changed
      bstr(ne) = rstk(1)
      bdp(ne) = rdip(1)
      bsv(ne) = rsv(1)
      stk1 = rstk(1)*pi2/360.
      dp1 = rdip(1)*pi2/360.
      sv1 = rsv(1)*pi2/360

      bm1(ne) = 10.**(ma*ma1(ne) + mb)
      bm(ne) = bm2(ne)
      xlat = blat(ne)
      xlon = blon(ne)
      xh   = bh(ne)
      call disazm(ezm,azinv,dis,hdis,xlat,xlon,xh,slatt,slonn)
      hdis=sqrt(dis**2 + (bh(ne)-sdpzz)**2)
!630jbs      call indexc(asbb,lentemp5,5)
!630jbs      pathname(ne) = GFpath(1:lentemp1)//asbb(1:lentemp5)//'_'//evnm(1:lentemp3)
!630jbs      call indexc(pathname(ne),lentemp4,120)
      xh = bh(ne)
      call linvel(xh,dis,rp,angi,ainp,apz,vsp,vpm,zm,spp)
      spt(ne) = spp*1.76
! find closest event for point source solution
      hdiss = abs(hdis-hdis2)
      if(hdiss.lt.hdisss) then
      hdisss = hdiss         
      ijne = ne
      endif
!021313      write(11,212) evid(ne),blat(ne),blon(ne),bh(ne),hdis,spt(ne),
!021313     *ma1(ne),bm(ne)
 20   continue
! 212  format(a20,1x,f8.5,1x,f9.5,1x,f5.2,1x,f6.2,1x,f5.2,1x,f5.2,1x,e11.4)
      endif   
                           !............................1
      if (rsq.eq.'y'.or.rsq.eq.'yes') goto 9998

      kasp = 1

! factors to get latitude longitude of elements from r3:
      trig=sqrt(1.0-(6.76866e-3*sin(alat*pi2/360.)**2))
      rhom=6.3782e+3*(1.0-6.76866e-3)/trig**3
      rhon=6.3782e+3/trig

! compute time shift for main seismogram: seconds from origin (sfo)

      call disazm(ezm,azinv,dis2,hdis2,alat,alon,ah,slat,slon)
!021313      if(ah.gt.zm) write(11,*) 'hypocenter in mantle, linvel increments 
!021313     *for ray parameter at 0.1 km intervals'
      call linvel(ah,dis2,rp,angi,ainp,apz,vsp,vpm,zm,tt2)

! compute time shift for synthesized seismogram
      xlat = rlat(1)
      xlon = rlon(1)
      xh   = rh(1)
      call disazm(azm,azinv,dis,hdis,xlat,xlon,xh,alat,alon)
      hdis = sqrt((ah-rh(1))**2 + dis**2)
      yy(1) = (ah-rh(1))/sin(dp1)
      xxa = (hdis**2 - yy(1)**2)
      if(xxa.lt.0.0) xxa = 0.0
      xx(1) = sqrt(xxa) 
      stkr = stk + 90.
      stkrr = stkr-azinv
      if(stkrr.lt.0.0) stkrr = stkr + 360.0
      if(stkrr.ge.180.and.stkrr.le.360.) xx(1) = -xx(1)
!.....................
      adis = hdis2
      eladis = hdis2
      sfo = tt2 - 4.0
      if(sfo.lt.0.0) sfo = 0.0
      eltim(1) = 0.0
      slptim(1) = 0.0
      eazm(1) = ezm
! ridigity eq to vs x 10**11
      vp = vsp + ah*apz
      if(ah.gt.zm) vp = vpm
      vs = vp/1.76
      if(avr.ne.'y'.and.avr.ne.'yes') vr = vs*vrs
      den = (vp-0.35)/1.88
      rgd = (den*vs**2)/10.0
! get values for first elemental at hypocenter 
! in Pascals
! rgd = rgd*10**10
! assuming confining pressure 1.0 psi/ft,
! 6.894*10**3 Pa/psi, 1 bar/10**5 Pa
      std0 = 10. + 0.75 * rh(1) * 97.91 * den
      rstd(1) = real(min(rstd(1),std0))
      rgd1(1) = rgd * rstd(1)/std
      dishsk = rela(1)*rgd1(1)*1.0e+21

      if(slm.ne.'ksv') go to 12

! rise time computed for kostrov slip function

      if(avr.eq.'y'.or.avr.eq.'yes') vh = vr*vrh
      print *, apz,vsp,vpm,zm,vr,vh,fl(1),ip,r3,ang4,rstm,avr,ah,    &
      vrs,dp1,vrh,fx(1),fy(1),ixy

      call ristim(apz,vsp,vpm,zm,vr,vh,ip,r3,ang4,rstm,avr,ah,    &
      vrs,dp1,vrh,fx,fy,ixy)
      ristm(1)= rstm

      rslp(1) = 0.81*rstd(1)*ristm(1)*vs/rgd1(1)
! contrain to maximum slip to be controlled by parameter maxslp (cm)
! recalculate rise time for stress drop used
      amo = rslp(1)*rgd1(1)*rela(1)
   12 if(slm.ne.'ksv') ristm(1) = rtmhsk
      durtim = ristm(1)
      sdur = r2/vr + ristm(1) 
      kkii = 0
      kkiii = 1
!  160 kki = 1

! *************************
! element will be used
      do 161 i = 1,kk
      xlat = rlat(i)
      xlon = rlon(i)
      xh   = rh(i)
      call disazm(azm,azinv,dis,hdis,xlat,xlon,xh,alat,alon)

! change back-LJH 
      ang4 = azm
      r3 = sqrt(dis**2 + (ah-rh(i))**2)
      yy(i) = (ah-rh(i))/sin(dp1)
      xxa = (hdis**2 - yy(i)**2)
! - LJH 04/27/99 round off, i.e. alat, alon calculated from r6 in hazard, but 
! rounded off to pass to synhaz looks like third decimal place
      if(xxa.lt.0.0) xxa = 0.0
      xx(i) = sqrt(xxa) 
      stkr = rstk(i) + 90.
      stkrr = stkr-azinv
      if(stkrr.lt.0.0) stkrr = stkrr + 360.0
      if(stkrr.ge.180.and.stkrr.le.360.) xx(i) = -xx(i)
!...............................
      ang44 = ang4*360./pi2
      if(ang44.lt.0.0) ang44 = 360.0 + ang44
      ang55 = ang5*360./pi2
      if(ang55.lt.0.0) ang55 = 360.0 + ang55
! compute rdis and eltim 
      xlat = rlat(i)
      xlon = rlon(i)
      xh   = rh(i)
      call disazm(ezm,azinv,dis2,hdis2,xlat,xlon,xh,slat,slon)
      rdis(i) = dis2
      vp = vsp + rh(i)*apz 
      if(rh(i).gt.zm) vp = vpm 
      vs = vp/1.76 
      den = (vp-0.35)/1.88
      rgd = (den*vs**2)/10.0
! assuming confining pressure 1.0 psi/ft, 
! 6.894*10**3 PA/psi, 1 bar/10**5 PA 
      std0 = 10. + 0.75 * rh(i) * 97.91 * den 
      rstd(i) = real(min(rstd(i),std0))       
      rgd1(i) = rgd *rstd(i)/std 
      vr = vs*vrs 
      x1 = 0.0
      y1 = 0.0
      x2 = r3*sin(ang4)
      y2 = r3*cos(ang4)
      call ttvdp(x1,y1,ah,x2,y2,rh(i),dp1,apz,vsp,vpm,zm,ttvd)
! following is another fudge
      ttvd = sqrt(ttvd**2)
      eltim(i) = (1.76/vrs)*ttvd
!      slptim(i) = real(min(eltim(i),slpdis/vr)) 
      eazm(i) = ezm
      call ristim(apz,vsp,vpm,zm,vr,vh,ip,r3,ang4,rstm,avr,ah,       &
      vrs,dp1,vrh,fx,fy,ixy)
! slip is a continuous function, below rise time varies with roughness, 
! but slip amplitude is kept continuous and stress drop varies
      rslp(i) = 0.81*rstd(i)*vs*sqrt(rstm**2+2.0*slptim(i)*rstm)/rgd1(i) 
       if(rstm.le.0.01) then
      rstm = rslp(i)/200.0
       endif
      ristm(i) = rstm
      kasp = kasp + 1
      if(kasp.eq.iasp) then
      if(asp.eq.'y'.or.asp.eq.'yes') then
!-LJH 04.21.10 installed random number generator from Park and Miller, "Random Number 
! Generators: Good Ones are Hard to Find," Communications of the ACM, October 1988.
      rasp = (rand_max - rand_min)*rand() + rand_min
      ristm(i) = rasp*rstm
      eltim(i) = eltim(i) + rstm*(1. - rasp)
      rstd(i)=rslp(i)*rgd1(i)/(0.81*vs*sqrt(ristm(i)**2+2.0*slptim(i)*ristm(i)))
      endif
      kasp = 0
      llb(i)=1
      endif
      amo = amo + rslp(i)*rgd1(i)*rela(i)
      durtim1 = ristm(i) + eltim(i) + r2/(2.0*vr) 
      if(durtim1.gt.durtim) durtim = durtim1
      sdur1 = eltim(i) + r2/(2.0*vr) + ristm(i) + (hdis2 - adis)/vp
      if(sdur1.gt.sdur) sdur = sdur1
      dishsk = dishsk + rela(i)*rgd1(i)*1.0e+21
 161  continue
      amo = amo*1.0e+21
      stdd = std
      std = std*mo/amo
      do 288 i = 1,kk
      rstd(i) = rstd(i)*mo/amo
 288  rslp(i) = rslp(i)*mo/amo

!------------------------------------------------------
!-------------------MODULE RsqSim input--------------------
!cccccccccccccccccccccccccccccccccccccccccccccccccccccc
 9998 if(rsq.eq.'y'.or.rsq.eq.'yes') then     !..........................3
      call disazm(ezm,azinv,dis2,hdis2,alat,alon,ah,slat,slon)
      call linvel(ah,dis2,rp,angi,ainp,apz,vsp,vpm,zm,tt2)
      adis = hdis2
      sfo = tt2 - 4.0
      if(sfo.lt.0.) sfo = 0.0
      if(ah.eq.0.0) go to 1000
      durtim = 0.0
      sdur = 0.0
      amo = 0.0
      armo = 0.0
      slpvl(1) = rslp(1)/ristm(1)
      mindispl = rslp(1)
      maxdispl = rslp(1)
      minristm = ristm(1)
      maxristm = ristm(1)
      minslipv = slpvl(1)
      maxslipv = slpvl(1)
      sumdispl = 0
      sumristm = 0   
      sumslipv = 0
!------------925 loop-----------------------
      do 925 j = 1, kk
      totalslp = totalslp + rslp(j)
      aarea = aarea + rela(j)
      amo = amo + ramo(j) 
      armo= armo + rslp(j) * rgd1(j) * rela(j) * 1.0e+21

! MT now determining largest, smallest and sum of rslp and ristm
      if (rslp(j).gt.maxdispl)  maxdispl  = rslp(j)
      if (rslp(j).lt.mindispl)  mindispl  = rslp(j)
      if (ristm(j).gt.maxristm) maxristm  = ristm(j)
      if (ristm(j).lt.minristm) minristm  = ristm(j)
      if (slpvl(j).gt.maxslipv) maxslipv= slpvl(j)
      if (slpvl(j).lt.minslipv) minslipv= slpvl(j)  
      sumdispl = sumdispl + rslp(j)
      sumristm = sumristm + ristm(j)
      sumslipv = sumslipv + slpvl(j)
      xlat = rlat(j)
      xlon = rlon(j)
      xh   = rh(j)
      call disazm(ezm,azinv,dis2,hdis2,xlat,xlon,xh,slat,slon)
      rdis(j) = dis2
      eazm(j) = ezm      
 925  continue
      avgslp = totalslp/float(kk)
      aspkk = 0.0
      do 344 i = 1,kk
 344  if(rslp(i).gt.1.5*avgslp) aspkk = aspkk + 1.0
      prctasp = 100.0*aspkk/float(kk)
!------------925 loop end----------------------
! MT information about range of displacement and risetime and their averages
      avrdispl = sumdispl/kk
      avrristm = sumristm/kk
      avrslipv = sumslipv/kk
!021313      write(11,512) mindispl, maxdispl, avrdispl, minristm, maxristm, a
!021313     *vrristm, minslipv, maxslipv, avrslipv
! 512  format(/,'slip values of the RSQ:'/'displacement : smallest:',f6.2,     &
!      'cm,   largest: ',f7.2,'cm,   average: ',f6.2,'cm'/'risetime      ',    &
!      '  : smallest:',f6.3,'s,    largest:',f7.2,'s,     average: ',f6.2,     &
!      's'/'slip velocity: smallest:',f6.2,'cm/s, largest:',f7.2,'cm/s,   ',   &
!      ' average:',f6.2,'cm/s'/)
! MT 09/04/01 next lines: comparing different moments (input, sum, calc, sum)
      aamo = amo
!021313      write(11,301)
      if (armo.ne.amo) then
!021313      write (11,513)
! 513  format('sum of moments from elements of RSQ not equal desired moment',   &
!      ' given in input'/' file. Calculated moments for each element (see',     &
!      ' contour_RSQ_out). However, these are not used.')
      endif
!021313      write(11,*) 'moments (inputfile, sum of RSQ, sum of calculated):'
!021313      write(11,*) 'moment passed for event (mo), moment from sum of elem
!021313     *ents (amo), moment calculated from contour file parameters (armo)'
!      write(11,511) mo, aamo, armo, mopass
! 511  format('mo=',e10.4,5x,'sum armo(j)=',e10.4,3x,'sum indiv=',e10.4,        &
!      'mopass=',e10.4)
      if(armo.gt.1.001*aamo.or.armo.lt..999*aamo) then
!     print *,'check the RSQ inputs, values not matching'
!      print *,'mopass= ',mopass,' armo= ',armo,' amo= ',amo
!      stop
      endif
      goto 924
        endif                        !...................................3
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! END MODULE RSQ                                                        c
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!------------------------------------------------------
!-------------------MODULE asperity--------------------
!cccccccccccccccccccccccccccccccccccccccccccccccccccccc
! following is for asperity rupture
!cccccccccccccccccccccccccccccccccccccccccccccccccccccc
      if(lsp.ne.'y'.and.lsp.ne.'yes') go to 920
! fix to set moment before asperities added
! dont scale rise time for differences in moment because you scale stress drop
      mo1 = mo
      do 725 i = 1,nfsp
      nnsf1 = aspf(i)
      open(unit=9,file=nnsf1)
      read(9,*) alpha
      read(9,47) alat1,alon1,ah1,amo1,kk1,ela1,fdumm1,fdumm2,fdumm3,fdumm4,   &
      cdumm1,idumm1,cdumm2,idumm2,fdumm5,fdumm6,fdumm7
      mo1 = mo1 - amo1
      aspmo(i) = amo1
      asparea(i) = float(kk1)*ela1
      close(9)
  725 continue
      stdd = std
      std = std*mo1/mo
      do 722 i = 1,kk
      rslp(i) = rslp(i)*mo1/mo
      rstd(i) = rstd(i)*mo1/mo
      std0 = 10. + 0.75 * rh(i) * 97.91 * den
      std11 = real(min(rstd(i),std0))      
      rgd1(i) = rgd1(i) * std11/rstd(i)
      rstd(i) = std11
 722  continue
      amo = mo1
! keep track of non-asperity elements
      do 25 i = 1,nfsp
      nnsf1 = aspf(i)
      open(unit=9,file=nnsf1)
      read(9,*) alpha
      read(9,47) alat1,alon1,ah1,amo1,kk1,ela1,fdumm1,fdumm2,fdumm3,fdumm4,   &
      cdumm2,idumm1,cdumm3,idumm2,fdumm5,fdumm6,fdumm7
       if(ela1.ne.elaa) then
       print *,'the asperity elemental area does not equal main elaa'
       stop
       endif                              
       if(kk1.gt.maxel) then
       print *,'you need to up the dimensions for asperities'
       stop
       endif
      do 23 ii = 1,kk
 23   ll(ii) = 0
      do 24 ii = 1,kk1
 24   mm(ii) = 0
      call disazm(azm,azinv,dis,hdis,alat1,alon1,ah1,slat,slon)
       if(hdis.gt.100.0) then
!021313      write(11,928) nnsf1
!  928 format('asperity ',a10,' not used, greater than 100.0 km from station')
      go to 25
       endif
! add in the moment of asperities 
      amo = amo + amo1

!     need asperity hypocenter rupture time for kanamori rupture
      call disazm(ezm,azinv,dis2,hdis2,alat1,alon1,ah1,alat,alon)
      r3 = sqrt(dis2**2 + (ah1-ah)**2)
       if(avr.eq.'y'.or.avr.eq.'yes') then
      asptime(i) = r3/vr
       else
      x1 = 0.0
      y1 = 0.0
      y2 = (ah - ah1)/sin(dp1)
! round-off error for negative square root
      if(r3.lt.y2) r3 = y2
      x2 = sqrt(r3**2 - y2**2)
      call ttvdp(x1,y1,ah,x2,y2,ah1,dp1,apz,vsp,vpm,zm,ttvd)
      asptime(i) = (1.76/vrs)*ttvd
       endif
!021313      write(11,929) asptime(i)
! 929  format('origin time of asperity delayed by ',f6.2,' relative to',      &
!     ' origin time of main event')
! end of asperity time calculation
!
      read(9,*) alpha
      read(9,304) cdumm1,cdumm2,cdumm3,fdumm1,fdumm2,fdumm3,fdumm4,          &
      fdumm5,fdumm6,fdumm7,ipa
      read(9,48) (fla(k), k = 1,ipa)
      read(9,*) flat11,flon11,flat22,flon22,ffh11,dipdis11,fdumm1,fdumm2,fdumm3

      call disazm(azm,azinv,dis,hdis,alat,alon,ah,alat1,alon1)
      azm1 = (stk-azm)*pi2/360.
      x0 = dis*cos(azm1)
      z0 = dis*sin(azm1)
            if(abs(z0).lt.0.1) then
          if(abs(ah-ah1).lt.0.1) then
      y0 = 0
          else
        if(dp.gt.89.) then
      y0 = ah - ah1
        else
      y0 = z0/cos(dp1)
        endif
          endif
            else
      y0 = z0/cos(dp1)
            endif
      azm1 = pi2/4.0 - atan2(y0,x0)
      azmm = azm1*360./pi2
!021313      write(11,926) nnsf1,alat1,alon1,ah1,amo1,kk1,dis,azmm
! 926  format(/,'asperity model: ',a20/,f8.4,f9.4,f6.2,2x,e8.2,i6/,         &
!      'in plane distance: ',f6.2,' in plane angle ',f5.1)
!021313      if(kanamori.eq.1) write(11,921) asptime(i)
! 921  format('asperity rupture time ',f6.2)
      cplta(i) = 0.0
      icplta(i) = 1
! sqrt(elaa/2) is the diagional distance from element center
      d2 = sqrt(elaa)
      do 26 j = 1,kk1
      read(9,41) rlat1(j),rlon1(j),rh1(j),ela1,rslp1(j),fdumm6,fdumm2,     &
      ristm1(j),fdumm1,eltim1(j),fdumm7,fdumm3,fdumm4,fdumm5
      do 28 l = 1,kk
      if(ll(l).eq.1) go to 28
      xlat = rlat(l)
      xlon = rlon(l)
      xh   = rh(l)
      xlat1 = rlat1(j)
      xlon1 = rlon1(j)
      call disazm(azm,azinv,dis,hdis,xlat,xlon,xh,xlat1,xlon1)
      d1 = sqrt(dis**2 + (rh(l)-rh1(j))**2)
      if(d1.le.d2) then
      ll(l) = 1
      lla(l) = 1
      mm(j) = 1
      go to 27
      endif
 28   continue
      go to 26
! This is a run to 'take out' contribution of asperities
! risetime stays the same
! stress drop increases
 27   vp = vsp + rh(l)*apz
      if(rh(l).gt.zm) vp = vpm
      vs = vp/1.76
      rslp(l) = rslp(l) + rslp1(j)
! LJH 06/28/07, independent rupture for Kanamori at rupture time of asperity
      if(kanamori.eq.1) then
      eltim(l) = eltim1(j) + asptime(i)
      endif
!..LJH 10/13/03
      rstd(l)=rslp(l)*rgd1(l)/(0.81*vs*sqrt(ristm(l)**2+2.0*slptim(l)*ristm(l)))
 26   continue
! look for more elements that are farther from desired element
      d2 = 4.*sqrt(elaa)
      do 226 j = 1,kk1
      if(mm(j).eq.1) go to 226
      do 228 l = 1,kk
      if(ll(l).eq.1) go to 228
      xlat = rlat(l)
      xlon = rlon(l)
      xh   = rh(l)
      xlat1 = rlat1(j)
      xlon1 = rlon1(j)
      call disazm(azm,azinv,dis,hdis,xlat,xlon,xh,xlat1,xlon1)
      d1 = sqrt(dis**2 + (rh(l)-rh1(j))**2)
      if(d1.le.d2) then
      ll(l) = 1
      lla(l) = 1
      mm(j) = 1
      go to 227
      endif
228   continue
      go to 226
 227  vp = vsp + rh(l)*apz
      if(rh(l).gt.zm) vp = vpm
      vs = vp/1.76
      if(kanamori.eq.1) eltim(l) = eltim1(j) + asptime(i)
      rslp(l) = rslp(l) + rslp1(j)
!       ristm(l) = ristm(l) + ristm1(j)
!..LJH 10/13/03
      rstd(l)=rslp(l)*rgd1(l)/(0.81*vs*sqrt(ristm(l)**2+2.0*slptim(l)*ristm(l)))
 226  continue
! search an even a larger area for missed elements
      d2 = 10.*sqrt(elaa)
      do 526 j = 1,kk1
      if(mm(j).eq.1) go to 526
      do 528 l = 1,kk
      if(ll(l).eq.1) go to 528
      xlat = rlat(l)
      xlon = rlon(l)
      xh   = rh(l)
      xlat1 = rlat1(j)
      xlon1 = rlon1(j)
      call disazm(azm,azinv,dis,hdis,xlat,xlon,xh,xlat1,xlon1)
      d1 = sqrt(dis**2 + (rh(l)-rh1(j))**2)
      if(d1.le.d2) then
      ll(l) = 1
      lla(l) = 1
      mm(j) = 1
      go to 527
      endif
528   continue
      go to 526
 527  vp = vsp + rh(l)*apz
      if(rh(l).gt.zm) vp = vpm
      vs = vp/1.76
      if(kanamori.eq.1) eltim(l) = eltim1(j) + asptime(i)
      rslp(l) = rslp(l) + rslp1(j)
!       ristm(l) = ristm(l) + ristm1(j)
!..LJH 10/13/03
      rstd(l)=rslp(l)*rgd1(l)/(0.81*vs*sqrt(ristm(l)**2+2.0*slptim(l)*ristm(l)))
 526  continue
!
! add in missing elments that may be off of fault
      ill1 = 0
      do 326 j = 1,kk1
      if(mm(j).eq.1) go to 326
      ikk1 = ikk1 + 1
! add an element
      kk = kk + 1
      ll(kk) = 1
      lla(kk) = 1
      mm(j) = 1
      rh(kk) = rh1(j)
      rlat(kk) = rlat1(j)
      rlon(kk) = rlon1(j)
      rela(kk) = elaa
      vp = vsp + rh(kk)*apz
      if(rh(kk).gt.zm) vp = vpm
      vs = vp/1.76
      den = (vp-0.35)/1.88
      rgd1(kk) = (den*vs**2)/10.0
      if(kanamori.eq.1) eltim(l) = eltim1(j) + asptime(i)
      rslp(kk) = rslp1(j)
      ristm(kk) = ristm1(j)
! calculate rstd(kk) after determine slptim(kk)
!.................. point on grid
      xlat = rlat(kk)
      xlon = rlon(kk)
      xh   = rh(kk)
      call disazm(azm,azinv,dis,hdis,xlat,xlon,xh,alat,alon)
      hdis = sqrt((ah-rh(kk))**2 + dis**2)
      yy(kk) = (ah-rh(kk))/sin(dp1)
      xxa = (hdis**2 - yy(kk)**2)
! - LJH 04/27/99 round off, i.e. alat, alon calculated from r6 in hazard, but 
! rounded off to pass to synhaz looks like third decimil place
      if(xxa.lt.0.0) xxa = 0.0
      xx(kk) = sqrt(xxa) 
      stkr = stk + 90.
      stkrr = stkr-azinv
      if(stkrr.lt.0.0) stkrr = stkrr + 360.0
      if(stkrr.ge.180.and.stkrr.le.360.) xx(kk) = -xx(kk)
!.....................
      xlat = rlat(kk)
      xlon = rlon(kk)
      xh   = rh(kk)
      call disazm(ezm,azinv,dis1,hdis1,xlat,xlon,xh,slat,slon)
      rdis(kk) = dis1
      eazm(kk) = ezm
      xlat = rlat(kk)
      xlon = rlon(kk)
      xh   = rh(kk)
      call disazm(azm,azinv,dis2,hdis2,xlat,xlon,xh,alat,alon)
      r3 = sqrt(dis**2 + (rh(kk)-ah)**2)
      eltim(kk) = r3/vr
      if(avr.ne.'y'.and.avr.ne.'yes') then
      vr = vs*vrs
      x1 = 0.0
      y1 = 0.0
      y2 = (ah-rh(kk))/sin(dp1)
! round off
      if(r3.lt.abs(y2)) r3 = y2
      x2 = sqrt(r3**2 - y2**2)
      call ttvdp(x1,y1,ah,x2,y2,rh(kk),dp1,apz,vsp,vpm,zm,ttvd)
      eltim(kk) = (1.76/vrs)*ttvd
      endif
! delay element time for rough rupture
      if(asp.eq.'y'.and.j.ne.1.or.asp.eq.'yes'.and.j.ne.1) then
      eltim(kk) = eltim(kk) + abs(ristm1(j)-ristm1(j-1))
      endif
!..LJH 10/13/03
      rstd(kk) = rslp(kk)*rgd1(kk)/(0.81*vs*sqrt(ristm(kk)**2+2.0       &
      *slptim(kk)*ristm(kk)))
      std0 = 10. + 0.75 * rh(kk) * 97.91 * den
      std11 = real(min(rstd(kk),std0))      
      rgd1(kk) = rgd1(kk) * std11/rstd(kk)
      rstd(kk) = std11
 326  continue
!021313      write(11,327) ikk1,aspf(i)
! 327  format(i4,' elements added in seperatly for asperity: ',a80)

!cccccccccccccccccccccc
! check for elements that were missed
! eltim and ristm stay the same
      llkk = 0
      do 39 j = 1,kk
      if(ll(j).eq.1) go to 39
      rslpp = rslp(j)
      if(ll(j-1).eq.1.and.ll(j+1).eq.1) then
      if(kanamori.eq.1) eltim(j) = eltim(j+1)
      rslp(j) = rslp(j+1)
      rstd(j) = rstd(j+1)
      rgd1(j) = rgd1(j+1)
      go to 38
      endif 
      if(ll(j-2).eq.1.and.ll(j-1).eq.0.and.ll(j+1).eq.1) then
      if(kanamori.eq.1) eltim(j) = eltim(j+1)
      rslp(j) = rslp(j+1)
      rstd(j) = rstd(j+1)
      rgd1(j) = rgd1(j+1)
      go to 38
      endif
      if(ll(j+2).eq.1.and.ll(j+1).eq.0.and.ll(j-1).eq.1) then
      if(kanamori.eq.1) eltim(j) = eltim(j+1)
      rslp(j) = rslp(j-1)
      rstd(j) = rstd(j-1)
      rgd1(j) = rgd1(j-1)
      go to 38
      endif
      go to 39
 38   llkk = llkk + 1
      ll(j) = 1
      lla(j) = 1
      amo = amo + (rslp(j)-rslpp)*rgd1(j)*rela(j)*1.0e+21
      aspmo(i) = aspmo(i) + (rslp(j)-rslpp)*rgd1(j)*rela(j)*1.0e+21
 39   continue
!021313      write(11,927) llkk,aspf(i)
! 927  format(i5,' elements missed in background rupture when adding ',    &
!      'asperity: ',a80)
! end of adding an element
!!!!!!!!!!!!!!! ***************************************************
      do 31 j = 1,kk
      if(ll(j).eq.1.and.llb(j).eq.0) then
       if(rslp(j).ge.cplta(i)) then
      cplta(i) = rslp(j)
      icplta(i) = j
       endif
      endif
 31   continue
!
      close(9)
 25   continue
      do 32 i = 1,kk
      rstd(i) = rstd(i)*mo/amo
 32   rslp(i) = rslp(i)*mo/amo
      stdd = std
      std = std*mo/amo
!021313       write(11,126) amo,stdd,mo,std
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! END MODULE asperity                                                 c
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc



!------------------------------------------------------
!-----------------------MODULE readjust-----------------------------------------------
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

! from this point on rslp, ristm, std for Haskell, Kostrov and asperities
! models are handled the same.

 920  continue
!021313       write(11,221) maxslp,maxslpvel
! 221  format('before scale to mo, and limit rslp to ',f6.1,/'Also, maxim',   &
!      'um slip velocity limited to ',f6.1,'cm/sec',/'and rise time adjust',  &
!      'ed to: ristm(i) = rslp(i)/maxslpvel')
! fix to set moment, limit maximum slip to 'maxslp' cm, keep rise time the same
! dont scale rise time for differences in moment because you scale stress drop
      amo1 = 0.0
      do 922 i = 1,kk
      std0 = 10. + 0.75 * rh(i) * 97.91 * den
      std11 = real(min(rstd(i),std0))      
      rgd1(i) = rgd1(i) * std11/rstd(i)
      rstd(i) = std11
! scale rise time for difference above 'maxslp', here only scale linearly because 
! of rough rupture
        if(rslp(i).gt.maxslp) then
      rslpp = rslp(i)
      ristm(i) = ristm(i)*maxslp/rslpp
      rslp(i) = maxslp
        endif
      amo1 = amo1 + rslp(i)*rgd1(i)*rela(i)*1.0e+21
 922  continue
      if(slm.eq.'hsk') dishsk = dishsk*mo/amo1
!021313      write(11,*) ' '
!021313      write(11,*) 'after scale, but took out amo for limit of rslp'
      stdd = std
      std = std*mo/amo1
!021313      write(11,126) amo1,stdd,mo,std
      cplt = 0.0
! do not scale rise time here because you are changing stress drop
      amo11 = 0.0
      do 923 i = 1,kk
      rstd(i) = rstd(i)*mo/amo1
      rslp(i) = rslp(i)*mo/amo1
      std0 = 10. + 0.75 * rh(i) * 97.91 * den
      std11 = real(min(rstd(i),std0))      
      rgd1(i) = rgd1(i) * std11/rstd(i)
      rstd(i) = std11
      if(llb(i).eq.0) then 
       if(lla(i).eq.0.and.rslp(i).ge.cplt) then
      cplt = rslp(i)
      icplt = i
       endif
      endif
      amo11 = amo11 + rslp(i)*rgd1(i)*rela(i)*1.0e+21
 923  continue
      stdd = std
      std = std*mo/amo11
!021313      write(11,126) amo11,stdd,mo,std
      do 1111 i = 1,kk
      rslp(i) = rslp(i)*mo/amo11
      totalslp = totalslp + rslp(i)
 1111 aarea = aarea + rela(i)
      avgslp = totalslp/float(kk)
      if(slm.eq.'hsk') then
      dishsk = dishsk*mo/amo11
      rslp(i) = dishsk
      icplt = 1
      cplt = dishsk
      endif
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! END MODULE readjust                                                c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

! at this point rupture parameters set, RSQ enters here

! find source event for each element, and delay time for P and S arriavals

 924  if(ne.eq.0) then          
      print *,'incorrect name for sources file'
      stop
      endif  
      nct = ifix(float(kk/ne) - .01) + 1
      aspkk = 0.0
      do 244 i = 1,kk
      edis = 1.e30
      if(rslp(i).gt.1.5*avgslp) aspkk = aspkk + 1.0
! distance from element to hazard site
      xlat = rlat(i)
      xlon = rlon(i)
      xh   = rh(i)
      call disazm(azm,azinv,dis2,hdis2,xlat,xlon,xh,slat,slon)
      do 144 ii = 1,ne
! limit the number of times a source event is used
      if((asyn.eq.'y'.or.asyn.eq.'yes').and.(ict(ii).gt.nct)) goto 144
! distance from GF to synthesis site
      xlat = blat(ii)
      xlon = blon(ii)
      xh   = bh(ii)
      call disazm(azm,azinv,dis1,hdis1,xlat,xlon,xh,slatt,slonn)
      ediss = abs(hdis2-hdis1)
      if(ediss.gt.edis) go to 144
      edis = ediss
      diss = dis1
      inel(i) = ii
  144 continue
      if(inel(i).eq.0) then
       print *,'source events greater than 1000 Km distant'
       stop
       else
       ict(inel(i)) = ict(inel(i)) + 1     
      endif                       
!
! compute p and s delay time due to distance difference of GF and HAZ
!
      xh = rh(i)
      call linvel(xh,dis2,rp,angi,ainp,apz,vsp,vpm,zm,tt2)
      xh = bh(inel(i))
      call linvel(xh,diss,rp,angi,ainp,apz,vsp,vpm,zm,tt1)
! when synthesis station is farther than the source event station, the 
! seismogram has to be "moved farther", i.e. dtimp is positive
      dtimp(i) = tt2 - tt1
244   continue

!cccccccccccccccccccccccccccccccccccccccccccccccccccccc
 
         if (rsq.ne.'y'.and.rsq.ne.'yes') then    !............................4
       if(slm.eq.'ksv') then
!021313      write(11,127) durtim,ristm(1),rslp(1),sdur
       endif
       if(slm.eq.'hsk') then
!021313      if(slp.eq.'spc') write(11,419) dishsk,rtmhsk,durtim,sdur
!021313      if(slp.eq.'ttm') write(11,499) dishsk,durtim,sdur
       endif
      rriss = r1/vr + ristm(kk)
      amo = mo
 48   format(10f8.2)
      ers = 0.0
      po = 0.0
      do 45 i = 1,kk
      po = po + rslp(i)*rela(i)*1.0e+10
      vp = vsp + rh(i)*apz
      if(rh(i).gt.zm) vp = vpm
      vs = vp/1.76
      ers = ers + 0.5*rstd(i)*rslp(i)*rela(i)*1.0e+10
      slpvl(i) = rslp(i)/ristm(i)
      if(slm.eq.'hsk') then
      dishsk = rslp(1)
      rtmhsk = ristm(1)
      endif
      if (rsq.ne.'y'.and.rsq.ne.'yes') then 
        if(slpvl(i).ge.maxslpvel) then
          slpvl(i) = maxslpvel
          ristm(i) = rslp(i)/maxslpvel
        endif
      endif
 45   continue
 41   format(f9.5,1x,f10.5,1x,f5.2,1x,f7.4,1x,f9.4,1x,f6.2,1x,f7.2,1x,      &
      f6.3,1x,f6.2,1x,f7.3,1x,f7.3,1x,e13.6,1x,f8.3,1x,f8.3)
      close(12)
      close(13)
      close(14)
!
!021313      write(11,124) kk,r1
      stdrs = 2.0*ers/po
!021313      write(11,44) ers,po,stdrs
! 44   format(/,'radiated seismic energy =',e10.3,' potency =',e10.3,/,      &
!      'Orian stress drop =',f10.1/)
!021313      write (11,*)
!021313      write (11,*) 'total number of elemental areas is ',kk
!JBS      write (6,*) 'total number of elemental areas is ',kk
      if(rgd1(1).lt.1.0) then
!021313      write(11,444)
! 444  format(/'*****WARNING!****'/,'rigidity is low to match lithostatic',  & 
!      ' limit***, moment is OK, but slip may be low, check maxslp, see 92', &
!      '2 loop')
      endif
        endif                                     !............................4

!cccccccccccccccccccccccccccccccccccc
! begin synthesis
!cccccccccccccccccccccccccccccccccccc
! read in each file
      if(filow.ne.0.0.and.filhi.ne.100.0) then
!021313      write(11,426) filow,filhi
      else
!021313      write(11,427)
      endif
       if(slp.eq.'spc') then
!021313      write(11,423)
      if(slm.eq.'ksv') write(11,418)
      if(slm.eq.'hsk') then
!      if(rsq.eq.'y'.or.rsq.eq.'yes') sdur = sdur2
!021313      write(11,414) dishsk,rtmhsk,rriss,sdur
      endif
       endif
       if(slp.eq.'ttm') then
!021313      if(slm.eq.'ksv') write(11,422) maxdim
      if(slm.eq.'hsk') then
!021313      write(11,421) maxdim
!021313      write(11,415) dishsk,rriss,sdur
      endif
       endif 
      if(slm.eq.'ksv') then
      kk1 = min0(kk,160)
!021313      write(11,417)kk1
!JBS      write(11,416) (rslp(i), i = 1,kk1)
      endif
!021313      if(asp.eq.'y'.or.asp.eq.'yes') write(11,312) iasp
! 312  format(/,'roughness included in rupture'/,                              &
!      'the density of roughness is determined by choosing an interval'/       &
!      ,'of elements that will be asperities, i.e. 3 means every third'/,      &
!      'this will distribute randomly on the fault because of the radial',     & 
!      /,'arrangement of elements.  the hypocenter is not an asperity'/,       &
!      'the density of asperities is ',i2//,                                   &
!      'rise time for roughness is a random percentage (0.1<rasp<0.9)',        &   
!      /,'of neighboring elements.  This is applied so that the full'/,        &
!      'displacement is reached at the same time as neighboring elements.',    &
!      /,'source events are split between P- and S- arrival for seperate',     &
!      ' processing')


! parameters from source file:
!
!  timm:  real time of first data point (for non-SAC files)
!  tim0:  seconds from origan of first data point in data file
!  tim1:  seconds from origin of first data point to be used in synthsis
!  tegf:  maximum seconds for length of input traces
!  npGF:  tegf/dt; maximum data points to be used for input traces
!   azm:  azimuth from source event to station, in degrees
! azinv:  back-azimuth of station from source event, in degrees
!  cmp1:  vertical suffix for output
!  sazm:  orientation of trace two in output trar, in degrees
! sazmm:  orinntation of trace three in output, in degrees
!  rot2:  orientation of recordings EGFs for trace trar w.r.t. N, degrees
!         radial orientation of FKR GFs
!  rot3:  orientation of trace trat 
!    np:  first, length in points of each GF data file, then reset to be npGF
!    dt:  sampling interval in seconds
!   epi:  epicentral distance
!     h:  depth of event
!
! cccccccccccccc
!  ready to begin synthesis
!
! go through 400 loop once for each source event, apply to all
! elements using the event
!!c jjk to count elements during computation
      jjk = 0
      do 400 ji = 1,ne
      jjkk = 0
      do 410 i = 1,maxegf
      traz(i) = 0.0
      trar(i) = 0.0
  410 trat(i) = 0.0
      vqp = vsp + bh(ji)*apz
      vqs = vqp/1.76
      if(GFtype.eq.'EGF') then
      if(acmp1.ne.'z'.and.acmp1.ne.'zzz'.and.acmp1.ne.'vz'.and.     &
      acmp1.ne.'v'.and.acmp1.ne.'1'.and.acmp1.ne.'Z'.and.           &
      acmp1.ne.'DDD'.and.acmp1.ne.'UUU') then 
!021313      write(11,403) asbb
      endif 
! 403  format('Station ',a8,' doesnt have first component at vertical'/,   &
!      'terminate program') 
      go to 1000 
      endif 
!
! following is for FKPROG synthetic 10 tensor component Green's functions.
!
! The ordering of the components are: transverse component vertical
! strike-slip (tss) and vertical dip-slip (tds) faults; radial component 
! vertical strike-slip (xss), vertical dip-slip (xds) and 45-degree 
! dip-slip (xdd) faults; and vertical component vertical strike-slip (zss),
! vertical dip-slip (zds), and 45-degree (zdd) faults.

      if(GFtype.eq.'FKR') then                  !...............................5
!      call indexc(asbb,lentemp2,5)
!      nnsf1 = pathname(ji)(1:lentemp4)
!      open(unit=18,file=nnsf1)
!      read(18,'(///,i8,3x,f5.3,15x,f5.2)') np,dt,tim0
!*******************************************************************************
!Correspondance between variables here and in SYNHAZ
!***************************************************
!         GFtensor(ji,j,i)
!         Assumes the there is only ONE station and nGF sources      
!         GF Source Index  ji   (ji = 1, nGFsources)
!         tensor component j    (j = 1, 10)
!         Time step        i    (i = 1,np, typically np=1024)
!*******************************************************************************
!
!JBS6-18-14
!Here reload the tensor in file ...ji
      !!!GFpath1 = GFfile2(ji)     
      !!!open (unit=9,file=GFpath1,status="unknown",iostat=IERR)


      np    = SynNp(ji)
      jxnp  = np
      npSYN = INT (tsyn/dt)
      npGF  = ifix(tegf/dt + .001)
      npGF = np 
      if(npGF.gt.np) then
      print *,'npGF needs to be less than np'
      stop
      endif
      tim0 = Syntim(ji)

      !If the number of components and format is not given in the files,
      !enter it here and skip the next 2 resd statemens
      ncomp = 10
      GFformat = '(6e13.5)'
      !read (9,*) ncomp
      !read (9,*) GFformat

      !Load GF Array in GFtensor
      !!!do j = 1, ncomp
         !!! read (9,*) dummy
         !!! read (9,*) nt,dt,tim
         !!! read (9,GFformat) (GFtensor(j,k),k=1,nt)
      !!!end do
      !!!close (9)
      nt = GFnt(ji)
      !Load GF in appropriate Synhaz arrays
          do i = 1, nt
          xxx = GFtensor(ji,1,i)
          xxx = GFtensor(ji,2,i)
          xxx = GFtensor(ji,3,i)
          xxx = GFtensor(ji,4,i)
          xxx = GFtensor(ji,5,i)
          xxx = GFtensor(ji,6,i)
          xxx = GFtensor(ji,7,i)
          xxx = GFtensor(ji,8,i)
          xxx = GFtensor(ji,9,i)
          xxx = GFtensor(ji,10,i)

          tra_tss(i) = GFtensor(ji,1,i)
          tra_tds(i) = GFtensor(ji,2,i)
          tra_rss(i) = GFtensor(ji,3,i)
          tra_rds(i) = GFtensor(ji,4,i)
          tra_rdd(i) = GFtensor(ji,5,i)
          tra_zss(i) = GFtensor(ji,6,i)
          tra_zds(i) = GFtensor(ji,7,i)
          tra_zdd(i) = GFtensor(ji,8,i)
          tra_rex(i) = GFtensor(ji,9,i)
          tra_zex(i) = GFtensor(ji,10,i)
          end do

      !dt    = Syndt(ji)

      xxx = 1.1
      xxx = bh(ji)
      xlat = blat(ji)
      xlon = blon(ji)
      xh   = bh(ji)
      call disazm(bazm,azinv,bdis,hdis,xlat,xlon,xh,slatt,slonn)
      azm1 = bazm*pi2/360.
!
      Mxx = -(sin(dp1)*cos(sv1)*sin(2.*stk1) + sin(2.*dp1)*sin(sv1)*          &
      (sin(stk1))**2)
      Myy = sin(dp1)*cos(sv1)*sin(2.*stk1) - sin(2.*dp1)*sin(sv1)*(cos(stk1))**2
      Mzz = sin(2.*dp1)*sin(sv1)
      Mxx = Mxx - (Mxx+Myy+Mzz)/3.
      Myy = Myy - (Mxx+Myy+Mzz)/3.
      Mzz = Mzz - (Mxx+Myy+Mzz)/3.
      Mxy = sin(dp1)*cos(sv1)*cos(2.*stk1) + 0.5*sin(2.*dp1)*sin(sv1)*        &
      sin(2.*stk1)
      Mxz = -(cos(dp1)*cos(sv1)*cos(stk1) + cos(2.*dp1)*sin(sv1)*sin(stk1))
      Myz = -(cos(dp1)*cos(sv1)*sin(stk1) - cos(2.*dp1)*sin(sv1)*cos(stk1))
      xlat = blat(ji)
      xlon = blon(ji)
      xh   = bh(ji)
      call disazm(azm,azinv,bdis,bhdis,xlat,xlon,xh,slatt,slonn)
      azm1 = azm*pi2/360.
      rot2 = azm
      rot3 = azm + 90.
      if(rot3.ge.360.0) rot3 = rot3 - 360.0
!021313      write(11,147) rot2,rot3  
      do 1113 i = 1,npGF
      traz(i) = Mxx*(tra_zss(i)*cos(2*azm1)/2. - tra_zdd(i)/6.               &
      + tra_zex(i)/3.)                                                       &
      + Myy*(-tra_zss(i)*cos(2.*azm1)/2. - tra_zdd(i)/6.                     &
      + tra_zex(i)/3.)                                                       &
      + Mzz*(tra_zdd(i)/3. + tra_zex(i)/3.)                                  &
      + Mxy*tra_zss(i)*sin(2.*azm1) + Mxz*tra_zds(i)*cos(azm1)               &
      + Myz*tra_zds(i)*sin(azm1)
      xxx = traz(i)
!
      trar(i) = Mxx*(tra_rss(i)*cos(2*azm1)/2. - tra_rdd(i)/6.               &
      + tra_rex(i)/3.)                                                       &
      + Myy*(-tra_rss(i)*cos(2.*azm1)/2. - tra_rdd(i)/6.                     &
      + tra_rex(i)/3.)                                                       &
      + Mzz*(tra_rdd(i)/3. + tra_rex(i)/3.)                                  &
      + Mxy*tra_rss(i)*sin(2.*azm1) + Mxz*tra_rds(i)*cos(azm1)               &
      + Myz*tra_rds(i)*sin(azm1)
      xxx = trar(i)
!
      trat(i) = Mxx*(tra_tss(i)*sin(2*azm1)/2.)                              & 
      - Myy*(tra_tss(i)*sin(2.*azm1)/2.)                                     & 
      - Mxy*tra_tss(i)*cos(2.*azm1)                                          & 
      + Mxz*tra_tds(i)*sin(azm1)                                             & 
      - Myz*tra_tds(i)*cos(azm1)
      xxx = trat(i)
 1113 continue
! Point Source Solution
      if(SynSwitch.eq.'yes'.and.ji.eq.ijne) then
      lt = ifix(.2/dt)
      nt1 = lt + 1
      nt2 = npGF - lt
      filGF = 1.0/(2.*ristm(1))
      signi = -1.0
      call taper(maxdim,maxegf,npGF,nt1,nt2,traz,tzpz,n,lt,mp,m1,ks)
      call cool(n,tzpz,signi)
      call filter(tzpz,ks,dt,0.0,filGF,Maxdim)
!
      call taper(maxdim,maxegf,npGF,nt1,nt2,trar,tzpr,n,lt,mp,m1,ks)
      call cool(n,tzpr,signi)
      call filter(tzpr,ks,dt,0.0,filGF,Maxdim)
!
      call taper(maxdim,maxegf,npGF,nt1,nt2,trat,tzpt,n,lt,mp,m1,ks)
      call cool(n,tzpt,signi)
      call filter(tzpt,ks,dt,0.0,filGF,Maxdim)
!
      signi = 1.0
      call cool(n,tzpz,signi)
      call cool(n,tzpr,signi)
      call cool(n,tzpt,signi)
      xlat = blat(ji)
      xlon = blon(ji)
      xh   = bh(ji)
      call disazm(azm,azinv,bdis,bhdis,xlat,xlon,xh,slatt,slonn)
      call disazm(azm,azinv,dis2,hdis2,alat,alon,ah,slat,slon)
      facc = (bhdis/hdis2)*(mopass/bm(ji))
      ii = 0
      do 286 i = mp-lt,mp-lt+npGF
      ii = ii + 1
      traaz(ii) = facc*real(tzpz(i))
      traar(ii) = facc*real(tzpr(i))
      traat(ii) = facc*real(tzpt(i))
  286 continue

!JBS DEBUGG^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      do i = mp-lt,mp-lt+npGF
           ii = ii + 1
          print *, 'i,traaz,traar,traat=',ii,traaz(ii),traar(ii),traat(ii)
      end do
!JBS END print debugg^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

      call peak(traaz,traar,traat,npGF,synpeak)


!*******************************************************************************
!     Response Spectra
!     ----------------
!
      INgm  = 1
      OUTgm = 1
  
      call RespSpectra (damp, freq, INgm, npGF, nfreq, OUTgm, &
                                PARS, dt)

      print *, '  '
      print *, ' Frequency     Spectral Value'
      do ifreq = 1, nfreq
          print *, freq(ifreq), PARS(ifreq)
      end do

!END RESPONSE SPECTRA **********************************************************


      aradius = sqrt(aarea*2./pi2)
!      write(6,108) kk,mopass,rstd(1),avgslp,aradius,maxristm
 !     if(synflag.eq.1) write(6,109) synpeak
  !    if(synflag.eq.2) write(6,110) synpeak
   !   if(synflag.eq.3) write(6,111) synpeak
      return
      endif
! end of point source


! Skip the part on empirical GF. Will come back to it later when needed
      go to 1112
      endif                                   !...............................5                                 
!
! following is for empirical Green's functions
      nnsf1 = pathname(ji)(1:lentemp4)//'/'//asbb(1:lentemp2)//'.'//acmp1//'.txt'
      open(unit=18,file=nnsf1)
      read(18,'(f5.3,1x,e11.4)',end=16) (ttim(i),traz(i),i=1,maxdim)
 16   close(18)
      np = i - 1
      nnsf1 = pathname(ji)(1:lentemp4)//'/'//asbb(1:lentemp2)//'.'//acmp2//'.txt'
      open(unit=18,file=nnsf1)
      read(18,'(f5.3,1x,e11.4)',end=17) (ttim(i),trar(i),i=1,maxdim)
 17   close(18)
      if(i-1.ne.np) print *,'data files different length for event ',pathname(ne)
      nnsf1 = pathname(ji)(1:lentemp4)//'/'//asbb(1:lentemp2)//'_'//acmp3//'.txt'
      open(unit=18,file=nnsf1)
      read(18,'(f5.3,1x,e11.4)',end=18) (ttim(i),trat(i),i=1,maxdim)
 18   close(18)
      tim1 = tt - 4.0
      if(tim1.lt.tim0) tim1 = tim0
      call fixtim(traz,trar,trat,np,npGF,dt,tim0,tim1,Maxegf)
!021313      write(11,314) evid(ji),tim0,tim1,np
! 314  format(//,'*** source event ',a12,' ***'/,                               &
!      'file begin, tim0 = ',f6.2,'; trace begin, tim1 = ',f5.2,                &
!      '; npGF is set to ',i4)
      read(acmp2,'(f3.0)') rot2
      rot3 = rot3 + 90. 
      if(i-1.ne.npGF) print *,'data files different length for event ',        &
      pathname(ji)
      nnsf1 = asbb(1:lentemp2)//'.'//acmp2
      nnsf2 = asbb(1:lentemp2)//'.'//acmp3
!021313      write(11,147) rot2,rot3
      write(6,147) rot2,rot3  


! 
! find first point to be used in synthesis
!
 1112 itk = ifix((float(ks)*dt - 1.0)/dt + .001)
      titk = float(itk)*dt
      if(npGF.gt.itk) then
!021313      write(11,431) ks,titk
      endif
      xlat = blat(ji)
      xlon = blon(ji)
      xh   = bh(ji)
      call disazm(bazm,azinv,bdis,bhdis,xlat,xlon,xh,slatt,slonn)
      xh = bh(ji)
      call linvel(xh,bdis,rp,angi,ainp,apz,vsp,vpm,zm,tt)
!
! cccccccccccccccccc
!
! compute radiation factor for source event
!
      bzmm = bazm - bstr(ji)
      aangi = angi*360./pi2
      aainp = ainp*360./pi2
      ains1 = sin(ainp)/1.76
! roundoff accuracy
      if(ains1.gt.1.0) ains1 = 1.0
      ains = asin(ains1)
      vss = vsp/1.76
      aains = ains*360./pi2
      bsv1 = bsv(ji)*pi2/360.
      bzm1 = bzmm*pi2/360.
      bdp1 = bdp(ji)*pi2/360.
      bazm1 = (rot2-bazm)*pi2/360.
      bazm2 = (rot3-bazm)*pi2/360.
!
!
! ffpp, ffsv,ffsh are radiation correction factors for where the Green's 
! functions were created
!
! average values for p,sv,sh arrivals from Prejean and Ellsworth (bssa, 91, pp.
! 165-177. However, they are only used for moment calcculations since they are 
! also muliplied for elpp, elsv and elsh
      ffpp = cos(bsv1)*sin(bdp1)*(sin(angi)**2)*sin(2.*bzm1)-                 &
      cos(bsv1)*cos(bdp1)*sin(2.*angi)*cos(bzm1)+sin(bsv1)*sin(2.*bdp1)       &
      *((cos(angi)**2)-(sin(angi)**2)*(sin(bzm1)**2))                         &
      + sin(bsv1)*cos(2.*bdp1)*sin(2.*angi)*sin(bzm1)
      if(ffpp.lt.0..and.ffpp.gt.-.25) ffpp = -.25
      if(ffpp.ge.0..and.ffpp.lt..25) ffpp = .25
      if(bstr(ji).eq.0.0.and.bdp(ji).eq.0.0.and.bsv(ji).eq.0.0) ffpp = 0.52
      if(rdc.ne.'y'.and.rdc.ne.'yes') ffpp = 1.0
!
      ffsv = sin(bsv1)*cos(2.*bdp1)*cos(2.*angi)*sin(bzm1)-cos(bsv1)          &
      *cos(bdp1)*cos(2.*angi)*cos(bzm1) + .5*cos(bsv1)*sin(bdp1)              &
      *sin(2.*angi)*sin(2.*bzm1) - .5*sin(bsv1)*sin(2.*bdp1)                  &
      *sin(2.*angi*(1.+sin(bzm1)**2))
      if(ffsv.lt.0..and.ffsv.gt.-.25) ffsv = -.25
      if(ffsv.ge.0..and.ffsv.lt..25) ffsv = .25
      if(bstr(ji).eq.0.0.and.bdp(ji).eq.0.0.and.bsv(ji).eq.0.0) ffsv = 0.48
      if(rdc.ne.'y'.and.rdc.ne.'yes') ffsv = 1.0
!
      ffsh = cos(bsv1)*cos(bdp1)*cos(angi)*sin(bzm1)                          &
      + cos(bsv1)*sin(bdp1)*sin(angi)*cos(2.*bzm1)+sin(bsv1)*cos(2.*bdp1)     &
      *cos(angi)*cos(bzm1) - .5*sin(bsv1)*sin(2.*bdp1)*sin(angi)              &
      *sin(2.*bzm1)
      if(ffsh.lt.0..and.ffsh.gt.-.25) ffsh = -.25
      if(ffsh.ge.0..and.ffsh.lt..25) ffsh = .25
      if(bstr(ji).eq.0.0.and.bdp(ji).eq.0.0.and.bsv(ji).eq.0.0) ffsh = 0.41
      if(rdc.ne.'y'.and.rdc.ne.'yes') ffsh = 1.0
!
!021313      write(11,311) bzmm,bdp(ji),bsv(ji),aangi,aainp,aains,ffpp,
!021313     *ffsv,ffsh
! 311  format(/'fault-stat azi   dip   slip vector    takeoff angle',          &
!      '   p s incid angles'/,6x,f6.1,5x,f4.1,4x,f6.1,13x,f5.1,12x,            &
!      f4.1,2x,f4.1/'radiation factors           p       sv       sh',         &
!      /,24x,f6.3,3x,f6.3,3x,f6.3)
!
!   iss: points into trace to split p and s arrivals
!
      iss = ifix((spt(ji)-tim1)/dt)
!021313      write(11,424) spt(ji),iss
!  424 format(/'source event split for analysis ',f6.2,' sec from origin',     &
!      ' iss = ',i4)
!      if (iss .le. 0) then
!         print*, 'ERROR! split time is less than begin time, exiting'
!         stop
!      endif
! rotate into radial and transverse of station, remove focal mechanism from source event
! for FKR the records are already in radial and transverse
      do 434 i = 1,npGF
        if(GFtype.eq.'EGF') then
        trarr = trar(i)*cos(bazm1) + trat(i)*cos(bazm2)
        tratt = trar(i)*sin(bazm1) + trat(i)*sin(bazm2)
        else
        trarr = trar(i)
        tratt = trat(i)
        endif
      if(i.lt.iss) then
      traz(i) = traz(i)/ffpp
      trar(i) = trarr/ffpp
      trat(i) = tratt/ffpp
      else
      traz(i) = traz(i)/ffsv
      trar(i) = trarr/ffsv
      trat(i) = tratt/ffsh
      endif
 434  continue
!
      if((filow.eq.0.0.and.filhi.eq.100.0).and.                          &
      (decon.ne.'y'.and.decon.ne.'yes').and.(slp.eq.'ttm')) go to 435
! Filter & calculate moment of Green's Function; both ttm and spc slip models
      lt = ifix(.1/dt)
      nt1 = lt + 1
      nt2 = iss - lt
      nt3 = iss
      nt4 = npGF - lt
       if(nt2.gt.maxegf) then  
       print *,'array elements exceed dimensions, maxegf in synhaz.f'  
       print *,'STOP - SYNHAZ '
       stop
       endif  
      if(filow.ne.0.0.and.filhi.ne.100.) then
      write(6,427) filow,filhi
      else
      print *,'no filter applied'
      endif
! both traces must have same ks value for convolution with slip function
! mp and ms are point shifts of traces after tapering
!
! amplitude factors for cool are applied in subroutine taper
      signi = -1.0
! first P-arrivals
      call taper(maxdim,maxegf,npGF,nt1,nt2,traz,tzpz,n,lt,mp,m1,ks)
      call cool(n,tzpz,signi)
      if(filow.ne.0.0.and.filhi.ne.100.)           &
          call filter(tzpz,ks,dt,filow,filhi,Maxdim)
!
      call taper(maxdim,maxegf,npGF,nt1,nt2,trar,tzpr,n,lt,mp,m1,ks)
      call cool(n,tzpr,signi)
      if(filow.ne.0.0.and.filhi.ne.100.)           &
          call filter(tzpr,ks,dt,filow,filhi,Maxdim)
!
      call taper(maxdim,maxegf,npGF,nt1,nt2,trat,tzpt,n,lt,mp,m1,ks)
      call cool(n,tzpt,signi)
      if(filow.ne.0.0.and.filhi.ne.100.)           &
          call filter(tzpt,ks,dt,filow,filhi,Maxdim)
      mp1 = m1
!
! now S arrivals
      call taper(maxdim,maxegf,npGF,nt3,nt4,traz,tzsz,n,lt,ms,m1,ks)
      call cool(n,tzsz,signi)
      if(filow.ne.0.0.and.filhi.ne.100.)           &
          call filter(tzsz,ks,dt,filow,filhi,Maxdim)
!
      call taper(maxdim,maxegf,npGF,nt3,nt4,trar,tzsr,n,lt,ms,m1,ks)
      call cool(n,tzsr,signi)
      if(filow.ne.0.0.and.filhi.ne.100.)           &
          call filter(tzsr,ks,dt,filow,filhi,Maxdim)
!
      call taper(maxdim,maxegf,npGF,nt3,nt4,trat,tzst,n,lt,ms,m1,ks)
      call cool(n,tzst,signi)
      if(filow.ne.0.0.and.filhi.ne.100.)           &
          call filter(tzst,ks,dt,filow,filhi,Maxdim)
      ms1 = m1
!
! deconvolution with Brune source
      if((decon.ne.'y'.and.decon.ne.'yes').and.(slp.eq.'ttm')) go to 534
      if(decon.ne.'y'.and.decon.ne.'yes') go to 435
!
!021313      write(11,555)
! 555  format('deconvolution with Brune source')
!
! following is source corner frequency from specfit.f     
      ffc(ji) = fc
! following is source corner frequency from sources file
      ffc(ji) = ffc2(ji)
      if(ffc(ji).eq.0.0) then
!021313      write(11,315) evid(ji)
! 315  format('event ', a12,' has no source corner frequency')
      stop
      endif
!
!_LJH-07/15/99 Deconvolve Brune point source spectra from EGF 
      del = 1.0/(float(ks)*dt) 
      j2 = ifix(float(ks)/2. + 1.001) 
      do 55 ki = 1,j2
      ws = float(ki-1)/(float(ks)*dt)
      if(ki.eq.1) ws = 1./(float(ks)*dt)
      ww = pi2*ws
! Brune 1972
      denom = 1.0 + (ws/ffc(ji))**2.
!
!-simple from Brune, zero phase atan(a/b) = 0, implies b = 0.0 and a = 1/denom
      tzpr(ki) = tzpr(ki)*cmplx(denom,0.0)
      tzpt(ki) = tzpt(ki)*cmplx(denom,0.0)
      tzpz(ki) = tzpz(ki)*cmplx(denom,0.0)
      tzsr(ki) = tzsr(ki)*cmplx(denom,0.0)
      tzst(ki) = tzst(ki)*cmplx(denom,0.0)
      tzsz(ki) = tzsz(ki)*cmplx(denom,0.0)
! following is for plotting purposes, obtain displacement signal from acceleration
! remove correction for IFFT applied in taper.f
      if(rec.eq.'ACC') then
      tz1 = tzsr(ki)/cmplx(-ww*ww,0.0)
      tz2 = tzst(ki)/cmplx(-ww*ww,0.0)
      tz3 = tzsz(ki)/cmplx(-ww*ww,0.0)
      endif
      if(rec.eq.'VEL') then
      tz1 = tzsr(ki)/cmplx(0.0,ww)
      tz2 = tzst(ki)/cmplx(0.0,ww)
      tz3 = tzsz(ki)/cmplx(0.0,ww)
      endif
      if(rec.eq.'DIS') then
      tz1 = tzsr(ki)
      tz2 = tzst(ki)
      tz3 = tzsz(ki)
      endif
      y1 = sqrt(real(tz1)**2 + aimag(tz1)**2) 
      y2 = sqrt(real(tz2)**2 + aimag(tz2)**2) 
      y3 = sqrt(real(tz3)**2 + aimag(tz3)**2) 
      barray(ki) = sqrt(y1**2 + y2**2 + y3**2)*float(ks)*dt
! remove correction for IFFT applied in taper.f, ks*dt
      y1 = y1/denom
      y2 = y2/denom
      y3 = y3/denom
      aarray(ki) = sqrt(y1**2 + y2**2 + y3**2)*float(ks)*dt
 55   continue 
!      nnsf1 = GFpath(1:lentemp1)//'Brune_EGF/'//asbb(1:lentemp2)//'_'
!     *//evid(ji)(1:lentemp3)//'_fft.before.Brune'
!      nnsf1 = GFpath(1:lentemp1)//'Brune_EGF/'//asbb(1:lentemp2)//'_'
!     *//evid(ji)(1:lentemp3)//'_fft.Brune.correc'
 534  aphp = -1.*float(-mp)*dt
      aphs = -1.*float(ms1 - mp1 + 1 - ms)*dt
      do 65 i = 1,j2
      ww = pi2*float(i-1)/(float(ks)*dt)
      if(i.eq.1) ww = pi2/(float(ks)*dt)
      zphp = cmplx(cos(ww*aphp),sin(ww*aphp))
      zphs = cmplx(cos(ww*aphs),sin(ww*aphs))
      trazz(i) = tzpz(i)*zphp + tzsz(i)*zphs
      trazr(i) = tzpr(i)*zphp + tzsr(i)*zphs
      trazt(i) = tzpt(i)*zphp + tzst(i)*zphs
 65   continue
      k2 = ks + 1 
      j3 = j2 - 1 
      do 75 i = 2,j3 
      k2 = k2 - 1 
      trazz(k2) = conjg(trazz(i))
      trazr(k2) = conjg(trazr(i))
      trazt(k2) = conjg(trazt(i))
 75   continue
      signi = 1.0
      call cool(n,trazz,signi)
      call cool(n,trazr,signi)
      call cool(n,trazt,signi)
      do 85 i = 1,npGF
      traz(i) = real(trazz(i))
      trar(i) = real(trazr(i))
 85   trat(i) = real(trazt(i))
!
! now use source event for all elements specifying this event
!
 435  do 450 jk = 1,kk
      if(inel(jk).ne.ji) go to 450
      vp = vsp + rh(jk)*apz
      if(rh(jk).gt.zm) vp = vpm
      vs = vp/1.76
      den = (vp-0.35)/1.88
!      rgd1(jk) = (den*vs**2)/10.0
      if(slm.eq.'hsk') rgd1(jk) = 3.0
      selm = rslp(jk)
      jjk = jjk + 1
      jjkk = jjkk + 1
      ajk = float(jjk)/1000.0 - float(ifix(float(jjk)/1000.0))
!
!  idel: total delay in points for stacking in p phases.  Note this assumes that 
!        the synthesized trace starts at sfo seconds.
!  idels: additional delay for s phases
!
      idel = ifix(eltim(jk)/dt + dtimp(jk)/dt + tim1/dt - sfo/dt)
      idels = ifix((dtimp(jk)/dt)*(1.76-1.0))
!
! correction factors for geometrical spreading and attenuation
!
      xlat = rlat(jk)
      xlon = rlon(jk)
      xh   = rh(jk)
      call disazm(ezm,azinv,rdis1,rhdis,xlat,xlon,xh,slat,slon)
      a1 = bhdis/rhdis
!
! set element trace to zero
!
      do 445 i = 1,maxdim
      etrz(i) = cmplx(0.0,0.0)
      etrr(i) = cmplx(0.0,0.0)
  445 etrt(i) = cmplx(0.0,0.0)
!
      if(slp.eq.'spc') go to 455
!
! kostrov slip function
!
!   kr:  number of increments to get desired rise time.  risetime
!        incremented at digitizing rate
!  rslp(jk): total slip desired for element
!  facc: scale seismogram to get contribution from desired moment
!    ss: current slip amplitude at element
! delss: incremental step in slip
!
! incremental values are scaled to provide exact desired offset
! remember rela(km**2) = rela(cm**2 * 1.0e+10)
!
      if(slm.eq.'ksv') kr = ifix(ristm(jk)/dt + 0.0001)
      if(kr.lt.1) kr = 1
      if(slm.eq.'hsk') kr = 1
! since we get rslp(i) from RsqSim and not synhaz.f calculation, need to 
! scale for desired RsqSim amplitude
      sum = 0.0
      tim = 0.0
      do 460 i = 1,kr
      if(kr.eq.1) go to 90
      tim = tim + dt
      ss = 0.81*rstd(jk)*vs*sqrt(tim**2 + 2.0*tim*slptim(jk))/rgd1(jk)
      delss = ss - sum
      sum = sum + delss
!      facc = rgd1(jk)*1.0e+11*rela(jk)*1.0e+10*delss/bm(ji)
      facc = ramo(jk)/(float(kr)*bm(ji))
      nr = ifix(tim/dt + .0001)
   90 if(kr.eq.1) then
      nr = 1
!      facc = rgd1(jk)*1.0e+11*rela(jk)*1.0e+10*rslp(jk)/bm(ji)
      facc = ramo(jk)/bm(ji)
      endif                
      kki = 0
      nrr = nr + npGF
      do 470 ik = nr,nrr
      kki = kki + 1
      etrz(ik) = etrz(ik) + cmplx(traz(kki)*a1*facc,0.0)
      etrr(ik) = etrr(ik) + cmplx(trar(kki)*a1*facc,0.0)
  470 etrt(ik) = etrt(ik) + cmplx(trat(kki)*a1*facc,0.0)
  460 continue
      facmo = facmo + ramo(jk)
!
! apply radiation correction for element
   455 ezm1 = ( eazm(jk) * pi2/360.) - stk1
       
!
! now other parameters
!
      xh   = rh(jk)
      xdis = rdis(jk)
      call linvel(xh,xdis,rp,angii,ainp,apz,vsp,vpm,zm,tt)
      elpp = cos(sv1)*sin(dp1)*(sin(angii)**2)*sin(2.*ezm1)                  &
      - cos(sv1)*cos(dp1)*sin(2.*angii)*cos(ezm1) + sin(sv1)*sin(2.*dp1)     &
      *((cos(angii)**2)-(sin(angii)**2)*(sin(ezm1)**2))                      &
      + sin(sv1)*cos(2.*dp1)*sin(2.*angii)*sin(ezm1)
      if(elpp.lt.0..and.elpp.gt.-.25) elpp = -.25
      if(elpp.ge.0..and.elpp.lt..25) elpp = .25
      if(bstr(ji).eq.0.0.and.bdp(ji).eq.0.0.and.bsv(ji).eq.0.0) elpp = 0.52
       if(rdc.ne.'y'.and.rdc.ne.'yes') elpp = 1.0
!
      elsv = sin(sv1)*cos(2.*dp1)*cos(2.*angii)*sin(ezm1)-cos(sv1)           &
      *cos(dp1)*cos(2.*angii)*cos(ezm1) + .5*cos(sv1)*sin(dp1)               &
      *sin(2.*angii)*sin(2.*ezm1) - .5*sin(sv1)*sin(2.*dp1)                  &
      *sin(2.*angii*(1.+sin(ezm1)**2))
      if(elsv.lt.0..and.elsv.gt.-.25) elsv = -.25
      if(elsv.ge.0..and.elsv.lt..25) elsv = .25
      if(bstr(ji).eq.0.0.and.bdp(ji).eq.0.0.and.bsv(ji).eq.0.0) elsv = 0.48
      if(rdc.ne.'y'.and.rdc.ne.'yes') elsv = 1.0
!
      elsh = cos(sv1)*cos(dp1)*cos(angii)*sin(ezm1)                          &
      + cos(sv1)*sin(dp1)*sin(angii)*cos(2.*ezm1) + sin(sv1)*cos(2.*dp1)     &
      *cos(angii)*cos(ezm1) - .5*sin(sv1)*sin(2.*dp1)*sin(angii)*sin(2.*ezm1)
      if(elsh.lt.0..and.elsh.gt.-.25) elsh = -.25
      if(elsh.ge.0..and.elsh.lt..25) elsh = .25
      if(bstr(ji).eq.0.0.and.bdp(ji).eq.0.0.and.bsv(ji).eq.0.0) elsh = 0.41
      if(rdc.ne.'y'.and.rdc.ne.'yes') elsh = 1.0
!
! rotate etrz(i),etrr(i),etrt(i) into station coordinates:
!                       trz(i) up-comp, trr(i) north, trt(i) west
!
! put in p and s delay, source event of delay, rupture delay, and
! add into synthetic seismogram
!
!
      azm1 = (eazm(jk) - bazm) * 0.017453
      sazm1 = (sazm - eazm(jk)) * 0.017453
      if(slp.eq.'ttm') go to 420
!
! convolve slip function apply phase delays
!
! phase delay
      aphp = -1.*float(idel - mp)*dt
      aphs = -1.*float(idel + idels + (ms1-mp1+1) - ms)*dt
!
      if(ristm(jk).ne.0.0) aslp = rslp(jk)/ristm(jk)
      if(ristm(jk).eq.0.0) aslp = 0.0
!
! ramp source-time function, deconvolved with step function of green's
!
      j2 = ifix(float(ks)/2. + 1.001)
!
! correction factors for geometrical spreading and attenuation
!
      rhdis = sqrt(rh(jk)**2 + rdis(jk)**2)
!
! remember ws in linear frequency
! ww is angular frequency
!
! slip function is analytically deconvolved with a step function
!
!-----LJH 02/15/00 calculate variables outside do loop
      fac4 = rela(jk)*rgd1(jk)*1.0e+11*1.0e+10/bm(ji)
! a1 calculated above
      a2 = bhdis-rhdis
      a3 = 2.*vqp*q
      a4 = 2.*vqs*q
      a5 = sin(azm1)
      a6 = cos(azm1)
      a7 = sin(sazm1)
      a8 = cos(sazm1)
      do 430 is = 1,j2
      ww = pi2*float(is-1)/(float(ks)*dt)
      rpfac = fac4*elpp*a1*exp(ww*a2/a3)
      rrfac = fac4*elsv*a1*exp(ww*a2/a4)
      rtfac = fac4*elsh*a1*exp(ww*a2/a4)
      zphp = cmplx(cos(ww*aphp),sin(ww*aphp))
      zphs = cmplx(cos(ww*aphs),sin(ww*aphs))
      if(ww.ne.0.0) eslp = cmplx(0.0,(aslp/ww))*cmplx(cos(ww*ristm(jk)),    &
      -sin(ww*ristm(jk))) - cmplx(0.0,(aslp/ww))
      if(ww.eq.0.0) eslp = cmplx(0.0,0.0) 
! next for step function
      if(ristm(jk).eq.0.0) eslp = cmplx(rslp(jk),0.0)
!
! apply phase delays and FMS
      etrz(is) = tzpz(is)*eslp*zphp*rpfac + tzsz(is)*eslp*zphs*rrfac
!
!-LJH 04/19/00 the following lines have been extended to long strings because
! a bug appeared if the lines were broken up into seperate lines.  The bug
! results in a scrambling of phase, and there is no fortran warning.
! 
! rotate to radial and tangential of station, apply radiation correction
      etrar=(tzpt(is)*eslp*zphp*rpfac                                        &
      + tzst(is)*eslp*zphs*rtfac)*cmplx(a5,0.0)+(tzpr(is)*eslp*zphp*rpfac    &
      + tzsr(is)*eslp*zphs*rrfac)*cmplx(a6,0.0)
      etrat=(tzpt(is)*eslp*zphp*rpfac                                        &
      + tzst(is)*eslp*zphs*rtfac)*cmplx(a6,0.0)-(tzpr(is)*eslp*zphp*rpfac    &
      + tzsr(is)*eslp*zphs*rrfac)*cmplx(a5,0.0)
      etrr(is) = etrar
      etrt(is) = etrat
!
! now rotate to sazm for trace 2 and sazm + 90.0 for trace 3
      tr2=etrt(is)*cmplx(a7,0.0)+etrr(is)*cmplx(a8,0.0)
      tr3=etrt(is)*cmplx(a8,0.0)-etrr(is)*cmplx(a7,0.0)
!
!
      trz(is) = trz(is) + etrz(is)
      trr(is) = trr(is) + tr2
  430 trt(is) = trt(is) + tr3
!
! conjugate of trz,trr,trt done in subroutine filter
!
      go to 450
!
! apply radiation correction for element
!
  420 do 480 i = 1,npGF
      if(i.lt.iss) then
      elac1 = cmplx(elpp,0.0)
      elac2 = cmplx(elpp,0.0)
      else
      elac1 = cmplx(elsh,0.0)
      elac2 = cmplx(elsv,0.0)
      endif
!
! - 11.22.07      etraz = etrz(i)/fac4*elsv
      etraz = etrz(i)
!
! rotate to station radial and tangential
      etrar=etrt(i)*cmplx(sin(azm1),0.0) + etrr(i)*cmplx(cos(azm1),0.0)
      etrat=etrt(i)*cmplx(cos(azm1),0.0) - etrr(i)*cmplx(sin(azm1),0.0)
!
! now rotate to sazm and sazm + 90.0
      tr1 = etraz*elac2
      tr2 = etrat*elac1*cmplx(sin(sazm1),0.0) + etrar*elac2*cmplx(cos(sazm1),0.0)
      tr3 = etrat*elac1*cmplx(cos(sazm1),0.0) - etrar*elac2*cmplx(sin(sazm1),0.0)
!
      idel = idel + 1
      if(i.eq.iss) idel = idel + idels
      if(idel.lt.1) go to 480
      if(idel.gt.maxdim) then
      print *,'idel GT maxdim'
      stop
      endif
      trz(idel) = trz(idel) + tr1
      trr(idel) = trr(idel) + tr2
      trt(idel) = trt(idel) + tr3
  480 continue
  450 continue
  400 continue
      if(slp.eq.'ttm') go to 490
!
! specta correction done in taper
      k2 = ks + 1
      j3 = j2 - 1
      do 465 i = 2,j3
      k2 = k2 - 1
      trz(k2) = conjg(trz(i))
      trr(k2) = conjg(trr(i))
  465 trt(k2) = conjg(trt(i))
!
      signi = 1.0
      call cool(n,trz,signi)
      call cool(n,trr,signi)
      call cool(n,trt,signi)
 490  do 486 i = 1,maxsyn
      trcz(i) = (mopass/facmo)*real(trz(i))
      trcr(i) = (mopass/facmo)*real(trr(i))
      trct(i) = (mopass/facmo)*real(trt(i))
  486 continue
      
      call peak(trcz,trcr,trct,maxsyn,synpeak)
         !do ifreq = 1, 200
         !print *, trcz(ifreq), trcr(ifreq) , trct(ifreq)
         !end do


!*******************************************************************************
!     Response Spectra
!     ----------------  
      if (synflagRS .gt. 0)  then
          !call RespSpectra (damp, freq, INgm, npGF, nfreq,  &
          !                 OUTgm, PARS, dt)

          do i = 1, nfreq
              omega = pi2 * freq(i)
              call rscalc_interp_acc (trct,npGF,omega,damp,dt,rd,rv,absacc)
              PARS(i) = absacc
          end do
      end if

!      print *, '  '
 !     print *, ' Frequency     Spectral Value    in SYNHAZ'
  !    do ifreq = 1, nfreq
   !       print *, freq(ifreq), PARS(ifreq)
    !  end do  

!END RESPONSE SPECTRA **********************************************************



!
      aradius = sqrt(aarea*2./pi2)
      aradiusmeter = aradius*1000.
      write(6,108) kk,mopass,rstd(1),avgslp,aradiusmeter,maxristm
      if(synflag.eq.1) write(6,109) synpeak
      if(synflag.eq.2) write(6,110) synpeak
      if(synflag.eq.3) write(6,111) synpeak
!
      !LJH DEBUGG^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      !To print the seismograms
!      if(kk.gt.10000) then
!      print *,'stop here synhaz.f90 '
!      open(unit=18,file='tra.z')
!      open(unit=19,file='tra.r')
!      open(unit=20,file='tra.t')
!      timm1 = sfo
!      mag = (alog10(mopass) - 16.05)/1.5
!      print *,'mag = ',mag,' hypo dist = ',adis
!      do i = 1,maxsyn
!      timm1 = timm1 + dt
!      write(18,'(f6.2,1x,f10.5)') timm1,trcz(i)
!      write(19,'(f6.2,1x,f10.5)') timm1,trcr(i)
!      write(20,'(f6.2,1x,f10.5)') timm1,trct(i)
!      end do
!      close(18)
!      close(19)
!      close(20)
!      stop
!      endif
!!LJH END print debugg^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      if (rsq.eq.'y'.or.rsq.eq.'yes') go to 1000
      if(nfsp.eq.0) go to 1000
      do 2001 i = 1,nfsp
      cplta(i) = rslp(icplta(i))
      asprstm(i) = ristm(icplta(i))
      aspslpvel(i) = slpvl(icplta(i))
 2001 aspstress(i) = aspslpvel(i)/0.81
!
      cplta(nfsp+1) = rslp(icplt)
      asprstm(nfsp+1) = ristm(icplt)
      aspslpvel(nfsp+1) = slpvl(icplt)
      aspstress(nfsp+1) = aspslpvel(nfsp+1)/0.81
      aspf(nfsp+1) = nnsf2
      aspmo(nfsp+1) = mo1
      asparea(nfsp+1) = aarea
!
      if(aarea.gt.100) call ruptureout(aspf,cplta,asprstm,       &
      aspslpvel,aspmo,asparea,aspstress,nfsp)
 1000 close(11)
      return
      end subroutine synhaz

