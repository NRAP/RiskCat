!This subroutine is called by subroutine RISKCAT
!
!*******************************************************************************
!
        subroutine PlotRegional ( CSTR1, iepis, Kscale, nbin, NMspl, Nscreen, &
                                Ntp1, pagenumber, TrackNumber, yearscat )                       
!
!*******************************************************************************
!       This routine plots 
!           - The average regional recurrence curves for the Standard maps
!             of sources and reccurence parameters,
!           - The unscaled regional recurrence curve, averaged over all
!             aleatory sampled catalogs
!           - The scaled regional recurrence curve, averaged over all
!             aleatory sampled catalogs
!
!       The scaling is done on the moments. For a scaling factor Kscale, each 
!       moment values Mo is changed to Kscale.Mo
!       The standard recurrence curve for the catalog is first created
!       Log10(Neq=m) = f(m)
!       Since m ~ 2/3 Log10(Mo), 
!       Then change of m axis to reflect the scaling:
!       m' = m + 2/3.Log10(Kscale)
!
!       Input:
!           - Set of aleatory catalogs (minimum number is 1)
!           - Regional annual recurrence array: LTSReg()
!           - Catalog 
!           - Moment scaling: Kscale
!           - Number of aleatory simulations or catalogs: NMspl
!           - Catalogs' length in years: Nyears

!           - Flag for plotting: Plotcalibration (1 for "Yes", 0 for "No")
!           - Unit number for printing recurrence curves: printunit
!
!       Output:
!           - Tables of recurrence curves, printed in general output file
!           - Plot of all recurrence curves
!
!*******************************************************************************
!
        USE ModuleEq

        IMPLICIT NONE

        INTEGER :: i, iepis, ispl, j, j1, jwdt, kmag, ktmw, N, nbin, Nc, Nev
        INTEGER :: Nf, NMspl, Nportrait, Nscreen, Ntp1
        INTEGER :: Nview, Nwidth1, Nwidth2
        INTEGER :: Pagenumber, pflag, Tracknumber

        REAL    :: Kscale,  time, wm, w1, w2, w3, xM, Years, yearscat

        !Declarations for plotting
        CHARACTER (LEN=41) :: CSTR1
        CHARACTER (LEN=20) :: CSTR2
        CHARACTER (LEN=60) :: CTIT1
        CHARACTER (LEN=27) :: CTIT2        
        CHARACTER (LEN=20) :: LegendTIT
        CHARACTER (LEN=5)  :: NameEXT
        CHARACTER (LEN=80) :: NameRoot
        CHARACTER (len=256):: source 
        
        CHARACTER (len=20),dimension(6)      :: Legperc
 
        REAL,    ALLOCATABLE, dimension(:,:) :: xcumul   
        REAL,    ALLOCATABLE, dimension(:,:) :: XX, YY    
        

!*******************************************************************************
!
        Years = yearscat

        !Arrays allocation
        ALLOCATE ( xcumul(3,nMagbins) )
        ALLOCATE ( XX(4,nMagBins)    , YY(4,nMagBins)     )
        do j = 1,nMagBins
            do i = 1, 3
                xcumul(i,j) = 0.
                XX(i,j)     = 0.
                YY(i,j)     = 0.
            end do
            XX(4,j)         = 0.
            YY(4,j)         = 0.
        end do

        !MAGNITUDE HISTOGRAMS FOR REGIONAL SEISMICITY:
        !---------------------------------------------
        !Cumul number of events in each Mag bin, over the NMspl catalogs 
        !and obtain a representative occurrence distribution 
        do ispl = 1, NMspl

            !Number of events in this catalog 
            Nev  = NCatevents(ispl)
                
            !For full catalog, unscaled
            !cumul all events simulated in NMspl catalogs
            do j = 1, Nev
                xM   = CatEqMag(ispl,j)
                time = CatEqTime(ispl,j)
                call GetBin(j1,NMagBins,VMagBnd,xM)
                if (j1 .gt. 0)  then
                    !Magnitude calibration weights must be applied
                    call IndexMagTmw (kmag, ktmw, xM, nbin, Ntp1, time)
                    wm = wMagbin(kmag,ktmw)
                    xcumul(1,j1) = xcumul(1,j1) + wm
                end if
            end do
                  
            !For full catalogs, scaled
            do j = 1, Nev
                xM   = CatEqMag(ispl,j)
                time = CatEqTime(ispl,j)
                call IndexMagTmw (kmag, ktmw, xM, nbin, Ntp1, time)
                wm = wMagbin(kmag,ktmw)
                xM = CatEqMag(ispl,j) + 0.666*log10( Kscale )
                call GetBin(j1,NMagBins,VMagBnd,xM)
                if (j1 .gt. 0)  then
                    xcumul(2,j1) = xcumul(2,j1) + wm
                end if
            end do

            !For long-term after perturbation period ended   
            !for Moment scaled catalogs   
            !Skip the events during the perturbation period
            do j = nRelax(ispl), Nev
                time = CatEqTime(ispl,j)
                xM   = CatEqMag(ispl,j)
                call IndexMagTmw (kmag, ktmw, xM, nbin, Ntp1, time)
                wm = wMagbin(kmag,ktmw)
                xM = CatEqMag(ispl,j) + 0.666*log10( Kscale )
                call GetBin(j1,NMagBins,VMagBnd,xM)
                if (j1 .gt. 0)  then
                    xcumul(3,j1) = xcumul(3,j1) + wm
                end if
            end do            
        end do

        do i = 1, nMagBins
           print *, 'xcumul 1,2,3=',(xcumul(j,i),j=1,3)
        end do


        !Regional magnitude histograms for each set of NMspl catalogs:
        !-------------------------------------------------------------
        !Normalized to ONE year (there are NMspl catalogs stacked, for a
        !duration of Years (minus the perturbation period for Long-Term period)
        w1 = Years * Float(NMspl)
        w2 = w1
        w3 = Years * Float(NMspl)
        do j = 1, nMagBins
            xcumul(1,j) = xcumul(1,j) / w1
            xcumul(2,j) = xcumul(2,j) / w2
            xcumul(3,j) = xcumul(3,j) / w3
        end do 


        !Create resulting complementary cumulative functions:
        !----------------------------------------------------    
        !Initialize Regional seismicity from seimic source maps
        CumLTS(nMagBins)   = LTSReg(nMagBins)

        do i = (nMagBins-1), 1, -1
            xcumul(1,i) = xcumul(1,i+1) +  xcumul(1,i)
            xcumul(2,i) = xcumul(2,i+1) +  xcumul(2,i)
            xcumul(3,i) = xcumul(3,i+1) +  xcumul(3,i)
            CumLTS(i)   = CUmLTS(i+1)   +  LTSReg(i)
        end do

        do i = 1, nMagBins
           print *, 'CumLTS,xcumul 1,2,3=',CumLTS(i),(xcumul(j,i),j=1,3)
        end do

        
!*******************************************************************************
!
!      Plot Regional mean recurrence curves- Natural and simulated seismicity:
!      ----------------------------------------------------------------------
!
!       Assignements:

!       CSTR1,  = For the identification title at top of the page    
!       CSTR2     which includes:
!                 "string CSTR1"/Date and Time of the Run/"String CSTR2"      
!       jwdt    = Selection of Date and Time page header, and page number
!               = 1, YES
!               = 0, NO Date and time page header, and NO page number
!       LegendTIT   = Legend Title
!       Maxmag  = Max number of magnitude points on X axis
!       N       = Number of points in each curve to be plotted (same for all)
!       NameModels  = Name of the models plotted
!       Nc      = Number of curves
!       Nf      = number of points in each curve
!       Nportrait   = 1 plot in portrait layout
!                     2 plot in landscape layout
!       Nscreen = For Nview=1 only (Screen Display), counts number of frames 
!                 displayed. This number runs through the entire application
!       Nwidth1 = Width of the major lines (Nwidth=15 default)
!       Nwidth2 = Width of other lines (good number is Nwidth2=10)
!       Nview   = Selects the screen or file format (See above note)
!       pagenumber  = For (Nview.NE.1 (storing on file'NamePlotRec'), keeps
!                 a counter of number of pages.
!       PlotCalibration = Plotting control. (1=Yes, 0=No)
!       SOURCE  = Name of Seismic SOURCE
!       TEXTREC = Root name for the Recurrence plot file (to which we add
!                 the selected extension, .png, .pdf, ...)
!       XMAG1   = Second part of the second line in the figure title
!       X(I,J)  = Array of max dimension (Nf,NcMax) of X values for Nc curves
!       Y(i,j)  = Array of max dimension (Nf,NcMax) of Y values for Nc curves
     
!*******************************************************************************

#ifdef USE_DISLIN

!         Output graphic file format and default settings
              jwdt = 1
              NameEXT    = 'CONS '
              !Create the root name of the output plot file
              if (Tracknumber.lt. 10)  then
                  if ( iepis .lt. 10 )  then
                      write ( Nameroot,"('PL00',i1,'A-00',i1)" )  &
                      TrackNumber,iepis
                  elseif ( iepis .lt. 100)  then
                      write (Nameroot,"('PL00',i1,'A-0',i2)" )    &
                      TrackNumber,iepis
                  else
                      write (Nameroot,"('PL00',i1,'A-',i3)" )      &
                      TrackNumber,iepis
                  end if
              elseif (Tracknumber .lt. 100)  then
                  if ( iepis .lt. 10 )  then
                      write ( Nameroot,"('PL0',i2,'A-00',i1)" )  &
                      TrackNumber,iepis
                  elseif ( iepis .lt. 100)  then
                      write (Nameroot,"('PL0',i2,'A-0',i2)" )    &
                      TrackNumber,iepis
                  else
                      write (Nameroot,"('PL0',i2,'A-',i3)" )      &
                      TrackNumber,iepis
                  end if
              else
                  if ( iepis .lt. 10 )  then
                      write ( Nameroot,"('PL',i3,'A-00',i1)" )  &
                      TrackNumber,iepis
                  elseif ( iepis .lt. 100)  then
                      write (Nameroot,"('PL',i3,'A-0',i2)" )    &
                      TrackNumber,iepis
                  else
                      write (Nameroot,"('PL',i3,'A-',i3)" )      &
                      TrackNumber,iepis
                  end if
              end if

              Nportrait  = 1
              Nview      = 1
              NWIDTH1    = 15
              NWIDTH2    = 15
                    
      !       Header title for plots    
              CSTR2 = '---- Jean Savy, SRC'
              CTIT1 = 'Regional Seismicity Calibration'
              CTIT2 = 'Epistemic Simulation:'
            
              !Legend labels for percentile hazard curves
              write (LegendTIT,"(a4,I2,a11,I2)") 'Spl=',   &
              NMspl,'     iepis=',iepis
              Legperc(1) = 'Natural Seismicity'
              Legperc(2) = 'Full Cat. Unscaled'
              Legperc(3) = 'Full Cat. Scaled'
              Legperc(4) = 'L-T Cat.  Scaled'
              !  	 
              N     = nMagBins
              Nc    = 4
              Nf    = nMagBins
              source   = ' '
              
              YY   = 0.  
              do j = 1, N
                  XX(1,j) = (xMagLBnd(j) + xMagHBnd(j)) / 2.
                  if (CumLTS(j).gt. 1.e-7) then
                      YY(1,j)  = CumLTS(j)
                  endif
                  do j1 = 1, 3
                     XX(j1+1,j) = XX(1,j)
                     if (xcumul(j1,j).gt. 1.e-7)    then
                         YY(j1+1,j) = xcumul(j1,j)  
                     endif           
                  end do
              end do

      !        do i = 1, N
       !           print *,'XX,YY1,2,3,4=',XX(1,i),(YY(j,i),j=1,4)
        !      end do        
  
!.........Plot  Regional Recurrence Calibration curves: 
!             Natural and Simulated seismicity 
              do i = 1,2
                  pflag = 1
                  if ((i.eq.1) .and. (PlotScreen.ne."Display-Screen"))  then
                      pflag = 0
                  end if
                  if (pflag .eq. 1)  then
                      CALL PlotRecurrence (CSTR1,CSTR2,CTIT1,CTIT2,jwdt,   &
                      LegendTIT,N,N,NameEXT,Legperc,NameRoot,iepis,source, &
                      Nc,Nportrait,Nscreen,NVIEW,NWIDTH1,pagenumber,XX,YY)
                  end if
                  
                  if (i .eq. 2)  then
                     CALL DISFIN()
                  end if
                  NameEXT = 'PDF  '
                  NVIEW = 2
              end do

  
!.........END Plot Regional Reccurrence----------------------------------------

#endif 

        DEALLOCATE ( xcumul, XX, YY )

        return

        END SUBROUTINE PlotRegional
