
!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss

      subroutine getmagi (ns,ne,amag,x,indx,iflag,Maxmag)

!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss

! this routine produces the index of the nearest value in a
! vector, given an input value

      IMPLICIT NONE                                         

      INTEGER :: i, iflag, indx, Maxmag, ne, ns
      REAL    :: t1, t2, x
      REAL, dimension(Maxmag) :: amag

! make sure program stops when iflag=99
       iflag=0
       if (x .lt. amag(ns)) then
          indx=ns
          return
       endif

       do  i=ns,ne

          if (x .eq. amag(i))  then
              indx = i   
              return          
          endif
    
          if (x .gt. amag(i))  then
              iflag = 99
              return
          endif

          if (x .lt. amag(i))  then
              t1 = x-amag(i-1)
              t2 = amag(i)-x
              if (t1 .lt. t2)  then
                  indx = i - 1
                  return
               else
                  indx = i
                  return
               endif
          endif

       end do   

       return
       end
