! 
!                   This routine calculates the Response spectra 
!
!*******************************************************************************
!
        subroutine RespSpectra (damp, freq, INgm, ndata, nfreq, OUTgm, &
                                PARS, ts)
!
!*******************************************************************************
!
!       Calculates the response spectra of the horizontal component of motion
!
!       Calculated spectra can be either of acceleration or velocity
!       Input time-series can also be either acceleration or velocity
!
! Input:
!       damp  = Reduceddamping factor. Ex: dzeta = 0.05 = 5% Damping of 1 DOF
!       freq  : Array of frequencies for which  spectra are calculated (Hz)
!       INgm  = Flag that indicates input ground-motion unit:
!               INgm  = 1, Input time-series are in velocity   (cm/s)
!                       2, Input time-series in acceleration (cm/s/s)
!       ndata = Number of data points in the time-series
!       nfreq = Number of frequency data points for the spectra
!       OUTgm = Flag for ground-motion Response Spectra units
!               OUTgm = 1, Output spectra in velocity       (cm/s)
!                       2, Output spectra in acceleration (cm/s/s)
!       traar = Array, Ground-motion horizontal Radial component, dim(ndata).
!       traat = Array, Horizontal transverse component, dimension(ndata).
!       ts    : time step (sec.) of velocity time-series
!
! Ouput: 
!       PARS  = Array (Dimension nfreq) of spectral values
!
!*******************************************************************************
!
        USE ModuleSYNHAZ

        IMPLICIT NONE

        INTEGER :: INgm, k, ndata, nfreq, OUTgm
        INTEGER ,  dimension(2,2) :: CaseGM
        REAL    :: damp, omega, pi2, ts
        REAL    ,  dimension(nfreq) :: freq, PARS

        CaseGM(1,1) = 1   !Case of input T/H velocity, output RS velocity
        CaseGM(1,2) = 2   !................. velocity, ......... acceleration
        CaseGM(2,1) = 3   !................. acceleration, ......velocity
        CaseGM(2,2) = 4   !................. acceleration....... acceleration


        pi2 = 6.28318531  !2 pi


        SELECT CASE ( CaseGM(INgm,OUTgm) )

            Case ( 1 )
                !Vel in, Vel out
                call RSvit (damp, freq, ndata, nfreq, PARS, ts, trct)

            Case ( 2 )
                !Vel in, Acc out
                call RSvit (damp, freq, ndata, nfreq, PARS, ts, trct)
                    !PSR-acc = PSR-vel * (omega)
                do k = 1, nfreq
                    omega   = pi2 * freq(k)
                    PARS(k) = PARS(k) * omega
                end do

            Case ( 3 )
                !Acc in, Vel out
                call RSacc (damp, freq, ndata, nfreq, PARS, ts, trct)
                !PSR-acc = PSR-vel * (omega)
                do k = 1, nfreq
                    omega   = pi2 * freq(k)
                    PARS(k) = PARS(k) / omega
                end do                

            Case (4)
                !Acc in, Acc out
                call RSacc (damp, freq, ndata, nfreq, PARS, ts, trct)
   
            Case Default
                !Did not get Ground-Motion typeselection options right
                Write (6,*) '>>>>>>> Did not get ground-motion type selection'
                Write (6,*) '>>>>>>> right for input and output.'
                Write (6,*) '>>>>>>> Please check Menu file for input of' 
                Write (6,*) '>>>>>>> Ground-motion type, i.e. Vel or Acc'
                Write (6,*) '>>>>>>> STOP here.'
                STOP
 
        END SELECT

        return
  
        end subroutine RespSpectra





        
