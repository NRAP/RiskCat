      subroutine regionall(region,apz,vsp,vpm,zm,q,ma,mb)

      CHARACTER (LEN=80) :: region

      REAL ::  apz, ma, mb, q, vpm, vsp, zm



! conversion to moment using Fletcher, et al (bssa v.74,no.4)
!      Mo = 10.**(1.1*M + 18.)
! conversion to moment from USGS duration magnitude for northern San
!      Andreas Bakun (bssa v.74, no. 2)
!      Mo = 10.**(1.2*Md + 17.)
! conversion to moment/10**20 for San Fernando from Hutchings thesis
!      Mo = 10.**(1.271*Ml + 18.)
! conversion to moment from Kanamori, JGR 82, 2981-2987, 1977
!      Mo = 10.**(1.5*Mw+  16.05)
!
!1778  continue
!     write(6,1777)
!1777  Format(/,'Enter selection for regional parameters:'//,
!     *'region1, Loma Prieta:  Q=999,  Mo=10**(1.2xMl+17.0),  vp=0.065z +
 !    * 5.4, 8.0@25.0'/
  !   *'region2, San Fernando: Q=250,  Mo=10**(1.27xMl+18.0),  vp=0.20z +
   !  * 3.0, 6.5@15.0'/
    ! *'region3, Northridge:   Q=250,  Mo=10**(1.5Mw+16.05),  vp=0.04z  +
     !* 6.0, 7.8@32.0'/
!     *'region4, SanAndreas:   Q=999,  Mo=10**(1.2xMl+17.0),  vp=0.070z +
 !    * 5.0, 8.0@21.5'/
  !   *'region5, Hayward:      Q=250,  Mo=10**(1.2xMl+17.0),  vp=0.182z +
   !  * 4.0, 6.5@11.0'/
    ! *'region6, Athens:       Q=200,  Mo=10**(1.5xMw+16.05),  vp=0.10z +
     !* 4.8, 7.3@25.0'/
!     *'region7, Saguenay:     Q=999,  Mo=10**(1.2xMb+17.0),  vp=0.020z +
 !    * 6.0, 8.0@45.0'/
  !   *'region8, CapeMendocino:Q=500,  Mo=10**(1.2xMl+17.0),  vp=0.104z +
   !  * 5.0, 8.0@25.0'/,
    ! *'region9, SantaBarbara: Q=300,  Mo=10**(1.5Mw+16.05),  vp=0.75z  +
     !* 1.8, 6.3@4.0'/,
!     *'region10, NewZealand:  Q=400,  Mo=10**(1.5Mw+16.05),  vp=0.072z +
 !    * 5.3, 8.2@40.0'/,
  !   *'region11, Colfiorito:  Q=130,  Mo=10**(1.5Mw+16.05),  vp=0.086z +
   !  * 5.4, 6.7@15.0'/,
    ! *'region12, WestTurkey:  Q=180,  Mo=10**(1.5Mw+16.05),  vp=0.165z +
     !* 4.4, 8.0@30.0'/,
!     *'region13, Molise:      Q=300,  Mo=10**(1.5Mw+16.05),  vp=0.110z +
 !    * 4.5, 8.0@30.0'/,
  !   *'region14, Tbilisi:     Q=300,  Mo=10**(1.5Mw+16.05),  vp=0.110z +
   !  * 4.5, 8.0@30.0'/,
    ! *'region15, Caucasus:    Q=300,  Mo=10**(1.5Mw+16.05),  vp=0.110z +
     !* 4.5, 8.0@30.0'/,
!     *'region16, Salton Sea:  Q=200,  Mo=10**(1.5Mw+16.05),  vp=0.600z +
 !    * 2.0, 5.1@5.0'/,
  !   *'region17, Geysers:     Q=200,  Mo=10**(1.5Mw+16.05),  vp=0.600z +
   !  * 2.0, 5.1@5.0'/,
    ! *'region18, Alexandria:  Q=200,  Mo=10**(1.5Mw+16.05),  vp=0.600z +
     !* 2.0, 5.1@5.0'/,
!     *'region19, DOE_NE:      Q=200,  Mo=10**(1.5Mw+16.05),  vp=0.022z +
 !    * 5.9, 7.0@17.8.0')
!      read(5,*) region
!      write(6,'(/a10)') region
      if(region.ne.'region1'.and.region.ne.'region2'.and.region.ne.     &
      'region3'.and.region.ne.'region4'.and.region.ne.'region5'.and.    &
      region.ne.'region6'.and.region.ne.'region7'.and.region.ne.        &
      'region8'.and.region.ne.'region9'.and.region.ne.'region10'.and.   &
      region.ne.'region11'.and.region.ne.'region12'.and.region.ne.      &
      'region13'.and.region.ne.'region14'.and.region.ne.'region15'.and. &
      region.ne.'region16'.and.region.ne.'region17'.and.region.ne.      &
      'region18'.and.region.ne.'region19') then
      print *,'you need to enter region1, region2, etc.'
!      go to 1778
      endif
      if(region.eq.'region1') then
      apz=0.065
      vsp=5.4
      vpm=8.0
      zm=25.0
      q=999.
      ma=1.2
      mb=17.0
      endif
      if(region.eq.'region2') then
      apz=0.20
      vsp=3.0
      vpm=6.5
      zm=15.0
      q=250.0
      ma=1.27
      mb=18.0
      endif
      if(region.eq.'region3') then
      apz=0.04
      vsp=6.0
      vpm=7.8
      zm=32.0
      q=250.
      ma=1.5
      mb=16.05
      endif
      if(region.eq.'region4') then
      apz=0.07
      vsp=5.0
      vpm=8.0
      zm=21.5
      q=999.
      ma=1.2
      mb=17.0
      endif
      if(region.eq.'region5') then
      apz=0.182
      vsp=4.0
      vpm=6.5
      zm=11.0
      q=250.
      ma=1.2
      mb=17.0
      endif
      if(region.eq.'region6') then
      apz=0.1
      vsp=4.8
      vpm=7.3
      zm=25.0
      q=200.
      ma=1.5
      mb=16.05
      endif
      if(region.eq.'region7') then
      apz=0.02
      vsp=6.0
      vpm=8.0
      zm=45.0
      q=999.
      ma=1.2
      mb=17.0
      endif
      if(region.eq.'region8') then
      apz=0.104
      vsp=5.0
      vpm=8.0
      zm=25.0
      q=500.
      ma=1.2
      mb=17.0
      endif
      if(region.eq.'region9') then
      apz=0.75
      vsp=1.8
      vpm=6.3
      zm=4.0
      q=300.
      ma=1.5
      mb=16.05
      endif
      if(region.eq.'region10') then
      apz=0.0752
      vsp=5.3
      vpm=8.2
      zm=40.0
      q=400.
      ma=1.5
      mb=16.05
      endif
      if(region.eq.'region11') then
      apz=0.086
      vsp=5.4
      vpm=6.7
      zm=15.0
      q=130.
      ma=1.5
      mb=16.095
      endif
      if(region.eq.'region12') then
      apz=0.165
      vsp=4.4
      vpm=8.0
      zm=30.0
      q=1000.
      ma=1.5
      mb=16.05
      endif
      if(region.eq.'region13') then
      apz=0.110
      vsp=4.5
      vpm=8.0
      zm=30.0
      q=300.
      ma=1.5
      mb=16.05
      endif
      if(region.eq.'region14') then
      apz=0.110
      vsp=4.5
      vpm=8.0
      zm=30.0
      q=300.
      ma=1.5
      mb=16.05
      endif
      if(region.eq.'region15') then
      apz=0.110
      vsp=4.5
      vpm=8.0
      zm=30.0
      q=300.
      ma=1.5
      mb=16.05
      endif
      if(region.eq.'region16') then
      apz=0.600
      vsp=2.0
      vpm=5.1
      zm=5.0
      q=200.
      ma=1.5
      mb=16.05
      endif
      if(region.eq.'region17') then
      apz=0.600
      vsp=2.0
      vpm=5.1
      zm=5.0
      q=200.
      ma=1.5
      mb=16.05
      endif
      if(region.eq.'region18') then
      apz=0.600
      vsp=2.0
      vpm=5.1
      zm=5.0
      q=200.
      ma=1.5
      mb=16.05
      endif
      if(region.eq.'region19') then
      apz=0.0193
      vsp=5.9
      vpm=7.0
      zm=34.5
      q=130.
      ma=1.5
      mb=16.05
      endif

      return

      end subroutine regionall
