!
!........This module defines the general parameters of the solution
!
        Module ModuleSYNHAZ
        
        use ModuleEq

        implicit none

!Variables to be passed to SYNHAZ from simmulperil
        INTEGER, PARAMETER :: MaxSynCorners = 1000
        INTEGER, PARAMETER :: MaxSynSubs    = 17100  !Max # fault elts
        INTEGER, PARAMETER :: MaxSubEq      = 85000  !Max # of sub eq in a Main

        INTEGER            :: icorner, Synl1, Synl2, Synl3, Synl4
        INTEGER            :: Ncorners, NSynSubElt  

        INTEGER, DIMENSION(MaxEq)        :: nsubelt
        INTEGER, DIMENSION(MaxSubEq)     :: SynEqSubMap

        CHARACTER (LEN=80) :: synregion
        CHARACTER (LEN=20) :: SynStation

        REAL    :: dtGF, rgd2
        REAL    :: SigmaSynhaz,SynDip, SynHazAreaMin,SynStationX,SynStationY
        REAL    :: SynStrike, SynSV, SynVh, SynVr
        REAL    :: xnSigmaSynhaz

        REAL, DIMENSION(Maxepis)         :: scaleM0
        REAL, DIMENSION(MaxSynSubs)      :: SynAreaSub,SynRdpp
        REAL, DIMENSION(MaxSynSubs)      :: SynRvh,SynRsv
        REAL, DIMENSION(MaxSynSubs)      :: SynSubDip,SynSubSlipRate,SynSubRake
        REAL, DIMENSION(MaxSynSubs)      :: SynSubstk
        REAL, Dimension(MaxSYNCorners,3) :: SynCorners
        REAL, Dimension(3)               :: SynXYZinit
        !REAL, Dimension(MaxSubEq,3)      :: SynXYZsub

        REAL, DIMENSION(MaxSubEq)        :: SynRise,SynStdp
        REAL, DIMENSION(MaxSubEq)        :: SynSubM0,SynSubSlip,SynSubTim


!Variables for loading GF info
        INTEGER, PARAMETER :: MaxGFsources  = 1710
        INTEGER, PARAMETER :: MaxGFcomp     = 10
        INTEGER, PARAMETER :: Maxnt         = 2048
        INTEGER            :: nGFsimul,nGFsources,ntGF

        CHARACTER (len=3)  :: elmms

        CHARACTER (len=5)  :: GFobsName
        CHARACTER (len=3)  :: GFacmp1,GFacmp2,GFacmp3 
        CHARACTER (len=3)  :: pointsource

        CHARACTER (len=4) , dimension(MaxGFsources) :: GFcspp
        CHARACTER (len=80), dimension(MaxGFsources) :: GFfile2, GFfile3

        INTEGER, dimension(MaxGFsources) :: GFnt, SynNp

        REAL               :: GFsdpzz
        REAL               :: Magmaxpoint

        REAL, dimension(MaxGFsources) :: GFbdpp,GFbstrr,GFbsvv,GFfc2,GFma11,GFm0
        REAL, dimension(2)            :: GFobsxy
        REAL, dimension(MaxGFsources) :: Syndt, Syntim

        REAL, dimension(mAXgfSOURCES,MaxGFcomp,Maxnt) :: GFtensor
        REAL, dimension(MaxGFsources,MaxGFcomp)       :: GFpeak
        REAL, dimension(MaxGFsources,3)               :: GFxyz
   


!*******************************************************************************      
!
! number of fault perimeter points to specify, must be .ge. ip for dimensions
        INTEGER, PARAMETER :: ipfl = 1000
! maximum number of elemental areas
        INTEGER, PARAMETER :: maxel=90000
! maximum number of events used for empirical Greens functions
        INTEGER, PARAMETER :: maxev=90000
! maximum number of points in sac files of empirical Green's functions,
!       must be large enough to allow fixtim to shift start of record (tegf).
!       must be larger than the NPTS of the SAC files for EGF's
        INTEGER, PARAMETER :: maxegf=50000
! maximum time length of empirical Green's functions used in synthesis
        REAL,    PARAMETER :: tegf=20.0
! maximum time length of synthesized output, should allow for Green's 
!       functions plus source duration. 
        REAL,    PARAMETER :: tsyn=30.0
! maximum dimension of spectral arrays, should be for time greater than tsyn,
!       careful to have powers of 2.
! maximum spectral dimensions for synthesis and EGF's
        INTEGER, PARAMETER :: maxdim = 80000
!
!        CHARACTER (LEN=4), DIMENSION(maxev)  :: cspt  

!        CHARACTER (LEN=20), DIMENSION(maxev) :: evid

!        CHARACTER (LEN=120), DIMENSION(maxev):: pathname

!        INTEGER, DIMENSION(maxel)            :: inel,ll,lla,mm,llb,pkid

 !       REAL, DIMENSION(maxev) :: blat,blon, bh, bm, bm1, bm2, spt,        &
 !                                 bstr, bdp, bsv, rstd, ffc, ffc2, pkkcc,  &
!                                  ma1
 !       REAL, DIMENSION(maxel) :: rdis,ramo,rdpp,rdpp1,rlat,rlon,rh,rlat1, &
!                                  rlon1,rh1,rela,rsv,razm,ristm,eltim,     &
!                                  slptim,slpvl,dtimp,ristm1,ict,rgd1,      &
!                                  eltim1,xx,yy
!        REAL, DIMENSION(ipfl)  :: fl, fla
!        REAL, DIMENSION(maxegf):: trar,trat,traz,traaz,traar,traat
!        REAL, DIMENSION(maxdim):: trcz,trcr,trct,ttim,fx,fy,aarray,barray, &
!                                  ttt,tra_tss,tra_tds,tra_rss,tra_rds,      &
!                                  tra_rdd,tra_zss,tra_zds,tra_zdd,        &
!                                  tra_rex,tra_zex
!        complex, DIMENSION(maxdim) :: tzpz,tzpr,tzpt,tzsz,tzsr,tzst,       &
!                                      ttsp,ttsr,ttst,etrz,etrr,etrt,trz,   &
!                                      trr,trt,trazz,trazr,trazt

        CHARACTER (LEN=4),   ALLOCATABLE, DIMENSION( : ) :: cspt  

        CHARACTER (LEN=20),  ALLOCATABLE, DIMENSION( : ) :: evid

        CHARACTER (LEN=120), ALLOCATABLE, DIMENSION( : ) :: pathname

        INTEGER, ALLOCATABLE, DIMENSION( : ) ::                            &
                                  inel,ll,lla,mm,llb,pkid

        REAL, ALLOCATABLE, DIMENSION( : )    ::                            &
                                  blat,blon, bh, bm, bm1, bm2, spt,        &
                                  bstr, bdp, bsv, rstd, ffc, ffc2, pkkcc,  &
                                  ma1

        REAL, ALLOCATABLE, DIMENSION( : )    ::                            &
                                  eazm,rdip,rdis,ramo,rlat,rlon,rh,rlat1, &
                                  rlon1,rh1,rela,rstk,rsv,razm,ristm,eltim,     &
                                  rslp,rslp1,slptim,slpvl,dtimp,ristm1,ict,rgd1,      &
                                  eltim1,xx,yy

        REAL, ALLOCATABLE, DIMENSION( : )    :: fl, fla

        REAL, ALLOCATABLE, DIMENSION( : )    :: trar,trat,traz,traaz,traar,traat
                                           
        REAL, ALLOCATABLE, DIMENSION( : )    ::                            &
                                  trcz,trcr,trct,ttim,fx,fy,aarray,barray, &
                                  ttt,tra_tss,tra_tds,tra_rss,tra_rds,     &
                                  tra_rdd,tra_zss,tra_zds,tra_zdd,         &
                                  tra_rex,tra_zex

        complex, ALLOCATABLE, DIMENSION( : ) ::                            &
                                  tzpz,tzpr,tzpt,tzsz,tzsr,tzst,ttsp,ttsr, &
                                  ttst,etrz,etrr,etrt,trz,trr,trt,trazz,   &
                                  trazr,trazt


        end Module ModuleSYNHAZ
