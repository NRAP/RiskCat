! Called from FaultPoint
!*******************************************************************************!
        subroutine Point_Rise_Time ( Str, tau, Vs, xM0 )
!
!******************************************************************************
!
!        Uses Brune's model
!
         IMPLICIT NONE

         REAL :: fc, pi, r, r3, Str, tau, Vs, xM0

         pi = 3.14159265

!        Str   = stress drop            (bars)
!        tau   = rise time               (sec)
!        Vs    = Shear wave velocity  (km/sec)
!        xM0   = Seismic Moment      (dyne.cm)

       !tau = pi*(7.*xM0/[(Str*16.*10.**0.6)*(2.34*Vs*10.0**5.)**3.])**0.33
         r3 = 7. * xM0 / (16.*str)
         r  = r3 ** (1./3.)
         fc = 2.34*Vs / (2.*pi*r)
         tau = (1. / fc) * 1.e-7

         return

         end subroutine Point_Rise_Time
