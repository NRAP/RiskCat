      subroutine taper(maxdim,maxegf,nt,nn1,nn2,tra,tz,n,lt,nnk,m1,k)


      INTEGER :: i, ia, iia, iib, k, lt
      INTEGER :: m1, m2, m3, m4, m5, m6, n, n1, nk, nnk, nn1, nt
      REAL    :: ang, cs, xi
      REAL, dimension(maxegf)    :: tra
      COMPLEX, dimension(maxdim) :: tz

! Lawrence Hutchings, Lawrence Livermore National Laboratory, 1987.
!
! tapers data outside limits requested for processing (nn1 - nn2).
! if taper extends past first or last data point (1 or nt), then
! end point of processed data is moved in.  zeros are added out to
! the number of points needed for fft.  dont exceede array dimensions.
!
! parameters calculated:  
! m1,m2,m5,m6 ends of taper
! m3,m4 first and last data points not tapered for tra;
! nk number of points paded to get total K points
! nnk first data point not tapered
! of tapern points; n is value passed to cool.  k is number of points
! in trace, zeros added in front and rear
! iia,iib are first and last indicies not tapered in output trace
!
! amplitude factor (1.0/k) applied to have traces ready for inverse transform.  
! When using cool you need to mulitply by time increment (dt) after forward transform, 
! and multiply by frequency increment (1.0/dt*k) after reverse transform. So, the 1.0/k is
! applied here making it ready for inverse transform. To get Fourier amplitude spectra
! you need to multiply by dt*k.
!
      n1 = n
      m1 = nn1-lt-1
      if(m1.le.0) then
      m1 = 1
      lt = nn1 - 1
      endif
      m2 = m1 + lt
      m3 = m2 + 1
      m4 = m3 + nn2 - nn1
      m5 = m4 + 1
      m6 = m5 + lt
      if(m5.gt.nt) m4 = nt - lt + 1
      nk = (k - m6 + m1 - 1)/2
      nnk = nk + lt + 1
      ia = 0
      do  i = 1,nk
         ia = ia + 1
         tz(ia) = cmplx(0.0,0.0)
      end do

! apply hanning taper
      ang = 3.14159265/float(lt)
      xi = float(lt) + 1.0
      do  i = m1,m2
         ia = ia + 1
         xi = xi - 1.0
         cs = (1.0 + cos(xi*ang))/2.0
         tz(ia) = cmplx(cs/float(k),0.0) * cmplx(tra(i),0.0)
      end do
      do  i = m3,m4
         ia = ia + 1
         tz(ia) = cmplx(tra(i)/float(k),0.0)
      end do
      xi = 0.0
      do  i = m5,m6
         ia = ia + 1
         xi = xi + 1.0
         cs =(1.0 + cos(xi*ang))/2.0
         tz(ia) = cmplx(cs/float(k),0.0) * cmplx(tra(i),0.0)
      end do
      do  i = m6+1,k
         ia = ia + 1
         tz(ia) = cmplx(0.0,0.0)
      end do
      iia = nk + m3 - m1 + 1
      iib = iia + (m4 - m3) + 1
      return
    end subroutine taper
    
