!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
         subroutine  gmsiglib  (xm,attn,zk,ierrsig,       &
         corsite,gmxsig,sigma,MaxAttnCoeff)
!
!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
!	This subroutine generates the standard deviation for a number
!       of ground motion models. It is used in routine SIA and in SIA2.
! Input is:
!	model  = model index
!	xm     = magnitude value
!	xi     = intensity value
!       r      = actual arithmetic distance value
!	attn   = 1D- Array containing the coefficients of the 
!		 ground motion model with index as above: "model"
!	zzk    = the Neperian (base-e) logarithm of the 
!		 ground motion calculated output from grdmlib.
!	gmxsig = Default value of sigma, read from the GMX file
!	corsite= Site correction factor for this frequency, 
!		 applied on the log of gropund motion estimate
!		 corsite is stored, for each frequency (ifre),
!		 into array corfrq(ifre) that is calculated in
!		 subroutine SETCOR

! Output is:
!       sigma = the standard error on the natural log of the 
!               ground motion estimate (zzk)



        IMPLICIT NONE

                INTEGER :: ierrsig, imodel, iparam, ir, iwarn
                INTEGER :: MaxAttnCoeff, model
                REAL    :: corsite, ggk, gmxsig, rdummy, sigma
                REAL    :: VS30 , xm, zk, zz, zzk
                REAL, dimension(MaxAttnCoeff) :: attn                                         

!		The equations given by the experts give sigma for a specific
!		ground motion estimate, without consideration of site 
!		correction. Therefore the site correction has to be taken
!		away from the ground motion value before calculating the
!		estimate of sigma.
                zzk = zk - corsite

                ierrsig = 0
                model = INT ( attn(21) )
!		if (model .eq. 0)  then		!Default case. Use value in GMX file  
!		sigma = gmxres  
!		else

        Ground_Motion_models: select case  ( model)

            case (0);  sigma = gmxsig
                !Default case. Use value in GMX file
            case (1);  sigma = attn(22)
                !Abramson SLR97	
                if (xm .ge. 7.0) then
                    sigma = attn(22) - 2.0*attn(23)
                else
                    if ((xm.lt.7.0) .and. (xm.gt.5.0))  then
                        sigma = attn(22) - attn(23)*(xm-5.0)
                    end if
                end if
 
            case (2);  sigma = attn(22)
                !Boore, SLR97
        
            case (3);  
                !Campbell SLR97, gm level dependent
                !First back to g's
                ggk = zzk - 6.889  
                zz = exp (ggk)
                if (zz .lt. attn(22))  then
                    sigma = attn(23)
                else
                    if (zz .le. attn(24))  then
                       sigma = attn(25) - attn(26)*ggk
                    else
                        sigma = attn(27)
                    end if
                end if
                sigma = 0.47
                sigma = sqrt (sigma*sigma + attn(28)*attn(28))
        
            case (4);  sigma = attn(22)
                !Saddigh SLR97, magnitude dependent	
                if (xm .lt. attn(23))  then
                   sigma = attn(24) - attn(25)*xm
                end if
	           
            case (5); 
                !Campbell and Bozorgnia NGA 2007
                !For ANPP assume VS30 = 620 m/s for rock
                VS30 = 620. 
                if (attn(14) .gt. VS30)  then      
                    sigma = sqrt (attn(22)*attn(22) +     &
                    attn(23)*attn(23))
                else
                    sigma = attn(24)
                end if

            case (6)
                !Douglas, BSSA 2013 model
                imodel = INT( attn(1) )
                iparam = INT( attn(2) )
                ir     = INT( attn(3) )
                iwarn  = INT( attn(4) )
                rdummy = 1.0
                call DOUGLAS_2013 (xm, imodel, iparam, ir, iwarn, &
                rdummy, sigma, zz)
                
	             
            case default; ierrsig = 1

        end select Ground_Motion_models

        return

        end  subroutine  gmsiglib
	
