!
!..........This routine select a magnitude of occurrence for a seismic
!          source i, with a given reccurrence relationship.
!          It returns the simulated Magnitude value and the simulated
!          parameters of the uncertain reccurrence relationship.
!
!          Input:
!          ******
!          i        = seismic source index
!          Arec(i,*)= initial (Real numbers) reccurrence rel. parameters
!          Brec(i,*)=    ""    (Integer)   ""         ""
!
!          Output:
!          *******
!          xMagsim      = Magnitude value selected by simulation
!          ArecSim(i,*) = Simulated parameters for Reccurrence rel.
!
!*******************************************************************************
!
        Subroutine  TruncSelect (i,M0,xMagSim)
        
        use ModuleEQ
        use GENparam
        
        implicit none
        
        integer :: i
        
        real    :: alfa,beta,delbeta,dt,Lam1,Lam2,Lambda
        real    :: M0,PM0,r1,rnunf,sum,xMagSim,xMax
!
!*******************************************************************************
        
!.......Construct the simulated probability density of Magnitude
                           
       Lam1 = Arec(i,3) * (1.+ Arec(i,5))
       Lam2 = Arec(i,3) / (1.+ Arec(i,5))
       delbeta = Arec(i,6)


       !Simulate parameters of Reccurrence 
       
       !Mmax. 
       r1 = rnunf()
       !uniform distribution on [MmaxLower,MmaxUpper]
       xMax = Arec(i,7) + r1*(Arec(i,9)-Arec(i,8))
       ArecSIM(i,7) = xMax
       
       !beta.
       r1 = rnunf()
       !uniform distribution on [beta-delbeta,beta+delbeta]
       beta = Arec(i,4)-delbeta + r1*2.*delbeta
       ArecSIM(i,4) = beta
       
       !alfa.
       r1 = rnunf()
       !uniform distr. of Lambda(MLam) 
       !with +/-  delalfa % increase/decrease
       Lambda = lam2 + r1*(Lam1-Lam2)
       alfa = alog(Lambda) + beta*Arec(i,1)
       ArecSIM(i,3) = Lambda
       ArecSIM(i,4) = alfa !NOTE: this is NOT delbeta but ALFA simulated
              
       !Truncated Exponential: Input G-R parameters and more
       
       !Real variables:
       !Arec(i,1)= MLam Magnitude for which Lambda is given 
       !Arec(i,2)= MG (Center of Gravity of the seis data, if known, or 0)
       !Arec(i,3)= Lambda at MLam (annual rate of ocurrence)
       !Arec(i,4)= Beta (Slope of G-R in natural log)
       !Arec(i,5)= Std dev on  Lambda at MLam, in %
       !Arec(i,6)= Std dev on Beta
       !Arec(i,7)= Best Estimate of Max Magnitude for the source
       !Arec(i,8)= Lower bound MaxMag. If 0, use  + or - delM
       !Arec(i,9)= Upper bound MaxMag. If 0, use  + or - delM
       !Arec(i,10)= delM, uncertainty on Magmax
         
       !Integer Variables:             
         
       !Brec(i,1)= Type of probability distribution for Max Magnitude
       !           1: Uniform between MmaxLower and MmaxUpper
       !           2: Trapeze. 0 at Mlow, uniform on [Mbest-Mupper]
       !           3: Trapeze. Uniform on [Mlower-Mbest], 0 at Mupper
       !           4: Triangular. 0 at Mlower, Mode at Mbest, 0 at Mupper
       !
       !ArecSIM(i,*) same as Arec for one specific simulation

   
!      EQTruncRec produces Pm(j), the density function for values centered 
!      on the middle of the magnitude bins. (Stored in ModuleEQ) 
!      dt is a dummy here
       dt = 1.
       call  EQTruncRec (alfa,beta,dt,M0,xMax,M0,xMax,sum,PM0)

!......Construct the CDF of Magnitude and select a sample value at random
       r1 = rnunf()
       sum = Pm(1)
       do i = 1, nMagbins
           if (r1 .lt. sum)  exit
           sum = sum + Pm(i)
       end do
       xMagSim = xMagbins(i)
        
       return
        
       end subroutine TruncSelect