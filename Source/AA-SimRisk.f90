!
!Program of RISK analysis for Natural and Induced seismic events
!
!                                SimRisk
!
!*******************************************************************************
!
        PROGRAM SimRisk 
               
        implicit none

        character (len=232) :: arg
        character (len=232) :: Menufile 
        integer :: i

        !Default name of menu file if no name is given on command line
        ! ex. > ./riscat (Then return)
        Menufile = 'MenuIn.csv'
  
        !If a name is given after the executable, it is interpreted as the
        !name of the menu file
        !ex. > ./riscat mymenufile   (then return)
        ! Number of arguments in the command line
        i = iargc()

        if (i .lt. 1)  then
            Menufile = 'MenuIn.csv'
        else
            do i = 1, iargc()
                call getarg (i,arg)
                ! The first argument is the menu file 
                if (i.eq.1)  then
                    Menufile = arg
                end if
            end do
        end if

        call riskcat ( Menufile )
                
        END PROGRAM SimRisk
