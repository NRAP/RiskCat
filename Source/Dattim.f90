!
!Get Date and Time
!
       subroutine dattim(dat,tim)
        
       CHARACTER (LEN=10) :: dat,tim
       CHARACTER (LEN=10) :: datx,timx,aax
       CHARACTER (LEN=5)  :: tzone

       CHARACTER (LEN=1), dimension(8)  :: ax

       INTEGER, dimension(8)            :: datarr

       equivalence (ax(1),aax)


       call date_and_time(datx,timx,tzone,datarr)
       write(aax,2000)datarr(2),datarr(3),datarr(1)

       if(ax(1).eq.' ')ax(1)='0'
       if(ax(4).eq.' ')ax(4)='0'
       dat=aax
       write(aax,1001)datarr(5),datarr(6),datarr(7)

       if(ax(1).eq.' ')ax(1)='0'
       if(ax(4).eq.' ')ax(4)='0'
       if(ax(7).eq.' ')ax(7)='0'

       tim=aax
   
 1001  format(i2,':',i2,':',i2)
 2000  format(i2,'/',i2,'/',i4)
   
       return
       end
