         
        SUBROUTINE influence (lamda,lastoccur,Maxtot,nptot,uplamda)
        
        implicit none
        
        INTEGER :: Maxtot
        INTEGER :: i,j,lastoccur,nptot
        INTEGER, dimension(Maxtot) :: idsubper
        
        REAL, dimension(Maxtot) :: lamda
        REAL, dimension(Maxtot,Maxtot) :: uplamda
        
        !Case of the stationary Poisson occurrence process: 
        !no influence,no correction 
            Do i=1,nptot
                do j=1,nptot
                uplamda(i,j) = 1.
                end do
            end do
        !case with influence, exemple for earthquakes
        SELECT CASE (idsubper(lastoccur))
        CASE (1)
             !Case of last event being an earthquake
             !No correction
             uplamda(lastoccur,lastoccur) = 1.
             do i=1,nptot  
             lamda(i) = lamda(i) * uplamda(lastoccur,i)
             end do
        CASE (2)
        !Case of peril type 2

        CASE (3)
        !Case of peril type 2

        CASE (4)
        !Case of peril type 2
        
        CASE DEFAULT
        !No correction to the lamda's
        !Case of the stationary Poisson occurrence process: 
        !no influence,no correction 
            Do i=1,nptot
                do j=1,nptot
                uplamda(i,j) = 1.
                end do
            end do
      END SELECT
           
      return
             
      end subroutine influence
            !