!
        subroutine InRoads (CapnRD,IDnRD,IDlkRD,         &         
                    IDofOvUnLink,ID1OvUn,infile,nlinkRD,nodesRD, &
                    nOverUnderRD,N1N2,nsubseg,TypenRD,unit1)
                            
!This routine reads the description of the Road network system
!CapnRD  = flow capacity of node, such as max number of cars (Integer)
!capsegRD= flow capacity of segment (integer)
!IDnRD   = a unique nodes identifier (Integer)
!IDlkRD  = a unique link identifier
!IDofOvUnLink(i,j)= Sequencial Index of weak point on j-th in i-th link
!ID1OvUn(k,:)= (k,1) for k-th sequencial weak point link appartenance
!              (k,2) for k-th sequencial weak point sub appartenance
!Maxnode = Max number of Major nodes (Interchanges, major bridges etc)
!Maxlink = Max number of links between Major nodes
!MAxsublk= Max number of weak points (Special structures on a link 
!          (Over/Under passes...)
!nlinkRD = total number of links between major nodes in road system
!nodesRD = number of major nodes in the system (Integer)
!          (Interchanges, bridges, etc...)
!N1N2    = a vector of pairs of nodes defining links
!nsubseg = number of special structures on link. 
!NumofOvUninLink(i) = Number of Over/Underpasses on link 'i'
!SegRD   = node index pairs that link connects (Integers)
!TypenRD = fragility type for major node(Integer)
!TypsubRD= substructure type of  portion of link
!         (surface or elevated) (Integer)
!xynodeRD= longitude and latitude of nodes 1 for Long, 2 for Lat(reals)
!xysegRD = long & lat of elevated parts, overpass or underpass
!          on link of road.

        USE CritNodes

        IMPLICIT NONE
        

        INTEGER :: i,ID1,IERR,j
        INTEGER :: nlinkRD,nodesRD,nOverUnderRD,unit1
        INTEGER, dimension(Maxnode)          :: CapnRD,IDnRD,TypenRD
        INTEGER, dimension(Maxlink)          :: IDlkRD,nsubseg
        INTEGER, dimension(Maxlink,Maxsublk) :: IDofOvUnLink               
        INTEGER, dimension(Maxlink,2)        :: N1N2
        INTEGER, dimension(MaxOverUnderRD,2) :: ID1OvUn
        REAL, dimension(Maxlink,Maxsublk,2)  :: fragRDlink
        
        CHARACTER (LEN=20)                   :: Dummy, infile

        
        open (unit=unit1,file=infile,status="old",iostat=IERR)
        call CantFindFile (infile,ierr)
        rewind (unit1)
        
        ID1          = 0
        nodesRD      = 0
        nlinkRD      = 0
        nOverUnderRD = 0
        ID1OvUn(:,:) = 0
        nsubseg(:)   = 0
        
        read (unit1,*) nodesRD
        if (nodesRD .le. 0)  then
            close (unit1)
            RETURN
        end if
        
        Read (unit1,*)  Dummy
        do  i = 1,nodesRD
            Read (unit1,*) IDnRD(i),CapnRD(i),TypenRD(i), &
            xynodeRD(i,1),xynodeRD(i,2)
        end do
        
        Read (unit1,*)  Dummy
        read (unit1,*) nlinkRD
        Read (unit1,*)  Dummy
        
        do i = 1,nlinkRD
            read (unit1,*) IDlkRD(i),N1N2(i,1),N1N2(i,2),nsubseg(i)
            do j = 1,nsubseg(i)
            ID1 = ID1 + 1
                read (unit1,*,END=100) xysegRD(i,j,1),xysegRD(i,j,2),  &
                fragRDlink(i,j,1),fragRDlink(i,j,2)
                IDofOvUnLink(i,j) = ID1 !sequencial ID of jth sub in ith link
                ID1OvUn(ID1,1) = i      !link ID of ID1-th sequencial sub
                ID1OvUn(ID1,2) = j      !sub ID of ID1-th sequencial sub
                xyOVURD(ID1,1) = xysegRD(i,j,1)
                xyOVURD(ID1,2) = xysegRD(i,j,2)
            end do
        end do
        nOverUnderRD = ID1
  
100     close (unit1)
        
        Return
        end subroutine InRoads
