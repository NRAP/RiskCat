!       Called by MagSampling.f90
!
!*******************************************************************************
!
        SUBROUTINE LoadCulledCat ( i, iCaseCat,ispl, time, X, xMag3,    &
                                 xMagMin2, xMagMax2, Y, Zcat )
!
!*******************************************************************************
!
!       Loads events parameters for those saved into the culled catalog
!
!       Input:
!       i        = index of the saved event in the culled catalog
!       iCaseCat = type of formating of information in catalog
!                = 1, Corrine's in UTM X,Y,Z
!       ispl     = index of the aleatory cycle
!       time     = time of saved event in years
!       X, Y, Z, xMag coordinates and Magnitude of event
!
!       Output:
!       Loaded arrays stored on Module ModuleEq
!       
!*******************************************************************************
        USE ModuleEq
        USE GenParam

        IMPLICIT NONE

        INTEGER :: i, iCaseCat, ispl, iizone
        REAL    :: time, X, Xcat, xMagMin2, xMagMax2, xMag3
        REAL    :: xxx, Y, Ycat, Zcat
        REAL (KIND=8) :: conv,Xcat1,Xutm,Ycat1,Yutm
!
!*******************************************************************************

        SELECT CASE ( iCaseCat )

            CASE (1)     !Corrine's catalog with UTM coordinates

                !X and Y are in UTM
                Xutm = DBLE ( X )
                Yutm = DBLE ( Y )
                !Corrine's catalog in UTM
                CatEqMag(ispl,i)  = xMag3
                CatEqTime(ispl,i) = time 
                !Coordinates of events in problem coordinates
                !Convert UTM to Long-Lat
                !Zone 10 for Kimberlina, WGS84
                !Xutm1 = Northing
                !Yutm1 = Easting (Negative if zone < or = 30)

                iizone = 10
                call UTM2LL (Xutm,Yutm,Ycat1,Xcat1,conv,iizone,2)
                if (iizone .le. 30)  then
                    Xcat1 = - Xcat1
                end if
                Xcat = SNGL(Xcat1) / 3600.  !Longitude
                Ycat = SNGL(Ycat1) / 3600.  !Latitude
                Xrsq(ispl,i) = Xcat
                Yrsq(ispl,i) = Ycat 
                Zrsq(ispl,i) = Zcat

                !Seismic Moment in N.m:
                xxx  = 10.**(1.5*xMag3+9.1)
                !Converted in dynes.cm
                CatEqM0(ispl,i) = xxx * 1.e07
                !Min and Max magnitudes in this catalog
                if (CatEqMag(ispl,i) .gt. xMagMax2)  then
                   xMagMax2 = CatEqMag(ispl,i)
                end if
                if (CatEqMag(ispl,i) .lt. xMagMin2)  then
                   xMagMin2 = CatEqMag(ispl,i)
                end if

            CASE (2)     !RSQSim/GEOS simulated catalog
      
                CatEqMag(ispl,i)  = xMag3
                CatEqTime(ispl,i) = time 
                
               !Seismic Moment in N.m:
                xxx  = 10.**(1.5*xMag3+9.1)
                !Converted in dynes.cm
                CatEqM0(ispl,i) = xxx * 1.e07
                !Min and Max magnitudes in this catalog
                if (CatEqMag(ispl,i) .gt. xMagMax2)  then
                   xMagMax2 = CatEqMag(ispl,i)
                end if
                if (CatEqMag(ispl,i) .lt. xMagMin2)  then
                   xMagMin2 = CatEqMag(ispl,i)
                end if

   ! Need to know if RSQSim corrdinates are in UTM or what???

            CASE DEFAULT

        END SELECT

        RETURN

        END SUBROUTINE LoadCulledCat
