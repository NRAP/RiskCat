
! ----------------------------------------------------------------- begin rscalc_interp_acc
      subroutine rscalc_interp_acc(acc, na, omega, damp_in, dt_in,
     :                             rd, rv, aa)
        
c-----------------------------------------------------------------------
c This version does not return response time series.

* Dates: 03/04/10 - Program rsp obtained from B. Chiou.  cmpmax written by
*                   I. Idriss; ucmpmx by R. Youngs.  D. Boore changed the input 
*                   parameters to be equivalent to rdrvaa.for
*        03/05/10 - Substituted rdrvaa for cmpmax
*        03/06/10 - Renamed from rsp_rdrvaa_ucmpmx to rscalc_interp_acc.
*                 - Renamed subroutine ucmpmx to icmpmx ("i" for "interpolate
*                   acceleration) and modified icmpmx.
!        08/13/12 - Norm Abrahamson suggests doing a more exact interpolation 
!                   when period < n*dt (n=10 here).   He suggests interpolating
!                   by adding zeros in the frequency domain, figuring
!                   out at the beginning what will be the shortest period desired,
!                   interpolating the input time series accordingly, and then feeding 
!                   this into rdrvaa.    This requires a major restructuring of this 
!                   subroutine.  I will not do this yet; I just wanted to write
!                   down the suggested revision someplace.
!        10/09/12 - Following up on the last comment, the interpolation is in the driver
!                   smc_interpolate_time_series_using_fork.for, and it uses 
!                   interpolate_time_series_using_fork.for.  I have not incorporated
!                   it yet into programs such as smc2rs, smc2rs2, or blpadflt.

      real*4 acc(*), omega, damp_in, dt_in, 
     :       rd, rv, aa, d0, v0 
      
      integer na

      integer kg, kug, npr


      double precision ug(:), pr, damp, dt, z(3)

      double precision w, twopi
      
      allocatable :: ug
      
      integer i, nn

      dt = dble(dt_in)
      damp = dble(damp_in)
      
      kg = na
      
      allocate(ug(na))

          do i=1,kg
            ug(i) = dble(acc(i))
          enddo
c...
c... Compute response spectra
c...
           kug=kg-1
           
      w = dble(omega)
      
      twopi = 4.0d0*dasin(1.0d0)
      
      pr = twopi/w

      if(dt == 0.0d0 .or. pr < 10.0d0*dt) then
        call icmpmx(kug, ug, dt, pr, w, damp, z)
        rd = sngl(z(1))
        rv = sngl(z(2))
        aa = sngl(z(3))
      else
        d0 = 0.0
        v0 = 0.0
        call rdrvaa(acc,kg,omega,damp_in,dt_in,rd,rv,aa, d0, v0)
      endif
            
      deallocate(ug)

      return
      end

!-----------------------------------------------------------------------
      subroutine icmpmx(kug, ug, dt_in, pr, w, d, z)
      
* z(1) = SD
* z(2) = RV
* z(3) = AA

! Dates: 03/06/10 - Original program ucmpmx (written by Bob Youngs/I. Idriss)
!                   renamed icmpmx and modified to assume equal time spacing
!                   if the original and interpolated acceleration time
!                   series (the "u" in the original name referred to 
!                   "u"nequal spacing).

      implicit none

c Input
      integer kug
      double precision ug(*), pr, w, d, dt_in

c Output
      double precision z(*)

c Working variables
      double precision  t(3), c(3), x(2,3)
      double precision  f1, f2, f3, f4, f5, f6, wd, w2, w3
      double precision  dt, e, g1, g2, h1, h2, dug, g, z1, z2, z3, z4
      double precision  a, b
      integer nn, i, k, ns, is, j
c
      nn=1
      wd=sqrt(1.-d*d)*w
      w2=w*w
      w3=w2*w
      DO i=1,3
        x(1,i)=0.
        z(i)=0.
      END DO
      
      f2=1./w2
      f3=d*w
      f4=1./wd
      f5=f3*f4
      f6=2.*f3
      
      ns= int(10.*dt_in/pr-0.01)+1   !! 05/05/2008
      dt=dt_in/real(ns)
      
      DO k=1,kug
      
        f1=2.*d/w3/dt
        e=dexp(-f3*dt)
        g1=e*dsin(wd*dt)
        g2=e*dcos(wd*dt)
        h1=wd*g2-f3*g1
        h2=wd*g1+f3*g2
        dug=(ug(k+1)-ug(k))/real(ns)
        g=ug(k)
        z1=f2*dug
        z3=f1*dug
        z4=z1/dt
        
        DO is=1,ns
          z2=f2*g
          b=x(1,1)+z2-z3
          a=f4*x(1,2)+f5*b+f4*z4
          x(2,1)=a*g1+b*g2+z3-z2-z1
          x(2,2)=a*h1-b*h2-z4
          x(2,3)=-f6*x(2,2)-w2*x(2,1)
          nn = nn + 1
          DO j=1,3
            c(j)=abs(x(2,j))
            IF (c(j) > z(j)) THEN
              z(j)=c(j)
            END IF
            x(1,j)=x(2,j)
          END DO
          
          g=g+dug
          
        END DO
      END DO
  
      RETURN
      END

!------------------------------
 
!      include '\forprogs\rdrvaa.for'

!----------------- BEGIN RDRVAA -----------------------------
      subroutine rdrvaa(acc,na,omega,damp,dt,rd,rv,aa, d0, v0)
* This is a modified version of "Quake.For", originally
* written by J.M. Roesset in 1971 and modified by
* Stavros A. Anagnostopoulos, Oct. 1986.  The formulation is that of
* Nigam and Jennings (BSSA, v. 59, 909-922, 1969).  

*   acc = acceleration time series
*    na = length of time series
* omega = 2*pi/per
*  damp = fractional damping (e.g., 0.05)
*    dt = time spacing of input
*    rd = relative displacement of oscillator
*    rv = relative velocity of oscillator
*    aa = absolute acceleration of oscillator
* d0,v0 = initial displacement and velocity (usually set to 0.0)

* Dates: 02/11/00 - Modified by David M. Boore, based on RD_CALC
!        03/11/01 - Double precision version
!        03/14/01 - Added d0, v0 (note on 05 March 2010: I recommend 
!                   that they not be used, by setting them to 0.0.  
!                   I've kept them as arguments in the subroutine call
!                   so that I do not have to modify programs that use
!                   this subroutine).                   
!        03/14/01 - Changed name back to rdrvaa
*        01/31/03 - Moved implicit statement before the type declarations
!        10/10/07 - Initial variable assignments and iteration loop modified 
!                   to double-precision (Chris Stephens)
!        03/05/10 - Delete old (single precision) lines of code
!        12/22/10 - Remove minus sign in front of the initialization of y, ydot. 
!                   The minus sign was a remnant of an earlier version where I did not
!                   understand the meaning of y and ydot.

      implicit real*8 (a - h, o - z)

      real*4 acc(*), omega, damp, dt, rd, rv, aa, d0, v0

      d2=1.d0-dble(damp)*dble(damp)
      d2=dsqrt(d2)
      bom=dble(damp)*dble(omega)
      d3 = 2.d0*bom                 ! for aa
      omd=dble(omega)*d2
      om2=dble(omega)*dble(omega)
      c1=1.d0/om2
      
      omt=dble(omega)*dble(dt)
      omdt=omd*dble(dt)
      c2=2.d0*dble(damp)/(om2*omt)
      c3=c1+c2
      c4=1.d0/(dble(omega)*omt)
      ss=dsin(omdt)
      cc=dcos(omdt)
      bomt=dble(damp)*omt
      ee=dexp(-bomt)
      ss=ss*ee
      cc=cc*ee
      s1=ss/omd
      s2=s1*bom
      s3=s2+cc
      a11=s3
      a12=s1
      a21=-om2*s1
      a22=cc-s2
      s4=c4*(1.d0-s3)
      s5=s1*c4+c2
      b11=s3*c3-s5
      b12=-c2*s3+s5-c1
      b21=-s1+s4
      b22=-s4
      
      rd=0.
      rv = 0.                           ! for rv
      aa = 0.                           ! for aa
      
      y=    dble(d0)
      ydot= dble(v0)    
!      y=0.
!      ydot=0.

      do i=1, na-1
      
        y1=a11*y+a12*ydot+b11*dble(acc(i))+b12*dble(acc(i+1))
        ydot=a21*y+a22*ydot+b21*dble(acc(i))+b22*dble(acc(i+1))
        y=y1    ! y is the oscillator output at time corresponding to index i
        z=dabs(y)
        if (z.gt.rd) rd=z
        z1 = dabs(ydot)                   ! for rv
        if (z1.gt.rv) rv = z1            ! for rv
        ra = -d3*ydot -om2*y1            ! for aa
        z2 = dabs(ra)                     ! for aa
        if (z2.gt.aa) aa = z2            ! for aa
        
      end do
      
      return
      end
! ----------------- END RDRVAA -----------------------------
! ----------------------------------------------------------------- end rscalc_interp_acc


! ---------------------- BEGIN MNMAXIDX ----------------------
      subroutine mnmaxidx(a,nstrt,nstop,ninc,
     :                    amin,amax,indx_min,indx_max) 

! A rewrite of mnmax, returning the indices of the min and max values
!
! Dates: 10/05/99 - written by D. M. Boore
!        05/16/02 - Replaced "dimension a(1)" with "real a(*)"
!        11/03/12 - Replace if-then with if--then-else

      real a(*)
      amax = a( nstrt)
      indx_max = nstrt
      amin=amax         
      indx_min = nstrt
      do i=nstrt,nstop,ninc
      
        if (a(i) > amax) then
          amax=a(i)
          indx_max = i
        else if (a(i) < amin) then
          amin=a(i)
          indx_min = i
        end if
        
      end do
      
      return                  
      end                     
! ---------------------- END MNMAXIDX ----------------------


! ---------------------- BEGIN Abs_MnMaxIdx ----------------------
      subroutine abs_mnmaxidx(a,nstrt,nstop,ninc,
     :                    amin,amax,indx_min,indx_max, abs_max) 

* A rewrite of mnmax, returning the indices of the min and max values
*
* Dates: 07/04/05 - Written by D. M. Boore, adapted from mnmaxidx
 
      real a(*)
      amax = a( nstrt)
      indx_max = nstrt
      amin=amax         
      indx_min = nstrt
      do i=nstrt,nstop,ninc
        if(a(i) .gt. amax) then
          amax=a(i)
          indx_max = i
        end if
        if(a(i) .lt. amin) then
          amin=a(i)
          indx_min = i
        end if
      end do
      if (abs(amin) .gt. abs(amax)) then
        abs_max = abs(amin)
      else
        abs_max = abs(amax)
      end if
      
      return                  
      end                     
! ---------------------- END Abs_MnMaxIdx ----------------------


! ------------------------------------------------------------ moment
      SUBROUTINE moment(data,n,ave,adev,sdev,var,skew,curt)
      INTEGER n
      REAL adev,ave,curt,sdev,skew,var,data(n)
      INTEGER j
      REAL p,s,ep
      if(n.le.1)pause 'n must be at least 2 in moment'
      s=0.
      do 11 j=1,n
        s=s+data(j)
11    continue
      ave=s/n
      adev=0.
      var=0.
      skew=0.
      curt=0.
      ep=0.
      do 12 j=1,n
        s=data(j)-ave
        ep=ep+s
        adev=adev+abs(s)
        p=s*s
        var=var+p
        p=p*s
        skew=skew+p
        p=p*s
        curt=curt+p
12    continue
      adev=adev/n
      var=(var-ep**2/n)/(n-1)
      sdev=sqrt(var)
      if(var.ne.0.)then
        skew=skew/(n*sdev**3)
        curt=curt/(n*var**2)-3.
      else
        pause 'no skew or kurtosis when zero variance in moment'
      endif
      return
      END
! ------------------------------------------------------------ moment

      FUNCTION select(k,n,arr)
      INTEGER k,n
      REAL select,arr(n)
      INTEGER i,ir,j,l,mid
      REAL a,temp
      l=1
      ir=n
1     if(ir-l.le.1)then
        if(ir-l.eq.1)then
          if(arr(ir).lt.arr(l))then
            temp=arr(l)
            arr(l)=arr(ir)
            arr(ir)=temp
          endif
        endif
        select=arr(k)
        return
      else
        mid=(l+ir)/2
        temp=arr(mid)
        arr(mid)=arr(l+1)
        arr(l+1)=temp
        if(arr(l).gt.arr(ir))then
          temp=arr(l)
          arr(l)=arr(ir)
          arr(ir)=temp
        endif
        if(arr(l+1).gt.arr(ir))then
          temp=arr(l+1)
          arr(l+1)=arr(ir)
          arr(ir)=temp
        endif
        if(arr(l).gt.arr(l+1))then
          temp=arr(l)
          arr(l)=arr(l+1)
          arr(l+1)=temp
        endif
        i=l+1
        j=ir
        a=arr(l+1)
3       continue
          i=i+1
        if(arr(i).lt.a)goto 3
4       continue
          j=j-1
        if(arr(j).gt.a)goto 4
        if(j.lt.i)goto 5
        temp=arr(i)
        arr(i)=arr(j)
        arr(j)=temp
        goto 3
5       arr(l+1)=arr(j)
        arr(j)=a
        if(j.ge.k)ir=j-1
        if(j.le.k)l=i
      endif
      goto 1
      END
      SUBROUTINE indexx(n,arr,indx)
      INTEGER n,indx(n),M,NSTACK
      REAL arr(n)
      PARAMETER (M=7,NSTACK=50)
      INTEGER i,indxt,ir,itemp,j,jstack,k,l,istack(NSTACK)
      REAL a
      do 11 j=1,n
        indx(j)=j
11    continue
      jstack=0
      l=1
      ir=n
1     if(ir-l.lt.M)then
        do 13 j=l+1,ir
          indxt=indx(j)
          a=arr(indxt)
          do 12 i=j-1,l,-1
            if(arr(indx(i)).le.a)goto 2
            indx(i+1)=indx(i)
12        continue
          i=l-1
2         indx(i+1)=indxt
13      continue
        if(jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        k=(l+ir)/2
        itemp=indx(k)
        indx(k)=indx(l+1)
        indx(l+1)=itemp
        if(arr(indx(l)).gt.arr(indx(ir)))then
          itemp=indx(l)
          indx(l)=indx(ir)
          indx(ir)=itemp
        endif
        if(arr(indx(l+1)).gt.arr(indx(ir)))then
          itemp=indx(l+1)
          indx(l+1)=indx(ir)
          indx(ir)=itemp
        endif
        if(arr(indx(l)).gt.arr(indx(l+1)))then
          itemp=indx(l)
          indx(l)=indx(l+1)
          indx(l+1)=itemp
        endif
        i=l+1
        j=ir
        indxt=indx(l+1)
        a=arr(indxt)
3       continue
          i=i+1
        if(arr(indx(i)).lt.a)goto 3
4       continue
          j=j-1
        if(arr(indx(j)).gt.a)goto 4
        if(j.lt.i)goto 5
        itemp=indx(i)
        indx(i)=indx(j)
        indx(j)=itemp
        goto 3
5       indx(l+1)=indx(j)
        indx(j)=indxt
        jstack=jstack+2
        if(jstack.gt.NSTACK)pause 'NSTACK too small in indexx'
        if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
        else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
        endif
      endif
      goto 1
      END
! ---------------------- BEGIN SKIP -------------------
      subroutine SKIP(lunit, nlines)
        if (nlines .lt. 1) then
          return
        else
          do i = 1, nlines
             read(lunit, *)
          end do
          return
        end if
      end
! ---------------------- END SKIP -------------------
! --------------------- BEGIN UPSTR ----------------------------------
      Subroutine UPSTR ( text )
* Converts character string in TEXT to uppercase
* Dates: 03/12/96 - Written by Larry Baker

C
      Implicit   None
C
      Character  text*(*)
C
      Integer    j
      Character  ch
C
      Do 1000 j = 1,LEN(text)
         ch = text(j:j)
         If ( LGE(ch,'a') .and. LLE(ch,'z') ) Then
            text(j:j) = CHAR ( ICHAR(ch) - ICHAR('a') + ICHAR('A') )
         End If
 1000    Continue
C
      Return
      End
! --------------------- END UPSTR ----------------------------------
! --------------------------- BEGIN TRIM_C -----------------------
      subroutine trim_c(cstr, nchar)

* strips leading and trailing blanks from cstr, returning the
* result in cstr, which is now nchar characters in length

* Strip off tabs also.

* Here is a sample use in constructing a column header, filled out with 
* periods:

** Read idtag:
*        idtag = ' '
*        read(nu_in, '(1x,a)') idtag
*        call trim_c(idtag, nc_id)
** Set up the column headings:
*        colhead = ' '
*        colhead = idtag(1:nc_id)//'......' ! nc_id + 6 > length of colhead

* Dates: 12/23/97 - written by D. Boore
*        12/08/00 - pad with trailing blanks.  Otherwise some comparisons
*                   of the trimmed character string with other strings
*                   can be in error because the original characters are left
*                   behind after shifting.  For example, here is a string
*                   before and after shifting, using the old version:
*                      col:12345
*                           MTWH  before
*                          MTWHH  after (but nc = 4).
*        03/21/01 - Check for a zero length input string
*        11/09/01 - Change check for zero length string to check for all blanks
*        10/19/09 - Strip off tabs

      character cstr*(*)

      if(cstr .eq. ' ') then
        nchar = 0
        return
      end if

      nend = len(cstr)

! Replace tabs with blanks:

      do i = 1, nend
        if(ichar(cstr(i:i)) .eq. 9) then
           cstr(i:i) = ' '
        end if
      end do



*      if(nend .eq. 0) then
*        nchar = 0
*        return
*      end if

      do i = nend, 1, -1
        if (cstr(i:i) .ne. ' ') then
           nchar2 = i
           goto 10
        end if
      end do

10    continue

      do j = 1, nchar2
        if (cstr(j:j) .ne. ' ') then
          nchar1 = j
          goto 20
        end if
      end do

20    continue
   
      nchar = nchar2 - nchar1 + 1
      cstr(1:nchar) = cstr(nchar1: nchar2)
      if (nchar .lt. nend) then
        do i = nchar+1, nend
          cstr(i:i) = ' '
        end do
      end if

      return
      end
! --------------------------- END TRIM_C -----------------------

! ------------------------------------------------------------------ skipcmnt
      subroutine skipcmnt(nu, comment, ncomments)

* Skip text comments in the file attached to unit nu, but save skipped 
* comments in character array comment.  Skip at least one line, and more as 
* long as the lines are preceded by "|" or "!".

* Dates: 04/16/01 - Written by D. Boore
*        12/07/01 - Added check for eof
*        11/04/03 - Use trim_c to trim possible leading blank
*        02/03/07 - Initialize comments to blank

      character comment(*)*(*), buf*80

      ncomments = 0
100   buf = ' '
      read (nu,'(a)',end=999) buf
      call trim_c(buf,nc_buf)
      if (buf(1:1) .eq.'!' .or. buf(1:1) .eq.'|' .or. 
     :                     ncomments + 1 .eq. 1) then
        ncomments = ncomments + 1
        comment(ncomments) = ' '
        comment(ncomments) = buf(1:nc_buf)
        goto 100
      else 
        backspace nu
      end if

999   continue
 
      return
      end
! ------------------------------------------------------------------ skipcmnt

! --------------------------- BEGIN GET_LUN ----------------
      subroutine get_lun(lun)

* Finds a logical unit number not in use; returns
* -1 if it cannot find one.

* Dates -- 05/19/98 - Written by D. Boore, following
*                     Larry Baker's suggestion

      logical isopen
      do i = 99,10,-1
        inquire (unit=i, opened=isopen)
        if(.not.isopen) then
          lun = i
          return
        end if
      end do
      lun = -1

      return
      end
! --------------------------- END GET_LUN ----------------
     

!------------------   Begin SMCRead   ----------------------------------
      subroutine SMCRead(f_in,  
     :                   tskip, tlength,
     :                   y, char_head, int_head, real_head, comments)

*  If tskip = 0.0, no data are skipped
*  If tlength = 0.0, only headers are read
*  If tlength < 0.0, read all data, less tskip
*  Otherwise tlength of data is read, after skipping tskip, unless 
*  tskip + tlength exceeds the length of the input time series, in which
*  case tlength is adjusted so that all data, less tskip, are read.  Thus
*  setting tlength to a large number (or setting it to a negative number)
*  guarantees that all data (minus tskip) are read.

* The program requires get_lun.for

* The output arrays char_head, int_head, real_head should be dimensioned
* as follows in the calling program:

*      character char_head(11)*80, comments(500)*80 ! "80" is the critical parameter
*      integer int_head(48)
*      real real_head(50)

*  Dates: 07/17/97 - Modified order of input parameters, to put input
*                    first (unit, file, tskip), followed by output.
*         12/18/97 - Read tlength
*         05/19/98 - Determine unit from within program; remove "unit"
*                    from argument list
*         03/09/99 - Change logic to determine amount of record to
*                    read and allow reading of all record, minus tskip,
*                    by setting tlength < 0.0.
*         05/18/99 - Deleted npts, sps from input parameter list.  Added 
*                    character array with comments string (allow for 
*                    50 comments).
*         12/16/99 - Renamed from ReadSMC
*         05/10/00 - Replaced
*                        tlength = float(npts_out/sps)
*                    with
*                        tlength = float(npts_out)/sps
*                    (the error and correction was pointed out by Wang Guoquan
*                    in email on 05/06/2000)
*         01/04/01 - Check for unevenly spaced data (sps = rnull) and
*                    return 0.0 in y
*         02/07/01 - If tlength = 0, only read headers
*         12/14/01 - Fixed logic for case when tlength < 0 and tskip > 0.
*                    Also, program used to reset tlength, but it does not do
*                    that now.  The user can compute tlength as 
*                    float(int_head(17))/real_head(2)
*         01/01/05 - Use "int" in calculation of npts (do not use
*                    implicit conversion from real to integer)
*         10/15/07 - Use higher precision input format if int_head(47) = 8
!         04/04/10 - Add call to trim_c, and use nc_f_in.

      real real_head(*)
      integer int_head(*)
      character*80 char_head(*), comments(*)
      character f_in*(*) 
      real y(*)

      
      
      call get_lun(nu)
      call trim_c(f_in, nc_f_in)
      open(unit= nu, file=f_in(1:nc_f_in), status='unknown')

      do i = 1, 11
        read(nu, '(a)') char_head(i)
      end do

      read(nu, '(8I10)') (int_head(i), i=1, 48)

      read(nu, '(5e15.7)') (real_head(i), i = 1, 50)

      sps = real_head(2)

      do i = 1, int_head(16)
        read(nu,'(a)') comments(i)
      end do

      if(real_head(2) .eq. real_head(1)) then   ! unevenly sampled data

         int_head(17) = 2
         do i = 1, int_head(17)
           y(i) = 0.0
         end do

      else

* Skip into the trace:

        nskip = int(tskip * sps)

* How much record to read?

        npts_in = int_head(17)
        npts2read = int(tlength * sps)

        if (tlength .lt. 0.0) then  
          npts_out = npts_in - nskip
        else if (nskip + npts2read .le. npts_in) then
          npts_out = npts2read
        else
          npts_out = npts_in - nskip
*          tlength = float(npts_out)/sps
        end if

        int_head(17) = npts_out
      
        if (tlength .eq. 0.0) then
          return    ! only read headers
        else
          if (int_head(47) .eq. 8) then
            read(nu, '(5(e14.7))') (y(i), i = 1, npts_out + nskip)
          else
            read(nu, '(8(1pe10.4e1))') (y(i), i = 1, npts_out + nskip)
          end if
          do i = 1, npts_out
            y(i) = y( i + nskip)
          end do
        end if

      end if

      close(unit=nu)

      return
      end
!------------------   End SMCRead   ----------------------------------





! ------------------------------------------------------------- SMC_Npts
      subroutine smc_npts(f_smc, npts, sps)

* Open smc file and obtain length of time series:

* Dates: 12/07/00 - Written by D. Boore
*        07/17/03 - Read all of int_head and real_head, and add sps to output
!        04/04/10 - Add trim_c, and use nc_f_smc.

      integer int_head(48)
      real real_head(50)
      character f_smc*(*)

      call get_lun(nu_smc)
      call trim_c(f_smc,nc_f_smc)
      open(unit=nu_smc,file=f_smc(1:nc_f_smc),status='unknown')
      call skip(nu_smc,11)

      read(nu_smc, '(8I10)') (int_head(i), i=1, 48)
      npts = int_head(17)

      read(nu_smc, '(5e15.7)') (real_head(i), i = 1, 50)
      sps = real_head(2)

      close(nu_smc)        

      return
      end
! ------------------------------------------------------------- SMC_Npts  

! ---------------------- BEGIN IMNMAX ----------------------
      subroutine imnmax(ia,nstrt,nstop,ninc,imin,imax) 
c
* Compute min, max for an integer array

c author: D. M. Boore
c last change: 7/17/97 - Written, based on mnmax.for
c
      integer ia(*)
      imax = ia( nstrt)
      imin=imax         
      do 10 i=nstrt,nstop,ninc
      if(ia(i)-imax) 15,15,20  
20    imax=ia(i)               
      go to 10                
15    if(ia(i)-imin) 25,10,10  
25    imin=ia(i)               
10    continue                
      return                  
      end                     
! ---------------------- END IMNMAX ----------------------

!----------------- BEGIN Acc2VD -----------------------------
      subroutine acc2vd(acc, npts, dt, rmv_trnd, vel0, dis0, vel, dis)


* Compute velocity and displacement time series from acceleration,
* assuming that the acceleration
* is represented by straight lines connecting the digitized values.

* Dates: 02/09/99 - Written by D.M. Boore
*        01/07/00 - Added initial velocity and displacements (v0, d0).
*                   This also requires the addition of a linear trend in
*                   displacement.
*                   Also, Bill Joyner, Chris Stephens, and I considered how to
*                   handle the first point.  Assuming v0 = 0, BAP uses a 
*                   trapezoidal rule such that the first vel(1) = 0.5*dt*a(1),
*                   but in acc2vd, vel(1) = 0.  In effect, BAP starts the
*                   integration at -dt rather than 0.0, with the result that
*                   because vel(1) depends on dt, vel(1) will change if the
*                   digitization interval is changed.  We agreed that this is 
*                   not correct.
*        12/18/02 - Some minor changes, such as specifying 0.0d0 rather 
*                   than 0.0 and using the function "dble" in various places
*        10/15/07 - Rename "d0" to "dis0" so as not to confuse with "d0"
*                   in "0.0d0".  For consistency, change "v0" to "vel0".
*                   More importantly, restructure the calculations so as not to
*                   lose the double-precision estimates of velocity when
*                   accumulating the displacement, and abandon the calculation 
*                   of disp based on integrating straightlines in acceleration, 
*                   using a simple trapezoidal rule instead.  These changes were
*                   made in an attempt to track down a drift in displacement
*                   for the HHE component of the KYTH recording of the
*                   Kythera earthquake recorded on a station of the EGELADOS
*                   velocity-sensor network.   The drift did not appear when I
*                   integrated the acceleration in Excel, and the SD computed 
*                   using SMC2RS also showed no indication of the drift (the
*                   long-period SD equals the peak displacement during the
*                   strong part of the shaking).   Unfortunately, the changes
*                   made today did not eliminate the drift.   My next check is
*                   to see if the 5 digit resolution in the smc files might
*                   be responsible (the data given to me by Andreas Skarlatoudis
*                   were in column files with 7 digits of resolution, with a maximum
*                   exponent of 9).  When I 
*                   opened the asc file made by smc2asc in Excel and computed dis, 
*                   I found the same trend as in the smc2vd file. This tells me
*                   that the problem is in the asc2smc conversion (but why the
*                   SD computed using the smc files shows no sign of the drift
*                   is a mystery to me).  I eventually confirmed that the problem
*                   is the limited resolution of the standard format; I have added
*                   an option to the smc utility programs to use a higher-resolution
*                   format (converting all of the utility programs will take some
*                   time---I am only doing those that I currently need right now).
!        02/06/11 - Added comments to make it clear that now use trapezoidal integration.
!                 - Delete "sngl(cumv)" and "sngl(cumd)" for vel(1) and dis(1)
!        02/07/11 - Call a subroutine to do the integration
!        06/01/11 - Delete the include statement for the integration routine; it should be placed in the programs that
!                   call acc2vd instead.  The reason for this is because
!                   the subroutine acc2vd is also included in \smsim\td_subs, and a compile
!                   error can occur if the \forprogs folder with the integration routine
!                   is not available.  This means that the trapezoidal_integration.for must be available in smsim.
!                   I guarantee this by including it in make_td_subs_file.bat, which should be called
!                   in time domain cl*.bat files.

      real acc(*), vel(*), dis(*) 
      logical rmv_trnd
      double precision cumv, cumd, a1, a2, v1, v2,
     : ddt, ddt_2, ddtdt_6

      if (rmv_trnd) then      
* remove trend first (straight line between first and last points)
* Note: acc is replaced with detrended time series
*        call dcdt(acc, dt, npts, 1, npts, .false., .true.)  ! old routine,
*                                                         ! gives steps at ends

         call rmvtrend(acc, npts)
      end if

* compute velocity and displacement

      call Trapezoidal_Integration(acc, npts, dt, vel0, vel)
      call Trapezoidal_Integration(vel, npts, dt, dis0, dis)
      
!      ddt     = dble(dt)
!      ddt_2   = 0.5d0 * dble(dt)
!      
!!      ddtdt_6 = dble(dt)*dble(dt)/6.0d0
!
!      cumv = 0.0d0
!      cumd = 0.0d0
!
!      v1 = 0.0d0
!      v2 = 0.0d0
!      
!!      vel(1) = sngl(cumv) + vel0
!      dis(1) = sngl(cumd) + dis0
!
!      vel(1) = vel0
!      dis(1) = dis0
!
!      do j=2,npts
!      
!        a1 = dble(acc(j-1))
!        a2 = dble(acc(j))
!        
!        cumv = cumv + (a1 + a2)*ddt_2
!        v1 = v2
!        v2 = cumv + dble(vel0)  ! the vel0 term is the constant of integration
!        vel(j) = sngl(v2)
!        
!        cumd = cumd + (v1 + v2)*ddt_2
!        dis(j) = sngl(cumd) + dis0  ! the dis0 term is the constant of integration
!        
!! Legacy code, when I used analytical formulas based
!! on representing the acceleration as a series of straightline segments.
!!        cumd = cumd + v1*ddt + (2.0*a1 + a2)*ddtdt_6
!        dis(j) = sngl(cumd) + dis0  ! linear term accounted for in v1*ddt
!        dis(j) = sngl(cumd) + v0*float(j-1)*dt + dis0
!
!      end do

      return
      end
!----------------- END Acc2VD -----------------------------



! ----------------------------- BEGIN RMVTREND ----------------
      subroutine rmvtrend(y, n)

* Removes a straightline fit to first and last points, replacing
* the input array with the detrended array

* Dates: 02/09/99 - written by D. Boore


      real y(*)

      y1 = y(1)
      y2 = y(n)
      slope = (y2 - y1)/float(n-1)

      do i = 1, n
        y(i) = y(i) - (y1 + slope*float(i-1))
      end do

      return
      end
! ----------------------------- END RMVTREND ----------------


! --------------------------------------------- begin rotate
      subroutine Rotate(z1,z2,znull,azm1,azm2,azmr,nz)

! Rotates z1, z2 into azmr (stored in z1 on return) and
! azmr+90.0 (stored in z2 on return).  No assumptions are made 
! about the relation between azm1 and azm2.  Znull is a number that
! represents no data or clipped data; if encountered, both z1 and z2
! are replaced by znull.

! NOTE:  The azimuths can be absolute (clockwise from north), or azmr
!        can be relative to the azimuth for component 1, in which
!        case azm1 is 0.0 and azm2 is relative to azm1 (+-90).

! NOTE NOTE NOTE:  z1, z2, azm1, azm2 are both input and output variables
!  I've been fooled by this, calling rotate several times without resetting 
!  azm1 and azm2

! Conventions: all angles are clockwise from north and are measured
! in degrees.

! See NB#10, p. 22 for development of equations.

! Written by:  Dave Boore
! Dates: 07/07/88 - written
!        07/04/05 - replace isiz in real declaration with "*",
!                   and remove isize from calling list.
!        09/22/12 - Small changes in coding (e.g., remove statement number,
!                   replace .ge. with >=, etc).

      real z1(*), z2(*)

      pi = 4.0*atan(1.0)
      dtor = pi/180.0

      cr1 = cos( (azmr-azm1)*dtor )
      sr1 = sin( (azmr-azm1)*dtor )
      cr2 = cos( (azmr-azm2)*dtor )
      sr2 = sin( (azmr-azm2)*dtor )

      loop over points: DO i = 1, nz

         if (z1(i) == znull .or. z2(i) == znull) then

            z1(i) = znull
            z2(i) = znull
            cycle loop over points

         endif

         cp1 = z1(i)
         cp2 = z2(i)

         z1(i) =  cr1*cp1 + cr2*cp2
         z2(i) = -sr1*cp1 - sr2*cp2
         
      END DO loop over points

      azm1 = azmr
      azm2 = azmr + 90.0
      if (azm2 >= 360.0) azm2 = azm2-360.0

      return
      end
! --------------------------------------------- begin rotate
 
! ------------------------ BEGIN DISTAZ ----------------------
      subroutine distaz( wlongsign, alat, along, blat, blong,
     * rdeg, rkm, az, baz)
c 
c compute distances, azimuths using formulas from
c Bruce Julian.
c
c latest modification: 1/27/84
c
*        11/24/07 - Trap for colocated pair in subroutine distaz

      if (alat .eq. blat .and. along .eq. blong) then
        rdeg = 0.0
        rkm = 0.0
        az = 0.0
        baz = 0.0
        return
      end if

      pi = 4.0 * atan( 1. )
      dtor = pi/ 180.
c
c convert from degrees to radians and correct sign of
c longitude so that east longitude is positive.
c
      alatr = dtor * alat
      alongr = -dtor * along * wlongsign
      blatr = dtor * blat
      blongr = -dtor * blong * wlongsign
c
c compute geocentric latitudes.
c
      alatr = atan( 0.993305 * tan( alatr ) )
      blatr = atan( 0.993305 * tan( blatr ) )
c
c compute latitude dependent quantities
c
      ca = cos( alatr )
      cb = cos( blatr )
      sa = sin( alatr )
      sb = sin( blatr )
c
c now compute other quantities
c
      a = cb * sin( blongr - alongr )
      b = ca * sb - sa * cb * cos( blongr - alongr )
      cd = ca * cb * cos( blongr - alongr ) + sa * sb
      sd = sqrt( a*a + b*b )
c
c compute distances
c
      rdeg = atan2( sd, cd )/ dtor
      rkm = 111.19 * rdeg
c
c compute azimuth (from a to b) and make it positive.
c
      az = atan2( a, b )/ dtor
      if ( az .lt. 0.0 ) az = az + 360.0
c
c compute back azimuth (from b to a) and make it positive.
c
      a = ca * sin( alongr - blongr )
      b = cb * sa - sb * ca * cos( alongr - blongr )
      baz = atan2( a, b)/ dtor
      if ( baz .lt. 0.0 ) baz = baz + 360.0
c
      return
      end
! ------------------------ END DISTAZ ----------------------


! ----------------------- BEGIN RCC -----------------------------------
      subroutine RCC (KPARM,CSTR_IN,NCSTR_IN,C,NC,ibgn,iend)
c RCC - [MUELLER.FS.GEN]CSMGENLB
c Read character string C(1:NC), the KPARMth element in CSTR_IN(1:NCSTR_IN).
c  CSTR_IN contains one-or-more mixed-type fields
*(OLD COMMENT:c  separated by blanks or commas to the right of an optional '='.)
c  KPARMth field contains C to the right of an optional '>'.
c If the KPARMth field is nonexistent or empty, RCC sets NC=0 and returns.
c  This simulates a <carriage-return> in a Q-format read.
c  (An empty field must be separated by commas, not blanks.)

* Dates:  xx/xx/xx - Written by C. Mueller, USGS
*         06/22/98 - Modified by L. Baker
*         11/12/00 - Dave Boore introduced a working array, and a call to 
*                    rmv_tabs (this replaces a tab with a blank, but does not
*                    expand the tabs).  This was done because a file in which 
*                    fields are separated by tabs characters will not be parsed 
*                    correctly; apparently the tab characters (ascii 9) is 
*                    not the same thing as a blank.
*         12/04/00 - added ibgn, iend to rcf, rci, rcc calling arguments
*         06/11/01 - Disable use of "=" to indicate where the parameters
*                    start.  I did this because I sometimes add comments
*                    to the right of the parameters, and if these comments
*                    contain the character "=" then the parameters are not
*                    parsed correctly.  I reasoned that eliminating the check
*                    for "=" is the best solution to the problem (as opposed
*                    to restricting the use of "=" to only indicate 
*                    where the parameters start) because I rarely used
*                    "=" in this way.
*         06/12/01 - Minor modification (i2 = 0 rather than i2 = 1)
*         10/29/02 - Changed "rmv_tabs" to "tabs_rmv"
!         07/25/10 - Increase dimension of cstr

      character CSTR_IN*(*),C*(*),term*1

      character cstr*500

      if (ncstr_in .gt. 500) then
         write(*,'(a, 1x,i5, a)') '  WARNING: IN RCC, NCSTR_IN = ', 
     :          ncstr_in,
     :         ' WHICH IS GREATER THAN THE DIMENSION OF THE'//
     :         ' WORK ARRAY; QUITTING!!!!!'
         return
      end if

      ncstr = ncstr_in

      cstr = ' '
      cstr(1: ncstr) = cstr_in
      call tabs_rmv(cstr, ncstr)

c Eliminate trailing white space.
      N = NCSTR+1
1     N = N-1
      if (CSTR(N:N).eq.' '.or.CSTR(N:N).eq.',') goto 1

* Changes on 6/11/01:
*c Find optional '='.
*      I2 = INDEX(CSTR(1:N),'=')
*      term = '='
      i2 = 0         ! DMB, 6/12/01
      term = ' '     ! DMB, 6/11/01
* Changes on 6/11/01:

c Find start and end of KPARMth field.
      do 4 K=1,KPARM
         I1 = I2
2        I2 = I1
3        I1 = I1+1
         if (CSTR(I1:I1).eq.' ') goto 3
         if (term.eq.' '.and.CSTR(I1:I1).eq.',') then
            term = ','
            goto 2
         end if
         IBLANK = INDEX(CSTR(I1:N),' ')
         ICOMMA = INDEX(CSTR(I1:N),',')
         if (IBLANK.eq.0.and.ICOMMA.eq.0.and.K.lt.KPARM) goto 801
         if (IBLANK.eq.0) IBLANK = N-I1+2
         if (ICOMMA.eq.0) ICOMMA = N-I1+2
         I2 = I1+MIN(IBLANK,ICOMMA)-1
         if (I2.gt.N) then
            term = '.'
         else
            term = CSTR(I2:I2)
         end if
4        continue
c Check for empty field.
      if (I2.eq.I1) goto 801
c Find optional '>'.
      I1 = I1+INDEX(CSTR(I1:I2),'>')
c Get C.
      C = CSTR(I1:I2-1)
      NC = I2-I1
      ibgn = i1
      iend = i2-1
      return
c Errors.
801   NC = 0
      return
      end
! ----------------------- END RCC -----------------------------------


      
! --------------------------- BEGIN RCF ------------------------------
      subroutine RCF (KPARM,CSTR_IN,NCSTR_IN,F,ibgn,iend, ISTAT)
c RCF - [MUELLER.FS.GEN]CSMGENLB
c Read real F, the KPARMth element in CSTR_IN(1:NCSTR_IN).
c  CSTR_IN contains one-or-more mixed-type fields
*(OLD COMMENT:c  separated by blanks or commas to the right of an optional '='.)
c  KPARMth field contains F to the right of an optional '>'.
c ISTAT= 0 for success;
c      = 1 if CSTR contains fewer than KPARM fields;
c      = 2 if KPARMth field is empty;
c      = 3 if KPARMth field is unreadable;
c      if ISTAT>0, return with F unchanged.


* Dates:  xx/xx/xx - Written by C. Mueller, USGS
*         06/22/98 - Modified by L. Baker
*         11/12/00 - Dave Boore introduced a working array, and a call to 
*                    rmv_tabs (this replaces a tab with a blank, but does not
*                    expand the tabs).  This was done because a file in which 
*                    fields are separated by tabs characters will not be parsed 
*                    correctly; apparently the tab characters (ascii 9) is 
*                    not the same thing as a blank.
*         12/04/00 - added ibgn, iend to rcf, rci, rcc calling arguments
*         06/11/01 - Disable use of "=" to indicate where the parameters
*                    start.  I did this because I sometimes add comments
*                    to the right of the parameters, and if these comments
*                    contain the character "=" then the parameters are not
*                    parsed correctly.  I reasoned that eliminating the check
*                    for "=" is the best solution to the problem (as opposed
*                    to restricting the use of "=" to only indicate 
*                    where the parameters start) because I rarely used
*                    "=" in this way.
*         06/12/01 - Minor modification (i2 = 0 rather than i2 = 1)
*         10/29/02 - Changed "rmv_tabs" to "tabs_rmv"
!         07/25/10 - Increase dimension of cstr

      character CSTR_IN*(*), term*1

      character cstr*500

      if (ncstr_in .gt. 500) then
         write(*,'(a, 1x,i5, a)') '  WARNING: IN RCF, NCSTR_IN = ', 
     :          ncstr_in,
     :         ' WHICH IS GREATER THAN THE DIMENSION OF THE'//
     :         ' WORK ARRAY; QUITTING!!!!!'
         return
      end if

      ncstr = ncstr_in

      cstr = ' '
      cstr(1: ncstr) = cstr_in
      call tabs_rmv(cstr, ncstr)

      ISTAT = 0
c Eliminate trailing white space.
      N = NCSTR+1
1     N = N-1
      if (CSTR(N:N).eq.' '.or.CSTR(N:N).eq.',') goto 1

* Changes on 6/11/01:
*c Find optional '='.
*      I2 = INDEX(CSTR(1:N),'=')
*      term = '='
      i2 = 0         ! DMB, 6/12/01
      term = ' '     ! DMB, 6/11/01
* Changes on 6/11/01:

c Find start and end of KPARMth field.
      do 4 K=1,KPARM
         I1 = I2
2        I2 = I1
3        I1 = I1+1
         if (CSTR(I1:I1).eq.' ') goto 3
         if (term.eq.' '.and.CSTR(I1:I1).eq.',') then
            term = ','
            goto 2
         end if
         IBLANK = INDEX(CSTR(I1:N),' ')
         ICOMMA = INDEX(CSTR(I1:N),',')
         if (IBLANK.eq.0.and.ICOMMA.eq.0.and.K.lt.KPARM) goto 801
         if (IBLANK.eq.0) IBLANK = N-I1+2
         if (ICOMMA.eq.0) ICOMMA = N-I1+2
         I2 = I1+MIN(IBLANK,ICOMMA)-1
         if (I2.gt.N) then
            term = '.'
         else
            term = CSTR(I2:I2)
         end if
4        continue
c Check for empty field.
      if (I2.eq.I1) goto 802
c Find optional '>'.
      I1 = I1+INDEX(CSTR(I1:I2),'>')
c Get F.
10    read (CSTR(I1:I2-1),90010,err=803) F
90010 format (f15.0)
      ibgn = i1
      iend = i2 - 1
      return
c Errors.
801   ISTAT = 1
      return
802   ISTAT = 2
      return
803   ISTAT = 3
      return
      end
! --------------------------- END RCF ------------------------------



! -------------------------- BEGIN RCI -----------------------
      subroutine RCI (KPARM,CSTR_IN,NCSTR_IN,I,ibgn,iend,ISTAT)
c RCI - [MUELLER.FS.GEN]CSMGENLB
c Read integer I, the KPARMth element in CSTR_IN(1:NCSTR_IN).
c  CSTR_IN contains one-or-more mixed-type fields
*(OLD COMMENT:c  separated by blanks or commas to the right of an optional '='.)
c  KPARMth element contains I to the right of an optional '>'.
c ISTAT= 0 for success;
c      = 1 if CSTR contains fewer than KPARM fields;
c      = 2 if KPARMth field is empty;
c      = 3 if KPARMth field is unreadable;
c      if ISTAT>0, return with I unchanged.

* Dates:  xx/xx/xx - Written by C. Mueller, USGS
*         06/22/98 - Modified by L. Baker
*         11/12/00 - Dave Boore introduced a working array, and a call to 
*                    rmv_tabs (this replaces a tab with a blank, but does not
*                    expand the tabs).  This was done because a file in which 
*                    fields are separated by tabs characters will not be parsed 
*                    correctly; apparently the tab characters (ascii 9) is 
*                    not the same thing as a blank.
*         12/04/00 - added ibgn, iend to rcf, rci, rcc calling arguments
*         06/11/01 - Disable use of "=" to indicate where the parameters
*                    start.  I did this because I sometimes add comments
*                    to the right of the parameters, and if these comments
*                    contain the character "=" then the parameters are not
*                    parsed correctly.  I reasoned that eliminating the check
*                    for "=" is the best solution to the problem (as opposed
*                    to restricting the use of "=" to only indicate 
*                    where the parameters start) because I rarely used
*                    "=" in this way.
*         06/12/01 - Minor modification (i2 = 0 rather than i2 = 1)
*         10/29/02 - Changed "rmv_tabs" to "tabs_rmv"
!         07/25/10 - Increase dimension of cstr

      character CSTR_IN*(*),term*1

      character cstr*500

      if (ncstr_in .gt. 500) then
         write(*,'(a, 1x,i5, a)') '  WARNING: IN RCI, NCSTR_IN = ', 
     :          ncstr_in,
     :         ' WHICH IS GREATER THAN THE DIMENSION OF THE'//
     :         ' WORK ARRAY; QUITTING!!!!!'
         return
      end if

      ncstr = ncstr_in

      cstr = ' '
      cstr(1: ncstr) = cstr_in
      call tabs_rmv(cstr, ncstr)

      ISTAT = 0
c Eliminate trailing white space.
      N = NCSTR+1
1     N = N-1
      if (CSTR(N:N).eq.' '.or.CSTR(N:N).eq.',') goto 1

* Changes on 6/11/01:
*c Find optional '='.
*      I2 = INDEX(CSTR(1:N),'=')
*      term = '='
      i2 = 0         ! DMB, 6/12/01
      term = ' '     ! DMB, 6/11/01
* Changes on 6/11/01:

c Find start and end of KPARMth field.
      do 4 K=1,KPARM
         I1 = I2
2        I2 = I1
3        I1 = I1+1
         if (CSTR(I1:I1).eq.' ') goto 3
         if (term.eq.' '.and.CSTR(I1:I1).eq.',') then
            term = ','
            goto 2
         end if
         IBLANK = INDEX(CSTR(I1:N),' ')
         ICOMMA = INDEX(CSTR(I1:N),',')
         if (IBLANK.eq.0.and.ICOMMA.eq.0.and.K.lt.KPARM) goto 801
         if (IBLANK.eq.0) IBLANK = N-I1+2
         if (ICOMMA.eq.0) ICOMMA = N-I1+2
         I2 = I1+MIN(IBLANK,ICOMMA)-1
         if (I2.gt.N) then
            term = '.'
         else
            term = CSTR(I2:I2)
         end if
4        continue
c Check for empty field.
      if (I2.eq.I1) goto 802
c Find optional '>'.
      I1 = I1+INDEX(CSTR(I1:I2),'>')
c Get I.
10    read (CSTR(I1:I2-1),90010,err=803) I
90010 format (i12)
      ibgn = i1
      iend = i2-1
      return
c Errors.
801   ISTAT = 1
      return
802   ISTAT = 2
      return
803   ISTAT = 3
      return
      end
! -------------------------- END RCI -----------------------



! ---------------------------------------------------------------- Tabs_Rmv
      subroutine tabs_rmv(string, nchar)

* Replaces tabs in the character string with a blank

* Dates: 11/04/00 - Written by D. Boore
*        10/29/02 - Renamed from rmv_tabs.for

      character string*(*)


      do i = 1, nchar
        if(ichar(string(i:i)) .eq. 9) then
           string(i:i) = ' '
        end if
      end do

      return
      end
! ---------------------------------------------------------------- Tabs_Rmv

! ----------------------------------------------------- interpolate_time_series_using_fork
      subroutine interpolate_time_series_using_fork(
     : y_in, sps_in, npts_in, 
     : interpolate_factor,
     : y_out, sps_out, npts_out
     : )


! Interpolates an smc file to finer spacing, using an FFT with zeroes
! added after the Nyquist.

! Dates: 09/29/12 - Written by D.M. Boore, patterned after smc_interpolate_time_series_using_fork.
!                 - This version does not allow for linear resampling after interpolation;
!                   for that, see smcintrp.for.
!                 - Return input time series if interpolate_factor = 1
 
      real y_in(*), y_out(*) 
      integer npts_in, npts_out, npw2
      real sps_in, sps_out

 
!
! Dimension and declaration statements:

      complex cx_in(:), cx_intrp(:)
      allocatable :: cx_in, cx_intrp
 
      if (interpolate_factor == 1) then
        npts_out = npts_in
        sps_out = sps_in
        do i = 1, npts_out
          y_out(i) = y_in(i)
        end do
        return
      end if

! Check if interpolate_factor is a power of 2: 
       if (modulo(alog(real(interpolate_factor)), alog(2.0)) /= 0) then
         print *,' interpolate_factor= ', 
     :              interpolate_factor, ' not a power of 2; QUIT!!'
         stop
       end if
 
 
! Compute the Fourier spectra:

! Fill working array with input data:

      signnpw2 = +1.0
      call get_npw2(npts_in,signnpw2,npw2_in)
      
      n4ts_alloc = max0(npts_in, npw2_in)
      allocate( cx_in(npw2_in) )
 
      do i = 1, npw2_in
        cx_in(i) = 0.0
      end do

      do i = 1, npts_in
        cx_in(i) = cmplx(y_in(i), 0.0)
      end do

      df = sps_in/float(npw2_in)
      
      n_nyq_in = npw2_in/2 + 1

! FFT to get spectrum

!      call forkdp(npw2_in,cx_in,-1.)
      call fork(npw2_in,cx_in,-1.)

! Fill new complex array, inserting zeros

      npw2_intrp = npw2_in * interpolate_factor
      sps_intrp = sps_in * real(interpolate_factor)
      sps_out = sps_intrp
      scale = sps_intrp/sps_in
      anorm = sqrt(scale)
      npts_intrp =int( scale * float(npts_in) )
      
      
      allocate( cx_intrp(npw2_intrp) )
 
      do j = 1, npw2_intrp
        cx_intrp(j) = 0.0
      end do

      do j = 1, n_nyq_in
        cx_intrp(j)= cx_in(j)
      end do

      do j = 1, n_nyq_in - 2
        cx_intrp(npw2_intrp+1-j)= cx_in(npw2_in+1-j)
      end do

      deallocate( cx_in)

! Inverse FFT:

!      call forkdp(npw2_intrp,cx_intrp,+1.)
      call fork(npw2_intrp,cx_intrp,+1.)

! Scale for different lengths and save only the points up to the old last point:

      do j = 1, npts_intrp
        y_out(j) = anorm * real(cx_intrp(j))
      end do

      deallocate( cx_intrp)
      
  
      return
      end
! ----------------------------------------------------- interpolate_time_series_using_fork

 

! ------------------------------------------------------------- Get_NPW2
      subroutine get_npw2(npts,signnpw2,npw2)

* Find npw2 (less than npts if signnpw2 < 0)

* Dates: 12/12/00 - Written by D. Boore
!        04/04/10 - Correct error that it does not return
!                   npw2 = npts, if npts is a power of 2.

      npw2_exp = int( alog(float(npts))/alog(2.0) )
      if (signnpw2 < 0.0) then
        npw2 = 2.0**npw2_exp
      else 
        npw2_temp = 2.0**npw2_exp
        if (npw2_temp == npts) then 
          npw2 = npts
        else
          npw2 = 2.0**(npw2_exp+1)
        end if
      end if

      return
      end
! ------------------------------------------------------------- Get_NPW2


! --------------------------------------------------------------- TprFrctn
      subroutine tprfrctn (frctnFRONT,frctnBACK,Z,NZ)

c  Apply cosine tapers to first frctnfront*nz points of beginning
*  and last frctnback*nz points of time series array Z.
c  Written by Chuck Mueller, USGS.
c  Modified by D. M. Boore on 8/31/88 to eliminate the use of ZNULL;
c  see FBCTPR_CSM for the original version.

*  Dates:  2/13/90 - if ifront or iback is zero, do not apply a taper.
*         12/12/00 - specify taper in terms of fraction of number of
*                    points rather than percent of number (copied from
*                    fbctpr.for)
!         09/29/12 - Small change to trap for frctn = 0.0

      real Z(*)

      PI = 4.0*ATAN(1.0)
      LZ = NZ*frctnFRONT

      if (lz >= 1) then

        SF = PI/LZ
        do I=1,LZ
          F = 0.5*(1.0-COS(SF*(I-1)))
          Z(I) = Z(I)*F
        end do
        
      end if

      LZ = NZ*frctnback

      if (lz >= 1) then
 
        SF = PI/LZ
        do I=NZ,NZ-LZ+1,-1
          F = 0.5*(1.0-COS(SF*(NZ-I)))
          Z(I) = Z(I)*F
        end do

      end if
      
      return
      end
! --------------------------------------------------------------- TprFrctn


! ------------------------ begin dcdt -------------------
      subroutine dcdt (y,dt,npts,indx1,indx2,ldc,ldt)
c+
c  dcdt - fits dc or trend between indices indx1 and indx2.
c         then removes dc or detrends whole trace.
c         y is real, dt = delta t.
c         if remove dc, ldc = .true.
c         if detrend, ldt = .true.
c-

* Dates: 12/14/00 - Cleaned up formatting of original program

      real y(*)
      logical ldc,ldt

      if (.not. ldc .and. .not. ldt) then
        return
      end if

c
c...fit dc and trend between indices indx1 and indx2.
      nsum = indx2-indx1+1
      sumx = 0.0
      sumx2 = 0.0
      sumy = 0.0
      sumxy = 0.0
      do i=indx1,indx2
         xsubi = (i-1)*dt
         sumxy = sumxy+xsubi*y(i)
         sumx = sumx+xsubi
         sumx2 = sumx2+xsubi*xsubi
         sumy = sumy+y(i)
      end do
c
c... remove dc.
      if (ldc) then
        avy = sumy/nsum
        do i=1,npts
          y(i) = y(i)-avy
        end do
* Debug
        write(*,'(a)') ' indx1, indx2, avy'
        write(*, *)      indx1, indx2, avy
* Debug



        return
      endif
c
c... detrend. see draper and smith, p. 10.
      if (ldt) then
        bxy = (sumxy-sumx*sumy/nsum)/(sumx2-sumx*sumx/nsum)
        axy = (sumy-bxy*sumx)/nsum
        qxy = dt*bxy
        do i=1,npts
          y(i) = y(i)-(axy+(i-1)*qxy)
        end do
        return
      endif
c
      return
      end
! ------------------------ end dcdt -------------------


! ------------------------------ BEGIN ZEROPAD --------------------
      subroutine ZEROPAD (Y,NIN,NPW2)

c Pads time-series array Y with (NPW2-NIN) zeroes.
*  With this program 
c the window of the data, which determines NIN, can be different 
c for different time series, yet the overall length of 
c the time series used in the FFT can 
c be the same (call get_npw2 with an overall duration NTOTIN), thus 
* guaranteeing 
c that the frequencies for which FFT
c values are computed are the same.
c I assume that the user makes sure that NTOTIN .ge. NIN.

* Dates: 12/12/00 - Written by D. Boore, patterned after zeropad2.for

      real Y(*)
      
      if (npw2 .le. nin) then
        return
      else
        do i=nin+1,npw2
          y(i) = 0.0
        end do
        return
      end if

      end
! ------------------------------ END ZEROPAD --------------------




! ----------------------------------------------------------- Four1
      SUBROUTINE four1(data,nn,isign)
* 02 October 2001 version from Numerical Recipes
      INTEGER isign,nn
      REAL data(2*nn)
      INTEGER i,istep,j,m,mmax,n
      REAL tempi,tempr
      DOUBLE PRECISION theta,wi,wpi,wpr,wr,wtemp
      n=2*nn
      j=1
      do 11 i=1,n,2
        if(j.gt.i)then
          tempr=data(j)
          tempi=data(j+1)
          data(j)=data(i)
          data(j+1)=data(i+1)
          data(i)=tempr
          data(i+1)=tempi
        endif
        m=nn
!        m=n/2 in version dated 04 November 2006 in folder Forprogs
1       if ((m.ge.2).and.(j.gt.m)) then
          j=j-m
          m=m/2
        goto 1
        endif
        j=j+m
11    continue
      mmax=2
2     if (n.gt.mmax) then
        istep=2*mmax
        theta=6.28318530717959d0/(isign*mmax)
        wpr=-2.d0*sin(0.5d0*theta)**2
        wpi=sin(theta)
        wr=1.d0
        wi=0.d0
        do 13 m=1,mmax,2
          do 12 i=m,n,istep
            j=i+mmax
            tempr=sngl(wr)*data(j)-sngl(wi)*data(j+1)
            tempi=sngl(wr)*data(j+1)+sngl(wi)*data(j)
            data(j)=data(i)-tempr
            data(j+1)=data(i+1)-tempi
            data(i)=data(i)+tempr
            data(i+1)=data(i+1)+tempi
12        continue
          wtemp=wr
          wr=wr*wpr-wi*wpi+wr
          wi=wi*wpr+wtemp*wpi+wi
13      continue
        mmax=istep
      goto 2
      endif
      return
      END
! ----------------------------------------------------------  Four1



! --------------------------------------------------- realft
      SUBROUTINE realft(data,n,isign)
      INTEGER isign,n
      REAL data(n)
CU    USES four1
      INTEGER i,i1,i2,i3,i4,n2p3
      REAL c1,c2,h1i,h1r,h2i,h2r,wis,wrs
      DOUBLE PRECISION theta,wi,wpi,wpr,wr,wtemp
      theta=3.141592653589793d0/dble(n/2)
      c1=0.5
      if (isign.eq.1) then
        c2=-0.5
        call four1(data,n/2,+1)
      else
        c2=0.5
        theta=-theta
      endif
      wpr=-2.0d0*sin(0.5d0*theta)**2
      wpi=sin(theta)
      wr=1.0d0+wpr
      wi=wpi
      n2p3=n+3
      do 11 i=2,n/4
        i1=2*i-1
        i2=i1+1
        i3=n2p3-i2
        i4=i3+1
        wrs=sngl(wr)
        wis=sngl(wi)
        h1r=c1*(data(i1)+data(i3))
        h1i=c1*(data(i2)-data(i4))
        h2r=-c2*(data(i2)+data(i4))
        h2i=c2*(data(i1)-data(i3))
        data(i1)=h1r+wrs*h2r-wis*h2i
        data(i2)=h1i+wrs*h2i+wis*h2r
        data(i3)=h1r-wrs*h2r+wis*h2i
        data(i4)=-h1i+wrs*h2i+wis*h2r
        wtemp=wr
        wr=wr*wpr-wi*wpi+wr
        wi=wi*wpr+wtemp*wpi+wi
11    continue
      if (isign.eq.1) then
        h1r=data(1)
        data(1)=h1r+data(2)
        data(2)=h1r-data(2)
      else
        h1r=data(1)
        data(1)=c1*(h1r+data(2))
        data(2)=c1*(h1r-data(2))
        call four1(data,n/2,-1)
      endif
      return
      END
! -------------------------------------------------- realft


! ----------------------------- BEGIN FORK --------------------------
      SUBROUTINE FORK(LX,CX,SIGNI)
C FAST FOURIER                                  2/15/69
C                          LX
C    CX(K) = SQRT(1.0/LX)* SUM (CX(J)*EXP(2*PI*SIGNI*I*(J-1)*(K-1)/LX))
C                          J=1                        FOR K=1,2,...,LX
C
C  THE SCALING BETWEEN FFT AND EQUIVALENT CONTINUUM OUTPUTS
C  IS AS FOLLOWS.
C
C
C     GOING FROM TIME TO FREQUENCY:
C             F(W)=DT*SQRT(LX)*CX(K)
C
C                  WHERE W(K)=2.0*PI*(K-1)*DF

*                  and    DF = 1/(LX*DT)
C
C
C     GOING FROM FREQUENCY TO TIME, WHERE THE FREQUENCY
C     SPECTRUM IS GIVEN BY THE DIGITIZED CONTINUUM SPECTRUM:
C
C             F(T)=DF*SQRT(LX)*CX(K)
*
C                  WHERE T(K)=(K-1)*DT
C
C
C  THE RESULT OF THE SEQUENCE...TIME TO FREQUENCY,POSSIBLE MODIFICATIONS
C  OF THE SPECTRUM (FOR FILTERING,ETC.), BACK TO TIME...
C  REQUIRES NO SCALING.
C
C
C  THIS VERSION HAS A SLIGHT MODIFICATION TO SAVE SOME TIME...
C  IT TAKES THE FACTOR 3.1415926*SIGNI/L OUTSIDE A DO LOOP (D.BOORE 12/8
C  FOLLOWING A SUGGESTION BY HENRY SWANGER).
C

* Some brief notes on usage:

* "signi" is a real variable and should be called either with the value "+1.0"
* of "-1.0".  The particular value used depends on the conventions being used
* in the application (e.g., see Aki and Richards, 1980, Box 5.2, pp. 129--130).

* Time to frequency:
* In calling routine,
* 
*       do i = 1, lx
*         cx(i) = CMPLX(y(i), 0.0)
*       end do
*  where y(i) is the time series and lx is a power of 2
* 
*  After calling Fork with the complex array specified above, the following 
* symmetries exist:
* 
*        cx(1)        = dc value (f = 0 * df, where df = 1.0/(lx*dt))
*        cx(lx/2 + 1) = value at Nyquist (f = (lx/2+1-1)*df = 1.0/(2*dt))
*        cx(lx)       = CONJG(cx(2))
*        cx(lx-1)     = CONJG(cx(3))
*         |           =      |
*        cx(lx-i+2)   = CONJG(cx(i))
*         |           =      |
*        cx(lx/2+2)   = CONJG(cx(lx/2))
* 
* where "CONJG" is the Fortran complex conjugate intrinsic function
* 
* This symmetry MUST be preserved if modifications are made in the frequency 
* domain and another call to Fork (with a different sign for signi) is used
* to go back to the time domain.  If the symmetry is not preserved, then the
* time domain array will have nonzero imaginary components.  There is one case
* where advantage can be taken of this, and that is to find the Hilbert 
* transform and the window of a time series with only two calls to Fork (there 
* is a short note in BSSA {GET REFERENCE} discussing this trick, which amounts 
* to zeroing out the last half of the array and multiplying all but the dc and 
* Nyquist values by 2.0; in the time domain, REAL(cx(i)) and AIMAG(cx(i)) 
* contain the filtered (if a filter was applied) and Hilbert transform of the 
* filtered time series, respectively, while CABS(cx(i)) and ATAN2(AIMAG(cx(i)), 
* REAL(cx(i))) are the window and instantaneous phase of the filtered time 
* series, respectively.

* Some references:

* Farnbach, J.S. (1975). The complex envelope in seismic signal analysis, 
* BSSA 65, 951--962. 
* He states that the factor of 2 is applied for i = 2...npw2/2 (his indices 
* start at 0, I've added 1), which is different than the next reference:

* Mitra, S.K. (2001). Digital Signal Processing, McGraw-Hill, New York.
* He gives an algorithm on p. 794 (eq. 11.81), in which the factor of 2 is 
* applied from 0 frequency to just less than Nyquist.

* 
* The easiest way to ensure the proper symmetry is to zero out the
* last half of the array (as discussed above), but the following is what
* I usually use:  
* modify (filter) only half
* of the cx array:
* 
*       do i = 1, lx/2
*         cx(i) = filter(i)*cx(i)
*       end do
* 
* where "filter(i)" is a possibly complex filter function (and recall that 
* the frequency corresponding to i is f = float(i-1)*df).  After this, fill out
* the last half of the array using
*       
*       do i = lx/2+2, lx
*         cx(i) = CONJG(cx(lx+2-j))
*       end do
* 
* Note that nothing is done with the Nyquist value.  I assume (but am not sure!)
* that this value should be 0.0
* 
* Dates: xx/xx/xx - Written by Norm Brenner(?), Jon Claerbout(?)
*        12/21/00 - Replaced hardwired value of pi with pi evaluated here,
*                     and added comments regarding usage.  Also deleted
*                     dimension specification of cx(lx) and replace it with
*                     cx(*) in the type specification statement.  I also
*                     cleaned up the formatting of the subroutine.
*        08/28/01 - Added comment about variable "signi" being real, and 
*                   added "float" in equations for "sc" and "temp", although 
*                   not strictly required.
*        06/19/02 - Added some comments about computing envelopes and
*                   instantaneous frequencies
             
      complex cx(*),carg,cexp,cw,ctemp

      pi = 4.0*atan(1.0)

      j=1
      sc=sqrt(1./float(lx))

      do i=1,lx
        if(i.gt.j) go to 2
        ctemp=cx(j)*sc
        cx(j)=cx(i)*sc
        cx(i)=ctemp
2       m=lx/2
3       if(j.le.m) go to 5
        j=j-m
        m=m/2
        if(m.ge.1) go to 3
5       j=j+m
      end do

      l=1
6     istep=2*l
      temp= pi * signi/float(l)

      do m=1,l
        carg=(0.,1.)*temp*(m-1)
        cw=cexp(carg)
        do i=m,lx,istep
          ctemp=cw*cx(i+l)
          cx(i+l)=cx(i)-ctemp
          cx(i)=cx(i)+ctemp
        end do
      end do

      l=istep
      if(l.lt.lx) go to 6

      return
      end
! ----------------------------- END FORK --------------------------


!----------------- BEGIN Trapezoidal_Integration -----------------------------
      subroutine Trapezoidal_Integration(y, npts, dt, yint_0, yint)


* Integrate a time series y with initial condition yint_0

 
* Dates: 02/07/11 - Written by D.M. Boore, patterned after acc2vd.for
      real y(*), yint(*) 
      double precision cumv, y1, y2, v1, v2,
     : ddt, ddt_2 

 
* Integrate

      ddt     = dble(dt)
      ddt_2   = 0.5d0 * dble(dt)
 
      cumv = 0.0d0
 
      v1 = 0.0d0
      v2 = 0.0d0
      
      yint(1) = yint_0
 
      do j=2,npts
      
        y1 = dble(y(j-1))
        y2 = dble(y(j))
        
        cumv = cumv + (y1 + y2)*ddt_2
        v1 = v2
        v2 = cumv + dble(yint_0)  ! the yint_0 term is the constant of integration
        yint(j) = sngl(v2)
        
      end do

      return
      end
!----------------- END Trapezoidal_Integration -----------------------------


! ---------------------------- BEGIN SELECT_SUBSETS_FOR_ROTATIONS -------------------
      subroutine select_subsets_for_rotations(
     :  y1_in, y2_in, n_in, dt, fraction,
     :  y1_out, y2_out, t_out, n_out,
     :  alevel)
     
! Implement Norm Abrahamson's algorithm (with my correction) to speed up computations:  
! save y1, y2 for times i for which 
! min(abs(y1(i)), abs(y2(i)) > fraction*min(abs(y1(all times), abs(y2(all times))).
! Norm used fraction = 2/3 (theoretically, it could have been 1/sqrt(2)).    

! The larger the value of fraction, the smaller the output time series
! fraction = 0.0 will return all of the input time series.   

! Dates: 09/23/12 - Written by D. M. Boore
!        09/26/12 - The algorithm did not work for a few of the test cases, so I am trying 
!                   a new algorithm.


      real y1_in(*), y2_in(*), dt, y1_out(*), y2_out(*), t_out(*),
     :     fraction
      
      integer n_in, n_out
      
!Find overall absolute maxima

      nstrt = 1
      ninc = 1
      call abs_mnmaxidx(y1_in, nstrt, n_in, ninc,
     :                  y1min, y1max, indx_min, indx_max, peak_y1) 
      call abs_mnmaxidx(y2_in, nstrt, n_in, ninc,
     :                  y2min, y2max, indx_min, indx_max, peak_y2) 
     
      alevel = fraction*amin1(peak_y1, peak_y2)

!DEBUG
!      print *,' y1min, y1max, peak_y1 =', y1min, y1max, peak_y1
!      print *,' y2min, y2max, peak_y2 =', y2min, y2max, peak_y2
!      print *,' fraction, alevel =', fraction, alevel
!DEBUG

      n_out = 0
      
      DO i = 1, n_in
      
!        if (abs(y1_in(i)) >= alevel .or. abs(y2_in(i)) >= alevel) then
        if (sqrt(y1_in(i)**2+y2_in(i)**2) >= alevel) then
          n_out = n_out + 1
          y1_out(n_out) = y1_in(i)
          y2_out(n_out) = y2_in(i)
          t_out(n_out) = real(i-1)*dt
          cycle
        end if
      
      END DO
      
      return
      end
! ---------------------------- END SELECT_SUBSETS_FOR_ROTATIONS -------------------

      
!      include '\forprogs\abs_mnmaxidx.for'  ! Included in smc2psa_util_subs
       
      
      
      !----------------- BEGIN RDRVAA_RD_TS -----------------------------
      subroutine rdrvaa_rd_ts(acc,na,omega,damp,dt,rd,rv,aa,d0,v0,ts)
! This is a modified version of "Quake.For", originally
! written by J.M. Roesset in 1971 and modified by
! Stavros A. Anagnostopoulos, Oct. 1986.  The formulation is that of
! Nigam and Jennings (BSSA, v. 59, 909-922, 1969).  This modification
! returns the time series of the relative displacement, in addition to
! rd, rv, and aa

!   acc = acceleration time series
!    na = length of time series
! omega = 2*pi/per
!  damp = fractional damping (e.g., 0.05)
!    dt = time spacing of input
!    rd = relative displacement of oscillator
!    rv = relative velocity of oscillator
!    aa = absolute acceleration of oscillator
! d0,v0 = initial displacement and velocity (usually set to 0.0)

! Dates: 05/06/95 - Modified by David M. Boore
!        04/15/96 - Changed name to RD_CALC and added comment lines
!                   indicating changed for storing the oscillator time series
!                   and computing the relative velocity and absolute
!                   acceleration
!        04/16/96 - This is RD_CALC, with the time series of the relative
!                   displacement added and the name changed
!        03/14/01 - Made this double precision, but did not rename it as I
!                   did rc_calc and rdrvaa.  Also added initial displacement 
!                   and velocity.
!        01/31/03 - Moved implicit statement before the type declarations
!        10/10/07 - Initial variable assignments and iteration loop modified 
!                   to double-precision (Chris Stephens)
!        02/08/08 - Set ts(1)=0.0 and fill ts array up to na points.  The previous
!                   version of the program filled ts to na-1, but the program 
!                   calling this subroutine thought that the time series had
!                   a length of na.  What I do now is shift the ts by dt, assuming
!                   that ts(1) = 0.0
!        08/14/12 - Included 12/22/10 revision made to rscalc_ts: 
!                     Remove minus sign in front of the initialization of y, ydot. 
!                     The minus sign was a remnant of an earlier version where I did not
!                     understand the meaning of y and ydot.
!                 - Included 12/26/10 revision made to rscalc_ts: 
!                     Correct error that first vel and dis points = 0.0
!        09/22/12 - Renamed from rdcalcts, and now include rv and aa in output
!                   (following rdrvaa.for).

      IMPLICIT REAL*8 (a-h,o-z)
      REAL*4 acc(*), omega, damp, dt, rd, rv, aa, d0, v0, ts(*)

      omt=dble(omega)*dble(dt)
      d2=1-dble(damp)*dble(damp)
      d2=dsqrt(d2)
      bom=dble(damp)*dble(omega)
      d3 = 2.*bom                 ! for aa
      omd=dble(omega)*d2
      om2=dble(omega)*dble(omega)
      omdt=omd*dble(dt)
      c1=1.d0/om2
      c2=2.d0*dble(damp)/(om2*omt)
      c3=c1+c2
      c4=1.d0/(dble(omega)*omt)
      ss=dsin(omdt)
      cc=dcos(omdt)
      bomt=dble(damp)*omt
      ee=dexp(-bomt)
      ss=ss*ee
      cc=cc*ee
      s1=ss/omd
      s2=s1*bom
      s3=s2+cc
      a11=s3
      a12=s1
      a21=-om2*s1
      a22=cc-s2
      s4=c4*(1.d0-s3)
      s5=s1*c4+c2
      b11=s3*c3-s5
      b12=-c2*s3+s5-c1
      b21=-s1+s4
      b22=-s4
      rd=0.
      rv = 0.                           ! for rv
      aa = 0.                           ! for aa

      y = dble(d0)
      ydot = dble(v0) ! These are initial values of the oscillator
!      y=0.
!      ydot=0.

      ts(1) = d0  ! 14aug12
!      ts(1) = 0.0  ! 08feb08

      DO i=1, na-1   ! 08feb08 (used new variable n1=na-1, but I replaced the upper limit with 
                     ! na-1 on 09/22/12, as n1 was only used here)

        y1=a11*y+a12*ydot+b11*dble(acc(i))+b12*dble(acc(i+1))
        ydot=a21*y+a22*ydot+b21*dble(acc(i))+b22*dble(acc(i+1))
        y=y1    ! y is the oscillator output at time corresponding to index i+1 
                ! changed "i" in this comment to "i+1" on 14aug12
        ts(i+1) = y   !08feb08
        z=dabs(y)
        if (z > rd) rd=z
        z1 = dabs(ydot)                   ! for rv
        if (z1 > rv) rv = z1            ! for rv
        ra = -d3*ydot -om2*y1            ! for aa
        z2 = dabs(ra)                     ! for aa
        if (z2 > aa) aa = z2            ! for aa

      END DO

      RETURN
      END
!----------------- END RDRVAA_RD_TS ----------------------------
