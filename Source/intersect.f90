!cssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!c
        subroutine  intersect  (b1,b2,b3,b4,b5,d2)
!c
!cssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!c
!c		This routine calculates the coordinates of the intersection
!		point of two strait line in an horizontal plane, at a depth d2.

!		Coordinates of two points B1 and B2 that define the first line
!		Coordinates of two points B4 and B5 that define the second line
!		The two lines are in a plane at depth D2, and intersect
!		at point B3.

!		Input:
!         *****
!		Coordinates of the points B1, B2, B4, B5, and depth D2.
!		Output:
!         ******
!		Coordinates of point B3:
!		B3(1) = longitude
!		B3(2) = latitude  decimal degrees
!		B3(3) = depth in km.
!		ipar  = flag that indicates if the two lines are parallel
!				1 : the lines are parallel. no intersection
!					the point B3 is arbitrarily set equal to B2
!				0 : lines intersect in B3.
!

        implicit none 
        integer :: ipar
        real :: d2,epsilon,xa1,xa2,xb1,xb2
        real, dimension(3) :: b1,b2,b3,b4,b5

        epsilon = 0.1
        ipar = 0
        xa1 = b2(1)-b1(1)

        if (xa1 .ne. 0.)  then
!       B1-B2 IS NOT on a Meridian
            xa1 = (b2(2)-b1(2)) / xa1
            xb1 = b1(2) - xa1*b1(1)
            xa2 = b5(1)-b4(1)
            if (xa2 .ne. 0)  then
!           B4-B5 IS NOT on a Meridian
                xa2 = (b5(2)-b4(2)) / xa2
                xb2 = b5(2) - xa2*b5(1)
!			Are B1-B2 and B4-B5 parallel?
                if (abs(xa1-xa2) .le. epsilon)  then
!	        Yes they are parallel. Set B3 equal to B2
                    ipar = 1
                    b3(1)=b2(1)
                    b3(2)=b2(2)
                else
!		    No they are not parallel
                    b3(1) = (xb2-xb1) / (xa1-xa2)
                    b3(2) = xa1*b3(1) + xb1
                end if
            else
!           B4-B5 IS on a Meridian (along Oy)
            b3(1) = b4(1)
            b3(2) = xa1*b3(1) +xb1
            end if

        else

!           B1-B2 IS on a Meridian (along Oy)	
            b3(1) = b2(1)
            xa2 = b5(1) - b4(1)
            if (xa2 .ne. 0.)  then
!               B4-B5 is NOT on a Meridian
                xa2 = (b5(2)-b4(2)) / xa2
                xb2 = b5(2) - xa2*b5(1)
                b3(2) = xa2*b3(1) + xb2
            else
!               B4-B5 IS on a Meridian (along Oy),parallel to B1-B2
                b3(2) = b2(2)
                ipar = 1
             end if
        end if

!       All points are at the same depth: D2	  
        b3(3) = d2

        return
        end subroutine intersect
