      subroutine ttvdp(x1,y1,zh1,x2,y2,zh2,dp1,apz,vsp,vpm,zm,ttvd)

! Time between two points on fault at P-wave velocity
!
! x and y are position on the fault plane with y axis up dip, and zh 
! is their vertical depth. x1,y1 are at the origin, 
!
! Natural log exceeds machine precision as argument approaches 1.0

      IMPLICIT NONE

      REAL :: ang, apz, dp1, pi2
      REAL :: ttvd, ttvd1, ttvd2, vpm, vsp
      REAL :: x1, x2, x3, y1, y2, y3, z3, zh1, zh2, zm

      pi2 = 6.283185308
      ang = atan2((x2-x1),(y2-y1))
      if(abs(abs(ang)-pi2/4.0).lt.0.000001)  then
          ang = ang + 0.000001
      endif

      if(abs(zh1-zh2).lt..01) then
          ttvd = abs(x2-x1)/(vsp + apz*zh1)
          if(zh1.gt.zm.and.zh2.gt.zm) then
              ttvd = sqrt((x2-x1)**2+(y2-y1)**2)/vpm 
          endif
         return
      endif

      if(zh1.le.zm.and.zh2.le.zm) then
          ttvd = (1./(apz*cos(ang)*sin(dp1)))*(log(vsp+apz*zh1)-    &
          log(vsp+apz*zh2))
          return
      endif

      if(zh1.gt.zm.and.zh2.gt.zm) then
          ttvd = sqrt((x2-x1)**2+(y2-y1)**2)/vpm 
          return
      endif

      if(zh1.gt.zm.and.zh2.le.zm) then
          y3 = y1 + (zh1-zm)/sin(dp1)
          x3 = x1 + (y3-y1)*tan(ang)
          z3 = zm
          ttvd1 = sqrt((x3-x1)**2+(y3-y1)**2)/vpm
          ttvd2 = (1./(apz*cos(ang)*sin(dp1)))*(log(vsp+apz*z3)-    &
          log(vsp+apz*zh2))
          ttvd = ttvd1 + ttvd2
          return
      endif

      if(zh2.gt.zm.and.zh1.le.zm) then
          y3 = y2 + (zh2-zm)/sin(dp1)
          x3 = x2 + (y3-y2)*tan(ang)
          z3 = zm
          ttvd1 = sqrt((x3-x2)**2+(y3-y2)**2)/vpm
          ttvd2 = (1./(apz*cos(ang)*sin(dp1)))*(log(vsp+apz*zh1)-   &
          log(vsp+apz*z3))
          ttvd = ttvd1 + ttvd2
          return
      endif

      print *,'you shouldnt be here, subroutine ttvdp'

      return
      end
