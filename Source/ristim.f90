      subroutine ristim(apz,vsp,vpm,zm,vr,vh,ip,r3,ang4,rstm,avr,    &
      ah,vrs,dp1,vrh,fx,fy,ixy)

      implicit none


!     parameter (ipfl = 1000)

! risetime is the minimum time from the onset of rupture until the time
! it takes the rupture to reach a fault edge and a healing phase to
! travel back to the element at the healing velocity.
! 
! vrs: is decimal percentage of S-wave velocity for rupture.  Remember,
!      velocity model is for P-waves, divide by 1.76 for S-waves
! vrh: is decimal percentage of rupture velocity of healing phase
!
!   td1 is travel time from hypocenter to element
!   td2 is travel time from fault edge to element, healing phase
!   td3 is travel time from hypocenter to fault edge
!   
!   uses perimeter fault points to search for healing phase initiation
!   points
!
!   ips, allows skiping perimeter fault point for computation
!
      CHARACTER (LEN=1) :: ans1
      CHARACTER (LEN=3) :: ans2, avr
      
      INTEGER :: i, ip, ips, ixy
      INTEGER, PARAMETER :: ipfl1 = 10000

      REAL    :: ah, ang4, ang44, apz, dp1, pi2, r3, rstm, rstm1
      REAL    :: td1, td11, td2, td22, td3, td33
      REAL    :: vh, vpm, vr, vrh, vrs, vsp, x0, x1, y0, y1, z0, z1, zm

      REAL, dimension(ipfl1) :: fx, fy

      DATA ans1,ans2 /'y','yes'/

      ips = 1
      if(ip.gt.300) then
          ips = 2
      endif
      pi2 = 6.28318531
      x0 = r3*sin(ang4)
      y0 = r3*cos(ang4)
      z0 = ah - r3*cos(ang4)*sin(dp1)
      td1 = sqrt(x0**2+y0**2)/vr
      if(avr.ne.ans1.and.avr.ne.ans2) then
          call ttvdp(0.0,0.0,ah,x0,y0,z0,dp1,apz,vsp,vpm,zm,td1)
          td1 = (1.76/vrs)*td1
      endif

! calculated in subroutine rupsur.f
      rstm = 100.
      do i=ips,ixy,ips
          x1 = fx(i-ips+1)
          y1 = fy(i-ips+1)
          z1 = ah - fy(i-ips+1)*sin(dp1)
          if(z1.ge.1.0) then
              td2 = sqrt((x0-x1)**2 + (y0-y1)**2)/vh
              if(avr.ne.ans1.and.avr.ne.ans2) then 
                  call ttvdp(x0,y0,z0,x1,y1,z1,dp1,apz,vsp,vpm,zm,td2)
                  td2 = td2*1.76/(vrs*vrh)
              endif
              td3 = sqrt(x1**2 + y1**2)/vr
              if(avr.ne.ans1.and.avr.ne.ans2) then
                  call ttvdp(0.0,0.0,ah,x1,y1,z1,dp1,apz,vsp,vpm,zm,td3)
                  td3 = (1.76/vrs)*td3
              endif
              rstm1 = -td1 + td3 + td2 
              if(rstm1.lt.rstm) then
                  td11 = td1
                  td22 = td2 
                  td33 = td3
                  rstm = rstm1
              endif
          endif
      end do

      ang44 = ang4*360./pi2
      if(rstm.lt.0.01) then
          rstm1 = rstm
          rstm = 0.01
      endif
      return
      end


