!Fortran Code for the Point in Polygon Test 
!Here it is, in Fortran; I wrote it 34 years ago. 
!C>>>PNP1
                                                                
                                                                       
!     ..................................................................
!                                                                       
!        SUBROUTINE PNPOLY                                              
!                                                                       
!        PURPOSE                                                        
!           TO DETERMINE WHETHER A POINT IS INSIDE A POLYGON            
!                                                                       
!        USAGE                                                          
!           CALL PNPOLY (PX, PY, XX, YY, N, INOUT )                     
!                                                                       
!        DESCRIPTION OF THE PARAMETERS                                  
!           PX      - X-COORDINATE OF POINT IN QUESTION.                
!           PY      - Y-COORDINATE OF POINT IN QUESTION.                
!           XX      - N LONG VECTOR CONTAINING X-COORDINATES OF         
!                     VERTICES OF POLYGON.                              
!           YY      - N LONG VECTOR CONTAING Y-COORDINATES OF           
!                     VERTICES OF POLYGON.                              
!           N       - NUMBER OF VERTICES IN THE POLYGON.                
!           INOUT   - THE SIGNAL RETURNED:                              
!                     -1 IF THE POINT IS OUTSIDE OF THE POLYGON,        
!                      0 IF THE POINT IS ON AN EDGE OR AT A VERTEX,     
!                      1 IF THE POINT IS INSIDE OF THE POLYGON.         
!                                                                       
!        REMARKS                                                        
!           THE VERTICES MAY BE LISTED CLOCKWISE OR ANTICLOCKWISE.      
!           THE FIRST MAY OPTIONALLY BE REPEATED, IF SO N MAY           
!           OPTIONALLY BE INCREASED BY 1.                               
!           THE INPUT POLYGON MAY BE A COMPOUND POLYGON CONSISTING      
!           OF SEVERAL SEPARATE SUBPOLYGONS. IF SO, THE FIRST VERTEX    
!           OF EACH SUBPOLYGON MUST BE REPEATED, AND WHEN CALCULATING   
!           N, THESE FIRST VERTICES MUST BE COUNTED TWICE.              
!           INOUT IS THE ONLY PARAMETER WHOSE VALUE IS CHANGED.         
!           THE SIZE OF THE ARRAYS MUST BE INCREASED IF N > MAXDIM   
   
!           WRITTEN BY RANDOLPH FRANKLIN, UNIVERSITY OF OTTAWA, 7/70.   
!                                                                       
!        SUBROUTINES AND FUNCTION SUBPROGRAMS REQUIRED                  
!           NONE                                                        
!                                                                       
!        METHOD                                                         
!           A VERTICAL LINE IS DRAWN THRU THE POINT IN QUESTION. IF IT  
!           CROSSES THE POLYGON AN ODD NUMBER OF TIMES, THEN THE        
!           POINT IS INSIDE OF THE POLYGON.                             
!                                                                       
!     ..................................................................
!                                                                       
      SUBROUTINE PNPOLY(PX,PY,XX,YY,N,INOUT)

      IMPLICIT NONE

      INTEGER, PARAMETER :: MAXDIM = 200
      INTEGER :: INOUT, O  
      INTEGER :: I, J, N
      REAL                    :: PX, PY, Z                    
      REAL, dimension(N)      :: XX, YY  
      REAL, dimension(MAXDIM) :: X, Y                                  
      LOGICAL :: MX,MY,NX,NY                                               
                                                         
!      OUTPUT UNIT FOR PRINTED MESSAGES                                 
      DATA O/6/                                                         
                                                       
      IF(N.LE.MAXDIM)GO TO 6                                            
      WRITE(O,7)                                                        
7     FORMAT('0WARNING:',I5,' TOO GREAT FOR THIS VERSION OF PNPOLY.',  &     
      'RESULTS INVALID')                                                
      RETURN 
                                                           
6     DO  I=1,N                                                        
          X(I)=XX(I)-PX                                                     
          Y(I)=YY(I)-PY 
      END DO                                                    
      INOUT=-1 
                                                         
      DO 2 I=1,N                                                        
          J=1+MOD(I,N)                                                      
          MX=X(I).GE.0.0                                                    
          NX=X(J).GE.0.0                                                    
          MY=Y(I).GE.0.0                                                    
          NY=Y(J).GE.0.0                                                    
          IF(.NOT.((MY.OR.NY).AND.(MX.OR.NX)).OR.(MX.AND.NX)) GO TO 2       
          IF(.NOT.(MY.AND.NY.AND.(MX.OR.NX).AND..NOT.(MX.AND.NX))) GO TO 3  
          INOUT=-INOUT                                                      
          GO TO 2 
 
3         Z =  (Y(I)*X(J)-X(I)*Y(J))/(X(J)-X(I))    
          IF (Z .gt. 0)  then
              INOUT = - INOUT
          ELSEIF (Z .eq. 0)  then
              INOUT = 0
          END IF
                                                    
2     CONTINUE
                                                          
      RETURN                                                            
      END                                                               
