!       This routine is called by Riskcal
!
!*******************************************************************************
!
        subroutine RegionalSeis ( dt, xM0  )
!
!*******************************************************************************
!
!       Creates the regional average recurrence curve based on the recurrence
!       functions (analytical functions or empirical curves) time dependent 
!       or not.
!       It cumulates the average number of events from each possible source
!       in a seismic source map, for each bin of magnitude.
!
!       Input:
!            Arec        : Recurrence parameters
!            dt          : time increment for simulation events
!            kzMap       : Seismic sources Map index
!            nMagbins    : Number of Magnitude bins
!            Ns          : Number of seismic sources in the Map
!            SeisRecType : recurrence distribution function type 
!            xMagLBnd    : Lower bound of Magnitude bins array
!            xMagHBnd    : Upper bound of Magnitude bins array
!            xMax        : Max magnitude considered for the source
!            xM0         : Minimum magnitude considered for the analysis
!
!       Output:
!            LTSReg      : Array of regional average magnitude recurrences
!
!-------------------------------------------------------------------------------
!
        USE ModuleEQ
!
        implicit none
!
        INTEGER :: i, is, j
!
        REAL    :: alfa1, beta1, dt, Lambda, M1, M2, Ninbin, PM0, xMax, xM0
!
!*******************************************************************************
!...........Regional Long-Term Seismicity recurrence function
!           for calibration of the catalog simulations: load in array LTSreg()
            do i = 1, nMagbins; LTSReg(i)=0.;enddo

            do is = 1,Ns
               !Index of the source in the zonation map
               i = Mapzid(kZMap,is)

               !Loop over magnitude bins
               do j = 1, nMagbins
                   M1 = xMagLBnd(j)
                   M2 = xMagHBnd(j)
                   SELECT CASE (SeisRectype(i))                   
                   !Truncated exponential model (G-R truncated model)
                   CASE (1)
                       !Mmax. 
                       xMax = Arec(i,7)
                       !beta.
                       beta1 = Arec(i,4)
                       !alfa.
                       Lambda = Arec(i,3)
                       alfa1 = alog(Lambda) + beta1*Arec(i,1)
                       !Truncated Exponential: Input G-R parameters
                       call  EQTruncRec (alfa1,beta1,dt,xM0,xMax,M1,   &
                       M2,Ninbin,PM0)
                       !Ninbin is the number of events expected in magnitude
                       !bin [M1 - M2], for this source.                       
                   !Characteristic model (Youngs-Coppersmith)
                   CASE (2)                   
                   !Segmented fault model (Foxall Model)
                   CASE (3)
                   !Empirical model manually entered
                   CASE (4)
                   CASE DEFAULT
                   END SELECT

!                  Update array of number of Eq. per magnitude bin per year
!                  The sources' recurrences are on a per year basis
                   LTSReg(j) = LTSReg(j) + Ninbin                  
               end do

            end do
!
            return
!
            END SUBROUTINE RegionalSeis
