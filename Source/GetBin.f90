!
! Get bin that value x, is in vector V of dimension .le. N
!
!   Input:
!       V()     Vector of N values. 
!               V() values must be ordered in ascending order
!       x       value whose bin of V() has to be found
!
!       NOTE:   There are N values in V(), but only (N-1) bins
!       *****
!   
!   Output:
!       k       index of the bin that x falls in
!               k = 0 if x is smaller than V(1)
!               k = N if x is greater than V(N)
!

!*******************************************************************************
        SUBROUTINE GetBin  (k,N,V,x)
      
        IMPLICIT NONE
      
        INTEGER :: i,k,N
      
        REAL :: x
      
        REAL, dimension(N+1) :: V
        
!*******************************************************************************
        
        !If x smaller than smallest value in V, return k = 0
        k = 0
        if (x .lt. V(1))  then
            return
        end if
        
        !If x greater than V(N) then k = N
        if (x .ge. V(N))  then
            k = N
            return
        end if
        
        !x falls between V(1) and V(N)
        do i = 1,N
            if (x .lt. V(i))  then
                k = i - 1
                exit
            end if
        end do
          
        return
        end subroutine GetBin