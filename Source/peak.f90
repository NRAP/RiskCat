      subroutine peak(traz,trar,trat,maxsyn,pkacc)

      implicit none

      INTEGER :: i, maxsyn
      REAL    :: pkacc
      REAL, dimension( maxsyn ) :: trar, trat, traz
 
     
!
      pkacc = 0.0
      do  i = 1,maxsyn
         if(abs(traz(i)).gt.pkacc) then
            pkacc = abs(traz(i))
         end if
      end do
      
      do  i = 1,maxsyn
         if(abs(trar(i)).gt.pkacc) then
            pkacc = abs(trar(i))
         end if
      end do
      
      do  i = 1,maxsyn
         if(abs(trat(i)).gt.pkacc) then
            pkacc = abs(trat(i))
         end if
      end do

!     write (2,*) '  '
!     write (2,*) 'Time histories from full solution, maxsyn=',maxsyn
!     write (2,*) 'traz'
!     write (2,'(6e12.5)') (traz(i),i=1,maxsyn)
!     write (2,*) 'trar'
!     write (2,'(6e12.5)') (trar(i),i=1,maxsyn)
!     write (2,*) 'trat'
!     write (2,'(6e12.5)') (trat(i),i=1,maxsyn)

      return 
      end
