!                      This routine is called from simulperil
!*******************************************************************************
!        Subroutine Percentiles calculates the vector of percentiles  for a
!        sample array of values stored in array Vin().
!        Input:
!        Vin() = Array of the sampled  parameter values, dimension = N  
!        N     = Number of data points in Vin()               
!        W     = Weight of each of the sample points in Vin(), dimension = N
!        Nperc = Number of desired pewrcentile values (values between 0 and 1)
!        
!        Output:
!        Vperc() = Array of dimension Nperc, of percentiles of the sampled 
!                  parameter.
!
!        Notes:
!        1. If perc() values are smaller than the leading weights sum, then
!           the corresponding percentile values are set to the minimum value
!           in the sample.
!        2. If perc() is greater than the trailing weights sum, set Vperc()
!           to the maximum values in the sample.
!        3. The interpolation beweent two values not falling exactly on one
!           of the desired perc() values, is linear.
!
!*******************************************************************************
!
        Subroutine Percentiles (N, Nperc, perc, Vin, Vperc, W)
!
!*******************************************************************************
!        
        implicit none

        INTEGER :: i, j, k1, k2, N, Nperc

        INTEGER, dimension(N) :: ir

        REAL    :: sum, x1, x2, y

        REAL, dimension(Nperc) :: perc, Vperc
        REAL, dimension(N)     :: Vin, Vrd, W, work,Wrd
!
!-------------------------------------------------------------------------------
        !1. Load sampled values into working array:
        do i = 1, N
            Work(i) = Vin(i)
            ir(i)   = i
        end do

        !2. Sort by increasing values and store ordered values into Vrd()
        call sortJBS (ir, N, Work, Vrd)
        do i = 1, N
            !Vrd(i) = Vin(ir(i))
            Wrd(i) = W(ir(i))
        end do

        !3. Find percentiles
        do i = 1, Nperc

            if (perc(i) .lt. Wrd(1))  then
                !perc() is smaller than lowest lower bound
                Vperc(i) = Vrd(1)
            else
                !perc() is greater than first weight
                !Find window of values that contain perc()
                sum = 0.
                do j = 1, N
                    sum = sum + Wrd(j)
                    if (perc(i) .lt. sum) then
                       exit
                    end if
                    k1 = j
                    y  = sum
                end do
                k2 = k1 + 1
                if (k2 .gt. N)  then
                    !largest perc() is smaller than largest lower bound
                    Vperc(i) =Vrd(Nperc)
                else
                    !Linear Interpolation of Vrd()
                    x1 = Vrd(k1)
                    x2 = Vrd(k2)
                    Vperc(i) = x1 + (x2-x1)*((perc(i)-y)/Wrd(k2))
                end if
             end if

         end do

         return
 
         END Subroutine Percentiles
                    
