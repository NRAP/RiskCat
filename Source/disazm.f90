      subroutine disazm(azm,azinv,dis,hdis,elat,elon,h,slat,slon)

      implicit none

      REAL :: avlat, azinv, azm
      REAL :: cora, const
      REAL :: dela, delo, delt, deltaz, delx, dely, dis, dumdeo
      REAL :: elat, elat1, elon, elon1
      REAL :: h, hdis
      REAL :: rhom, rhon
      REAL :: slat, slat1,slon, slon1
      REAL :: theta, trig


      const=57.2957795
      elat1=elat/const
      elon1=elon/const
      slat1=slat/const
      slon1=slon/const
      dela=slat1-elat1
      if(elon1) 11,12,12
   11 if(slon1) 3,4,4
   12 if(slon1) 5,3,3
    3 delo=elon1-slon1
      go to 6
    4 if(abs(elon1)+abs(slon1)-3.1415927) 3,8,8
    8 delo=6.283186-(abs(elon1)+abs(slon1))
      go to 6
    5 if(abs(elon1)+abs(slon1)-3.1415927) 3,10,10
   10 delo=-(6.283186-(abs(elon1)+abs(slon1)))
    6 if(abs(delo)-0.08727) 40,40,51
   40 if(abs(dela)-0.08727) 41,41,51
   51 continue
!      write(6,53)
   53 format('use geocentric earth distance')
      call disbaz(azm,azinv,dis,elat,elon,slat,slon)
      go to 52
   41 avlat=(elat1+slat1)/2.
      trig=sqrt(1.0-(6.76866e-3*sin(avlat)**2))
      rhom=6.3782e3*(1.0-6.76866e-3)/trig**3
      rhon=6.3782e3/trig
      delx=rhon*cos(avlat)*delo
      dely=rhom*dela
      deltaz=sqrt(delx**2+dely**2)
      if(deltaz) 130,130,30
  130 dis=0.
      azinv=0.
      azm=0.
      go to 52
   30 cora=deltaz*(delo**2)*sin(avlat)**2/24.0

! ljh 8/21/87 corb used only when deltaz greater than 1.0 to prevent 
! unreasonably large correction values
!      if(deltaz.gt.1.) corb=(1.0+sin(avlat)**2*delo**2*dela**2)/
!     *(24.0*deltaz)
! ljh 2/08/08 corb in LINUX has erroneous values when not used and undeclared. 
! Also, correction is small when deltaz is greater than 1.0 and I assume that
! it is calculated incorrectly because of large values when deltaz .lt. 1.0
! so not used
!      dis=deltaz-cora-corb
      dis=deltaz-cora
      delt=dis*57.29578/sqrt(rhom*rhon)
      dumdeo=abs(dely)/deltaz
      theta=57.29578*acos(dumdeo)
      if(dela) 22,23,24
   22 if(delo) 26,26,25
   23 if(delo) 29,130,28
   24 if(delo) 31,27,27
   25 azm=180.+theta
      azinv=theta
      go to 52
   26 azm=180.-theta
      azinv=360.-theta
      go to 52
   28 azm=270.
      azinv=90.
      go to 52
   29 azm=90.
      azinv=270.
      go to 52
   27 azm=360.-theta
      azinv=180.-theta
      go to 52
   31 azm=theta
      azinv=180.+theta
   52 hdis=sqrt(h**2 + dis**2)
      return
      end
