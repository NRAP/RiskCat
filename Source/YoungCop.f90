!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
        subroutine  YoungCop  (MaxAttnCoeff, MaxmAg, xm0, xmc,xdm,    &
                  dmp, dmpp, b, c, d, s, amag, cumrec, xmu,    &
                  nm, i3, i2, xminteg,area)
!
!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
!	Calculate the cumulative occurrence function for the Youngs and
!	Coppersmith model.
!	Inputs:
!	*******
!         s   = annual slip rate in m/yr.
!		xm0 = minimum magnitude value (usually M5)
!		xmc = characteristic magnitude for the source.Mode of the 
!               Truncated Normal distribution
!		xdm = Defines the half -width of the characteristic box (dm=0.5)
!         dmp = magnitude distance between box and point on exponential density
!			  function where the rate is equal to that in the box.
!		dmpp= distance to min of box. Range  where the rate is zero

!		b   = b-value (on Log10) (default 0.9)
!		c and d   = coefficients of Log10(M0)= c*M + d,
!					M0=seismic Moment
!		
!
!	xminteg = lower bound of integration for moment contributions
!	          to the moment balance. Usually xminteg = - infinity
!               or 5 sigmas
!	area    = total size of the fault (in m**2) that experiences the
!	          average slip rate s
!
!	Output:
!	*******
!		cumrec(i) = cumulative occurrence function per year.
!
 
        IMPLICIT NONE

        INTEGER :: i1, i2, i3, i4, iflag, j, MaxAttnCoeff, MaxmAg, nm
        REAL    :: a, alfa, area, b, c, d, dm, dmp, dmpp, rc, s, sumc, sumd
        REAL    :: x, xdm, xm0, xm1, xm2, xm3, xm4, xmc, xMcbar
        REAL    :: xMtbar, xM0bar, xM0dot, xminteg, xmu, xxx
        REAL, dimension(Maxmag)       :: amag
        REAL, dimension(MaxAttnCoeff) :: cumrec



!	Set defaults:
        alfa = 2.302585
	
        if (xdm .eq. 0.) xdm = .25
        if (dmpp.eq.0.)  dmpp = 1.0
        if (b .eq.  0.)  b   = 0.9
        !Magnitude integration step
        dm = 0.001

!	Set MAgnitude ranges
!	[xm0, xm4]	log N straight line slope b
!	]xm4, xm1]  zero density rate
!	]xm1, xm2]  constant density rate rc (characteristic)
        xm1 = xmc - xdm
        xm2 = xmc + xdm
        xm3 = xm1 - dmpp
        xm4 = xm1 - dmp

!	Starting values
        x = xm2
        !Magnitude integration step
        dm = 0.001
        !Starting value. Will be scaled to balance Mo
        rc = 1.
        a = b * xm3
        sumd = 0.


!	The area of rupture is read as input in the definition of the
!	seismicity of the fault
!	Moment rate due to yearly slip rate s
        xM0dot = xmu * area * s

        xMcbar = rc * (10.**d) * (exp(alfa*c*xm2)-exp(alfa*c*xm1))    &
        / (c*alfa)

        xxx = exp(alfa*(c-b)*xm4)
        if (xminteg .gt. -100.)  then
            xxx = xxx - exp(alfa*(c-b)*xminteg)
        end if

        xMtbar = xxx * 10.**(a+d) / (alfa*(c-b))

        xM0bar = xMcbar + xMtbar

!	Average rate of events greater than xminteg
        rc = xM0dot / xM0bar

        a = log10 (rc) + b*xm3


        do j=1,nm
            cumrec(j)=0.
        end do

!	Fill the vector of yearly cumulative occurrences
        x = xm2
        i1 =  999
        i2 = -999
        do  while (x .ge. xm1) 
            call  getmagi  (1, nm, amag, x, j, iflag,Maxmag)
            if (j.gt.i2)  i2 = j
            if (j.lt.i1)  i1 = j
            cumrec(j) = cumrec(j) + rc*dm
            x = x - dm
        end do

        sumc = 0.
        do j = i2,i1,-1
            sumc = sumc + cumrec(j)
            cumrec(j) = sumc
        end do

        x = xm4
        i3 =  999
        i4 = -999
        sumc = cumrec(i1)
        cumrec(i1) = 0.
        do  while (x .ge. xm0) 
            call  getmagi  (1, nm, amag, x, j, iflag,Maxmag)
            if (j.gt.i4)  i4 = j
            if (j.lt.i3)  i3 = j
            cumrec(j) = cumrec(j) + (dm * 10.**(a-b*x))
            x = x - dm
        end do

        do j = i4,i3,-1
            sumc = sumc + cumrec(j)
            cumrec(j) = sumc
        end do


        return

        end
			
