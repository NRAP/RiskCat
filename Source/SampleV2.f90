!
!
!*******************************************************************************
!
        subroutine SampleV2 (ns, nz, n1, n2, VS2)
!
!*******************************************************************************
!
!       Samples n2 integers from a set of n1 integers from 1 to n1.
!       The n2 sampled integers are distributed at random, uniformly on [1-n1]
!INPUT:
!       n1   length of integer suite from which sample is drawn
!       n2   size of sample set
!       V2   integer array of dimension n1, filled with zeros

!OUPUT:
!       ns   number of locations in V2 that have been switched (made > 0)
!       nz   number of remaining zeros in VS2 (Those not switched)
!       VS2  updated array V2 with values >0 where switched
!
!********************************************************************************

        USE GenParam

        implicit none

        integer :: i, k, ns, nz, n1, n2
 
        real    :: rnunf, x1

        integer, dimension(n1) :: VS2
!-------------------------------------------------------------------------------

!Generate the n2 samples. Some might be duplicates
        do i = 1, n2  
            x1 = rnunf()
            x1 = x1 * REAL(n1)
            k  = INT(x1) + 1      
            !Update array of switches
            VS2(k) = VS2(k) + 1
        end do

!Count number of locations in V2 that have been switched (Some more than once)
        ns = 0
        do i = 1, n1
            if (VS2(i) .gt. 0)  then
                ns = ns + 1
            end if
        end do

!Number of remaining zeros in V2
        nz = n1 - ns

        return

        end subroutine SampleV2
