      SUBROUTINE riskcat ( infile )
 
#ifdef USE_DISLIN 
        USE DISLIN
#endif

        USE CritFacilities
        USE ModuleEQ
        USE CritNodes
        USE GenParam

        
        USE modulersqsim
        USE ModuleSYNHAZ


        IMPLICIT NONE

 
        !Declarations for RSQSim externally generated catalog
        CHARACTER (LEN=3) :: componenttype
        CHARACTER (LEN=14):: Txtdum
        
        INTEGER   :: BadMinetFlag
        INTEGER   :: Goodminet, ntotnse
        !        INTEGER*8 :: k4
        INTEGER (kind=8) :: k4
        !INTEGER*4 :: elem4,elem41,nrup4
        INTEGER (kind=4)  :: elem4,elem41,nrup4
        INTEGER, dimension(Maxepis*MaxSampling) :: BadMinet

        
        REAL    :: AVGLTM0,AVGrsqM0,SUMrsqM0
        REAL    :: xcretMag,xcretM0
        !REAL*4  :: mw4
        REAL (kind=4) :: mw4
        !REAL*8  :: dur8,dur81,m08,slip8,slip81,std8,std81,time8,time81
        REAL (kind=8) :: dur8,dur81,m08,slip8,slip81,std8,std81,time8,time81

        !Declaration for  empirical and artificial catalogs
        INTEGER :: Annee1, Annee2, Heure,iflagev, iCaseCat
        INTEGER :: Jour, Minutes, MJS, Mois
        INTEGER :: Naccum, Naccumlines,nAnnees, ncat, Neq
        INTEGER :: nJourHist, nJourStart, nLeap,nodes,nsenonzeroflag
        INTEGER :: SC1, Task
        INTEGER, dimension(12) :: MoisJour
        REAL    :: CalibrateYears, Secondes, SecondesAnnee, SecondesStart
        REAL    :: TimeStart1, TimeStart2, xMagCal, yearscat

        !For output of samplinumed epistemic parameters used in RSQSim 
        INTEGER, PARAMETER :: nRSQreal = 10
        INTEGER, PARAMETER :: nRSQintg = 10

        !General RISKCAT declarations 
        INTEGER :: Calibrate,FLAG_Mag_Moment, iass, iATN_Larry, iATN_Larry1,i
        INTEGER :: icrt,idum
        INTEGER :: iearthquake,iepis,ierr,ierr1,ierr3,ierr4,ierr5,ierr6
        INTEGER :: iflt,ifreq,ihh
        INTEGER :: imm,iNaccum,inev,inevTotal,INgm,inse
        INTEGER :: iperc,irp
        INTEGER :: is,isec,ispl
        INTEGER :: itp,itp1
        INTEGER :: ix,ix1,ix2,ix3,ix4,ixM0,IxSCL,iy,iy1
        INTEGER :: j,j1,j2
        INTEGER :: jrs,jwdt
        INTEGER :: k
        INTEGER :: Large,MaxEqINPUT,MaxMagSampling,method
        INTEGER :: N,nbin,Nc,Nchar,ncrt
        INTEGER :: Nepis,Nepis1,Nepis2,Nepis3,Nepis4,Neq1,Neq2,Neq3,Nextsc
        INTEGER :: Nev,Nev1,Nev2
        INTEGER :: nf,nf1,nfreq,nfreq1,nGM,nGMbins
        INTEGER :: nLHbins,nLHbins1,nLHsamples,nLHsamples1
        INTEGER :: NsimEMP1,NsimEMP2,NMspl,NMspl1
        INTEGER :: NpageCalib,Nperc,Nrp
        INTEGER :: Nscreen,nse,nsegt
        INTEGER :: Nsites,nSources,nStations,nstr,nsub,Nview,NWIDTH1
        INTEGER :: Nyears,Nyears2
        INTEGER :: OUTgm,pagenumber1,pagenumber2,PlotHazEpis
        INTEGER :: PlotRecSimul,PlotCalibration,Printdetails, printunit
        INTEGER :: synflag,synflagRS 
        INTEGER :: TrackNumber,TrackNumber1
        INTEGER :: unitmenu,unitcat,unitcat1,unitcat2,unit1,unit2
               
        REAL :: aGM, aGM1, asb, BetaGM, BetaGM1
        REAL :: CatAzim1, CatAzim2, Catdepth1, Catdepth2, CatDip1, CatDip2
        REAL :: CatRake1, CatRake2, CatStr1, CatStr2, CatVs1, CatVs2
        REAL :: cigar, CPUtime
        REAL :: damp, delevents, delf, delGM, Dip_usual, Dm, dt, dt1, dt2
        REAL :: dxlong, dylat
        REAL :: elt,epsilon2,etime,EX1,EX2,fminlog,freqlog,freqmax,freqmin
        REAL :: GMoutMax,GMoutMin
        REAL :: radeg,RADtheta,rake_usual,r,r1,r2,rdegree,rearth
        REAL :: rigidity,rigidity2,rLW,rnunf,rp1
        REAL :: rq1,rq2
        REAL :: scaleM0forced,sectoyear
        REAL :: sigMmax,slip1,strike_usual  
        REAL :: sum
        REAL :: T1,T2,TimeKeep1,twinmax,Timesc,timexy,time1,tp1,tp2
        REAL :: TRelax,TRelax1
        REAL :: wepi,wepigm
        REAL :: xarea,xN1,xN2,x1,x2,Xcat
        REAL :: xdum,xEpis,xhMin,xhMax,xLmax
        REAL :: xM0,xMagMax2,xMagMin2,xMag3
        REAL :: xMSc,xmuRSQSim
        REAL :: xxx,xxxM0,xYears
        REAL :: yarea,y1,Ycat,Years,YearsHist
        REAL :: zarea,Zcat,zzk,z1
      
        REAL, dimension(5)                :: pex, RP
        REAL, dimension(2)                :: tarray
        REAL, dimension(MaxGMbins)        :: X
        REAL, dimension(maxGMbins,NcMax)  :: Y

        REAL, dimension(MaxGMbins,MaxNtp,Maxcritpt)    :: HazWin
  
        CHARACTER (LEN=18) :: CalibrationCat
        CHARACTER (LEN=16) :: charac
        CHARACTER (LEN=13) :: charac1  
        CHARACTER (LEN=41) :: CSTR1
        CHARACTER (LEN=20) :: CSTR2
        CHARACTER (len=20) :: Dummy
        CHARACTER (LEN=8)  :: Dummy1
        CHARACTER (LEN=14) :: Dummy2
        CHARACTER (LEN=80) :: fmt1
        CHARACTER (LEN=4)  :: label_seis 
        CHARACTER (LEN=13) :: LegendTitle          
        CHARACTER (LEN=5)  :: NameEXT
        CHARACTER (LEN=80) :: Nameplotfile
        CHARACTER (LEN=14) :: Plotscreen1
        CHARACTER (LEN=18) :: string1, StoreHaz

        CHARACTER (LEN=10) :: idatex,idatex1,itimex,itimex1
        CHARACTER (LEN=1),  dimension(256):: fln   
        CHARACTER (LEN=80), dimension(4)  :: label_RUN_Case    
        CHARACTER (LEN=16), dimension(9)  :: NameLegends

!Declarations for Earthquakes:
        INTEGER, PARAMETER :: Maxmagbin=50

!Declarations for InRoads & faultband:
        INTEGER, PARAMETER :: Mxstrp=5
        INTEGER, PARAMETER :: maxnodl=Maxseg+1
        integer, parameter :: maxnodw=mxstrp+1
        
        INTEGER :: NcritRD

!Declarations for input output file names
        character (len=3)  :: ExtNUZ,Ftype,FtypeNUZ
        character (len=80) :: file1
        character (len=20) :: filemenu, infile
        character (len=22) :: Outfile
        character (len=256):: GFpath, GFsources, GFstations
        character (len=9)  :: ProblemID   
        character (len=80), dimension(10) :: FichiersIN,FichiersOUT
    
 !Declarations for Plots
        CHARACTER (LEN=13) :: CTIT  
        CHARACTER (LEN=80) :: Mainline2, MAINTITLE
        CHARACTER (LEN=32) :: TitleX
        CHARACTER (LEN=25) :: TitleY
        CHARACTER (LEN=80), dimension(MaxNtp)           :: Mainline3

!ALLOCATABLES:
!*************
        !Plots
        character (len=16), allocatable, dimension(:)   :: NuisLegend
        INTEGER,            allocatable, dimension(:)   :: TrackID
        REAL,               allocatable, dimension(:)   :: Xplot
        REAL,               allocatable, dimension(:,:) :: Yplot

        !Hazard and Nuisance 
        REAL, allocatable, dimension(:)       :: Dmg, freq, HazNuis
        REAL, allocatable, dimension(:)       :: UHS, XB
        REAL, allocatable, dimension(:,:)     :: haz2,HazNuistp
        REAL, allocatable, dimension(:,:,:)   :: UHSRP
        REAL, allocatable, dimension(:,:,:,:) :: Hazwin3, perctlSPEC
        REAL, allocatable, dimension(:)       :: srt1, srt2


!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
!
!                              START PROGRAM
!                              -------------


!Set starting Date and Time
        CALL Dattim (idatex1,itimex1)

       
!ASSIGN INDEX "TrackNumber" FOR TO IDENTIFY OUTPUT FILES OF CURRENT RUN
!**********************************************************************
!       Needs a dummy file named 'TrackRun'that remains in the directory 
!       and which contains a unique number for the run. The index number is
!       updated before each run.
        open (unit=1,file='TrackRun.txt',status="old",iostat=IERR)
        if (IERR .eq. 0)  then
            rewind (1)
            read (1,*) TrackNumber1
            TrackNumber = TrackNumber1 + 1
            if (TrackNumber .gt. 999)  then
                TrackNumber = 1
            end if
            rewind (1)
            write (1,*) TrackNumber
        else
            !TrackCat.txt does not exist in the directory. 
            !Will create the file and write a run index of 1.
            !Create "TrackCat.txt" and write an index of 1 in it.
            TrackNumber = 1
            write (*,*) "File -TrackCat.txt- does not exist in this directory"
            write (*,*) "Will create it with a Run Index of 1"
            open (unit=1,file="TrackCat.txt",status="new",iostat=IERR)
            write (1,*) TrackNumber
        end if
        close (1)

       !Compilation date and code identification used on plots:
       !------------------------------------------------------- 
        write(CSTR1,"(a38,i3)")   &
        'J.Savy-RISKCAT Compiled:08/20/17--RUN: ',TrackNumber
        CSTR2 = ' '  
        print *,'  '
        print *,'**************************************************************'
        print *,'  '
        print *,'          ---- ',CSTR1,' ----'
        print *,'  '
        print *,'**************************************************************'
        print *,'  '      
        WRITE(6,'(4X,A)') ' Welcome to RISKCAT version 3.0'
        WRITE(6,'(4X,A)') '      Cummulates results'
        write(6,'(4x,A)') '    '


!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX        
!SET CONSTANTS AND INITIALIZE DEFAULT PARAMETERS:
!***********************************************
!   Contants:
!   ---------
    rearth   = 6371200.             !Earth radius in meters
    rdegree  = rearth * 6.28318531 / 360. ! Length of 1 degree at equator
    radeg    = 57.2957795           ! Conversion degree-radian
    pp_theta = -45.                 ! Rotation to accomodate pp axis system 
    RADtheta = pp_theta / radeg     ! Convert pp_theta to radians
    sectoyear= 3600. * 24. * 365.   ! Number of seconds in a year
!
!   Defaults:
!   ---------
    Dm             =0.25 !Size of the earthquake magnitude bins
    dt             = 1.  !time unit in years
    iearthquake    = 0   !total number of earthquakes simulated
    iStd_dieterich = 1   !Selection of seismicity catalog generation:
                         !=1 (Default) Standard Map and G-R relationship based 
                         !=2 for using Dieterich Eq generation model
                         !=3 for using an existing catalog in file "CatalogTest"
    iATN_Larry     = 1   !Selection for calculating ground-motion
                         !=1 from Standard attenuations (Default)
                         !=2 with EGF,  Larry's code
    Large    = 1000000   !Large number of trials for sampling magnitudes
    Nepis          = 10  !Number of epistemic sets
    nLHbins1       = 10  !Default number of Latin-Hypercube discretization bins
    nLHsamples1    = 5   !Default number of samples in each LH bin
    NMspl          = 10  !Number of sample catalogs per LTS mag bins
    Nscreen        = 1
    NsimEMP2       = 50  !Number of aleatory simulations in SYNHAZ
    pagenumber1    = 1
    Printdetails   = 0   !flag for printing details of simulation progress
    TRelax         = 1000. !Time period of perturbation relaxation (years)
    xMLTLow        = 5.5 !Default starting magnitude for seismicity calibration
    xMLTHigh       = 8.0 !Stopping magnitude for seismicity calibration
    xmuRSQSim      = 9.29e9 !   Earth shear modulus mu  (N/m2)

!   Default values for calculating Nuisance:
!   ----------------------------------------
    aGM            = 14.0   !cm/s/s
    BetaGM         = 0.35   !Slope of fragility curve
         
!   Initialize random seed:
!   -----------------------
    call random_seed
    idseed  = 59237547


!xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
!Read name of input Menu file and set other Input and Output file names: 
!***********************************************************************

!       Default Input menu file:   MenuIn.csv
        !------------------------------------
        print *, '   The name of input Menu file is:   ', infile
        print *, '   *******************************'
        print *, '  '
        !Nyears  = number of time cycles of simulation of Global perils
        !        = number of years only if dt=1
        !dt      = time duration of a time cycle for simulation 
        !Nsub    = number of sub-perils (eg. faults)
        !lamda() = average Poisson return period (in years) for each Global peril
        !lamdastor=initial values of the lamdas, stored. They are
        !          updated according to the correlation between Global events
        !PlotHazEpis = 1 to plot all Epistemic simulations  Hazard curves
        !              0 No plots. Only the final Hazard curve statistics of
        !                all epistemic simulations are plotted.
        !TRelax  = Time (in years) to achieve perturbation dissipation
        

        !INPUT and OUTPUT FILES:
        !------------------------
        !file names are read from Menu file:"MenuIn.csv", or set by default
        !if names read are blank.
        !Default file name for PSHA seismic sources, 
        !        and for empirical catalog input: 
        !        1. PSHA seismic zones characteristics  default: afile
        !        2. Catalog file of events for empirical case  : catalogTest


        !NOTE on Menu file structure:
        !****************************
        !Menu File: "infile" is composed of blocks of data with a begining
        !announced by an identifying string, and finishing with "End-...".  
        !The blocs can be placed in any order within the Menu file, but
        !their identifier must be exactly as below:

        !"Displays-Selection"
        !"Case-Title"
        !"Task-of-this-Run"
        !"Input-Output-files", 
        !"GM-Model-Selection"
        !"Catalog-Generation"
        !"Catalog-Calibrate"
        !"Points-Catalogs"
        !"Window-Percentiles"
        !"Facilities-at-Risk"
        !"GM-Array"   
        !"SYNHAZ-input"
        !"RSQSim-options"
        !"Nuisance-Functions"



        filemenu = infile
        FichiersIN(1) = infile

        unitmenu = 1

!       ************************************************************************
        !Open menu file whose name is read from the execute command line and
        !loaded into character variable "filemenu"
        ! ex:  ../riskcat KingsIl.menu
        !************************************************************************
        open (unit=unitmenu,file=filemenu,status="old",iostat=IERR)
        call CantFindFile (filemenu,IERR)

        ! 0. Read Plot and debugging control variables: (1=Yes/0=N0)
        !***********************************************************
        !PlotScreen     = "Display-Screen" or "Do-Not-Display"
        !                 "Display-Screen" plot selected plots on screen 
        !                 "Do-Not-Display" do not display any plots on screen.
        !                 Blank = default = "Display-Screen"
        !               Note: If screen display is selected, then the code will
        !               pause at each plot, waiting for order to continue from
        !               entry of "right-click" from mouse.
        !               Recommended to use 0 ,(N0) if the run is unattended.
        !PlotRecSimul   :simulated and calculated regional reccurence curves
        !PlotHazEpis    :Mean Hazard curve for each Epistemic cycle
        !PlotCalibration:Simulated and calculated regional reccurence curves
        !Printdetails   :flag for debugging. 1 for max info , 0 otherwise
        string1 = "Displays-Selection"
        Plotscreen = "Display-Screen"
        call reposition (filemenu, string1, unitmenu )
        read (unitmenu,*)  PlotScreen1
        if ((plotscreen1.eq."Do-Not-Display") .or. (plotscreen1.eq." ")) then
           Plotscreen = "Do-Not-Display"
        end if
        read (unitmenu,*)  PlotRecSimul, PlotCalibration, PlotHazEpis,Printdetails

        ! 1. Read RUN Case Title:
        !    --------------------
        string1 = "Case-Title"
        call reposition (filemenu, string1, unitmenu )
        !Read 4 lines of title text
        do i = 1,4
            read (unitmenu,"(a80)") label_RUN_Case(i)
        end do


        ! 2. Define the task to be performed in this run
        !***********************************************
        !Task = 0 A:
        !         Run RiskCat with all assigned parameters, catalogs etc.,
        !         Normal, standard run, get final results (and plots if desired)
        !         Do not accumulate results in file "StoreHaz"
        !       1 B:
        !         Standard run as in A, accumulate all catalog results in file
        !         "StoreHaz",(or name as specified below in "input-output files")
        !         Hazard results from each catalog used in this run will be
        !         accumulated in at the end of file StoreHaz.
        !         PSHA percentiles are calculated only for this run and NOT for
        !         set of thus far  accumulted in StoreHaz
        !       2 C:
        !         Same as Task 1, but Hazard percentiles are calculated for all
        !         epistemic realization already stored in "StoreHaz"
        !       3 D:
        !         Process all hazard curve results stored in file "StoreHaz"
        !         (as specified in input-output files) to develop the hazard
        !         percentiles for all the runs accumulated in file "StoreHaz"
        !         Does not make any new hazard calculations.
        !         Just process existing results previously stored in "StoreHaz"
        !
        !  Notes: (1) Options 0 and 1 need same type of menu file, with all
        !         parameters.
        !         (2) Option 2 needs same structure of menu file as above, but
        !         only a few parameters are actually used.
        !         ***Parameters and files that are used:
        !            -menu file
        !            -file "StoreHaz"(Default name), or same with actual name
        !            -flags for plotting
        !            -plotting parameters (if plotting is requested)
        !            -list of output file names
        !         ***Files not used, that do not need to be in directory
        !            -FKRs
        !            -Sources and  Stations files
        !
        !
        !***********************************************************
        !   *         *    PSHA     *   PSHA      *     PSHA       *
        !   *  Task   *  calculated *   stored    *  Calculated    *
        !   * request *  from input *    into     * from data in   *
        !   *         *  Catalog(s) *  'StoreHaz' * file 'StoreHaz *
        !***********************************************************
        ! A *    0    *     YES     *     NO      *       NO       *
        !***********************************************************
        ! B *    1    *     YES     *     YES     *       NO       *
        !***********************************************************
        ! C *    2    *     YES     *     YES     *       YES      *
        !***********************************************************
        ! D *    3    *     NO      *     NO      *       YES      *
        !***********************************************************
        !
        !
        string1 = "Task-of-this-Run"
        call reposition ( filemenu, string1, unitmenu )
        read (unitmenu,*) Task
        ! If task .gt. 0 must give name of file for accumulation of results
        !  Read name of file. Default name is "StoreHaz"
        if (Task .gt. 0)  then
           StoreHaz = "StoreHaz"
           read (unitmenu,*) string1
           if (string1 .ne. "   ")  then
              StoreHaz = string1
           end if
           open (unit=21,file=StoreHaz,status="unknown",iostat=IERR)
           call CantFindFile (filemenu,IERR)
           rewind (21)
           !Naccumlines is the number of lines in Hazard results file StoreHaz
           do i = 1, 1000000
              read (21,*,iostat=IERR) string1
              if (IERR .ne. 0)  then
                 Naccumlines = i-1
                 exit
              end if
           end do
           
           rewind (21)
           !Find number of epistemic realizations already stored in StoreHaz
           Nepis3 = 0
           do iNaccum = 1, 100000
              read (21,602,iostat=IERR)    &
              idum,Nepis2,Ntp,NcritRD,nfreq,nGMbins,txtdum
              if (IERR .ne. 0)  then               
                 exit
              else
                 Nepis3 = Nepis3 + 1
                 read (21,*) (tlife(i),i=1,Ntp)
                 read (21,572) (GMarray(i),i=1,nGMbins)
                 read (21,582) (Nonzerotp1(itp),itp=1,Ntp)
                 do itp = 1,Ntp
                    !Skip this itp cycle if Zero events in the time window.
                    if (Nonzerotp1(itp) .gt. 0)  then                
                       !Loop over number of sites
                       do icrt = 1, NcritRD
                          do ifreq = 1, nfreq
                             j1 = nGMbins/10
                             j2 = j1
                             j1 = nGMbins - (j1*10)
                             if (j1 .gt.0)  then
                                j2 = j2 + 1
                             end if
                             do j1 = 1, j2
                                read (21,*) xdum
                             end do
                          end do
                       end do  
                    end if
                 end do
              end if
           end do
           Naccum = Nepis3
           !Nepis3 = Number of epis simulations in StoreHaz
           close (21)
        end if

        ! 3. Names of Input-Output files:
        !********************************
        string1 = "Input-Output-files"
        call reposition ( filemenu, string1, unitmenu )

        ! 3.1. File name for seismic sources/faults geometry and general ppties
        !    ------------------------------------------------------------------
        read (1,*) afile 
        if (afile .eq. '   ')  then
            afile = 'sources.txt'           
        end if
        FichiersIN(2) = afile

        ! 3.2. File names for seismicity catalogs
        !    ----------------------------------
        !Max number of catalogs is MaxCatStandard, set in module ModuleEq
        !Max length of names is 20 characters
        !Indexing:
        !For Nepis and Nspl (sampling), that is Nspl for each epistemic cycle
        !file index in a given epistemic simulation (iepis) and aleatory (ispl):
        !    file index = ispl + (iepi-1)*Nspl
        read (1,*) ncat
        do i = 1, ncat
            read (1,*) catalogf(i)
            if (catalogf(i) .eq. "NONE")  then
                catalogf(i) = "  "
            end if
            Dummy = Catalogf(i)
        end do
        FichiersIN(7) = catalogf(1)

        ! 3.3. GM attenuation models and sources-GM assignments, default: GMmodels
        !    -------------------------------------------------------------------
        read (1,*)  GMfile
        if (GMfile .eq. '   ')  then
            GMfile = 'GMmodels'           
        end if 
        FichiersIN(3) = GMfile
       
        ! 3.4. Output text file name creation:
        !    -------------------------------
        read (1,*) ProblemID
        !Create Output file name with Track number:
        if (TrackNumber.lt.10)  then
            write (Outfile,"(i1,a1,a9,a1,a6)")     &
            TrackNumber,'.',ProblemID,'.','hazard' 
        elseif (TrackNumber.lt.100)  then
            write (Outfile,"(i2,a1,a9,a1,a6)")     &
            TrackNumber,'.',ProblemID,'.','hazard' 
        elseif (TrackNumber.lt.1000)  then
            write (Outfile,"(i3,a1,a9,a1,a6)")     &
            TrackNumber,'.',ProblemID,'.','hazard' 
        else
            write (Outfile,"(i4,a1,a9,a1,a6)")     &
            TrackNumber,'.',ProblemID,'.','hazard'
        end if  
        FichiersOUT(1) = Outfile
        
        !End reading/creating Input-Output file names.


        !Write title page and code version and misc.:
        !--------------------------------------------
        open (unit=2,file=outfile,status="unknown",iostat=IERR)
        write (2,262)  CSTR1, CSTR2
        !Write date and time started
        write (2,*)  '  '
        write (2,*)  '  '
        write (2,532) idatex1,itimex1
        write (2,*)  '  '
        write (2,*)  '  '
        do i = 1, 4
            write (2,*) label_RUN_Case(i)
        end do
        !Tracking run number, input and output print files
        write (2,482)  infile
        write (2,272)  outfile
        print *,'   Previous TrackNumber            =' ,TrackNumber1
        print *,'   TrackNumber for this run        =' ,TrackNumber
        print *,'   Name of output hazard file      = ', outfile  
        print *,'  '
        do i = 1, 4
            print *, '   ',label_RUN_Case(i)
         end do
         

      
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 
!READ PROBLEM MENU:
!******************  
              

!If Task = 3, no need to do any hazard calculations. Only perform statistics      
!on existing epistemic hazard results, stored into "StoreHaz"
         
    if (Task .ne.3)  then
 
        ! 4. Chose ground motion model and seismicity catalog simulation method
        !**********************************************************************
        string1 = "GM-Model-Selection"
        !iATN_Larry = 1, will use attenuation relationships
        !             2, will use Larry's GF simulations

        !synflag    = 1, hazard results in peak acceleration PGA (cm/s/s)
        !             2, .....................  velocity     PGV (cm/s)
        !             3, ...................... Displacement PGD (cm)

        !synflagRS  = 0, No Uniform Hazard Response Spectra (UHRS)
        !             1, ...................... Acc. UHRS (cm/s/s)
        !             2, ...................... Vel. UHRS (cm/s)

        !Flags for Response Spectra Calculation:
        !GF are always assumed to be in Velocity.
        !INgm         Flag indicating synhaz output ground-motion unit:
        !           = 1, Input time-series are in velocity   (cm/s)
        !             2, Input time-series in acceleration (cm/s/s)
        !OUTgm        Flag for ground-motion Response Spectra units
        !           = 1, Output spectra in velocity       (cm/s)
        !             2, Output spectra in acceleration (cm/s/s)
        call reposition (filemenu, string1, unitmenu )
                read (unitmenu,*) synflag
                read (unitmenu,*) synflagRS
                read (unitmenu,*) iATN_Larry1
                INgm  = 1
                if (synflag .eq. 1) then
                    INgm = 2
                end if
                OUTgm = 1
                if (synflagRS .eq. 1)  then
                    OUTgm = 2
                end if


        ! 5. Type of catalog generation:
        !*******************************
        string1 = "Catalog-Generation"
        !iStd_Dieterich1 = 1 Catalogs are generated from source-zones description  
        !                    in the zonation maps with seismicity rates
        !                  2 Catalogs from RSQSim runs. This requires inputs
        !                    of catalog files containing detailed description of
        !                    the events, including:
        !                    date/time,(sec)
        !                    seismic moment M0 (m.N,Joules), 
        !                    location of hypocenter,
        !                    geometry of the elemental faults,  
        !                    stress-drop, strike, dip, etc. 
        !                  3 Standard seismicity catalogs, from input files 
        !                    that contain minimal information on the Earthquakes
        !                    such as magnitudes, dates, and location.
        !                    For this case, needed source properties are  taken
        !                    from the source zones file.
        !                    Three current options of standard catalogs:
        !                    Option 1: SC1 = 1, Corrine's Catalog
!                                      UTM coordinates
!                                      Times/dates in years
!                            Option 2: SC1 = 2, Same as 1, but
!                                      Times/Dates = Year/Month/Day/Hour/Min./Sec.
!                            Option 3  SC1 = 3, Good Old Standard Catalog
!                                      Neq2 = Number of events,
!                                      YearsHist = length recording (years)
!                                      dummy, Xcat, Ycat, 	Zcat, Time1
!                                      "  ",  decimal degrees,km, Years
        !
        !When generating catalogs, some simulation parameters have epistemic
        !uncertainty and others are aleatory variables.Therefore there can be
        !multiple aleatory catalogs for each epistemic simulation.
        !
        !Nepis1        = Number of epistemic catalog sets
        !NMspl1        = Number of aleatory catalogs for each epistemic case
        !NLHbins1      = Number of bins in the discretization for the
        !                Latin-Hypercube (LH) randomization
        !nLHsamples1   = Number of samples in each bin of the LH randomization
        !NsimEMP1      = Number of aleatory simulations on output of synhaz,
        !                assuming normal distribution
        !scaleM0forced = Total moment budget in a catalog can be scaled to equal
        !                a specific value. For example it can be scaled to equal
        !                the budget moment of a reference catalog
        !                If scaleM0forced .gt. 0.001:
        !                then Moment scaling = scalM0forced
        !                (if =1, then total moment budget is unscaled)
        !                otherwise it takes the scaling calculated to equal that
        !                of the reference catalog
        !dt1           = Duration of time step in years
        !                Time step/cycle for generation of catalogs through
        !                simulation of seismicity modeled by sesimic sourc-zones
        !                as for traditional PSHA Cornell model
        !                ex: dt1 = 1.0 year
        !Nyears        = Duration/length of catalog(s)
        !TRelax1       = Relaxation period after injection has stopped (years)
        !MaxEqINPUT    = Maximum number of events to process from a catalog
        !                including for catalogs produced by RSQSim.
        !                The maximum number for MaxEqINPUT is MaxEQ (the absolute
        !                maximum dimension set in ModuleEq).
        !MaxMagSampling= Maximum number of events sampled per magnitude bin in
        !                histogram of magnitudes in catalogs. This to avoid
        !                large number of hazard calculations for small magnitudes
        !                a number of 10 gives acceptable statistical stability.
        call reposition (filemenu, string1, unitmenu )
                read (unitmenu,*) iStd_Dieterich1, SC1
                read (unitmenu,*) Dummy
                read (unitmenu,*) xM0
                read (unitmenu,*) Dummy
                read (unitmenu,*) Nepis1,NMspl1,    &
                     nLHbins1,nLHsamples1,NsimEMP1,scaleM0forced
                read (unitmenu,*) Dummy
                read (unitmenu,*) dt1,Nyears,TRelax1,MaxEqINPUT,MaxMagSampling
                if ( (MaxEqINPUT.eq. 0) .or. (MaxEqINPUT.gt.MaxEq) )  then
                    MaxEqINPUT = MaxEq
                    call reposition (filemenu, string1, unitmenu )
                end if


        ! 6. Choose a mode of calibration of catalogs:
        !*********************************************
        string1 = "Catalog-Calibrate"
        !Catalogs used to calculate hazard can be calibrated in one of two ways:
        !
        !  Option 1: Calibrate = 1
        !            Use a calibration/Reference catalog such as an empirical 
        !            catalog of actual observations, or any other "empirical" cat.
        !            This option only requires the calibrating catalog to be
        !            formatted as: event date, X,Y,Z, Magnitude
        !            
        !  Option 2: Calibrate = 2
        !            Use definition of sources as in standard PSHA method to
        !            simulate a reference catalog.
        !            This option requires input of geometry and seismicity
        !            parameters of the seismic sources.
        
        !  xMagCal : Maximum magnitude to be considered for catalog calibration,
        !but also for definition of magnitude bins by arange of magnitudes in
        !when doing the magnitude sampling in routine MagSampling1
        call reposition (filemenu, string1, unitmenu )
           read (unitmenu,*)  Calibrate
           read (unitmenu,*)  CalibrationCat
           read (unitmenu,*)  CalibrateYears
           read (unitmenu,*)  xMagCal


         
        ! 7. Additional properties for point-Sources Catalogs:
        !*****************************************************
        !This is case iStd_Dieterich = 3
        !Catalogs can be either standard empirical or simulated) 
        !(i.e. Corrinne's catalogs
        if (iStd_Dieterich1 .eq. 3)  then
            string1 = 'Points-Catalogs'
            call reposition (filemenu, string1, unitmenu )
                !Dip is read in orthoganal axis system (NOT Aki & Richards)
                !Assumes fault striking North. Dip is 0 for Vertical, positive
                !counter clockwise.
                !Azimuth is from North striking. Zero for North, positive
                !clockwise.
                !cigar = uncertainty (N(0,cigar) on log of area vs Mag 
                read (unitmenu,*) CatDepth1, CatDepth2   !(km)
                read (unitmenu,*) CatAzim1,  CatAzim2    !(dec. deg.)
                read (unitmenu,*) CatDip1,   CatDip2     !(dec. deg.)
                read (unitmenu,*) CatRake1,  CatRake2    !(dec. deg.)
                read (unitmenu,*) CatVs1,    CatVs2      !(km/sec)
                read (unitmenu,*) CatStr1,   CatStr2     !(bars)
                read (unitmenu,*) cigar                  !typically 0.2-0.5
        end if
     end if
     

        ! 8. Define Time windows and Hazard percentiles of interest:
        !***********************************************************
        string1 = "Window-Percentiles"
        call reposition (filemenu, string1, unitmenu )
                read (unitmenu,*) Dummy
                Read (unitmenu,*) Ntp
                !tlife(i) is the years ending the time period
                !so that the duration of time period i is: tlife(i+1)-tlife(i)
                read (unitmenu,*) (tlife(itp),itp=1,Ntp)
                read (unitmenu,*) Dummy
                read (unitmenu,*) Nperc
                !Percentiles are read in 100-percent units.
                read (unitmenu,*) (aperc(itp),itp=1,Nperc)
                !Transform percentiles to [0.,1.0] probability values
                do i = 1, Nperc
                    aperc(i) = aperc(i) / 100.
                end do
                read (unitmenu,*) Dummy
                !Read Return periods (Max Nrp = 5 Return Periods)
                read (unitmenu,*) Nrp
                read (unitmenu,*) (RP(itp),itp=1,Nrp)                
                do i = 1, Nrp
                    pex(i) = 1. / RP(i)
                end do

        if (Task .ne. 3)  then     
           !Setup calculation default parameters:
           !-------------------------------------
           if (dt1.gt. 0.001)        then; dt=dt1  ;                       endif
           if (iATN_Larry1.gt. 0)    then; iATN_Larry=iATN_Larry1;         endif
           if (iStd_Dieterich1.gt. 0)then; iStd_Dieterich=iStd_Dieterich1; endif
           if (Nepis1.gt. 0)         then; Nepis=Nepis1;                   endif
           if (nLHbins1.gt.0)        then; nLHbins=nLHbins1;               endif
           if (nLHsamples1.gt.0)     then; nLHsamples=nLHsamples1;         endif        
           if (NMspl1.gt. 0)         then; NMspl=NMspl1;                   endif
           if (TRelax1.gt. 0.)       then; TRelax=TRelax1;                 endif
           if (MaxMagSampling.eq.0)  then; MaxMagSampling=1000;            endif
           Nepis4 = Nepis
        
           !Number of aleatory simulation for ground motion estimates:
           !----------------------------------------------------------
           if (iATN_Larry .eq. 2)  then
               if (NsimEMP1.eq.0)  then
                   NsimEMP = NsimEMP2
               else
                   NsimEMP = NsimEMP1
               endif
           end if
        end if  

!*******************************************************************************
!LOCATION OF FACILITIES AT RISK, AND VULNERABILITY PARAMETERS:
!************************************************************

        ! 9. Read coordinates and vulnerability description of sites at risk:
        !********************************************************************     
        NodesInType    = 0     
        CritNumNodes   = 0
        NodesInType(4) = 1

        ! Single location sites and/or critical facilities (incl. NPPs)
        !reads from unitmenu=1 and closes it (i.e. close(1) )
        infile = filemenu
        call INcritical (infile,Nsites,unitmenu)
        CritNumNodes(4) = Nsites
                                   
        ! Load all hazard site locations into array CritXY
        ncrt = Nsites
        do k = 1, ncrt
                 !Critical Facilities Grid points
                 CritXY(k,1) = Xsites(k)
                 CritXY(k,2) = Ysites(k)
        end do

        !Total number of hazard Sites:
        !-----------------------------
        NcritRD = Nsites

           
       
!*****************************************************************************        
! Read general info on regional seismicity sources zones/faults/volumes:
! ****************************************************************************

        !Regional Seismicity - source zones and faults:
        !**********************************************
        !Earthquake sources info from unit 3 (sources file)
        unit1   = 3
        call EQsourcesinfo (CSTR1,CSTR2,dt1,Nscreen,PlotRecSimul,unit1)


!********************************************************************************
! 10. General info on ground motion modeling, and GM array info (from menu file):
!********************************************************************************
        ! Define magnitude array:
        !****************************
        !Open and re-position pointer after the flag "GM-Array"
        string1 = 'GM-Array'
        open (unit=unitmenu,file=filemenu,status="old",iostat=IERR)
        call CantFindFile (filemenu,IERR)
        call reposition ( filemenu, string1, unitmenu )

        !If hazard parameter is other than PGA, PGV or PGD,
        !Read frequency array and response spectra damping
        nfreq = 1
           read (unitmenu,*) nfreq1, freqmin,freqmax,damp
           !Read the array of nfreq frequencies (Hz):!               
           !Set the working values for frequency and in-out gm types
           !nfreq             = number of frequencies (recommended 31)
           !Minimum frequency = freqmin (recommended 0.1 Hz)
           !Maximum           = freqmax (recommended 100. Hz)
           !damp              = Damping (recommended 5%, i.e. damp = 0.05)
        if (synflagRS .ne. 0)  then
           nfreq = nfreq1
        end if
        ALLOCATE (freq(nfreq) ) 
        if (nfreq .gt. 1)  then
            freq(1) = freqmin
            fminlog = alog10 (freqmin) 
            delf = (alog10 (freqmax) - fminlog) / (nfreq-1)  
            do ifreq = 2, nfreq
                freqlog     = fminlog  + (delf*REAL(ifreq-1))
                freq(ifreq) = 10. ** freqlog
            end do
        end if

        ! GM Array description
        read (unitmenu,*) nGMbins, GMstart, GMend, method
            !nGMbins = number of GM bins
            !Gmstart = smallest GM value
            !GMend   = largest GM value
            !method  = Key for generation of array. Three possible methods:
            !method  = 1, values are spead uniformly in [GMstart,GMend]
            !        = 2, log(GM) uniform on [log(GMstart),log(GMend)]  
            !        = 3, read-in the nGMbins values

        !Create array of Magnitudes
        SELECT CASE (method)
            !Uniform 
            CASE (1)
                delGM = GMend / Float(nGMbins)
                do i = 1, nGMbins
                    GMarray(i) = delGM*Float(i)
                end do
            !Log
            CASE (2)
                delGM = alog(GMend/GMstart) / Float(nGMbins-1)
                x1 = alog (GMstart)
                do i = 1, nGMbins
                    GMarray(i) = exp( x1 + (delGM*Float(i-1)) )
                end do
            !Read-in values from file
            CASE (3)
                read (unitmenu,'(10F10.4)') (GMarray(i), i=1,nGMbins)
            CASE DEFAULT
                WRITE(6,*) ' >>>> Bad GM Array. Check Input file',GMfile
        END SELECT
        !Create working array of log ground motion values
        do i = 1, nGMbins
           GMarrayLog(i) = alog (GMarray(i))
        end do


        if (Task .ne. 3)  then
 
        !Read Ground-motion prediction models
        !************************************
        !GMPEs info, from GMmodels file:
        !Synhaz parameters, from menu file
        if (iATN_Larry .eq. 1)  then
            !Info on attenuation relationships
            unit2   = 4
            !GMfile  = 'GMmodels'         
            !read attenuation coefficients from GM file 
            !(default name "GMmodels")
            !read assignments of GM models to faults from unit2 source file
            !(default name source.txt)
            call RdinAttn (nGMmod,unit1,unit2)
         else
           !******************************************************
           !11. Info for SYNHAZ and SYNHAZPoint: (from menu file):
           !******************************************************
            string1 = 'SYNHAZ-input'
            call reposition (filemenu, string1, unitmenu)               
            read (unitmenu,*) synregion
            read (unitmenu,*) label_seis           
            read (unitmenu,*) SynVr
            read (unitmenu,*) SynVh
            read (unitmenu,*) nSources
            read (unitmenu,*) nStations
            read (unitmenu,*) componenttype
            read (unitmenu,"(a80)") GFpath 
            read (unitmenu,"(a80)") GFsources
            read (unitmenu,"(a80)") GFstations

            file1 = GFsources

            read (GFpath, '(256a1)')  (fln(i),i=1,256)
            !Test to make sure that file name + path is less than 80 characters
            do i = 1, 256
                if (fln(i) .eq. ' ' )  then
                    exit
                end if
                Nchar = i 
            end do
            if (Nchar .gt. 80)  then
                print *, '  GF path+filename is greater than 80 charaters'
                print *, '  STOP'
                STOP
            end if
            write (FichiersIN(5), '(80a1)' )  (fln(i),i=1,Nchar)
            FichiersIN(6) = file1

            !Read max area of elements for SYNHAZ full calulation
            read (unitmenu,*) SynhazAreaMin
            !Read case selection for SYNHAZ element Area size : elmms
                 !elmms = 'yes' SYNHAZ elt size varies, minimum=SynHazAreaMin
                 !elmms = 'no'  SYNHAZ elt size is provided by RSQSim calcs
                 !elmms = 'fix' SYNHAZ elt size is fixed equal to SynHazAreaMin
            read (unitmenu,*) elmms
            read (unitmenu,*) rLW, xLmax
            read (unitmenu,*)  Magmaxpoint,dummy
            pointsource = 'NO '
            if (dummy(1:12) .eq. 'point_source')  then
                pointsource = 'yes'
            end if

            !Read rigidity in Pa     (1.0Pa = 10.0 dynes/cm^2)
            !Conversion to dynes/cm2 for use in synhaz
            read (unitmenu,*) rigidity
            
            rigidity = rigidity * 10.
            !nGFsources is the number of sources per observation point
            !nGFsimul is number of simulations for each source

            !Number standard dev for sampling Peak GM, value of Std dev on logE
            read (unitmenu,*) xnSigmaSynhaz, SigmaSynhaz

            call LoadGF (componenttype,file1,GFpath,GFstations,nGFsimul,   &
            nGFsources,nSources,nStations,synflag)
        endif
       end if      

!******************************************************************************
! 12. Read input catalogs obtained with Dieterich code RSQSim:
!******************************************************************************
        !this part to open files of catalogs
        !Nrsqepis = number of epistemic sets
        !Nrsqrand = number of random simulated catalogs in each epistemic set
        !Total number of files Nrsqcat = Nrsqepis x Nrsqrand
        !File indexing is:
        !     irsqcat = (irsqepis-1) x Nrsqrand + irsqrand
        !Expected name of files is:  RSQcatROOT/irsqcat
        !Default root name is RSQcatROOT = "RSQcat"
        if (iStd_Dieterich .eq. 2)  then

            !Numbers of catalog and elements files
            Nrsqepis = Nepis1
            Nrsqrand = NMspl1
            Nrsqcat = Nrsqepis*Nrsqrand
            Allocate ( Catfile(Nrsqcat) )
            Allocate ( Faultfile(Nrsqcat) )

            
            !Read the names of catalog files 
            !Read all NMspl file names for the first epistemic cycle
            !Read all NMspl file names for the next epistemic cycle
            !etc.. for NrsqCat(=Nrsqepis*Nrsqrand) files
            string1 = 'Input-Catalogs'
            call reposition (filemenu, string1, unitmenu)
            do i = 1, Nrsqcat
                read (unitmenu,*) Catfile(i)
                Dummy2       = Catfile(i)
                print *, 'Catalog File number :',i,'  ',Catfile(i)
            end do

            !Read names of Nrsqepis elements file names
            string1 = 'In-Fault-files'
            call reposition (filemenu, string1, unitmenu)
            do i = 1, Nrsqepis
                read (unitmenu,*) Faultfile(i)
                Dummy2       = Faultfile(i)
                print *, 'Elements File number:',i,'  ',Faultfile(i)
            end do

        endif
 
        !Ns = Number of seismic sources, passed in "ModuleEQ". 
        !read from sub" RdinEQsources" within sub: "EQsourcesinfo", 
        Nsub = Ns 
!
!

!*******************************************************************************  
! 13. Read Nuisance functions:
! ****************************
        !Acceleration value that produces a 50% probability of non-acceptance
 !       open (unit=unitmenu,file=filemenu,status="old",iostat=IERR)
 !       rewind (unitmenu)
        string1 = 'Nuisance-Functions'
        call reposition ( filemenu, string1, unitmenu )
        Read (unitmenu,*)  aGM1
        Read (unitmenu,*)  BetaGM1
        if (aGM1 .gt. 0.)  then
            aGM = aGM1
        end if
        if (BetaGM1 .gt. 0.)  then
            BetaGM = BetaGM1
        end if
        !Read root-names for Nuisance functions plot files
        read (unitmenu,*)  FtypeNUZ,ExtNUZ
        read (unitmenu,*)  epsilon2

        close (unitmenu)        

!
!*******************************************************************************  
!WRITE GENERAL INFORMATION ABOUT THE RUN:
!*****************************************

        !General info on simulations of seismicity catalogs:
        !--------------------------------------------------- 
        print *, '  '
        
        if ((iStd_Dieterich.eq.1) .and. (Task.ne.3))  then
            write (*,'(4x,A,i10)') 'Generate catalogs internally in RISKCAT:'
            write (*,'(4x,A,i10)') '----------------------------------------'
            write (*,'(4x,A,i10)') 'Number of years of simulation   =', Nyears
            write (*,'(4x,A,i10)') 'Number of Epistemic Simulations =', Nepis
            write (*,'(4x,A,i10)') 'Number sample aleatory catalogs =', NMspl
            write (*,'(4x,A,i10)') 'Total number of simulations     =',    &
                                   NMspl*Nepis
        end if

        if ((iStd_Dieterich.eq.2) .and. (Task.ne.3))  then
            write (*,'(4x,A,i10)') 'Use RSQSim catalogs from input files:'
            write (*,'(4x,A,i10)') '-------------------------------------'
            write (*,'(4x,A,i10)') 'Number of years of Simulation   =', Nyears
            write (*,'(4x,A,i10)') 'Number of Epistemic Simulations =', Nepis
            write (*,'(4x,A,i10)') 'Number Sample aleatory Catalogs =', NMspl
            write (*,'(4x,A,i10)') 'Total number of simulations     =',    &
            NMspl*Nepis            
         end if
         if (Task .eq. 3)  then
            print *,'   Name of Input PSHA data file -StoreHaz:   ',storehaz
            write (*,'(4x,A,i10)')     &
            'Number of Epistemic Simulations in the file =',Nepis3
               print *,'   Use only PSHA percentile data stored in ',StoreHaz
         end if


        if (iStd_Dieterich.eq.3)  then
            write (*,'(4x,A,i10)') 'Use Empirical Catalog from input file'
            write (*,'(4x,A,i10)') '-------------------------------------'
        end if

        write (*,'(4x,A)')    &
        'Coordinates of Point of Origin  =  Longitude  Latitude'
        write (*,'(38x,2F10.5)') (Porigin(i),i=1,2)


        !General info on prediction of ground motion:
        !--------------------------------------------
        if (Task .ne. 3)  then
           if ( iATN_Larry1 .eq. 1 )  then
              write (*,'(4x,A33,a30)')                 &
              'Ground Motion Calculations      =',     &
              ' Use Attenuation relationships'
           else
              write (*,'(4x,A33,a15)')                 &
              'Ground Motion Calculations      =',     &
              ' Use G-F method'
              if (pointsource .eq. 'yes')  then
                 write (*,'(4x,A33,f10.2)')            &
                 'Use Point-Source for  Magitude <=',  &  
                 Magmaxpoint
              else
                 write (*,'(4x,A33,a29)')              &
                 '                                 ',  &  
                 'No Point-Source approximation' 
              end if
           end if
        end if


        !Print names of simulated input catalogs' files:
        !-----------------------------------------------
        if ( iStd_Dieterich .eq. 2 )  then
            write (*,'(4x,A)')'List of input files of RSQSim simulated catalogs:'
            write (*,'(4x,A)')'-------------------------------------------------'
            do i = 1, Nepis*NMspl
                write (*,'(13x,A)')  Catfile(i)
            end do
        end if
        if (iStd_Dieterich .eq. 3)  then
            write (*,'(4x,A)')'Use catalog of Earthquakes from file CatalogTest' 
        end if


        !Info on time windows for calculation of hazard and risk:
        !--------------------------------------------------------
        write (*,*) '  '
        write (*,'(4X,A,6X,i3)') 'Number of Hazard Time windows   =',Ntp 
        write (*,'(4x,A)')    'Window Number Start-Time  End-Time (years)'
        tp1 = 0.
        do itp = 1, Ntp
            if (itp .gt.1)  then
                tp1 = tlife(itp-1)
            end if
            tp2 = tlife(itp)
            write (*,'(4x,i10,4x,f10.1,2x,f10.1)') itp,tp1,tp2
        end do  
  
        !Info on Nuisance and damage Fragilities
        !---------------------------------------
        write (*,*) '  '
        write (*,'(4x,A)')  'Info on Nuisance and  Damage fragilities:' 
        write (*,'(4X,A,6x,f7.2)') 'Median Nuisance Acceleration (cm/s/s) =', &
        aGM
        write (*,'(4X,A,6x,f7.2)') 'Slope of fragility curve              =', &
        BetaGM
        write (*,*) '  '


!       Write same as above on output file (unit 2):
!       --------------------------------------------

!       General info on the run:
!       ------------------------
        if ((iStd_Dieterich.eq.1) .or. (iStd_Dieterich.eq.2))  then
        write (2,'(4x,A,i10)') 'Number of years of Simulation   =', Nyears
        write (2,'(4x,A,i10)') 'Number of Epistemic Simulations =', Nepis
        write (2,'(4x,A,i10)') 'Number of samples per Epis cycle=' ,NMspl
        write (2,'(4x,A,i10)') 'Total number of simulations     =', NMspl*Nepis
        end if
        if ( iATN_Larry1 .eq. 1 )  then
            write (2,'(4x,A)')     'Uses G-M Attenuation relationships'
        else
            write (2,'(4x,A)')     'Uses Larry SYNHAZ code for G-M prediction'
            if (pointsource .eq. 'yes')  then
                write (2,'(4x,A,F10.2)') 'Point Source approx. for Mag.le.',  &
                Magmaxpoint
            end if
        end if
        write (2,'(4x,A)')    &
        'Coordinates of Point of Origin  =  Longitude  Latitude'
        write (2,'(38x,2F10.5)') (Porigin(i),i=1,2)


!       Write names of input catalog files:
!       -----------------------------------
        if ( iStd_Dieterich .eq. 1 )  then
            write (2,'(4x,A)')     'Uses standard G-R Eq.occurence curves'
        elseif (iStd_Dieterich .eq. 2) then
            write (2,'(4x,A)')     'Use input catalogs of RSQSim simulations'
            write (2,'(13x,A)')    'List of input catalog files:'
            do i = 1, Nepis*NMspl
                write (2,'(13x,A)')  Catfile(i)
            end do
        else 
            write (2,'(4x,A)')     'Use Catalog of Eqs from file CatalogTest'
        end if

!
!       Write time windows considered for hazard and risk results:
!       ----------------------------------------------------------
        write (2,'(4X,A,6X,i3)') 'Number of Hazard Time windows   =',Ntp 
        write (2,'(4x,A)')    'Window Number Start-Time  End-Time (years)'
        tp1 = 0.
        do itp = 1, Ntp
            if (itp .gt.1)  then
                tp1 = tlife(itp-1)
            end if
            tp2 = tlife(itp)
            write (2,'(4x,i10,2x,f10.1,2x,f10.1)') itp,tp1,tp2
        end do


        !Write info on Nuisance and damage Fragilities:
!       -----------------------------------------------
        write (2,*) '  '
        write (2,'(4x,A)')  'Info on Nuisance and  Damage fragilities:' 
        write (2,'(4X,A,6x,f7.2)') 'Median Nuisance Acceleration (cm/s/s) =', &
        aGM
        write (2,'(4X,A,6x,f7.2)') 'Slope of fragility curve              =', &
        BetaGM      


!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
!
!                              START CALCULATIONS


!       Earthquake simulations notes:
!       *****************************
!       
!       This code considers two types of uncertainties: Epistemic and Aleatory.
!
!       Aleatory uncertainty:
!      ---------------------
!       Reflects inherent randomness in physical processes.
!       If our knowledge of the physical processes were perfect, all the 
!       probability distributions that characterizes the aleatory parameters 
!       would be known, and the probability of exceedance of GM would be characterized
!       by a unique function. 
!       Hence there would be a single mean Hazard curve representing all the
!       aleatory uncertainty.
! 
!       Epistemic uncertainty:
!       ----------------------
!       It means that there could be alternatives to the selected physical models, 
!       and to their parameter sets.
!       For each set of epistemic variables/models, there is a single mean 
!       hazard curve, and for Nepis sets of possible alternatives ther ar Nepis
!       mean hazard curves.
!       
!       
!       The code has 3 main loops of simulation calculations: 
      
!      1.  EPISTEMIC LOOP: (Nepis cycles)
!          This loop generates a set of Nepis mean hazard curves, the bulk of
!          which samples the epistemic sample space.
!          Statistics drawn on these Nepis hazard curves give the mean and 
!          percentile values of the hazard. To be interpreted as confidence
!          in the estimates of the mean exceedence hazard.
!          The sum of the epistemic weights is 1.00 
!          Epistemic uncertainty accounted for:
!               - location of seismic sources
!               - Geometry of seismic sources
!               - seismicity rates of seismic sources
!               - Ground-Motion modeling technique and model parameters
!


!          2. ALEATORY LOOP: (NMspl cycles)
!             This loop  applies first to the generation of catalogs.
!             Other loops are nested in this loop to account for aleatory
!             uncertainty in GM.

!             Case of using RSQSim:
!             the aleatory sampling is performed internally in RSQSim, expressed
!             in each set of NMspl samples of aleatory catalogs 

!             Case of using Seismic Zones  +  G-R: 
!             the catalogs are generated by a time-step algorithm that simulates
!             the location and time of occurrence of the next earthquake
!             (could be time-dependent)

!             Within one epistemic simulation, all the epistemic variables are
!             set to a constant value. The full set of (aleatory) catalogs
!             represents the aleatory uncertainty in events generation for the
!             corresponding epistemic sample.

!                  
!              3. GROUND-MOTION PREDICTION
!                 Aleatory uncertainty in GM prediction is accounted for in
!                 subroutines GMHR when using  attenuation relationships,
!                 and in GMHR2 when using the EGF approach
!                 (Subroutines synhaz and SYNHAZpoint).
!                 The result of the NMspl cycles is a suite of NMspl histograms
!                 which are linearly stacked with the sampling weights Wspl.
!                 This loop uses a standard MC simulation approach, 
!                 Wspl = 1. / NMspl , SUM(Wspls) = 1.00
!                 In addition, when using attenuation models with sigma,
!                 a Latin Hypercube (LH) techniques is used in two more 
!                 nested loops. One over the number of sampling intervals
!                 and a second over the sample size within each interval.
!                 The number ofsampled intervals is 5, and the number of 
!                 uniformly sampled values in each  interval is 10.
!                 The weights applied are 1/10 for the uniform sampling
!                 and for the interval weight it is their probability weight.
!                     
!       At this point the mean hazard curve for the iepis-th cycle is calculated
!       in Hazard(iepis)
!
!       end of LOOPS 1, 2, and 3.

!       Statistics are performed to determine the percentiles of the hazard
        !       Options scopes:
        !       Task = 0   Code does full calculations of PSHA and exits
        !              1   Does full calculations and accumulates results
        !                  of each individual catalog into file "StoreHaz"
        !              2   Does not do any catalog  PSHA calculations.
        !                  Performs statistics on data stored in "StoreHaz" to
        !                  calculate final percentiles
        
!*******************************************************************************
!

        !Set defaults:
        Years = Float (Nyears) 
        !Length of time cycle
        dt1   = dt
        !working arrays
        work4     = 0.
        ALLOCATE ( Neqtot(NMspl,Ntp) )
        ALLOCATE ( Nonzerotp2(Nepis,Ntp) )
        ALLOCATE ( work4SPEC(NcritRD,nGMbins,nfreq ) )
        ALLOCATE ( Haz1(NcritRD,nGMbins,Nepis,Ntp) )
        ALLOCATE ( Haz1SPEC(NcritRD,nGMbins,Nepis,Ntp,nfreq) )
        ALLOCATE ( HAZ2(nGMbins,nfreq) )
        ALLOCATE ( UHS(nfreq) )
        ALLOCATE ( UHSRP(Nrp,Nperc,nfreq) )
        
        !For task=3, Skip all the PSHA calculation part. Go directly to
        !calculation of percentiles with data stored in file StoreHaz
        Task_3_No_Calculations: if (task .ne. 3)  then

        
        work4SPEC = 0.
        !Default value of epistemic weight:
        wepi = 1. / Float(Nepis)
        CALL Dattim (idatex,itimex)
        read (itimex,'(i2,1x,i2,1x,i2)') ihh,imm,isec
        T1 = Float(3600*ihh + 60*imm + isec)
        pagenumber2 = 1

!
!****************************************************************************
!       Seismic Moment regional budget for calibration of catalogs:
!*****************************************************************************

!        Three options are available: (Parameter Calibrate read from menu file)
!
!        Option 1: Calibrate = 0 No calibration of simulated total Moment Budget
!           Scaling is 1.0 
!           unless a forced scaling in read as input of param. "ScaleM0Forced" 
!        Option 2: Calibrate = 1
!           Use an existing empirical data.
!           Need to read the catalog data from file "Calibrationat"
!        Option 3: Calibrate = 2
!           Use the model of seismicity from standard PSHA characterization
!           with including the geometry and seismicity parameters of the sources
!           An artificial catalog is generated with that information ands is
!           used for the calibration of catalogs.

         if (Calibrate .eq. 1)  then
            AVGLTM0 = 0.
            !Use empirical, (recorded) catalog
            open (unit=3,file=CalibrationCat,status="old",iostat=IERR)
            call CantFindFile (filemenu,IERR)
            if (IERR .ne. 0)  then
                STOP 
            end if
            j = 1
            if (j .eq. 1)  then
                do i = 1, nMagbins; LTSReg(i)=0.;enddo
                Do i = 1, 10000000
                    read (3,*,end=150) Ycat,Ycat,Ycat,xMag3
                    if (xMag3 .gt. xMagCal)  then
                        xMagCal = xMag3
                    end if
                    !Add Seismic Moment of event to Total M0 budget in N.m
                    xcretM0 = 10. ** (1.5*xMag3 + 9.1)
                    !Seismic moment must be in dynes.cm
                    AVGLTM0 = AVGLTM0 + xcretM0*1.e7
                    !What bin of Magnitude this event falls?
                    do k = 1, nMagbins
                        x1 = xMagLBnd(k)
                        x2 = xMagHBnd(k)
                        !Construct histogram of magnitude of calibration Catalog
                        if ((xMag3.ge.x1) .and. (xMag3.lt.x2))  then
                            LTSReg(k) = LTSReg(k) + 1
                            exit
                        end if
                    end do                    
                end do
                !Convert to annual seismicity
150             continue 
                is = 0
                do k = 1, nMagbins
                    is = is  + INT (LTSReg(k))
                end do
                
                print *, 'Total Events        =' ,i-1
                print *, 'Events in Histogram =', is
                print *, 'Calibration  Years  =', CalibrateYears,' years'
                print *, 'LTSReg():', (LTSReg(i),i=1,nMagbins)               
                do i = 1, nMagbins
                    LTSReg(i) = LTSReg(i) / CalibrateYears
                end do
                print *, 'LTSReg():', (LTSReg(i),i=1,nMagbins)  

            else
                !Read NCEDC empirical catalog
                do i = 1, 18    !Skip 20 lines
                    read (3,*) Dummy
                end do
                do i = 1, 1000000
                    read (3,542,END=200) Annee1,Mois,Jour,Heure,Minutes,  &
                    Secondes,Ycat,Xcat,Zcat,xMag3 
                    !Add Seismic Moment of event to Total M0 budget in N.m
                    xcretM0 = 10. ** (1.5*xMag3 + 9.1)
                    AVGLTM0 = AVGLTM0 + xcretM0
                end do
200             continue
            endif
            close (3)
      
         elseif (Calibrate .eq. 2)  then
!            1. Generate artificial catalogs with G-R,with all uncertainties
!              (i.e. aleatory and epistemic)
!            2. Calculate total seismic moment for each simulated catalog
!            3. Calculate average Total regional M0 budget AVGLTM0
!            This value will be used to calculate the Moment scaling factor of
!            all earthquakes in a catalog generated by G-R or RSQSIM simulations

             !Calculate average cumulative total seismic moment budget for PSHA 
             !standard map of seismic sources, for the period Trelax-Years 
             ! M0 (in dyne.cm)
             !Minumum magnitude is xM0, 
             !Max is max mag for the region. Is an input
             !in menu file 
             xMagMin2 = xM0
             xMagMax2 = xMagCal
   
             call BudgetM0(AVGLTM0,dt,Nepis,NMspl,Trelax,xMagMax2,    &
                  xMagMin2,xM0,Years)

         else
             !Case of no calibration
             idum = 1  !Dummy for debugging)

         end if

         if (Calibrate .ne. 0)  then
            print *, 'AVGLTM0 =',AVGLTM0
         end if

        !Allocation for Mag bins sampling:There are nbin Magnitude sampling bins
            !Create Mag bins. Max is 8+
            nbin = 10
            ALLOCATE ( Smag(nbin+1) )


        !MAx number of events in each mag sampling bin is MaxSampling
        !Sampling time is time of occurrence of last MaxSampling-th event
        !if less than MaxSampling events in entire time-window for the samplng
        !Mag bin, then time is set to entire length of time-window
     

        !Allocation for number of events in each bin of:
            ! epistemic simulation
            ! Magnitude sampling bin
            ! Time-window
            ! iepis,Mag,Ntp
            ALLOCATE ( Nbtm(Nepis,Ntp,nbin) )
            Nbtm = 0
    
        !Allocate array of mag sampling times
            ALLOCATE ( SamplingTime(Ntp,nbin) )
            !Set the default values to the lengths of the time-windows
            do i = 1, nbin
                tp1 = tlife(1)
                SamplingTime(1,i) = tp1
                do itp = 2, Ntp
                    tp1 = tlife(itp) - tlife(itp-1)
                    SamplingTime(itp,i) = tp1
                end do
            end do

        !Allocate  memory for array of magnitude sampling weights
        ALLOCATE ( wMagbin(nbin,Ntp) )

        !Remember to de-allocate somewhere


!EPISTEMIC SIMULATION LOOP1:
!***************************

        !Start at year number 1: (For the purists this is year Zero!)
        !Set matrix of Influence(i,j)= correction factor on lamda(j), 
        !given the last event that occurred was peril with tot index i.
        !if only perils are earthquakes 
        !Influence(1,2) = correction to apply to lamda(2), given the last 
        ! was a peril of tot index 1.

        !Check for relevant data. Is there any good catalog in this run
        !if not, then abort the run
        Goodminet = 0
        BadMinet  = 0

        LOOP1_Epistemic_Simulation: DO  iepis = 1, Nepis

!           Set default values and array allocations for epistemic cycle:
!           -------------------------------------------------------------
            pagenumber2 = 1    
            !Set initial value of Nonzerotp1 to 1. for having  >1 event in it
            do i = 1, Ntp
                Nonzerotp1(i) = 1
            end do

         
!           SET THE EPISTEMIC PARAMETERS FOR THIS EPISTEMIC SIMULATION   
!           Main Epistemic loop: has Nepis cycles
!           Samples the epistemic parameter space including:
!           1. Seismic maps and faults, volumes or areas geometry
!           2. Recurrence parameters (e.g. G-R a, b and Mmax)
!           3. Working parameters for RSQSim
!           4. Ground motion prediction models:
!                   -Physically based models, epistemic parameters
!                   -Attenuation relations alternatives
!           5. Miscel. other computation models, e.g. M to Mag relationship,
!              or Mag to Area ot Fault length relationship.
!           

!...........CALCULATE THE WEIGHT OF THIS EPISTEMIC SAMPLE
!           Start with uniform epistemic sampling
!           This can be modified later to reflect the actual non-uniform
!           distributions of epistemic variables
            Weightepi(iepis) = wepi

!...........Seismicity catalogs
            

!           Consider three cases (1) Sources zonation with reccurence functions
!                                (2) RSQSim catalog generated externally
!                                (3) Standard seismiciy catalog / others

            !Select zonation map index: kzMap
            sum = 0.
            r1 = rnunf()
            do i = 1, nMaps
                sum = sum + wMap(i)
                if (r1 .le. sum)  then;  exit;  endif
            end do
            kZMap = i
            Ns = NzMap(kzMap)

            !Contribution of Maps to epistemic sample weight.
            !Default weight, including case of multiple input catalogs
            Weightepi(iepis) = wepi
            if (iStd_Dieterich .eq. 1)  then
                !Case of generating catalogs from G-R simulations
                Weightepi(iepis) = wMap(kZMap)
            end if
            
!...........Ground Motion prediction (set of) model(s)
!           For each source, select the GM prediction model by simulation
            if (iATN_Larry .eq. 1)  then
                !Case of using GMPEs
                wepigm = 0.
                !Select GMPE assigned to each seismic source
                do i = 1, Ns
                    !Index of the source in the zonation map
                    iS = Mapzid(kZMap,i)
                    nGM = nattn(iSourceGM(iS))     
                    !The Source GM type is: iSourceGM(is)
                    !There are Kj different sets (Types) of attenuation models
                    !each with different weights adding to 1 in the set.
                    !Each source is associated with one of the sets. And in 
                    !the simulation, one of the models in the set is selected, 
                    !for each source in the zonation map.
                    !nattn(j)   = number of attenuation models of type j.
                    !             nattn(j) = 1,...,Kj
                    !wattn(j,k) = weight of k-th attenuation model 
                    !             of type j
                    !iattn(j,k) = Index (in the table of all attenuation
                    !             models) of the k-th model for source
                    !             of type j
                    r2 = 0.
                    r1 = rnunf()
                    do k = 1, nGM
                        r2 = r2 + wattn(iSourceGM(iS),k)
                        if (r2 .gt. r1)  exit
                    end do
                    XSimAttn(iS) = iattn(iSourceGM(iS),k)
                    !Sigma
                    Xgmxsig(iS) = defGMsig(iSourceGM(iS)) 
                    wepigm = wepigm + wattn(iSourceGM(is),k)      
                end do
                !Weight is average of all sources GM weights  
                wepigm = wepigm / Float(Ns) 
                !Contribution of GM models weights to epistemic weight  
            else
                !Case of using SYNHAZ
                !At this point use weight of 1.
                wepigm = 1.            
            end if
            Weightepi(iepis) = Weightepi(iepis) * wepigm  

            !Sample set of epistemic source parameters for selected source Map
            !---------------------------------------------------------------- 
            !Parameters sampled include: 
            !Dip
            !Rake
            !Recurrence parameters
            do i = 1, Ns
                iS = Mapzid(kZMap,i)  
                iflt  = iS
                nsegt = nsegFault(iS)
                nstr  = nwstrips(iS)
                call SampleEpis ( iflt,nstr,nsegt )
            end do


!...........Catalog used for Calibration of regional seismicity:
            !    Create Regional Long-Term Seismicity recurrence array
            !    for plotting the regional recurrence from catalog simulations,
            !    and from long-term seismicity data.  Output array LTSreg()
            !    LTSreg() is annual rate
            !If CalibrateCat = 1
            !    the empirical catalog is used. LTSreg() becomes just the
            !    histogram of magnitudes in the catalog. As already 
            !    determined when the total seismic moment budget is calculated.
            !If CalibrateCat = 2
            !    The regional seismicity is simulated from the standard PSHA
            !    zonation and rates models. Needs to have a zonation and
            !    seismicity information file.
            if (Calibrate .eq. 2)  then
                call RegionalSeis (dt, xM0 )
            endif
          
!...........Write info. on current epis. simulation on screen if requested
            if (Printdetails .gt. 0)  then
                write (6,*)'  '
                write (6,*)'Epistemic simulation number:', iepis
                write (6,*)'Zonation Map      =', kZMap
                write (6,*)'Number of sources =',Ns 
                write (6,*)   &
            'Simul #    M-Bin    Sample  N-Trials  Number-Events   CPU-(sec)'
            endif


!
!               ----------------------------------------------------------------
!               ALEATORY SAMPLING AND CALCULATION LOOP3:
!               ***************************************

                !Reset total regional M0 for NMspl simulations to 0.
                SUMrsqM0 = 0.
!             
!               Generate NMspl full catalogs and cumulate seismic moment:
                !-------------------------------------------------------
                Loop3_Sampling_Size: Do ispl = 1, NMspl

                   GMoutMin =  10.e10
                   GMoutMax = -10.e-10
                   !To make sure Random numbers start differently:
                   iy = (iepis-1)*NMspl + ispl
                   print *,'PSHA Catalog simulation cycle:',iy
                   print *,'IEPIS =',iepis
                   print *,'ISPL  =',ispl
                   print *,'  '
                       do i = 1, iy
                          r = rnunf()
                          !print *,'Random =',r
                       end do

                    !Three cases:
                    !------------    
                    !Standard Seimicity generation (1), 
                    !Dieterich model catalogs      (2)
                    !standard input catalogs       (3)  
                                                            
                    SELECT CASE (iStd_Dieterich)
                
                    Case (1)
                                 
                    !Zonations/Faults and Recurrence model simulated catalog 
                    !-------------------------------------------------------
                    !(G-R model, Truncated Expon., Time dependent, etc.     
                    !Set sources' properties to initial values                
                    do is = 1, Ns    
                        i = Mapzid(kZMap,is)
                        SELECT CASE (SeisRectype(i))                   
                           !Truncated exponential model
                           CASE (1)
                           !Mmax. 
                           ArecUpdt(i,7) = Arec(i,7)
                           !beta.
                           ArecUpdt(i,4) = Arec(i,4)
                           !alfa.
                           ArecUpdt(i,3) = Arec(i,3) 
                           ArecUpdt(i,1) = Arec(i,1)                      
                           !Characteristic model (Youngs-Coppersmith)
                           CASE (2)                   
                           !Segmented fault model (Foxall Model)
                           CASE (3)
                           !Empirical model manually entered
                           CASE (4)
                           CASE DEFAULT
                        END SELECT
                     end do

                    !For definition of Magnitude bins for sampling:
                    !----------------------------------------------
                    !We want bins where larger Mag events are definitely sampled
                    !Smallest magnitude event in catalog is not know because we
                    !are generating the catalog on the fly, but cannot be less
                    !than xM0: MagMin2
                    xMagMin2     =  1000.
                    !xMagMax2    = Max in the UNCULLED Catalog. Determined later
                    xMagMax2     = -1000.
           
                    
                    !Let it rip to generate a catalog
                    !--------------------------------
                    !Period of simulation during perturbation time
                    ! time = 0 to Trelax
                    !Find which is the next source, when, what mag,  etc..
                    dT1    = dt    !Duration of time step, in years
                    sigMmax = 2.   !range of uncertainty of number of events
                                   !range is sigMax at magnitude Mmax.
                                   !range is zero at M=0
                    inev    = 0    !total number of events generated in a map
                    inevTotal=0
                    ixM0    = 0    !number of events retained
                                   !(given conditions on M and time)
                    dt2     = dt
                    j1      = 0
                    SumrsqM0= 0.   !Total seismoc moment budget for the full
                                   !catalog. In dynes.cm
                    !Track time of occurrence of Eq in TimeKeep1, 2 and 3
                    TimeKeep1 = 0.

                    !Working files for storing events info and for sampling:
                    !CatTEMP1 = Full catalog of events,       unit = unitcat1
                    !CatTEMP2 = Culled catalog after sampling unit = unitcat2
                    unitcat1 = 7
                    unitcat2 = 17

                    !Open temporary file for storing UN-CULLED CATALOG: CatTEMP1
                    open(unit=unitcat1,file='CatTEMP1',status="UNKNOWN",    &
                         form='unformatted',iostat=IERR)
                    !open(unit=unitcat2,file='CatTEMP2',status="UNKNOWN",iostat=IERR)


                    

                    Generate_Catalog: do iy = 1, Large  
                  
                       !Select nextsource, Mag & time of occurrence of next event
                       !dt2 is fraction of year
                               
                       call NextEvent(dt2,xM0,xMSc,NextSc,TimeSc,sigMmax)

!print *,'dt2,xM0,xMSc,NextSc,TimeSc,sigMmax:',dt2,xM0,xMSc,NextSc,TimeSc,sigMmax
                      !Next event is occurring after TimeSc from last Eq
                      !Source ID: NextSc
                      !Magnitude: xMSc
                      !Time     : TimeSc

                      if (NextSc.gt.0) then 

                         Timekeep1 = Timekeep1 + TimeSc
                         xcretM0 = 10. ** (1.5*xMSc + 16.1)
                         !Update Total M0 budget 
                         SUMrsqM0 = SUMrsqM0 + xcretM0
                        
                         !If magnitude greater then M0 and Time less than Years
                         !then write event parameters on temporary unformatted
                         !file "CatTEMP1"
                         if  (xMSc.ge.xM0) then
                            !Time of event,Magnitude, Source ID, Seismic Moment
                            write (unitcat1) Timekeep1,xMSc,NextSC,xcretM0
                            if (xMSc .ge. xMagMax2)  then
                               xMagMax2 = xMSc
                            end if
                            if (xMSc .le. xMagmin2)  then
                               XmagMin2 = xMSc
                            end if
                            if (TimeKeep1 .ge. Years)  then
                              exit
                            end if

                            !Total number of events saved: inevTotal
                            !Number of event stored across all time-windows
                            !with xMSc .ge. xM0 
                            !All other simulations are skipped except to calculate
                            !total seismic moment
                            inevTotal = inevTotal + 1
 
                            !Update sources rate after occurrence in NextSC
                            !via influence  time dependent matrix  
                            call UpdateScRates (NextSC,TimeSc,xMSc)        
                         endif  
                    
                      else

                         !Update time
                         Timekeep1 = Timekeep1 + dt2
                           
                      end if



                    end do Generate_Catalog

                    yearscat = years

                    print *,'SumrsqM0=',SumrsqM0
                    AVGLTM0 = SumrsqM0
                    

                    !Perform the sampling and culling of the Catalog
                    !In:  inevTotal events
                    !Out: inev reulting after culling
                    !Rewind temporary file of original CATALOG "CatTEMP1"
                    rewind (unitcat1)
                    
                    !Open temporary file of  new CULLED catalog "CatTEMP2"
                    !Culled Catalog is stored in unitcat2 "CatTEMP2"
                    unitcat2 = 17
                    open(unit=unitcat2,file='CatTEMP2',status="UNKNOWN",  &
                         form='unformatted',iostat=IERR)
                    
                    call MagSamplingCase1 (ispl, MaxMagSampling, nbin, inev, &
                         inevTotal, Ntp, unitcat1, unitcat2, xMagmax2,       &
                         xMagMin2, xM0)
                    
                    !Load events description arrays of CULLED Catalog
                    print *,'inev=',inev
                    NCatevents(ispl) = inev
                    rewind (unitcat2)
                    do i = 1, inev
                       read (unitcat2) TimeKeep1,xMSc,NextSC,xcretM0
                       CatEqTime(ispl,i) = TimeKeep1
                       CatEqMag(ispl,i)  = xMSc
                       ICatEQ(ispl,i)    = NextSc
                       CatEqM0(ispl,i)   = xcretM0
                       !if the source is other than a finite fault, need to
                       !simulate an earthquake location for Poisson spatial
                       !distribution in the seismic zone.
                       nodes = scrnodnum(NextSC)
                       call AreaEqselect (NextSC, nodes, zarea, yarea, xarea)
                       Xrsq(ispl,i)      = xarea
                       Yrsq(ispl,i)      = yarea
                       Zrsq(ispl,i)      = zarea
                      ! print *,'xarea,yarea,zarea:',xarea,yarea,zarea
                    end do
                    

                    close(unitcat2)

             
                      
                Case (2)
                         
                !RSQSim Dieterich Model externally generated catalog:
                !----------------------------------------------------  
                       
                    i = (iepis-1)*NMspl+ispl

!................ .............................................................. 
!                   Variables that need to be loaded from RSQSim catalog
!                   ----------------------------------------------------
!                   NCatevents(ispl)     = # events in catalog
!                   CatEqTime(ispl,inev) = time of occurrence
!                   CatEqMag(ispl,inev)  = Magnitude
!                   ICatEQ(ispl,inev)    = Seismic source index
!                   Xrsq(ispl,inev)      = init. point X (m)
!                   Yrsq(ispl,inev)      = init. point Y (m)
!                   Zrsq(ispl,inev)      = init. point Z (m)
!                   CatEqSubElt(inev,k)  = center of  Sub-elt X,Y,Z  (m)   
!                   CatEqSubArea(inev,j) = area of sub-element j   (m^2) 
!                   SubEltNumber(inev)   = number of Sub-Elements in Eq
!                   CatEqM0(ispl,inev)   = Seismic Moment of Eq.
!................ .............................................................. 
                    xMagMax2 = -1000.
                    xMagMin2 =  1000.
                    !The average Regional seismic moment for each epistemic set
                    !of RSQSim catalog for the period of "Years" years is set 
                    !equal to the average seismic moment for the same period of
                    !natural seismicity.
                    !Same scaling for all aleatory sampling of catalogs 
                    !of each epistemic set: wrsqepi(iepis) or scaleM0(iepis)

                    !Original Catalog of events is read from file unit 3
                    unitcat = 3
                    open(unit=unitcat,file=Catfile(i),status="UNKNOWN",  &
                         form="unformatted",iostat=IERR)
                    
                    !Trimmed working catalog is file "CatWork1", unit 17.
                    open(unit=17,file="CatWork1",status="UNKNOWN",  &
                         form="unformatted",iostat=IERR)

                    !Trimmed working catalog is file "CatWork2", unit 19.
                    open(unit=19,file="CatWork2",status="UNKNOWN",  &
                         form="unformatted",iostat=IERR)         
                    
                    !Coordinates of subelements are read from file unit 7 
                    open(unit=7,file=Faultfile(i),status="UNKNOWN",   &
                    iostat=IERR)

                    !Print names of Catalogs and Elements files for this cycle
                    print *, '   Catalog file name  =',Catfile(i)
                    print *, '   Elements file name =',Faultfile(i)

                    !Read coordinates of subelements initiation points
                    !-------------------------------------------------
                    !store in array XYZsub(i)
                    !First find number of subelements:
                    !ntotnse is total number of subelements in RSQSim faults
                    do iy = 1, 1000000
                        read (7,*,iostat=IERR) xdum
                        if ((IERR.ne.0) .or. (ABS(xdum).le.1.e-6))  then
                            ntotnse = iy - 1
                            exit
                        endif
                     end do
                     !If ntotnse = 0, there is an error in the format of
                     !the fault file. Stop execution
                     if (ntotnse .le. 0)  then
                        print *,'  ERROR in Fault file ', Faultfile(i)
                        print *,'  STOP Execution'
                        stop
                     end if

                    ALLOCATE ( XYZsub( ntotnse , 3) )
                    rewind (7)

                    do iy = 1, ntotnse
                        j = iy
                        read (7,*) x1,y1,z1,dxlong,dylat,strike_usual,   &
                             Dip_usual,rake_usual,slip1
                        !x1, y1 in degrees,
                        !or transformed to degrees if in UTM meters
                        !see transfomation below if "UTM" = yes
                        !z1 read in meters
                        XYZsub(iy,1)      = x1           ! Long, deg
                        XYZsub(iy,2)      = y1           ! Lat,  deg
                        XYZsub(iy,3)      = z1 / 1000.   ! Z in km for synhaz
                        SynSubstk(iy)     = strike_usual ! deg
                        SynSubDip(iy)     = Dip_usual    ! deg
                        SynSubRake(iy)    = rake_usual   ! deg
                        SynAreaSub(iy)    = dxlong*dylat ! m^2
                        SynSubSlipRate(iy)= slip1        ! m/s
                    end do
                    close (7)

                    !Transform UTM for fault sub-elements definition
                    !into  Long-Lat-Z coordinates
                    !The point of origin is the origin of the UTM coordinates
                    ! of the sub-elements.
                    !The values read have to be transformed in Lat-Long and
                    !Coordinates of Porigin must be added to them.
                    !This transformation done only when x-y or UTM are used for
                    !the coordinates of fault elements
                    if (UTM .eq. 1)  then
                        do iy = 1, ntotnse
                           rq2 = (XYZsub(iy,2)/rearth) * radeg
                           rq2 = rq2 + Porigin(2)
                           XYZsub(iy,2) = rq2
                           rq1 = rearth * cos(rq2)
                           XYZsub(iy,1) = (XYZsub(iy,1)/rq1) + Porigin(1)
                        end do
                    end if

!
!                   Read full catalog and calculate total moment budget:        
                    !---------------------------------------------------
                    !Test the first line of file:
                    read (unitcat,iostat=IERR) iy
                    if (IERR .ne. 0) then
                       print *,'*******************************************'
                       print *,'  '
                       print *,'Catalog file:  ',Catfile(i)
                       print *,'               ************'
                       print *,'The file is bad, does not exist or is empty'
                       Print *,'File is Skipped. Go to next Catalog file'
                       print *,'  '
                       print *,'********************************************'
                       DEALLOCATE ( XYZsub )
                       close (unitcat)
                       close (17)
                       close (19)
                       go to 9990
                    end if
                    iflagev = 0
                    Neq1    = 1000000
                    !If iy>1 then it is the number of events: set iflagev to 1
                    if (iy .gt. 1)  then
                       !First line is number of events in original catalog
                       iflagev = 1
                       Neq1   = iy
                       !Write on working file
                       write (17) iy
                    end if

                    rewind (unitcat)

                    !If iflagev=1, Skip 1st line,
                    !then determine #events in working catalog by EOF
                    if (iflagev .eq. 1)  then
                       read (unitcat) iy
                    end if
                    
                    !Create working catalog of events .gt. xM0 in time-windows
                    twinmax = tlife(Ntp)
                    Neq2    = 0
                    !Flag for main events with more than zero elements
                    !Events with zero elements are skipped
                    nsenonzeroflag = 0
                    ierr3 = 0
                    ierr4 = 0
                    ierr5 = 0

                    !First, clean up the catalog.
                    !****************************
                    !Remove all events associated with read errors
                    !Write results on   "CatWork1"
                    do iy = 1, Neq1
                        read (unitcat,iostat=IERR) k4,nrup4,m08,mw4
                        !inev = k4
                        inev  = INT(k4,4)
                        nse  = nrup4
                        xdum = SNGL( m08 )
                        xxx  = mw4
                        xcretMag = xxx
                        xcretM0  = xdum
                        
                        !Skip events with read errors
                        if (IERR .ne. 0)  then
                           !Skip the event and its sub-events
                           if (nse .gt. 0)  then
                              do inse = 1, nse
                                  read (unitcat,iostat=IERR1) &
                                  elem4,time8,dur8,slip8,std8
                              end do
                           end if
                           go to 8000
                        end if


                        Neq2 = Neq2 + 1
                        !Write retained events on working file CatWork1, unit 17
                        write (17)  k4,nrup4,m08,mw4
                        do inse = 1, nse
                           read (unitcat,iostat=IERR1) &
                                       elem4,time8,dur8,slip8,std8
                           write (17)  elem4,time8,dur8,slip8,std8
                        end do
                           
8000                    continue
                    end do

                    !Check file CatWork1
                    rewind (17)
                    read (17) iy
                    do iy = 1, Neq2
                       read (17)  k4,nrup4,m08,mw4
                       nse = nrup4
                       do inse = 1, nse
                          read (17)  elem4,time8,dur8,slip8,std8
                       end do
                    end do
                       

                    !Remove events with zero sub-events (zero elements)
                    !Write results on "CatWork2", unit 19
                    rewind (17)
                    read (17) iy
                    write (19) iy
                    Neq3 = 0
                    do iy = 1, Neq2
                       read (17)  k4,nrup4,m08,mw4
                       nse = nrup4
                       if (nse .eq. 0)  then
                          go to  8250
                       end if                       
                       !Write retained events on working file CatWork2
                       Neq3 = Neq3 + 1
                       write (19)  k4,nrup4,m08,mw4
                       do inse = 1, nse
                          read (17,iostat=IERR1) &
                                      elem4,time8,dur8,slip8,std8
                          write (19)  elem4,time8,dur8,slip8,std8
                       end do
 8250                 continue
                    end do
                    
                    !Check file CatWork2
                    rewind (19)
                    read (19) iy
                    do iy = 1, Neq3
                       read (19)  k4,nrup4,m08,mw4
                       nse = nrup4
                       do inse = 1, nse
                          read (19)  elem4,time8,dur8,slip8,std8
                       end do
                    end do

                    !Remove events with :
                         !Magnitude less than xM0
                         !time greater than max time window twinMax
                    !Write results on "CatWork1", unit 17
                    rewind (19)
                    rewind (17)
                    read (19) iy
                    write (17) iy
                       !NOTE:
                       !*****
                       !This number may actually not be the final number of 
                       !events in the final working file, if events too small
                       !or too late are removed.
                       !Therefore the first entry stored in CatWork1 is not
                       !necessarily the number of events stored in it.
                       !The actual number of events to be used is Neq2
                    Neq2 = 0
                    do iy = 1, Neq3
                       read (19)  k4,nrup4,m08,mw4
                       nse = nrup4
                       read (19,iostat=IERR1) &
                            elem41,time81,dur81,slip81,std81
                       timexy = SNGL(time81) /sectoyear
                       xxx    = mw4
                       xcretMag = xxx
                       xcretM0  = xdum
                       !Moment Budget for the FULL CATALOG before trimming
                       !--------------------------------------------------
                       if (timexy .le. Years)       then
                          !Set min and max range of Magnitude
                          if (xcretMag .gt. xMagMax2)  then
                              xMagMax2    = xcretMag
                          end if
                          if (xcretMag .lt. xMagMin2)  then
                              xMagMin2    = xcretMag
                          end if
 
                          !Add overall event M0 to M0 budget in Joules
                          SUMrsqM0 = SUMrsqM0 + xcretM0 
                          !Update time last event with at least 1 subelement
                          YearsHist = timexy
                       endif
                        
                       if ((xxx.lt.xM0) .or. (timexy.gt.twinMax))  then
                          !Skip remaining sub-events
                          do inse = 2, nse
                             read (19,iostat=IERR1)  elem4,time8,dur8,slip8,std8
                          end do
                          go to  8750
                       end if                       
                       !Write retained events on working file CatWork2
                       Neq2 = Neq2 + 1
                       write (17)  k4,nrup4,m08,mw4
                       write (17)  elem41,time81,dur81,slip81,std81
                       if (nse .gt. 1)  then
                          do inse = 2, nse
                             read (19,iostat=IERR1) &
                                         elem4,time8,dur8,slip8,std8
                             write (17)  elem4,time8,dur8,slip8,std8
                          end do
                       end if
 8750                  continue
                    end do

                    !Check file CatWork1
                    !This will be the final trimmed working catalog.
                    !The number of events in this Catalog is Neq2
                    rewind (17)
                    read (17) iy
                    do iy = 1, Neq2
                       read (17)  k4,nrup4,m08,mw4
                       nse = nrup4
                       nsenonzeroflag = nsenonzeroflag + nse
                       do inse = 1, nse
                          read (17)  elem4,time8,dur8,slip8,std8
                       end do
                    end do
                  
                     
                    !Skip this catalog If number elements is zero for all events
                    !or total number of useable events is too small (say, less
                    !than: MaxMagSampling)
                    !The number of events in the trimmed final working catalog
                    ! CatWork1,  is:  Neq2
                    if ((nsenonzeroflag.lt.1) .or. (Neq2.lt.MaxMagSampling)) then
                       DEALLOCATE ( XYZsub )                      
                       print *, '   Catalog File: ',Catfile(i)
                       print *,' Catalog has zero elements in events'
                       print *,' OR too few useable events'
                       !Skip this file, go to the next catalog file
                       Print *,'SKIP file ',Catfile(i),'and go to next catalog'
                       print *,'*************************************************'
                       print *,'  '
                       print *,'  '
                       go to 9990
                    else
                       !Store index of good file
                       BadMinet(i) = 1
                    end if
                    
                    !Update number of good catalogs
                    GoodMinet = GoodMinet + 1
                    !Number of Events M.gt.xM0 and within time windows is Neq2
                    inev = Neq2

                 
                    ALLOCATE ( CatSubN(Neq2) )
                    
                    !Close original catalog file
                    close (unitcat)
                    !Check Working catalog file: CatWork1 is now unit unitcat=17
                    unitcat = 17
                    rewind (unitcat)
                    read (17) iy
                    do iy = 1, Neq2
                       read (unitcat)  k4,nrup4,m08,mw4
                       nse = nrup4
                       nsenonzeroflag = nsenonzeroflag + nse
                       do inse = 1, nse
                          read (unitcat)  elem4,time8,dur8,slip8,std8
                       end do
                    end do


                       
                    
                    !Read from CatWork1:
                    !number of subelements,Main event M0, Mw, time of main(sec)
                    !Main event seismic Moment (from RSQSim, in Joules)
                    !If iflagev = 1, first line is number of events. Skip it
                    rewind (unitcat)
                    if (iflagev .eq. 1)  then
                       read (unitcat) iy
                       !NOTE:
                       !*****
                       !This number may actually not be the number of events
                       !in the file, if events too small or too late were
                       !removed. (see operation above)
                       !The actual number of events is Neq2
                    end if
                    do iy = 1, Neq2
                        !Main Event #, number of sub-events, M0(joules), Mw
                        !read (unitcat,*) idum,nse,xcretM0,xcretMag
                        read (unitcat,iostat=ierr6) k4,nrup4,m08,mw4
                        if (ierr6 .ne. 0)  then
                           print *,'Problem in CatWork.  iy=',iy
                           stop
                        end if
                        nse = nrup4

                        if (nse .gt. MaxSubElt)  then
                           print *,    &
                           'NsubElt too large in iepis,ispl,inev =', &
                           iepis,ispl,iy
                           stop 'STOP'
                        end if
                        CatSubN(iy) = nse
                        !Skip  subelements
                        Do  inse = 1, nse
                           read (unitcat) elem4,time8,dur8,slip8,std8
                        end do


                    end do



                     !Total number of useable events in simulated catalog
                     !NCatevents(ispl) = Neq2
                     !yearscat = years

                     !Restructure catalog to have no more than MaxSample events
                     !per magnitude bin
                     call MagSamplingRSQ ( ispl, iflagev, MaxMagSampling,    &
                                  nbin, Neq, Neq2, Ntp, unitcat,             &
                                  xMagMin2, xMagMax2, xM0, YearsHist)    

                     !Total number events in CULLED CATALOG for final processing
                     NCatevents(ispl) = Neq
                     yearscat  = years


                     close (unitcat) !unit 17
                     close (19)
                     close (3)
                     deallocate ( CatSubN )



  
                Case (3)

                    !Standard catalogs:
                    !------------------
                    !Case of existing Eqs catalog externally generated
                    !Open existing catalogfile: default name "CatalogTest"

                    !Note on Ground-Motion used for this case:
                    !Ground-Motion set of model indeces is defined by using
                    !source number 1 properties.
                    ! That is:
                    !The set of GM models for use in case of standard catalogs
                    !is defined by properties of source number 1 in the  
                    !source definition file in data block "GM-Assignments"
                    !Typically use J. Douglas and a couple of other models 
                    !with weights to describe the discrete distribution of them.

                    !There are currently 3 Options for standard catalog types:
                    !      Catalog(s) of seismicity with following info:
                           !....................................................
!                          Variables needed from empirical catalog
!                          NCatevents(ispl)       = # events in catalog
!                          LongOriginCat          = Long (dec. deg) of origin
!                          LatOriginCat           = Lat
!                          CatEqTime(ispl,inev)   = time of occurrence (years)
!                          CatEqMag(ispl,inev)    = Magnitude
!                          Xcat,Ycat,Zcat         = Long,Lat,Depth (m)
!                                                   from origin point
!                          CatEqM0(ispl,inev)     = Seismic Moment (dyne.cm)
!                                                   If zero, the it is calculated
!                                                   with your preferred M0 vs Mag
!                                                   relationship
!                           .....................................................
                   !Three Options:
                   !    SC1 = Corrine's Catalog. Data as above but locations in UTM
                   !          Needs coordinates of point of origin and
                   !          Time = Years-decimal
                   !    SC2 = Standard Catalogs with locations in UTM and
                   !          Time = Year/Month/Day/Hour/Min./Sec.
                   !    SC3 = Standard Catalog(s), Locations Long-Lat decimal degrees
                   !          Time = Years-decimal

                    !Select the name of the catalog file:
                    !------------------------------------
                    unitcat = 3
                    i = (iepis-1)*NMspl + ispl
                    Dummy = Catalogf(i)
                    catalogfile = catalogf(i)
                    Dummy = Catalogfile
                    open(unit=3,file=catalogfile,status="UNKNOWN",iostat=IERR)
                    rewind (3)    

                    xMagMax2 = -1000.
                    xMagMin2 =   100.
                    

                    !Select standard catalog case
                    Catalogs_Options: SELECT CASE (SC1)

                    Case (1)
                    !Case of Corrine's catalogs with X, Y coordinates in UTM
                    YearsHist = 25.
                    Do i = 1, 10000000
                        read (3,*,end=300) Ycat,Ycat,Ycat,xMag3
                        !Add Seismic Moment of event to Total M0 budget in N.m
                        xcretM0 = 10. ** (1.5*xMag3 + 9.1)
                        SUMrsqM0 = SUMrsqM0 + xcretM0
                        !Total number of events considerd in cal
                        if (i .eq. MaxEq+1)  then
                            exit
                        end if
                    end do
300                 Neq2 = i-1
                    
                    !Create new catalog with only MaxMagSampling number of
                    !events in each sampling magnitude bin.
                    !Calculate weight to be assigned to each bin

                    !Case of event's coordinates in UTM
                    iCaseCat = 1
                    call MagSampling2 ( iCaseCat, ispl, MaxMagSampling,   &
                                        nbin, Neq, Neq2, Ntp, unitcat,    &
                                        xMagMin2, xMagMax2, xM0, YearsHist)
                                      

                    !Total number of events in the catalog
                    Neq2 = Neq
                    NCatevents(ispl) = Neq2
                    yearscat = YearsHist


                    Case (2)
                    !Case of NCEDC Catalog with dates in Time/day/month/year
                    !-------------------------------------------------------
                    MoisJour(1)  = 31
                    MoisJour(3)  = 31
                    MoisJour(5)  = 31
                    MoisJour(7)  = 31
                    MoisJour(8)  = 31
                    MoisJour(10) = 31
                    MoisJour(12) = 31
                    MoisJour(2)  = 28
                    MoisJour(4)  = 30
                    MoisJour(6)  = 30
                    MoisJour(9)  = 30
                    MoisJour(11) = 30
                    SecondesAnnee= 86400. * 365.
                    do i = 1, 18    !Skip 20 lines
                        read (3,*) Dummy
                    end do
                    Neq2 = 13988
                    ixM0 = Neq2
                    read (3,542) Annee1,Mois,Jour,Heure,Minutes,Secondes,   &
                    Ycat,Xcat,Zcat,xMag3
                    SecondesStart =  Secondes + Real(Minutes)*60. +         &
                    Real(Heure)*3600.
                    nJourStart    = Jour - 1
                    if (Mois .gt. 1)  then
                        do i =  1, Mois-1
                            MJS = MoisJour(i)
                            if ((MOD(Annee1,4).eq.0) .and. (i.eq.2)) then
                                MJS = 29
                            end if
                            nJourStart = nJourStart + MJS
                        end do
                    end if
                    TimeStart1 = SecondesStart + REAL(nJourStart)*86400.
                   
                    do i = 2, Neq2
                        read (3,542) Annee2,Mois,Jour,Heure,Minutes,       &
                        Secondes,Ycat,Xcat,Zcat,xMag3
542                     Format (i4,1x,i2,1x,i2,1x,i2,1x,i2,1x,f5.2,2f10.4,2f7.2)
                        SecondesStart =  secondes + Real(Minutes)*60. +    &
                        Real(Heure)*3600.
                        nJourStart    = Jour - 1
                        if (Mois .gt. 1)  then
                            do j =  1, Mois-1
                                MJS = MoisJour(j)
                                if ((MOD(Annee2,4).eq.0) .and. (j.eq.2)) then
                                    MJS = 29
                                end if
                                nJourStart = nJourStart + MJS
                            end do
                        end if
                        TimeStart2 = SecondesStart + REAL(nJourStart)*86400.
                        nAnnees   = Annee2 - Annee1
                        !Number of leap years between the two dates
                        nLeap     = (nAnnees-1) / 4
                        !Add one if Annee1 is leap.
                        if (MOD(Annee1,4) .eq. 0)  then
                            nLeap = nLeap + 1
                        end if
                        nJourHist = (nAnnees*365) + nLeap
                        time1     = TimeStart2 + REAL(nJourHist)*86400.  - &
                                  TimeStart1  
                        CatEqTime(ispl,i) = time1 / (3600.*24.*365.)
                        CatEqMag(ispl,i) = xMag3
                        Xrsq(ispl,i) = Xcat    
                        Yrsq(ispl,i) = Ycat 
                        Zrsq(ispl,i) = Zcat                   
                    end do

                    !Total duration of Catalog
                    YearsHist = time1 / SecondesAnnee
                    NCatevents(ispl) = Neq2
                    yearscat = YearsHist
                    print *, "N Eq=",Neq2,"      N Years=",YearsHist

                    !Seismic Moment in N.m:
                    xxx  = 10.**(1.5*xMag3+9.1)
                    !Converted in dynes.cm
                    CatEqM0(ispl,i) = xxx * 1.e07

                    !Min and Max magnitudes in this catalog
                    if (CatEqMag(ispl,i) .gt. xMagMax2)  then
                       xMagMax2 = CatEqMag(ispl,i)
                    end if
                    if (CatEqMag(ispl,i) .lt. xMagMin2)  then
                       xMagMin2 = CatEqMag(ispl,i)
                    end if

                    !Add Seismic Moment of event to Total M0 budget in N.m
                    xcretM0 = 10. ** (1.5*xMag3 + 9.1)
                    SUMrsqM0 = SUMrsqM0 + xcretM0


                    Case (3)
!                   Case of Good-Old-Standard Catalog:
                    !---------------------------------
                    read (3,*) Dummy
                    read (3,*) Neq2,YearsHist
                    read (3,*) LongOriginCat,LatOriginCat
                    ixM0 = Neq2
              
                    print *, "N Eq=",Neq2,"      N Years=",YearsHist
                    NCatevents(ispl) = Neq2

                    read (3,*) Dummy
                    do i = 1, Neq2

                       read (3,*)idum, Xcat,Ycat,Zcat,xMag3,time1
                       CatEqMag(ispl,i) = xMag3

                       !Time of occurrence: convert seconds to years.
                       CatEqTime(ispl,i) = time1 / (3600.*24.*365.)

                       !Coordinates of events in problem coordinates
                       Ycat = LatOriginCat + Ycat / (rdegree*6.283185/360.)
                       Xrsq(ispl,i) = LongOriginCat + (Xcat / &
                       (rdegree*cos(Ycat)*6.283185/360.))
                       Yrsq(ispl,i) = Ycat 
                       Zrsq(ispl,i) = Zcat



                       !Seismic Moment in N.m:
                       xxx  = 10.**(1.5*xMag3+9.1)
                       !Converted in dynes.cm
                       CatEqM0(ispl,i) = xxx * 1.e07

                       !Min and Max magnitudes in this catalog
                       if (CatEqMag(ispl,i) .gt. xMagMax2)  then
                          xMagMax2 = CatEqMag(ispl,i)
                       end if
                       if (CatEqMag(ispl,i) .lt. xMagMin2)  then
                          xMagMin2 = CatEqMag(ispl,i)
                       end if

                       !Add Seismic Moment of event to Total M0 budget in N.m
                       xcretM0 = 10. ** (1.5*xMag3 + 9.1)
                       SUMrsqM0 = SUMrsqM0 + xcretM0

                    end do

                    Case Default

                    END SELECT Catalogs_Options

                    close (3)

                    !Set time of perturbation to zero for this case.
                    Timekeep1 = 0.
                    print *,"Min & Max Magnitudes =",xMagMin2,xMagMax2
                    print *,"Time last event      =", CatEqtime(ispl,Neq2) 
                    write (2,*) 'First 10 sets of values in CatalogTest'
                    do jrs = 1,10
                       print *, 'Mag(',jrs,') =',CatEqMag(ispl,jrs),  &
                       "     Time=", CatEqtime(ispl,jrs)
                    end do
                    print *, 'End tail 10 sets of values in CatalogTest'
                    do jrs = Neq2-10, Neq2
                       print *, 'Mag(',jrs,') =',CatEqMag(ispl,jrs),  &
                       "     Time=", CatEqtime(ispl,jrs)
                    end do

                Case Default

                END SELECT

                 
                !One more aleatory simulation
                !----------------------------
                CALL Dattim (idatex,itimex)
                read (itimex,'(i2,1x,i2,1x,i2)') ihh,imm,isec
                T2 = Float(3600*ihh + 60*imm + isec)
                CPUtime     = T2 - T1
                if (Printdetails .gt. 0)  then
                    write (6,'(i10,i10,7x,f10.2)') ispl,inev,CPUtime
                endif
                T1 = T2
                
                !Skip Catalog for having zero events sub-elements or is empty
9990            continue
                
                end do Loop3_Sampling_Size  ! ispl

        


                !FOR ALL TYPES OF CATALOGS (G-R, RSQSim or standard catalogs) 
                !************************************************************   
                !New cropped and scaled catalog:
                !*******************************
!                   -Create new cropped and scaled catalog
!                   -Index of first event  after perturbation, in new catalog 
!                   -Index of start and End of each time-window in new catalog

                !Skip this part if there are no good sampling catalogs for this
                !epistemic simulation.
                if ((GoodMinet.lt.1) .and. (iStd_Dieterich.eq.2))  then
                    !Go to the next epistemic simulation
                    go to 9995
                end if

         print *, 'AVGLTM0 =',AVGLTM0   !dynes.cm



                !Scaling factor of Seismic Moment of NMspl catalogs:
                !---------------------------------------------------   
                !Default scaling for standard input catalogs is 1.
                xxx = 1.
                !For allRecurrence or RSQSim simulations it is calculated to
                !match total regional seismic moment to Long-Term tectonic M0
!                if (iStd_Dieterich .lt. 3)  then
                    !Average regional M0 per aleatory catalog 
                    !(AVGrsqM0 is in Joules. Factor of 1.e7))
                    AVGrsqM0 = (1.e7*SUMrsqM0) / Float(NMspl)
                    !Scaling M0 for aleatory catalogs in epistemic set iepis
                    if (AVGrsqM0 .gt. 0)  then
                       xxx = AVGLTM0 / AVGrsqM0
                    end if
!                end if        

                !If scale is set in menu file by a non-zero value use that.
                !Otherwise, if value in menu file is zero, use calculated scale
                scaleM0(iepis) = xxx
                xxxM0          = xxx
                if (scaleM0forced .ge. 1.e-5) then
                    scaleM0(iepis) = scaleM0forced 
                end if 
                xxx = scaleM0(iepis)
                wrsqepi(iepis) = xxx
                print *, '  Scaling epistemic set for iepis=',iepis, ' is:', xxx



!               PLot calibration using full catalog, before cropping:
                !----------------------------------------------------
                if (Plotcalibration .eq. 1)  then

                    !Index of first event past perturbation before cropping:
!                   --------------------------------------------------------
                    do ispl = 1, NMspl
                        Nev = NCatevents(ispl)
                        do i = 1, Nev
                        TimeKeep1 = CatEqTime(ispl,i)
                            if (TimeKeep1.ge.TRelax)  then
                                exit
                            end if
                        end do
                        nRelax(ispl) = i 
                    end do        

!                   Plot simulations and Long-Term regional seismicity
!                   --------------------------------------------------
                    !This for original catalgs with all events before cropping.
                    printunit  = 6  !unit for writing recurrence curve results 
                    NpageCalib = 1
                    Nyears2    = Nyears    
                   call PlotRegional ( CSTR1, iepis, xxx, nbin, NMspl, Nscreen,&
                                      Ntp, NpageCalib, TrackNumber, yearscat )
                                  
                end if
                       


                !Cropping and magnitude/Moment scaling of catalogs:
                !--------------------------------------------------
                do ispl = 1, NMspl
                    Nev1  = NCatevents(ispl)
                    FLAG_Mag_Moment = 1
                    if (istd_Dieterich .eq. 2)  then
                        Flag_Mag_Moment = 2
                     end if

                    call Scaled_Catalog (Flag_Mag_Moment, ispl, MaxEqINPUT,  &
                    xM0, Nev1, Nev2, xxx, xMagMax2, xMagMin2, Years)   



                    print *,'------------------------------------------------'
                    if (istd_Dieterich .eq. 1)  then
                       print *,'  Use PSHA zoning simulated Catalogs:'
                    end if
                    if (istd_Dieterich .eq. 2)  then
                       print *,'  Use RSQSim simulated catalog:'
                    end if
                    print *,'  Epistemic simulation  :',iepis
                    print *,'  Aleatory sample number:',ispl 
                    if (istd_Dieterich .eq. 2)  then 
                      i = ispl + (NMspl*(iepis-1))
                      print *,'  Catalog data file     :  ',Catfile(i)
                      print *,'  Elements data file    :  ',Faultfile(i)
                    end if 
                    print *,'  Total events read     :',Nev1
                    print *,'  Low Magnitude cutoff  :',xM0
                    print *,'  M0 scaling calculated :',xxxM0
                    print *,'  M0  scaling used      :',scaleM0(iepis)
                    print *,'  Events retained > M0  :',Nev2
                    print *,'  Minimum Magnitude in cropped Catalog:',xMagMin2 
                    print *,'  Maximum Magnitude in cropped Catalog:',xMagMax2
  
               end do  !ispl

                !Update nRela() for cropped catalog:
!               ------------------------------------
                do ispl = 1, NMspl
                Nev = NCatevents(ispl)
                do i = 1, Nev
                    TimeKeep1 = CatEqTime(ispl,i)
                    if (TimeKeep1.ge.TRelax)  then
                        exit
                    end if
                end do
                nRelax(ispl) = i 
                end do


               !Determine indeces of events in cropped catalog Time-Windows:
               !------------------------------------------------------------
               do ispl = 1, NMspl
                   !Perform window sorting
                  call Time_Window ( ispl, Nev, Ntp, XB )
                  !Update array of information on status of Time Windows
                  do itp = 1, Ntp
                     Nonzerotp2(iepis,itp) = Nonzerotp1(itp)
                  end do
               end do
      



 
!******************************************************************************
!
!........CALCULATE THE HAZARD FOR THIS EPISTEMIC SIMULATION
!
!       Use the catalog (Standard or Dieterich) and calculate the resulting
!       ground motion, followed by consequences (to be implemented later).
!       At this point, all the aleatory uncertainty has been account in the
!       occurrence rate and spatial distribution of earthquakes by using the
!       simulated catalogs.
!       Remaining aleatory to be considered is that in the ground-motion
!       estimates.
!       Using attenuations: aleatory uncertainty governed by sigma (see GMHR)
!       Using SYNHAZ: variability in the physical parameters (see GMHR2)
     
        SELECT CASE (iATN_Larry)
        
            CASE (1)
                if (istd_Dieterich .eq. 1)  then
                    Yearshist = Years
                end if
                !Use Standard Attenuation relationships
                !Calculate the Ground Motion at each point
                !Calculate histogram of GM, per GM bin.
                !Then calculate Hazard curves
                call GMHR (                                                    &
                           Catdepth1, Catdepth2, CSTR1, CSTR2, iATN_Larry,     &
                          iepis, MaxEqINPUT, maxnodl, maxnodw, Mxstrp, nbin,   &
                          NcritRD, nGMbins, nMspl, Nscreen, pagenumber2,       &
                          PlotHazEpis, TrackNumber, xLmax, xM0, YearsHist)
                               
            CASE (2)
                !Use SYNHAZ EQ simulation code
                !Rigidity needed in dynes/cm2 for synhaz
                rigidity2 = rigidity 
                call GMHR2                                                     &
                     (CatAzim1, CatAzim2, BadMinetFlag,cigar, CSTR1,CSTR2,     &
                     damp, Catdepth1,CatDepth2, CatDip1, CatDip2, freq,        &
                     GMoutMax, GMoutMin, iATN_Larry, iepis, INgm,              &
                     MaxEqINPUT, maxnodl, maxnodw, Mxstrp, nbin, NcritRD,Nepis,&
                     nfreq, nGMbins, nMspl, Nscreen, OUTgm, pagenumber2,       &
                     PlotHazEpis,CatRake1, CatRake2, rigidity2, CatStr2,       &
                     CatStr1, synflag, synflagRS,TrackNumber,CatVs1,CatVs2,    &
                     xLmax,xM0,Years)
                if (BadMinetFlag .eq. 1)  then !Case of good catalog
                   BadMinet(iepis) = 1
                end if

            CASE DEFAULT
       
        END SELECT
!.......End calculating Hazard
        
        if (iStd_Dieterich .eq. 2) then
            DEALLOCATE ( XYZsub )
        end if

!******************************************************************************
            
            
        !One more Epistemic simulation completed
        if (Printdetails .gt. 0)  then
            write (6,*)'End of Epistemic Simulation:',iepis              
        endif

9995    continue                                  
        END DO LOOP1_Epistemic_Simulation 

        !Check how many good catalogs before hazard calculations:
        !--------------------------------------------------------
        if ((iStd_Dieterich.eq.1) .or. (iStd_Dieterich.eq.3))  then
           GoodMinet = NMspl*Nepis
           
        else
             
           !Number of good catalogs: RSQsim generated catalogs (iStd_Dieterich=2)
           GoodMinet = 0
           do i = 1, Nepis*NMspl
              if (BadMinet(i) .eq. 1)  then
                 GoodMinet = GoodMinet + 1
              end if
           end do
           
           !If no good catalog for the entire run, then abort the run          
           if (GoodMinet .lt. 1)  then
              print *,'  '
              print *,'  ***********************************************'
              print *,'  There are NO GOOD Catalogs in this run'
              print *,'  '
              print *,'  RUN IS ABORTED. Check files'
              print *,'  '
              print *,'  List of Bad Catalog files:'
              do i = 1, Nepis*NMspl
                 if (BadMinet(i) .eq. 0)  then
                    print *,'     ',Catfile(i)
                 end if
              end do
              print *,'  '
              print *,'  ***********************************************'
              !No good RSQsim catalogs
              STOP              
           end if

        end if
        
        
        deallocate ( work4SPEC )
        deallocate ( wMagbin )

!*******************************************************************************
!*******************************************************************************
!
!    SUMMARY HAZARD STATISTICS FOR EPISTEMIC SIMULATIONS
!        FOR THIS RUN SET ONLY (Task=0), and/or
!        FOR EPISTEMIC RESULTS STORED IN FILE "StoreHaz"
!
!    ***************************************************************************

        !If Task=3, no PSHA calculations are performed
        !All calculations are skipped from first epistemic loop
        else
           Nepis = Nepis3
           do j = 1, Nepis
              WeightEpi(j) = 1. / Float(Nepis)
           end do  
           
        end if Task_3_No_Calculations


        
        !Allocate memory for percentile calculations
        ALLOCATE ( perctlSPEC(NcritRD,nGMbins,Nperc+4,nfreq) )
        
        !Normalize the epistemic weights:
        sum = 0.
        do i = 1, Nepis
            sum = sum + weightEpi(i)
        end do
        print *,'  '
        print *,'  Weight of the Epistemic simulations:'
        print *,'  Simulation   Weight'
        write (2,*) '  '
        write (2,*) '  Weight of the Epistemic simulations:'
        write (2,552) 
        do i = 1, Nepis
            weightEpi(i) = weightEpi(i) / sum
            print *,i,weightEpi(i)
        end do
        print *,'  '
        write (2,*) '  '
        
!       If task.eq. 1 or 2 store Hazard results on file "StoreHaz" :
!       ------------------------------------------------------------
        if ((Task.eq.1) .or. (Task.eq.2))  then
           open (unit=21,file=StoreHaz,status="unknown",iostat=IERR)
           rewind (21)
           !Skip Naccumlines lines
           do j = 1, Naccumlines
              read (21,*) string1
           end do


           do j = 1, Nepis
              Txtdum = Catfile(j)
              write (21,602) TrackNumber,Nepis,Ntp,NcritRD,   &
              nfreq,nGMbins,Txtdum
              write (21,*) (tlife(i),i=1,Ntp)
              write (21,572) (GMarray(i),i=1,nGMbins)              
              write (21,582) (Nonzerotp2(j,itp),itp=1,Ntp)
              do itp = 1,Ntp
                 !Testing for >1 events in time window
                 !Skip this itp cycle. Zero events in the time window.
                 if (Nonzerotp2(j,itp) .gt. 0)  then                
                    !Loop over number of sites
                    do icrt = 1, NcritRD  
                       !Store nGMbins values
                       if (nfreq .eq. 1)  then
                          write (21,572) (Haz1(icrt,i,j,itp),i=1,nGMbins)
                       else
                          do ifreq = 1, nfreq
                             write (21,572)   &
                             (Haz1SPEC(icrt,i,j,itp,ifreq),i=1,nGMbins)
                          end do
                       end if
                    end do  
                 end if
              end do
           end do
        end if
        close (21)

              
!       Define percentile curves for seismic Hazard
        !------------------------------------------
        !Default percentiles are:
        !         perctl(1) = standard deviation
        !               (2) = mean
        !               (3) = Min hazard value
        !               (4) = Max hazard value
        !               (5)to (Nperc+4)) = aperc(1 to Nperc)
        !                     as read from Menu file
 
        do iperc = 1, Nperc
            perc(iperc) = aperc(iperc)
        end do

        xYears  = Float (Nyears) 
        xEpis   = Float (Nepis)

        !Load hazard results from file 'StoreHaz'
        !****************************************
        if (Task .ge. 1)  then
           !PSHA statistics are calculated with data stored in file "StoreHaz"
           !including those stored from this last set of epitemic calculations
           !first load all the epistemic realizations of PSHA curves
           !Nepis2 =Number of epistemic simulations stored for each run
           !Nepis is updated to add all run sets: Nepis = SUM(all Nepis2)

           open (unit=21,file=StoreHaz,status="unknown",iostat=IERR)
           rewind (21)
           !First, need to determine total number of simulations for
           !dimensionning working arrays Haz1, Haz1SPEC.
           !If task=3, this has already been done at the beggining of the run
           if (task .lt. 3)  then
              Nepis3 = 0
              do iNaccum = 1, 100000
                 read (21,602,iostat=IERR)    &
                 idum,Nepis2,Ntp,NcritRD,nfreq,nGMbins,Txtdum
                 if (IERR .ne. 0)  then
                    exit
                 else
                    Nepis3 = Nepis3 + 1
                    read (21,*) (tlife(i),i=1,Ntp)
                    read (21,572) (GMarray(i),i=1,nGMbins)
                    read (21,582) (Nonzerotp1(itp),itp=1,Ntp)
                    do itp = 1,Ntp
                       !Skip itp cycle if Zero events in the time window.
                       if (Nonzerotp1(itp) .gt. 0)  then                
                          !Loop over number of sites
                          do icrt = 1, NcritRD
                             do ifreq = 1, nfreq
                                j1 = nGMbins/10
                                j2 = j1
                                j1 = nGMbins - (j1*10)
                                if (j1 .gt.0)  then
                                   j2 = j2 + 1
                                end if
                                do j1 = 1, j2
                                   read (21,*) xdum
                                end do
                             end do
                          end do  
                       end if
                    end do
                 end if
              end do
           end if

           Naccum = Nepis3

           DEALLOCATE ( Nonzerotp2 )
           ALLOCATE   ( Nonzerotp2(Naccum,Ntp) )
           ALLOCATE   ( TrackID(Naccum) )
           if (nfreq .eq. 1)  then
              DEALLOCATE ( Haz1 )
              ALLOCATE   ( Haz1(NcritRD,nGMbins,Naccum,Ntp) )
           else
              DEALLOCATE ( Haz1SPEC )
              ALLOCATE   ( Haz1SPEC(NcritRD,nGMbins,Naccum,Ntp,nfreq) )
           end if

           rewind (21)

           if (Task .ne. 3)  then
              deallocate ( Catfile )
           end if
           ALLOCATE ( Catfile(Naccum) )

           do iNaccum = 1, Naccum
              read (21,602,iostat=IERR) idum,Nepis2,Ntp,NcritRD,   &
                   nfreq,nGMbins,Txtdum
                   Catfile(iNaccum) = Txtdum
              TrackID(iNaccum) = idum
              read (21,*) (tlife(i),i=1,Ntp)
              read (21,*) (GMarray(i),i=1,nGMbins)
              read (21,*) (Nonzerotp2(iNaccum,itp),itp=1,Ntp)
              do itp = 1, Ntp
                 !Skip this itp cycle if Zero events in the time window.
                 if (Nonzerotp2(iNaccum,itp) .gt. 0)  then                
                    !Loop over number of sites
                    do icrt = 1, NcritRD  
                       !Read nGMbins values of Hazard
                       if (nfreq .eq. 1)  then
                          read (21,*) (Haz1(icrt,i,iNaccum,itp),i=1,nGMbins)
                       else
                          do ifreq = 1, nfreq
                             read (21,*)   &
                             (Haz1SPEC(icrt,i,iNaccum,itp,ifreq),i=1,nGMbins)
                          end do
                       end if  
                    end do
                 end if
              end do
           end do
           
        end if
        close (21)

        !Update actual number of epistemic realization to be processed
        if (Task .gt. 0) then
           Nepis = Naccum
        end if
        xEpis   = Float (Nepis)



           
        !Build CDF of Hazard for a given site for each Hazard time window
        !----------------------------------------------------------------
        allocate ( Hazwin3(nGMbins-1,Ntp,NcritRD,Nperc) )
        !Loop over number of Hazard time windows:
        Loop_Time_Window1: do itp = 1, Ntp
     
        !Loop over number of sites
    do icrt = 1, NcritRD
        
        do i = 1, nGMbins

            do ifreq = 1, nfreq
          
                !Loop over all epistemic realizations of hazard estimates
                if (ifreq .eq. 1)   then
                    !Case of Peak value
                    sum   = 0.
                    EX1   = 0.
                    EX2   = 0.
                    xhMin =  1.e10
                    xhMax = -1.e10
                    do j = 1, Nepis
                        !Test if any events in time window
                        if (Nonzerotp2(j,itp) .gt. 0)  then
                           !Calculate mean, sigma, min and max hazard
                           x1 = Haz1(icrt,i,j,itp)
                           EX1 = EX1 + x1
                           EX2 = EX2 + (x1*x1)
                           xhMin = AMIN1 (xhMin, x1)
                           xhMax = AMAX1 (xhMax, x1)
                        end if
                    end do                
                    EX1 = EX1 / xEpis
                    EX2 = EX2 / xEpis
                    x1 = EX2 - (EX1*EX1)
                    x1 = SQRT ( AMAX1(0.,x1 ) )                
                    perctl(icrt,i,1) = x1       !Sigma
                    perctl(icrt,i,2) = EX1      !Mean
                    perctl(icrt,i,3) = xhMin    !Min
                    perctl(icrt,i,4) = xhMax    !Max
                end if

                !Case of Response Spectra
                if (synflagRS .gt. 0)  then
                    sum   = 0.
                    EX1   = 0.
                    EX2   = 0.
                    xhMin =  1.e10
                    xhMax = -1.e10                
                    do j = 1, Nepis
                        if (Nonzerotp2(j,itp) .gt. 0)  then
                           !Calculate mean, sigma, min and max hazard
                           x1 = Haz1SPEC(icrt,i,j,itp,ifreq)
                           EX1 = EX1 + x1
                           EX2 = EX2 + (x1*x1)
                           xhMin = AMIN1 (xhMin, x1)
                           xhMax = AMAX1 (xhMax, x1)
                        end if
                    end do                
                    EX1 = EX1 / xEpis
                    EX2 = EX2 / xEpis
                    x1 = EX2 - (EX1*EX1)
                    x1 = SQRT ( AMAX1(0.,x1 ) )                
                    perctlSPEC(icrt,i,1,ifreq) = x1       !Sigma
                    perctlSPEC(icrt,i,2,ifreq) = EX1      !Mean
                    perctlSPEC(icrt,i,3,ifreq) = xhMin    !Min
                    perctlSPEC(icrt,i,4,ifreq) = xhMax    !Max
                end if
           
                    
                !Calculate percentiles. 
                !----------------------
                !Load vector of Hazard values into srt1
                allocate ( srt1(Nepis) )
                allocate ( srt2(Nperc) )

                !CAse of single peak value
                if (ifreq .eq. 1)  then
                    do j = 1, Nepis
                        srt1(j) = Haz1(icrt,i,j,itp)
                    end do  
                    print *, '  '             
                    call Percentiles (Nepis,Nperc,perc,srt1,srt2,weightEpi) 
                    do j = 1,Nperc
                       perctl(icrt,i,j+4) = srt2(j)
                    end do
                end if

                !Case of Response Spectra
                if (synflagRS .gt. 0)  then
                    do j = 1, Nepis
                        srt1(j) = Haz1SPEC(icrt,i,j,itp,ifreq)
                    end do  
                    print *, '  '             
                    call Percentiles (Nepis,Nperc,perc,srt1,srt2,weightEpi) 
                    do j = 1,Nperc
                        perctlSPEC(icrt,i,j+4,ifreq) = srt2(j)
                    end do
                end if

            
                deallocate ( srt1 )
                deallocate ( srt2 )

            end do    !......................Loop number of frequencies

        end do        !...................Loop number of magnitude bins


!*******************************************************************************
!************** U H S **********************************************************

        if (synflagRS .gt. 0)  then
        !Construct the UHS (Uniform Hazard Spectra) for each percentile
        !--------------------------------------------------------------

                !print *, '  '
                !print *, '  '
                !print *, ' GM         Haz RP1     Haz RP1      haz RP3       ',&
                !'haz RP4        haz RP5'


!Case of the Mean UHS (j=2)
!--------------------------
            j = 2
            x1 = perc(1)
            x2 = perc(3)
            xdum = perc(5)

            !Load the array of hazard nfreq curves, into array 
            do i = 1, nGMbins
                do ifreq = 1, nfreq
                    Haz2(i,ifreq) = perctlSPEC(icrt,i,j,ifreq)   
                end do
                !print *, GMarray(i),Haz2(i,10)
            end do

            !Calculate UHS for Return Period irp and time-window itp
            do irp = 1, Nrp
                rp1 = RP(irp)
                call UHSpec (GMarray,Haz2,nfreq,nGMbins,rp1,UHS)
                do ifreq = 1, nfreq
                    UHSRP(irp,j,ifreq) = UHS(ifreq)
                end do
            end do

#ifdef USE_DISLIN      
       !Plot the Mean Spectra
       ix         = 1
       IxSCL      = 0  ! = 1, X axis is linear, 0 for logarithmic
       Iy1        = 1
       Namelegends(1)  = 'Acc. (cm/s/s)'
       Nc         = Nrp
       Nview      = 1
       NWIDTH1    = 15 

       !Mean: j = 2 , Nrp mean spectra per plot 
       !---------------------------------------
       ALLOCATE ( Xplot(nfreq), Yplot(nfreq,Nrp) )       
       CTIT       = '  Mean UHS   ' 
       do i = 1, Nrp
           irp = INT(RP(i))
           write (NameLegends(i),"(a5,i7,a4)") 'RP = ',irp,' yrs'
       end do
       j = 2
       do i = 1, nfreq
           Xplot(i)   = freq(i)
           do irp = 1, Nrp
               Yplot(i,irp) = UHSRP(irp,j,i)
           end do
           !print *, 'i=',i,'  Xplot(i)=',Xplot(i),  &
           !         'Yplot(i)=',(Yplot(i,irp),irp=1,Nrp)
       end do
                    
       call PlotRespSpec (CTIT,Ix,IxSCL,Iy1,NameLegends,Nc,nfreq,Xplot,Yplot)
       deallocate ( Yplot )
#endif


!Case of Percentiles UHS: Nperc Percentiles per plot for One RP at a time
!------------------------------------------------------------------------
       ALLOCATE ( Yplot(nfreq,Nperc) )

       Return_Periods: do irp = 1, Nrp

       !Load nfreq arrays of percentile hazard curves, into array 
       do iperc = 1, Nperc
            do i = 1, nGMbins
                do ifreq = 1, nfreq
                    Haz2(i,ifreq) = perctlSPEC(icrt,i,iperc+4,ifreq)   
                end do
                !print *, GMarray(i),Haz2(i,10)
            end do

            !Calculate UHS for all percedntiles
            do j = 1, Nperc
                rp1 = RP(j)
                call UHSpec (GMarray,Haz2,nfreq,nGMbins,rp1,UHS)
                do ifreq = 1, nfreq
                    UHSRP(irp,j,ifreq) = UHS(ifreq)
                end do
            end do
       end do

#ifdef USE_DISLIN  
       write (charac1,"(F7.0,a6)") RP(irp),' years'
       CTIT       = charac1 
       Nc = Nperc
       do i = 1, Nperc
           if (ABS(perc(i)-.5) .lt. .01)  then
               charac = '  Median  UHS   '  
           else  
               idum = INT (perc(i)*100.) 
               write (charac,"(i3,a13)") idum,' Percent UHS'
           end if
           NameLegends(i) = charac
       end do

       do i = 1, nfreq
           Xplot(i)   = freq(i)
           do j = 1, Nperc
               Yplot(i,j) = UHSRP(irp,j,i)
           end do
           print *, 'i=',i,'  Xplot(i)=',Xplot(i),  &
                    'Yplot(i)=',(Yplot(i,j),j=1,Nperc)
       end do
                    
       call PlotRespSpec (CTIT,Ix,IxSCL,Iy1,NameLegends,Nc,nfreq,NVIEW,   &
                        Xplot,Yplot)


                                  
#endif 

       end do Return_Periods

       deallocate ( Xplot, Yplot ) 

       end if
             
   end do   !......................................Loop number of sites


        !***********************************************************************
        !Print percentile  Hazard Curves for time window itp:
        !***********************************************************************

        !********************
        !Write on Output file:
        !---------------------
        tp1 = 0.
        tp2 = tlife(itp)
        if (itp .gt. 1)  then
            tp1 = tlife(itp-1)
        end if
        write (2,232) Nepis, itp, tp1,tp2
        do icrt = 1, NcritRD
            Dummy = SiteName(icrt)
            write (2,162) Dummy
            write (2,172) (INT(100.*perc(i)),i=1,Nperc)
            do i = 1, nGMbins
                X(1) = perctl(icrt,i,2)  !H-Mean
                X(2) = perctl(icrt,i,1)  !Standard Deviation
                X(3) = perctl(icrt,i,3)  !H-Minimum
                X(4) = perctl(icrt,i,4)  !H-Maximum
                !H-Percentiles
                do j = 1,Nperc
                    X(j+4) = perctl(icrt,i,j+4)
                end do
                write (2,222) GMarray(i),(X(j),j=1,Nperc+4)
            end do     
        end do 
               
       !*******************************************
       !Display Percentile Hazard Curves on screen:
       !-------------------------------------------
        !Top of figure titles
        if (itp .eq. 1)  then
            x2  = tlife(1)
            ix4 = INT ( x2 )
            ix2 = INT (alog10(x2)) 
            write (fmt1,"(a6,i1,a3)") '(a18,i',ix2+2,'a6)'
            write (Mainline3(1),fmt1) 'Time window = 0 to',ix4,' Years'
        else 
            x1  = tlife(itp-1)
            x2  = tlife(itp)
            ix3 = INT ( x1 )
            ix4 = INT ( x2 )
            ix1 = INT (alog10(x1)) 
            ix2 = INT (alog10(x2))  
            write (fmt1,"(a6,i1,a4,i1,a3)") '(a11,i',ix1+2,'a3,i',ix2+2,'a6)'
            write (Mainline3(itp),fmt1)  &
            'Time window',ix3,' to',ix4,' Years'
        end if

        !Set titles of Hazard figures in plots
        if (synflag .eq. 1)  then
            TitleX     = 'Acceleration (cm/s/s)'
        elseif (synflag .eq. 2) then
            TitleX     = '   Velocity (cm/s)   ' 
        else
            TitleX     = '  Displacement (cm)  '            
        end if
       
        TitleY     = 'Probability of Exceedance'
        LegendTitle     = "Percentiles: "
        Namelegends(1)  = 'Mean'
        do i = 1, Nperc
            x1 = aperc(i)*100.
            write (Namelegends(i+1),"(i2,'%')") INT(x1)
        end do
        IxSCL      = 0
        jwdt       = 1
        N          = nGMbins-1
        Nc         = Nperc + 1
        Nf         = maxGMbins 
        Nview      = 1   
        pagenumber1= 1
        pagenumber2= 1

        do icrt = 1, NcritRD
            write (MAINTITLE,"(a22,i2)") "SEISMIC HAZARD - Site:",icrt
            !Load arrays
            do i = 1, nGMbins-1
                X(i)   = GMarray(i+1)
                Y(i,1) = perctl(icrt,i+1,2)
                do j = 1, Nperc
                    Y(i,j+1) = perctl(icrt,i+1,j+4)
                end do
            end do

            !Load Mean Hazard per Time window in array Hazwin
            do i = 1,nGMbins-1
                HazWin(i,itp,icrt) = Y(i,1) 
                !Load percentile hazard curves in Hazwin3
                do j = 1,Nperc
                    Hazwin3(i,itp,icrt,j) = Y(i,j+1)
                end do
            end do 
 
#ifdef USE_DISLIN     
            if ((PlotScreen.eq."Display-Screen")  .and.   &
                (PlotHazEpis.gt.0))  then 
                call PlotHazard (CSTR1,CSTR2,IxSCL,jwdt,LegendTitle,   &
                MAINTITLE,Mainline3(itp),Nameplotfile,NameLegends,     &
                Nc,Nf,NVIEW,pagenumber1,TitleX,TitleY,X,Y) 
            end if
#endif

        end do !End icrt loop

        End Do Loop_Time_Window1
     

        
        !********************************************
        !Write Percentile Hazard Curve  on PDF files:
        !--------------------------------------------
        Ftype    = 'PRC' 
        NameEXT  = 'PDF '
        IxSCL    = 0      !X-axis is logarithmic
        Nview    = 2      !for PDF
        call CreateFileName (Ftype,ProblemID,TrackNumber,NameEXT,Nameplotfile)

        Loop_Time_Window2: Do itp = 1, Ntp 

        !Testing for >1 events in time window
        !Skip this tp cycle if the number of events in the time window < 2.
        if (Nonzerotp1(itp) .gt. 0)  then
        
            do icrt = 1, NcritRD
                Dummy = SiteName(icrt)
                write (MAINTITLE,"(a15,a20)") "SEISMIC HAZARD ",Dummy
                !write (2,162) Dummy 
                pagenumber1 = (itp-1)*5 + icrt
                !Load arrays
                do i = 1, nGMbins-1
                    X(i)   = GMarray(i+1)
                    Y(i,1) = HazWin(i,itp,icrt)
                    do j = 2, Nperc+1
                        Y(i,j) = Hazwin3(i,itp,icrt,j-1)
                    end do
                end do

#ifdef USE_DISLIN
                call PlotHazard (CSTR1,CSTR2,IxSCL,jwdt,LegendTitle,  &
                MAINTITLE,Mainline3(itp),Nameplotfile,NameLegends,    &
                Nc,Nf,NVIEW,pagenumber2,TitleX,TitleY,X,Y) 
#endif

            end do !end icrt loop
        end if  !Skipping of time windows with zero events
        End Do Loop_Time_Window2
#ifdef USE_DISLIN   
        CALL DISFIN()
#endif
        deallocate ( Hazwin3 )


        !***********************************************************************
        ! Mean Hazard Curves for all Hazard Time Windows:
        !***********************************************************************

        !*************************************
        !Display Mean Hazard curves on screen:
        !-------------------------------------
        pagenumber1 = 0
        pagenumber2 = 0
        do icrt = 1, NcritRD
            Ftype       = 'MNH'
            IxSCL       = 0  !X-axis is logarithmic
            Nview       = 1  !for CONS
            pagenumber1 = pagenumber1 + 1
            MAINTITLE   = 'MEAN HAZARD CURVES'
            Mainline2 = SiteName(icrt)
            NameEXT     = 'CONS '
            TitleX      = 'Acceleration (cm/s/s)'
            if (Synflag .eq. 2)  then
                TitleX  = 'Velocity  (cm/sec)'
            end if
            TitleY      = 'Probability of Exceedance'
            LegendTitle = "Time (Yrs)"


            write ( Namelegends(1), "('     0 -',i8)" ) INT(tlife(1))
            itp1 = 1
            do i = 2,Ntp
                if (Nonzerotp1(i) .eq. 1)  then
                    itp1 = itp1 + 1
                    write ( Namelegends(itp1), "(i6,' -',i8)" )    &
                    INT(tlife(itp1-1)),INT(tlife(itp1))
                end if       
            end do
            Nc         = itp1
            Nf         = maxGMbins
            itp1       = 0
            do itp = 1, Ntp
                if (Nonzerotp1(itp) .gt. 0)  then
                    itp1 = itp1 + 1
                    do i = 1, nGMbins-1 
                        Y(i,itp1) =  HazWin(i,itp,icrt)
                    end do
                end if
            end do

            call CreateFileName (Ftype,ProblemID,TrackNumber,NameEXT,    &
            Nameplotfile)

#ifdef USE_DISLIN
           if (PlotScreen .eq. "Display-Screen")  then 
               call PlotHazard (CSTR1,CSTR2,IxSCL,jwdt,LegendTitle,      &
               MAINTITLE,Mainline2,Nameplotfile,NameLegends,Nc,Nf,       &
               NVIEW,pagenumber1,TitleX,TitleY,X,Y) 
            CALL DISFIN()   
           end if 
#endif

        end do !end icrt loop   

        !******************************************
        !Write Mean Hazard Curve Plots on PDF file:
        !------------------------------------------
        pagenumber1 = 0
        pagenumber2 = 1
        do icrt = 1, NcritRD
            Ftype       = 'MNH'
            IxSCL       = 0  !X-axis is logarithmic
            Nview       = 2  !for PDF
            MAINTITLE   = 'MEAN HAZARD CURVES'
            Mainline2   = SiteName(icrt)
            NameEXT     = 'PDF  '
            TitleX      = 'Acceleration (cm/s/s)'
            if (Synflag .eq. 2)  then
                TitleX  = 'Velocity  (cm/sec)'
            end if
            TitleY      = 'Probability of Exceedance'

            itp1 = 1
            write ( Namelegends(1), "('    0 -',i6)" ) INT(tlife(1))
            do i = 2,Ntp
                if (Nonzerotp1(i) .eq. 1)  then
                    itp1 = itp1 + 1
                    write ( Namelegends(itp1), "(i6,' -',i8)" )    &
                    INT(tlife(itp1-1)),INT(tlife(itp1))
                end if       
            end do

            Nc         = itp1
            Nf         = maxGMbins

            itp1       = 0
            do itp = 1, Ntp
                if (Nonzerotp1(itp) .gt.0)  then
                    itp1 = itp1 + 1
                    do i = 1, nGMbins-1 
                        Y(i,itp1) =  HazWin(i,itp,icrt)
                    end do
                end if
            end do

            call CreateFileName (Ftype,ProblemID,TrackNumber,NameEXT,    &
            Nameplotfile)

#ifdef USE_DISLIN
            call PlotHazard (CSTR1,CSTR2,IxSCL,jwdt,LegendTitle,MAINTITLE,Mainline2,   &
            Nameplotfile,NameLegends,Nc,Nf,NVIEW,pagenumber2,TitleX,TitleY,X,Y)   
#endif

            !********************************************
            !Write summary Hazard results on output file:
            !--------------------------------------------
            !Open file for dumping hazard and Nuisance results - "OutPlotdata"
            open (unit=3,file="OutPlotdata",status="unknown",iostat=IERR)
            !Mean Hazard curves for each Hazard Time period
            tp1 = 0.
            Dummy1 = SiteName(icrt)
            write (2,442) Dummy1,Ntp
            write (2,4422) tp1,(tlife(i),i=1,Ntp-1)
            write (2,4432) (tlife(i),i=1,Ntp)
            write (2,452) tlife(1),((tlife(i)-tlife(i-1)),i=2,Ntp)
            write (2,462)

            print *, '  '
            write (*,442)  Dummy1,Ntp
            write (*,4422) tp1,(tlife(i),i=1,Ntp-1)
            write (*,4432) (tlife(i),i=1,Ntp)
            write (*,452) tlife(1),((tlife(i)-tlife(i-1)),i=2,Ntp)
            write (*,462)

            write (3,442) Dummy1,Ntp
            write (3,4422) tp1,(tlife(i),i=1,Ntp-1)
            write (3,4432) (tlife(i),i=1,Ntp)
            write (3,452) tlife(1),((tlife(i)-tlife(i-1)),i=2,Ntp)
            write (3,462)

            do i = 1, nGMbins-1
                write (2,472) GMarray(i+1),(HazWin(i,itp,icrt),itp=1,Ntp)
                write (*,472) GMarray(i+1),(HazWin(i,itp,icrt),itp=1,Ntp)
                write (3,472) GMarray(i+1),(HazWin(i,itp,icrt),itp=1,Ntp)
            end do

       end do  !end icrt loop
#ifdef USE_DISLIN
       CALL DISFIN()    
#endif

       print *,'GMoutMin = ',GMoutMin
       print *,'GMoutMax = ',GMoutMax 


!*****************************************************************************
!
!             RISK CALCULATIONS
!
!-----------------------------------------------------------------------------
!
!       Calculate Nuisance for each site and for each time period

        !**********************************************
        !Info for plots of Mean Nuisance Hazard Curves:
        !----------------------------------------------
        Mainline2 = 'versus % of non-acceptance'
        TitleX    = ' NL = P[GM UNACCEPTABLE]  (%)'
        TitleY    = 'P[exceeding NL]'
        IxSCL     =  1    !X-axis is linear
        !IxSCL     =  2    !X-axis is logarithmic

!       Allocate arrays
        nGM = nGMbins-1
        allocate ( XB(nGM) )
        allocate ( HazNuis(nGM) )
        allocate ( Dmg(nGM) )
        allocate ( HazNUistp(Ntp,nGM) )

        !***********************************
        !Display Nuisance results on screen:
        !------------------------------------
        NVIEW       =  1    !Console view on screen
        pagenumber1 =  1
        NameEXT     = 'CONS '
        do icrt = 1, NcritRD
            write (MAINTITLE,"(a17,a10)") 'RISK of NUISANCE ',SiteName(icrt)
            itp1 = 0
            do  itp = 1, Ntp
              if (Nonzerotp1(itp) .gt. 0)  then
                itp1 = itp1 + 1
                do i = 1,nGM
                    !Reload hazard curve in XB and calculate HazNuis
                    zzk   = HazWin(i,itp,icrt)
                    XB(i) = zzk
                end do
 
                call HazNuisance (aGM,BetaGM,Dmg,GMarray,XB,HazNuis,nGM)

                do i = 1, nGM
                    HazNuistp(itp,i) = HazNuis(i)
                end do   
             end if    
            end do  !itp, time period index

            !Number of curves Nc
            Nc = itp1
            Nf = nGM
            !Eliminate the last point which is 0.
            nf1 = nf - 1

            !Reload legends for appropriate curve
            itp1 = 0
            if (Nonzerotp1(1) .gt. 0)  then
                itp1 = itp1 + 1
                write ( Namelegends(1), "('     0 -',i7)" ) INT(tlife(1))
            end if
            do i = 2,Ntp
                if (Nonzerotp1(i) .gt. 0)  then
                    itp1 = itp1 + 1
                    write ( Namelegends(itp1), "(i6,' -',i7)" )    &
                    INT(tlife(i-1)), INT(tlife(i)) 
                end if      
            end do

            allocate ( Xplot(Nf1) )
            allocate ( Yplot(Nf1,Nc))
            allocate ( NuisLegend(Nc) )

            itp1 = 0
            do itp = 1, Ntp
                if (Nonzerotp1(itp) .gt. 0)  then
                    itp1 = itp1 + 1
                    NuisLegend(itp1) = NameLegends(itp)
                end if
            end do


            do i = 1, Nf1
                !Nuisance level is plotted in percents
                Xplot(i)   = Dmg(i) * 100.
                itp1 = 0
                do itp = 1, Ntp
                    if (Nonzerotp1(itp) .gt. 0)  then
                        itp1 = itp1 + 1
                        Yplot(i,itp1) =  HazNuistp(itp,i)
                        if (Yplot(i,itp1) .lt. epsilon2)  then
                            Yplot(i,itp1) = epsilon2
                        end if
                    end if
                end do
            end do

            !Plot on screen
            call CreateFileName (FtypeNUZ,ProblemID,TrackNumber,    &
            ExtNUZ,Nameplotfile)

#ifdef USE_DISLIN
            if (PlotScreen .eq. "Display-Screen")  then 
               call PlotNuisance (CSTR1,CSTR2,IxSCL,jwdt,LegendTitle,       &
               MAINTITLE,Mainline2,Nameplotfile,NuisLegend,Nc,Nf1,NVIEW,    &
               pagenumber1,TitleX,TitleY,Xplot,Yplot)
            end if 
#endif

            deallocate ( Xplot )
            deallocate ( Yplot )
            deallocate ( NuisLegend ) 

        end do      !icrt sites index

        !***********************************
        !Write Nuisance results on PDF file:
!       ------------------------------------
        NVIEW       = 2    ! PDF file
        pagenumber1 = 1
        NameEXT     = 'PDF '
        do icrt = 1, NcritRD
            write (MAINTITLE,"(a17,a10)") 'RISK of NUISANCE ',SiteName(icrt)
            !pagenumber1 = pagenumber1 + 1
            do  itp = 1, Ntp
                do i = 1,nGM
                    !Reload hazard curve in XB and calculate HazNuis
                    zzk   = HazWin(i,itp,icrt)
                    XB(i) = zzk
                end do
 
                call HazNuisance (aGM,BetaGM,Dmg,GMarray,XB,HazNuis,nGM)
     
                !Load Hazard Nuisance and indexes of [0-1] into arrays          
                do i = 1, nGM
                    HazNuistp(itp,i) = HazNuis(i)
                end do             
            end do  !itp, time period index

        !Print results for each Time Period
        write ( Namelegends(1), "('    0 -',i6)" ) INT(tlife(1))
        do i = 2,Ntp
        write ( Namelegends(i), "(i5,' -',i6)" ) INT(tlife(i-1)),INT(tlife(i))       
        end do
        Nc         = itp1
        Nf         = nGM
        nf1 = nf - 1
        allocate ( Xplot(Nf1) )
        allocate ( Yplot(Nf1,Nc))
        allocate ( NuisLegend(Nc) )
        itp1 = 0
        do itp = 1, Ntp
            itp1 = itp1 + Nonzerotp1(itp)
            NuisLegend(itp1) = NameLegends(itp)
        end do

        !Epsilon2 is the minimum values desired for ploting 
        !Nuisance Exceedance Probability. Read from Menuin.csv file on line 
        !Eliminate the last point which by definition is 0. (take nf-1 points)


        do i = 1, Nf1
            !Nuisance level is plotted in percents
            Xplot(i)   = Dmg(i) * 100.
            itp1 = 0
            do itp = 1, Ntp
                if (Nonzerotp1(itp) .gt. 0)  then
                    itp1 = itp1 + 1
                    Yplot(i,itp1) =  HazNuistp(itp,i)
                    if (Yplot(i,itp1) .lt. epsilon2)  then
                        Yplot(i,itp1) = epsilon2
                    end if
                end if
            end do
        end do

        !Secondly create PDF file of Plot:
        !-------------------------------
        call CreateFileName (FtypeNUZ,ProblemID,TrackNumber,ExtNUZ,Nameplotfile)
#ifdef USE_DISLIN
        call PlotNuisance (CSTR1,CSTR2,IxSCL,jwdt,LegendTitle,       &
        MAINTITLE,Mainline2,Nameplotfile,NuisLegend,Nc,Nf1,NVIEW,    &
        pagenumber1,TitleX,TitleY,Xplot,Yplot) 
#endif

        !Write Nuisance results on output file:
        !--------------------------------------
        tp1 = 0.
        Dummy1 = SiteName(icrt)
        write (2,492) Dummy1, Ntp
        write (2,4422) tp1,(tlife(i),i=1,Ntp-1)
        write (2,4432) (tlife(i),i=1,Ntp)          
        write (2,452) tlife(1),((tlife(i)-tlife(i-1)),i=2,Ntp)
        write (2,502)

        write (*,4422) tp1,(tlife(i),i=1,Ntp-1)
        write (*,4432) (tlife(i),i=1,Ntp)  
        write (*,452) tlife(1),((tlife(i)-tlife(i-1)),i=2,Ntp)
        write (*,502)

        write (3,492) Dummy1, Ntp
        write (3,4422) tp1,(tlife(i),i=1,Ntp-1)
        write (3,4432) (tlife(i),i=1,Ntp)          
        write (3,452) tlife(1),((tlife(i)-tlife(i-1)),i=2,Ntp)
        write (3,502)

        itp1 = 0
        do itp = 1, Ntp
            if (Nonzerotp1(itp) .gt. 0)  then
                itp1 = itp1 + 1
            end if
        end do
        do i = 1, Nf1
            write (2,512) Xplot(i),(Yplot(i,itp),itp=1,itp1)
            write (*,512) Xplot(i),(Yplot(i,itp),itp=1,itp1)
            write (3,512) Xplot(i),(Yplot(i,itp),itp=1,itp1)
        end do

        deallocate ( NuisLegend )
        deallocate ( Xplot) 
        deallocate ( Yplot ) 

        end do      !icrt sites index

        close (3)

#ifdef USE_DISLIN
        call DISFIN ()
#endif

        deallocate ( Dmg )
        deallocate ( HazNuis ) 
        deallocate ( HazNuistp )
        deallocate ( Haz1, Haz1SPEC, Nonzerotp2 )
        deallocate ( perctlSPEC )
        deallocate ( haz2, UHS, UHSRP )
        deallocate ( XB )


!******************************************************************************
!.......Summary of input and output files for this run:
        print *, '  '
        print *, '  '
        print *,'SUMMARY OF NAMES OF FILES USED:'
        PRINT *,'*******************************'
        print *,'Input files:'
        print *,'Track Number         : ', TrackNumber
        print *,'General MenuIn       : ', FichiersIN(1)
        if (Task .ne. 3)  then
           print *,'Seismic Sources Info : ', FichiersIN(2)
           if (iATN_Larry .eq. 1)  then
              print *,'GM Attenuation models: ', FichiersIN(3)
           else
              print *,'Directory for G-F    : ', FichiersIN(5)
              print *,'GF-sources INfo      : ', FichiersIN(6)
           end if
        else
           print *,'  '
           print *,'****************************************************'
           print *,'Number of epistemic realizations in final  calcs:',  &
                Nepis
           print *,'  Realization      Track   Weight for      Catalog'
           print *,'   Sequence #     Number   simulation     file Name'
           do i = 1, Nepis
              print *,' ', i,TrackID(i),WeightEpi(i),Catfile(i)
           end do
           print *,'****************************************************'
        end if  
              

        if (iStd_dieterich .eq. 2)  then
           if (GoodMinet .gt. 0)  then
              print *,'  '
              print *,'*****************************************************'
              print *,'  '
              print *,'  '
              print *,'Case of using RSQSim generated catalogs:'
              print *,'****************************************'
              print *,'Number of Good Original Catalogs processed:  ',GoodMinet
              print *,'(Not the stored Catalog results)'
              print *,'List of GOOD Catalog files in these PSHA calcs:'
              do i = 1, Nepis4*NMspl
                 if (BadMinet(i) .eq. 1)  then
                    print *,'     ',Catfile(i)
                 end if
              end do

              idum = 0
              do i = 1, Nepis4*NMspl
                 if (BadMinet(i) .eq. 0)  then
                    idum = idum + 1
                 end if
              end do
              if (idum .eq. 0)  then
                 print *,'No BAD Catalog files'
              else
                 print *,   &
                 'List of BAD Catalog files in this PSHA calculation run:'
                 do i = 1, Nepis4*NMspl
                    if (BadMinet(i) .eq. 0)  then
                       print *,'     ',Catfile(i)
                    end if
                 end do
              end if
              print *,'  '
              
              if (Task .gt. 0)  then
                 print *,'Number of epistemic realizations in final  calcs:',  &
                      Nepis
                 print *,   &
                 'Realization           Track    Weight this   Hazard Curve '
                 print *,   &
                 '   Sequence #        Number     simulation   from Catalog'
                 do i = 1, Nepis
                    print *,' ', i,TrackID(i),WeightEpi(i),Catfile(i)
                 end do
              end if  
              print *,'****************************************************'
           end if
        end if
        
        if (iStd_dieterich .eq. 3)  then
           print *, 'Number of Empirical Catalog(s) used: ', Nepis
           if (task .le. 1)  then
              do i = 1, Nepis4
                 print *,  '     ', Catalogf(i)
              end do
           end if
        end if
        
        if (Task .gt. 0)  then
           DEALLOCATE ( TrackID, Catfile )
        end if
        
        print *,'  '
        print *,'  '
        print *,'    '
        print *,'General Output files:'
        print *,'*********************'
        print *,'Output final percentile hazard result text file:  ', FichiersOUT(1)
        if (Task .gt. 0) then
           print *,'All calculated Hazard curves stored in file :  ', StoreHaz
        end if
        write (2,*)  '  '
        write (2,*)  '  '
        write (2,*)  '  SUMMARY OF NAMES OF FILES USED:'
        write (2,*)  '  *******************************'
        write (2,*)  '  Input files:'
        write (2,*)  '  Track Number         : ', TrackNumber
        write (2,*)  '  General MenuIn       : ', FichiersIN(1)
        write (2,*)  '  Seismic Sources Info : ', FichiersIN(2)
        if (iATN_Larry .eq. 1)  then
        write (2,*)  '  GM Attenuation models: ', FichiersIN(3)
        else
        write (2,*)  '  Directory for G-F    : ', FichiersIN(5)
        write (2,*)  '  GF-sources INfo      : ', FichiersIN(6)
        end if
        if (iStd_dieterich .eq. 3)  then
            write (2,*) Nepis, ' Empirical Catalog(s) used: '
            do i = 1, Nepis
                 write (2,*)  '     ', Catalogf(i)
            end do
        end if
        write (2,*)  '    '
        write (2,*)  '  Outputs:'
        write (2,*)  '  Output hazard results: ', FichiersOUT(1)  
     
!.......Put a time and date stamp
        print *, '  '
        elt=etime(tarray)
        call dattim (idatex,itimex)  
        write (*,522) idatex1,itimex1
        write (*,252) idatex,itimex  
        write (*,212) elt,tarray(1),tarray(2)
        write (2,*)  '   '
        write (2,522) idatex1,itimex1
        write (2,252) idatex,itimex 
        write (2,212) elt,tarray(1),tarray(2)
       
                       
!************************* Formats *********************************************

162     format (/,a20)
172     format ( &
        'Acc(m/s/s)   H-mean  - St.Dev  -   Hmin  -   Hmax  ', &
        5('-   H',i2,'%  '))
        !'H15%   -  H50%   -  H85%   ')
222     format (e9.3,9(1x,e9.2))
212     format (//,                                   &
        5x,"Total time spent, including plotting:",  &
        /,5x,"time in seconds",/,5x,"cpu",e18.5,      &
        /,5x,"usr",e18.5,/,5x,"sys",e18.5)      
232     format (//,'Hazard Results for:',i4,' Epistemic Simulations',  &
        //,'For Time window:',i2,' from',f12.3,' to ',f12.3,' years')
252     format (5x,"Date finished Run:",a10,2x," Time:",a8,//) 
262     format (a41,a20,///, "HAZARD AND RISK SIMULATION PROGRAM",/,  &
        "Hazard results for each Site",/)
272     format  ("  Name of this output file:",19x, a25,//)
442     format (///,' Mean Hazard for ',a8, ' and',i3, &
        ' Hazard Time Period(s):',/,  &
        ' -----------------------------------------------------',/,  &
        '            Haz-TP1    Haz-TP2    Haz-TP3    Haz-TP4    Haz-TP5',/,  &
        '            (years)    (years)    (years)    (years)    (years)' &
        ,/,'Window:')
4422    format ('    Start ',5(1x,f10.1))
4432    format ('    End   ',5(1x,f10.1))
452     format (/,'Duration',f10.3,4(1x,f10.3))
462     format (/,  &
        ' Accel.       Haz1       Haz2       Haz3       Haz4       Haz5',/,  &
        '(cm/s/s)  -------------------------------------------------------')
472     format (e8.3,1x,5E11.3)   
482     format (/,'  Name of the input Menu file: ',a40)
492     format (///,' Mean Nuisance Hazard for ',a8, ' and',i3, &
        ' Hazard Time Period(s):',/,  &
        ' ----------------------------------------',/,  &
        '            Nuz-TP1    Nuz-TP2    Nuz-TP3    Nuz-TP4    Nuz-TP5',/,  &
        '            (years)    (years)    (years)    (years)    (years)' &
        ,/,'Window:')
502     format (/,  &
        ' Nuisance  HazNuz1    HazNuz2    HazNuz3    HazNuz4    HazNuz5',/,  &
        '(percent) -------------------------------------------------------')
512     format (e8.3,1x,5E11.3) 
522     format (5x,"Date  started Run:",a10,2x," Time:",a8) 
532     format (2x,"Date  started Run:",a10,2x," Time:",a8)
552     format (10('Sim. Weight'))
572     Format (10E10.3)
582     Format (5i5)
602     Format (6i5,5x,a14)



        close (unit=2)        

        return
        
        END SUBROUTINE riskcat
