!
!........This module defines the general parameters of the solution
!
        Module GenParam
        
        IMPLICIT NONE
        
        !General
        INTEGER, PARAMETER :: MaxCritypes=4 !(1) RD nodes,
                                            !(2) RD Over/Underpasses
                                            !(3) Dams
                                            !(4) Critical Facilities
        INTEGER, PARAMETER :: NcMax=8       !Number of Hazard curves to plot
        INTEGER, PARAMETER :: MaxNtp=5      !number of years life time periods

        INTEGER :: Ntp        
        INTEGER, dimension(MaxCritypes):: NodesInType
        INTEGER, DIMENSION(MaxNtp)     :: itlife
        REAL, DIMENSION(MaxNtp)        :: tlife
        
         
        
        !Critical facilities
        INTEGER, PARAMETER :: MaxSites = 62

        !Dams
        INTEGER, PARAMETER :: MaxDams  = 2                        
        
        !Roads
        INTEGER, PARAMETER :: Maxlink=60,MaxNode=50,Maxsublk=10 
        INTEGER, PARAMETER :: MaxOverUnderRD=Maxlink*Maxsublk
        INTEGER, PARAMETER :: Maxperils=24,Maxperyear=3
        INTEGER, PARAMETER :: Maxtot=10,Maxyears=1000000
        
        INTEGER, PARAMETER :: &
        Maxcritpt = Maxnode+MaxDams+MaxOverUnderRD+Maxsites 

        !General parameters, some used in 'common'
        INTEGER :: IDSEED

        !Parameters for Catalog sampling routine SampleCat
        INTEGER, ALLOCATABLE, dimension (:) :: VZ, V1, V2

        
        end Module GenParam
