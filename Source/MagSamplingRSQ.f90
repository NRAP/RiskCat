!
!     Called by riskcat for case of RSQSim generated catalogs: Case(2)
!
!*******************************************************************************
!
        subroutine MagSamplingRSQ ( ispl, iflagev, MaxSample, nbin,    &
                                  Neq, Neq2, Ntp1, unitcat,           &
                                  xMagMin2, xMagMax2, xM0, YearsHist)
!
!*******************************************************************************
!    This routine reads the catalog and throws away events in a magnitude bin
!    that are beyond the number of MaxMagSampling.
!    Creates a new catalog with no more than MaxMagSampling events in each M-bin
!    Determines the number of events in each magnitude bin and calculates the relative
!    weight of the corresponding M-bin as:
!
!     wmagbin(i) = Ntotal(i) / Nsample(i)
!
!     where:    Ntotal(i) = Number of events in magnitude bin i from full catalog
!               Nsample(i)= Number of events retained in magnitude bin i
!
!    The weights are used later to reconstruct the final ground-motion histogram
!    before calculating the hazard.
!
!    Input: 
!           Catalog of events
!           MaxMagSampling = max number of events in each magnitude sampling bin
!           xM0            = minimum magnitude considered in the problem
!           nbin           = number of magnitude bins sampled
!           Neq2           = Number of events in the original catalog
!           Ntp1           = Number of time-windows
!           Smag(i)        = array of lower-bound value of magnitude bin i
!           unitcat        = unit number for new culled catalog file
!
!    Output:
!           Neq            = Number of events in the culled catalog
!           Mag, Time, M0, X,Y,Z of each evnt in the culled catalog
!           wMagbin(i)     = Normalized weight of magnitude bin i.
!
!*******************************************************************************
!
        USE ModuleEq
        USE MOduleRSQSim
        USE GenParam


        IMPLICIT NONE

        !Declarations for RSQSim externally generated catalog
        CHARACTER (LEN=9) :: Xevent
        INTEGER (kind=8)  :: k4
        INTEGER (kind=4)  :: elem4,nev4,nrup4
        REAL (kind=4)     :: mw4
        REAL (kind=8)     :: dur8,m08,slip8,std8,time8
        !INTEGER*8 :: k4
        !INTEGER*4 :: elem4,nev4,nrup4
        !REAL*4    :: mw4
        !REAL*8    :: dur8,m08,slip8,std8,time8
        
        INTEGER :: i, icaseCat, IERR, iflagev, is, ispl, iMain, isub1, itp
        INTEGER :: j, js, j1, k, MaxSample, nsub, nTMW, nTotal, unitcat
        INTEGER :: n, nbin, Neq, Neq2, n1, Ntp1

        REAL    :: delem, rq1, rq7
        REAL    :: sectoyear, time, time1, time2
        REAL    :: x, xMagMin2, xMagMax2, xMSc, xM0, x1, x2, x3
        REAL    :: y, YearsHist, z

        INTEGER, dimension(nbin,Ntp1)      :: nEqMTW, NevMag

        REAL,    dimension(nbin,Ntp1)      :: TimeMag


        INTEGER,ALLOCATABLE,dimension(:)     :: isub2
        INTEGER,ALLOCATABLE,dimension(:,:)   :: VSDummy
        INTEGER,ALLOCATABLE,dimension(:,:,:) :: CatMap, CatSwitch
        REAL   ,ALLOCATABLE,dimension(:,:)   :: RQ

!-------------------------------------------------------------------------------
        sectoyear= 3600. * 24. * 365.   ! Number of seconds in a year

        !Reset the number of events in each M-bin. in full un-culled catalog
        NevMag   = 0
        !Resest number of events in culled catalog
        Neq      = 0
        !Reset Time till last event in magnitude bin
        TimeMag  = YearsHist
        !Reset total number of events for culled catalog in time window itp
        nEqtot   = 0
        !Reset Number of event per Mag,Time-window bin for full catalog
        nEqMTW   = 0
        !Number of events with time <= end of last time-window in full catalog
        nTotal   = 0
        Xevent = 'Main '

!Redefine Magnitude bins for sampling:
!-------------------------------------
!       We want bins such that larger Mag events are definitely sampled
!       Smallest magnitude event in catalog is: MagMin2
!       Largest magnitude event is : MagMax2
!       Number of magnitude bins is nbin (nbin=10)
!                 starting magnitude in bin i is Smag(i)
!                 upper magnitude in bin i is Smag(i+1)
!                 therefore Snag is dim nbin+1
        Smag(1)      = xMagMin2
        Smag(nbin+1) = xMagMax2
        delem        = (xMagMax2-xMagMin2) / REAL(nbin)
        do i = 2, nbin
            Smag(i) = Smag(i-1) + delem
        end do
        print *,'  '
        print *,'Magnitude Sampling bin:'
        print *,'Bin NUmber  Min bound Mag    Max bound Mag'
        do i = 1, nbin
            print *, i,Smag(i), Smag(i+1)
        end do

        !Catalog of events is read from file unitcat = 17
        rewind (unitcat)

        !Open temporary file for storing CULLED CATALOG: CatRSQ
        !open(unit=7,file='CatRSQ',status="UNKNOWN",iostat=IERR)
        open(unit=7,file='CatRSQ',status="UNKNOWN",form='unformatted',  &
             iostat=IERR)
  
        !Bounds of Magnitudes in magnitudes sampling bin "i" are:
            ! Lower bound = Smag(i)
            ! Upper bound = Smag(i+1)  
        !Bounds of time for definition of Time-Window "itp":
            ! Lower bound = tlife(itp-1) (0. for first one)
            ! Upper bound = tlife(itp)

!Preliminary indexes mappings:
!****************************
!       Preliminary stuff
!       1. Count number of events in each bin (Magnitude,Time Window), 
!          before culling the catalog, and 
!       2. create mappimg of index in full catalog versus index in each bin

        ALLOCATE ( VSDummy(Neq2,3) )
        rewind (unitcat)
        if (iflagev .eq. 1)  then
           !Skip the number of events
           read (unitcat) nev4
        end if

        Preliminary: Do j = 1, Neq2

            !Read subelements info for this event j in full catalog
            nsub = CatSubN(j)
            ALLOCATE ( isub2(nsub) )
            ALLOCATE ( RQ(nsub,8) )
            !Read properties of main event
            !read (unitcat,*) idum,idum,rq1,xMSc
            read (unitcat) k4,nrup4,m08,mw4
            rq1  = SNGL(m08)
            xMSc = mw4

            !Read time of main event as rupture time of first patch
            !Elt Index, Rupture time(s),Rise time(Rupture duration, s),
            !...slip(m), Stress Drop(MPa)
            !read (unitcat,*) idum,rq7,rq1,rq2,rq3
            read (unitcat) elem4,time8,dur8,slip8,std8
            rq7 = SNGL(time8)
            !Skip remaining subelements
            if (nsub .gt. 1)  then
               do is =2, nsub
                  !read (unitcat,*) idum,(RQ(is,js),js=1,4)
                  read (unitcat) elem4,time8,dur8,slip8,std8
               end do
            end if
            
            !Time (rq7) is in seconds. Must be in Years.
            !Time less than end of last TW, and mag less than xM0
            time = rq7 / sectoyear

            !Which Mag bin does xMSc fall into?
            !Which time-window bin does this event fall into 
            call IndexMagTmw ( k, itp, xMSc, nbin, Ntp1, time)
            !Number of events in Mag-TWM bin for full un-culled catalog
            !NevMag(k,itp)  = NevMag(k,itp) +1

            if ( (time.le.tlife(Ntp1)) .and. (xMSc .ge. xM0) )  then

                !Update number of events in each bin (Mag,Time Window)
                NevMag(k,itp)  = NevMag(k,itp) +1
                !Update total number of events with time <= tlife(Ntp1)
                nTotal = nTotal + 1

                !Number of events , per itp and ispl: nEqtot(ispl,itp)
                !Number of events , per Mag and itp bin : nEqMTW(k,itp)
                !Number of events, per ispl and per time window: nEqtot
                nEqtot(ispl,itp) = nEqtot(ispl,itp) + 1
                nEqMTW(k,itp)    = nEqMTW(k,itp)    + 1
    
                !Set the time of observation equal to the length of the time-window
                !if  number of events sufficient to obtain statistical convergence
                !is reached, then set time to actual time of occurrence of event
                ! since start of time-window.
                time2  = tlife(1)
                time1  = 0.
                if (itp .gt. 1)  then
                    time2 = tlife(itp)
                    time1 = tlife(itp-1)
                end if

                !Load working array of mapping indexes:
                ! j = index of the event in the full catalog
                !VSDummy(j,1) = Magnitude sampling bin index of the event
                VSdummy(j,1) = k
                !VSDummy(j,2) = Time window index of the event
                VSdummy(j,2) = itp
                !VSDummy(j,3) = Index of event for only those in (k,itp) bin
                n            = nEqMTW(k,itp)
                VSdummy(j,3) = n

            end if

            DEALLOCATE ( isub2 )
            DEALLOCATE ( RQ )

        end do Preliminary


        !Determine nTMW: largest number of events in any mag-TW bin
        nTMW = 0
        print *, '  '
        !print *, 'Number of events per each cell (Mag,TWM):'
        do  k = 1, nbin
            !print *, 'k=',k,('  itp=',is,'  N(k,itp)=',nEqMTW(k,is),is=1,Ntp1)
            do itp = 1, Ntp1
                if (nEqMTW(k,itp) .gt. nTMW)  then
                    nTMW = nEqMTW(k,itp)
                end if
            end do
        end do

        ALLOCATE ( CatMap(nbin,Ntp1,nTMW) )

        !Move info in VSDummy to CatMap()
        !CatMap() then contains mapping between an event in (mag,TW,eq) bin to 
        !original event in full catalog
        do j = 1, Neq2
            k   = VSDummy(j,1)
            itp = VSDummy(j,2)
            n   = VSDummy(j,3)
            if (n .eq. 0) then
                n = 1
            end if
            CatMap(k,itp,n) = j
        end do      

        deallocate ( VSDummy)
 
        !Create the array of switches for each mag-TW bin
        ALLOCATE ( CatSwitch(nbin,Ntp1,Neq2) )
        CatSwitch = 0
        do  k = 1, nbin
            do itp = 1, Ntp1
                !Number of events in bin
                n1 = nEqMTW(k,itp)
                if ( (n1.gt.MaxSample) .and. (n1.gt.0) )  then

!**************************************************************************
                    !Print info for debugging:
 !                   print *,'   '
  !                  print *,'Mag bins =',nbin,'  Time-windows=',Ntp1
   !                 print *,' Magg bin:',k,'   Time-Window bin:',itp
    !                print *,' Events in cell:',n1,'   MaxSample=',MaxSample
!***************************************************************************

                    !Max number of samples is MaxSample
                    ALLOCATE ( VZ(n1) )
                    call SampleV (n1, MaxSample)
                    !Load switches in CatSwitch
                    do j1 = 1,n1
                        j = CatMap(k,itp,j1)
                        CatSwitch(k,itp,j) = VZ(j1)
                    end do
                    deallocate ( VZ )
                elseif ((n1.gt.0) .and. (n1.lt.MaxSample))  then
                    !Less events in M-TW bin than MaxSample: Switch is ON
                    do j1 = 1, n1
                        j = CatMap(k,itp,j1)
                        CatSwitch(k,itp,j) = 1
                    end do 
                End if
            end do
        end do

        deallocate ( CatMap )
        
        rewind (unitcat)
        if (iflagev .eq. 1)  then
           !Skip number of events
           read (unitcat) nev4
        end if

        Do j = 1, Neq2         !Loop over all events in original catalog

            !Read Main event info from full catalog
            !Event index, Number of Sub-events, M0(joules), Mw
            !read (unitcat)  idum,idum,rq1,xMSc
            read (unitcat) k4,nrup4,m08,mw4
            rq1  = SNGL(m08)
            xMSc = mw4
           
            !Read sub-events info for event j in full catalog
            nsub = CatSubN(j)
            ALLOCATE ( isub2(nsub) )
            ALLOCATE ( RQ(nsub,8) )

            do is = 1, nsub
               !read (unitcat,*) isub2(is),(RQ(is,js),js=1,4)
               read (unitcat) elem4,time8,dur8,slip8,std8
               isub2(is) = elem4
               RQ(is,1)  = SNGL(time8)
               RQ(is,2)  = SNGL(dur8)
               RQ(is,3)  = SNGL(slip8)
               RQ(is,4)  = SNGL(std8)
            end do
            rq7   = RQ(1,1)
            iMain = isub2(1)
            
            !Determine which bin of mag and Time-window this event belongs to:
            call IndexMagTmw ( k, itp, xMSc, nbin, Ntp1, time)

            !Recover the switch status for this event:
            n1 = CatSwitch(k,itp,j)

            !Determine if event will be retained in culled catalog 'CatRSQ'
            !Time (rq7) is in seconds. Must be in Years.
            !To be part of the newly created culled catalog the conditions are:
            ! time <= end of last time-window
            ! Magnitude <= minimum magnitude consider4ed in the analysis: xM0
            ! Switch ON from the sampling switches, (n1=1)
            
            time = rq7 / sectoyear
            if ((time.le.tlife(Ntp1)) .and.  &
                (xMSc.ge.xM0)         .and.  &
                (n1 .gt. 0)                  )  then   
 
                !Update number of events in culled catalog
                Neq = Neq + 1
                !Number of subs in events of culled catalog
                SubEltNumber(Neq) = nsub
                !Using RSQSim or GEOS simulated catalogs is case: 2
                icaseCat = 2
                !Load the events parameters in appropriate arrays 
                call LoadCulledCat( Neq, icaseCat, ispl, time, x, xMSc,   &
                                   xMagMin2, xMagMax2, y, z )

                !Main event initiation coordinates in CULLED CATALOG indexing
                isub1 = iMain
                Xrsq(ispl,Neq) = XYZsub(isub1,1)
                Yrsq(ispl,Neq) = XYZsub(isub1,2)
                Zrsq(ispl,Neq) = XYZsub(isub1,3)
                x1 = Xrsq(ispl,Neq)
                x2 = Yrsq(ispl,Neq)
                x3 = Zrsq(ispl,Neq)


                !Write sub-events info on temporary file 'CatRSQ', unit 7.
                do is = 1, nsub
                    !Sub-event number,sub-elt. ID, time(sec), slip(m),
                    !rise time (sec), stress-drop (MPa)
                    !write (7,'(2i6,F20.4,3f12.6)')   &
                    !         is,isub2(is),(RQ(is,js),js=1,4)
                    write (7) is,isub2(is),(RQ(is,js),js=1,4)
                end do
                !Write Main event info on file 'CatRSQ'
                !'Main ', main events number,time event, M0(joules), Mw
                !write (7,'(a9,2i6,F20.4,E12.4,F12.6)')   &
                 !        Xevent,Neq,iMain,rq7,rq1,xMSc
                write (7) Xevent,Neq,iMain,rq7,rq1,xMSc
                
            end if

            DEALLOCATE ( isub2 )
            DEALLOCATE ( RQ )

        end do

        DEALLOCATE ( CatSwitch )

        close (7)
        close (unitcat)

!       Calculate weights
        do j = 1, Ntp1
            do i = 1, nbin
                x1 = REAL (nEqMTW(i,j))
                x2 = REAL (MaxSample)
                x3 = 1.
                if ((x1.ge.MaxSample) .and. (x2.gt.0.)) then                
                    x3 =  x1 / x2 
                end if
                wMagbin(i,j) = x3
            end do
        end do

!        print *, 'Min Mag   :', xMagMin2
 !       print *, 'Max Mag   :', xMagMax2
  !      print *, 'Mag bins  :', (Smag(i),i=1,nbin+1)
   !     do j = 1, Ntp1
    !        print *, 'N/Mag Bin:', (NevMag(i,j),i=1,nbin)
     !       print *, 'Weights  :', (wMagbin(i,j),i=1,nbin)
      !  end do


        return

        end subroutine MagSamplingRSQ




  
        
        
