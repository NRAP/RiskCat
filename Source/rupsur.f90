      subroutine rupsur(flat,flon,fh,alat,alon,ah,stk,dp,fl,ip,fx,fy,j)

 
     IMPLICIT NONE
!
! readjust the description of the fault perimeter to new location of
! hypocenter.  the program requires the perimeter to be described by
! the radial distance from the hypocenter, starting with updip and
! continuning clockwise.
!
! first check whether hypocenter is on fault.
! azm is azimuth from north from flat,flon to hypocenter alat alon
!
      INTEGER, parameter :: ipfl = 1000 

      INTEGER       :: i,iij,ij,ip,j,jji
      REAL          :: ah,ah2,alat,alon,angg,azinv,azm,dis,dis1,dp
      REAL          :: flat,flon,fxx1,fyy1,hdis,stk

      REAL (KIND=8) :: x0,y0,z0,x1,x2,y1,y2,x3,y3,am0,am1,am2,am11,am111
      REAL (KIND=8) :: ah1,r3,fh,b0,b1,pii,azm1,ang,ang0,ang1,ang2,dpp,dint

      REAL, dimension(ipfl) :: fl
      REAL, dimension(1000) :: ff
      REAL, dimension(10000):: fx, fxx, fy, fyy


!*******************************************************************************

      Print *,'Input inside routine rupsur.f,flat,flon,fh,alat,alon,ah,',   &
      'stk,dp,fl,ip,fx,fy,j='
      do  i = 1,10
         print *,flat,flon,fh,alat,alon,ah,stk,dp,fl(i),ip,fx(i),fy(i),j
      end do
      pii = 6.283185308
      dpp = dp*pii/360.
      if (ip.gt.1000) then
      print *,'ip greater than 1000'
      stop 
      endif
       if(flat.eq.alat.and.flon.eq.alon.and.ah.eq.fh) then
      do  i = 1,ip
         ff(i) = fl(i)
      end do
      go to 601
       endif
      call disazm(azm,azinv,dis,hdis,flat,flon,fh,alat,alon)
      azm1 = (stk-azm)*pii/360.
      x0 = dis*dcos(azm1)
      z0 = dis*dsin(azm1)
      if(dp.eq.90.) then
      if(abs(z0).lt.0.5) write(6,101) z0
      if(abs(fh-ah).lt.0.2) then
      y0 = 0
      else
      y0 = fh - ah
      endif
      endif

      if(dp.ne.90.) y0 = (fh-ah)/dsin(dpp)
      if(dp.eq.0.0) y0 = 1000000.0
 101  format('hypocenter ',f5.2,' km off of fault')
      r3 = dsqrt(x0**2+y0**2)
      if(r3.ne.0.0) azm1 = pii/4.0 - datan2(y0,x0)
      if(r3.eq.0.0) azm1 = 0.0
      if(azm1.lt.0.0) azm1 = pii + azm1
      ang = pii/float(ip)
      
      do  i = 1,ip         
         jji = i + 1         
         if(jji.gt.ip) then
            jji = 1
         end if         
         ang1 = float(i-1)*ang         
         ang2 = float(i)*ang         
         !ang1 is always smaller than ang2
         !Stop and print values if azm1 is outside of [ang1-ang2]
         if(azm1.ge.ang1.and.azm1.lt.ang2) then            
            exit           
         else            
            print*,'300 rupsur.f, angle calculated incorrectly:azm1,ang1,ang2',   &
                 azm1,ang1,ang2           
            stop           
         end if         
      end do
      
      am111 = fl(jji)*dsin(ang2)-fl(i)*dsin(ang1)
      if(am111.eq.0.0) go to 315
      am11 = fl(jji)*dcos(ang2)-fl(i)*dcos(ang1)
      am1 = am11/am111
      b1 = fl(i)*(dcos(ang1)-am1*dsin(ang1))
  315 if(azm1.eq.0.0.or.azm1.eq.(pii/2.0)) go to 312
      am2 = dcos(azm1)/dsin(azm1)
      if(am111.ne.0.0) x1 = b1/(am2-am1)
      if(am111.ne.0.0) y1 = am1*x1 + b1
      if(am111.eq.0.0) x1 = fl(i)*dsin(ang1)
      if(am111.eq.0.0) y1 = am2*x1
      go to 313
  312 x1 = 0.0
      y1 = b1
  313 dint = dsqrt(x1**2 + y1**2)
      if(r3.gt.dint) then
      print *,'Problem in routine rupsur.f:'
      write(6,314)
      write(11,314)
  314 format(15x,'********************************'//,   &
      'hypocenter is not on rupture surface, terminate program')
      write(6,102) r3,dint,azm1
 102  format('r3=',f10.3,' dint=',f10.3,'  azm1=',f10.3)
      print *,'ah=0.0'
      ah = 0.0
      return
      endif
!  now depth, correct if off fault
      ah1 = fh - r3*dcos(azm1)*dsin(dpp)
      ah2 = SNGL(ah1)
      if(ah2.ne.ah) write(6,137) ah,ah2
  137 format('hypocenter does not fall on fault, depth changed from ',   &
      f5.2,' to ',f5.2/)
      ah = ah2
!
! if new hypocenter equal to original, roundoff error makes x3 slightly
! less than x1 at multiples of 24 degrees.
       if(flat.eq.alat.and.flon.eq.alon.and.ah.eq.fh) then
      do  i = 1,ip
         ff(i) = fl(i)
      end do
      go to 601
       endif
!
! now set up fl(i) array from hypocenter
!
      ang = pii/float(ip)
      do 400 j = 1,ip
      ang0 = float(j-1)*ang
      if(ang0.ne.0.0.and.ang0.ne.pii/2.) then
      if(ang0.ne.pii/4..and.ang0.ne.3.*pii/4.) am0 = 1.0/dtan(ang0)
      if(ang0.eq.pii/4..or.ang0.eq.3.*pii/4.) am0 = 0.0
      b0 = y0 - am0*x0
      endif
      do 500 i = 1,ip
      jji = i + 1
      if(jji.gt.ip) jji = 1
      ang1 = float(i-1)*ang
      ang2 = float(i)*ang
      x2 = fl(jji)*dsin(ang2)
      x1 = fl(i)*dsin(ang1)
      y2 = fl(jji)*dcos(ang2)
      y1 = fl(i)*dcos(ang1)
      am11 = y2 - y1
      am111 = x2 - x1
      if(am111.ne.0.0) then
      am1 = am11/am111
      b1 = fl(i)*(dcos(ang1)-am1*dsin(ang1))
      if(ang0.ne.0.0.and.ang0.ne.pii/2.) then
      x3 = (b1 - b0)/(am0 - am1)
      if(x2.ge.x1) then
      if(x3.lt.x1.or.x3.gt.x2) go to 500
      endif
      if(x2.lt.x1) then
      if(x3.lt.x2.or.x3.gt.x1) go to 500
      endif
      y3 = am0*x3 + b0
      if(ang0.lt.pii/4..or.ang0.gt.3.*pii/4.) then
      if(y3.lt.y0) goto 500
      endif
      if(ang0.gt.pii/4..and.ang0.lt.3.*pii/4.) then
      if(y3.ge.y0) goto 500
      endif
      if(ang0.eq.pii/4..and.x3.lt.x0) go to 500
      if(ang0.eq.3.*pii/4..and.x3.gt.x0) go to 500
      ff(j) = REAL ( dsqrt((x3-x0)**2+(y3-y0)**2) )
      go to 400
      else
      x3 = x0
      if(x2.ge.x1) then
      if(x3.lt.x1.or.x3.ge.x2) go to 500
      endif
      if(x1.gt.x2) then
      if(x3.lt.x2.or.x3.ge.x1) go to 500
      endif
      y3 = am1*x3 + b1
      if(ang0.lt.pii/4.or.ang0.gt.3*pii/4.) then
      if(y3.lt.y0) goto 500
      endif
      if(ang0.gt.pii/4..and.ang0.lt.3.*pii/4.) then
      if(y3.ge.y0) goto 500
      endif
      if(ang0.eq.pii/4..and.x3.lt.x0) go to 500
      if(ang0.eq.3.*pii/4..and.x3.gt.x0) go to 500
      ff(j) = REAL ( dsqrt((x3-x0)**2+(y3-y0)**2) )
      go to 400
      endif
      endif
! if am111 is zero
!
      if(am111.eq.0.0) then
      if(ang0.eq.0.0.or.ang0.eq.pii/2.) goto 500
      x3 = x2
      y3 = am0*x3 + b0
      if(ang0.lt.pii/4.or.ang0.gt.3*pii/4.) then
      if(y3.lt.y0) goto 500
!-----LJH added next 5 line 4/30/91 
      if(y2.gt.y1) then
      if(y3.gt.y2.or.y3.lt.y1) go to 500
      else
      if(y3.lt.y2.or.y3.gt.y1) go to 500
      endif
      endif
      if(ang0.gt.pii/4..and.ang0.lt.3.*pii/4.) then
      if(y3.ge.y0) goto 500
!-----LJH added next 5 line 4/30/91 
      if(y2.gt.y1) then
      if(y3.gt.y2.or.y3.lt.y1) go to 500
      else
      if(y3.lt.y2.or.y3.gt.y1) go to 500
      endif
      endif
      if(ang0.eq.pii/4..and.x3.lt.x0) go to 500
      if(ang0.eq.3.*pii/4..and.x3.gt.x0) go to 500
      ff(j) = REAL ( dsqrt((x3-x0)**2+(y3-y0)**2) )
      go to 400
      endif
      print *,'you shouldnt be here'
  500 continue
  400 continue
! get perimeter coordinate points
 601  ang = pii/float(ip)
      do 600 i = 1,ip
      fl(i) = ff(i)      
      ang1 = float(i-1)*ang
      fxx(i) = REAL ( fl(i)*dsin(ang1) )
      fyy(i) = REAL ( fl(i)*dcos(ang1) )
  600 continue
      open (unit = 9, file='fault.out')
      write(9,136) alat,alon,ah,ip
      write(9,138) (fl(i), i= 1,ip)
  136 format(f8.4,2x,f9.4,2x,f5.2/,i4)
  138 format(10f7.2)
      close(9)
! interpolate for perimeter point to be .gt.2.0 and .le.3.0 km
      j = 0
      fxx(ip+1) = fxx(1)
      fyy(ip+1) = fyy(1)
      do 700 i = 1,ip
      j = j + 1
      fx(j) = fxx(i)
      fy(j) = fyy(i)
      dis = sqrt((fxx(i+1)-fxx(i))**2 + (fyy(i+1)-fyy(i))**2)
      if(dis.le.3.0) go to 700
      ij = ifix(dis/2.0)
      angg = atan2(fyy(i+1)-fyy(i),fxx(i+1)-fxx(i))
      fxx1 = 2.0*cos(angg)
      fyy1 = 2.0*sin(angg)
      do  iij = 1,ij
         j = j + 1
         fx(j) = fx(j-1) + fxx1
         fy(j) = fy(j-1) + fyy1
      end do
      dis1 = sqrt((fxx(i+1)-fx(j))**2 + (fyy(i+1)-fy(j))**2)
      if(dis1.le.1.0) j = j - 1
 700  continue

      return

      end subroutine rupsur
