        subroutine CantFindFile (filename,ierr)
        
        implicit none
        
        integer ierr
        character (LEN=20) filename
        
        if (ierr .eq. 2) then
           write (6,12) filename
        elseif (ierr .ne. 0)  then
           write (6,2) filename
        end if
        
2       format ("But...cannot open file:  ",a20,/  &
                "check that it is in the right directory!",/,  &
                "Must be in project directory, NOT in the Debug dir.",/,  &
                "STOP EXECUTION.")
12      format ("But...cannot open file:  ",a20,/  &
                "It looks like an END-OF-FILE was reached",    &
                "STOP EXECUTION.")

        return        
        end
