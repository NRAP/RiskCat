!        This routine is called from Riskcat
!
!*******************************************************************************
!
         subroutine BudgetM0 ( AVGLTM0,dt,Nepis,NMspl,Trelax,    &
                               xMagMax2,xMagMin2,xM0,Years )
!
!*******************************************************************************

!        Calculate the average M0 budget for a earthquake catalog
!        for a time period between T1 and T2 years, averaged over all epistemic
!        and aleatory catalog simulations, and for only events
!        with magnitudes between xMagMin2 and xMagmax2.
!
!        T1 will generally be the time 0
!        T2 will generally be the end of the catalog
!
!        These estimates are used for calibration of simulated catalogs with
!        long-term seismicity.
!        The averaging is over all epistemic 
!
!        Two modes to model long-term seismicity:
!        Mode 1. Stationary reccurence curves
!        Mode 2. Simulated with G-R time varying reccurence curves, with RSQSim,
!                or any other non-stationary model.
!
!        The output M0 is in dynes.cm
!
!*******************************************************************************
!
         USE ModuleEQ

         implicit none

         CHARACTER (len=3) :: stationary

         INTEGER :: i,iepis,iflt,inev,irec,is,ispl,iy,Large
         INTEGER :: Nepis,NextSc,NMspl,nsegt,nstr

         REAL    :: alfa1,alfa2,beta
         REAL    :: AVGLTM0,AVGLTM01,dt,dt1,dt2,Momentsum
         REAL    :: rnunf,r1,sigMmax,sum,SUMEqM0,SUMEqM01
         REAL    :: Timekeep1,Timekeep2,TimeSc,Trelax
         REAL    :: xMagMax2,xMagMin2,xMSc,xMmax,xM0,Years
!
!-------------------------------------------------------------------------------

         Large    = 10000000
         AVGLTM0  = 0.
         AVGLTM01 = 0.
         SUMEqM0  = 0.
         SUMEqM01 = 0.

         !Epistemic Loop:
         !---------------
         do iepis = 1, Nepis 
        
            !Select zonation map index: kzMap
            !--------------------------------
            sum = 0.
            r1 = rnunf()
            do i = 1, nMaps
                sum = sum + wMap(i)
                if (r1 .le. sum)  then;  exit;  endif
            end do
            kZMap = i
            Ns = NzMap(kzMap)

            !Sample set of epistemic source parameters for selected source Map
            !----------------------------------------------------------------- 
            !Parameters sampled include: 
            !Dip
            !Rake
            !Recurrence parameters
            do i = 1, Ns
                iS = Mapzid(kZMap,i)  
                iflt  = iS
                nsegt = nsegFault(iS)
                nstr  = nwstrips(iS)
                call SampleEpis ( iflt,nstr,nsegt )
            end do
  
  
            !Aleatory Loop:
            !-------------

            do ispl = 1, NMspl

                !Set sources' properties to initial values                
                do is = 1, Ns    
                    i = Mapzid(kZMap,is)
                    SELECT CASE (SeisRectype(i))                   
                        !Truncated exponential model
                        CASE (1)
                            !Mmax. 
                            ArecUpdt(i,7) = Arec(i,7)
                            xMmax = ArecUpdt(i,7)
                            !beta.
                            ArecUpdt(i,4) = Arec(i,4)
                            beta = ArecUpdt(i,4)
                            !alfa.
                            ArecUpdt(i,3) = Arec(i,3)
                            alfa1 = ArecUpdt(i,3)
                            ArecUpdt(i,1) = Arec(i,1) 
                            alfa2 = ArecUpdt(i,1) 
                            stationary    = 'yes'                    
                        !Characteristic model (Youngs-Coppersmith)
                        CASE (2)                   
                        !Segmented fault model (Foxall Model)
                        CASE (3)
                        !Empirical model manually entered
                        CASE (4)
                        CASE DEFAULT
                    END SELECT
                end do
                
                !if reccurence is stationary for all sources, use reccurence 
                !curves, otherwise simulate earthquake on time steps with  
                !recurence rates updated after each occrence
    
                Stationarity_if_Loop: if (stationary .eq. 'yes')  then 
                    !Occurrence rates are stationary for "Years"
                    !Cumulate seismic moment release for all sources
                    do is = 1, Ns    
                        i    = Mapzid(kZMap,is)
                        irec = SeisRectype(i)
                        xMmax = ArecUpdt(i,7)
                        beta  = ArecUpdt(i,4)
                        alfa1 = ArecUpdt(i,3)
                        alfa2 = ArecUpdt(i,1) 
                        call BudgetM0Stationary ( HKec,HKed,i,irec,    &
                        Momentsum,xMagmax2,xMagMin2,xM0,Years )
                        SUMEqM01 = SUMEqM01 + Momentsum
                    end do
                    !There is no aleatory uncertainty in this calculation
                    !Exit the aleatory loop.
                    exit

                else
              
                    !Occurence rates are updated after each occurence
                    !Let it rip to generate a catalog
                    !--------------------------------
                    !Find which is the next source, when, what mag,  etc..
                    dT1       = dt !Duration of time step, in years
                    dt2       = dt
                    inev      = 0
                    !Track time of occurrence of Eq in TimeKeep1, 2 and 3
                    TimeKeep1 = 0. 
                    sigMmax = 2. !range of uncertainty of number of events
                    !range is sigMax at magnitude Mmax.
                    !range is zero at M=0


                    ! Period of simulation during perturbation time = 0 to Trelax
                    !------------------------------------------------------------
                    Perturb_Period1: do iy = 1, Large   
                        if (TimeKeep1 .ge. Trelax)  then
                           exit
                        end if                   
                        !Select the nextsource, Mag, and time of occurrence
                        !of next event
                        !dt2 is fraction of year
                        call NextEvent(dt2,xM0,xMSc,NextSc,TimeSc,sigMmax)
                        !Next event is occurring after TimeSc from last Eq
                        !Source: NextSc
                        !Magnitude: xMSc
                        if (NextSc .gt. 0)  then
                             TimeKeep2 = TimeKeep1 + TimeSc
                             TimeKeep1 = TimeKeep2  
                             if((xMSc.ge.xMagMin2) .and. (xMSc.le.xMagMax2))then
                                !M0 in dynes.cm
                                SUMEqM0 =   SUMEqM0 + (10. ** (1.5*xMSc + 16.1))
                             end if                         
                             !Update sources rate after occurrence in NextSC
                             !via time dependent influence matrix
                             call UpdateScRates (NextSC,TimeSc,xMSc)
                         end if                    
                    end do Perturb_Period1

                    !Post perturbation period
                    !------------------------
                    Relaxed_Period1:   do iy = 1, Large    
                        !Stop simulation if Time > Nyears
                        if (TimeKeep1 .ge. Years)  then
                            exit
                        end if                   
                        !Select nextsource, Mag, and time of occurrence 
                        !dt2 is fraction of year,
                        call NextEvent(dt2,xM0,xMSc,NextSc,TimeSc,sigMmax)
                        !Next event is occurring after TimeSc from last Eq
                        !Source: NextSc
                        !Magnitude: xMSc
                        if (NextSc .gt. 0)  then
                            inev = inev + 1
                            TimeKeep2 = TimeKeep1 + TimeSc
                            if((xMSc.ge.xMagMin2) .and. (xMSc.le.xMagMax2))then
                               SUMEqM0 =   SUMEqM0 + (10. ** (1.5*xMSc + 16.1))
                            end if
                            TimeKeep1 = TimeKeep2
                            !Update sources rate after occurrence in NextSC
                            !via time dependent influence matrix
                            call UpdateScRates (NextSC,TimeSc,xMSc)
                        end if                                                                  
                    end do Relaxed_Period1  

                end if Stationarity_if_Loop        
         
            end do !Aleatory loop

!            print *, 'AVG M0 stationary-simulated       =',SUMEqM01,SUMEqM0,'    iepis:',iepis


         end do    !Epistemic loop

         !Average total regional M0 per catalog for post perturbation period
         !Based on reccurences curves (G-R or others, stationary or others)
         AVGLTM01 = SUMEQM01 / Float (Nepis)
         AVGLTM0  = SUMEqM0  / Float (Nepis*NMspl)
         if (stationary .eq. 'yes')  then
             AVGLTM0 = AVGLTM01
         end if

         return

         end subroutine BudgetM0
