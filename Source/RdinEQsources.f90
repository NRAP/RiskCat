!     Called by:  subroutine EQsourcesinfo
!
!.....This routine reads in the info on the seismic sources
!       -Nodes used to describe sources 
!        (apexes of areas, nodes on fault traces)
!               all nodes coordinates X=long, Y=Lat
!       -Source type 
!               1=area, 2=fault)
!       -Source recurrence relationship 
!               1=trunc exp., 
!               2=characteristic
!               3=segmented fault
!               4=enter empirical values
!       -Maps of seismic sources for Nzonations Maps (NZM)
!       -Weights for each maps: wnzm()
!-------------------------------------------------------------------------------

      subroutine RdinEQsources(M0,nsimrec,Ns1,unit1)
      
      use ModuleEQ
      
      implicit none
      
      integer :: i,IDrel,IERR,ix1,j,k
      integer :: MapID
      integer :: n3,n4,n5,n6,nerror,Nexpected,Ns1
      integer :: Nlast,nMag,Nodesource,nsimrec
      integer :: nrel,Nstring,unit1
      INTEGER, DIMENSION(80) ::Ndumread
      integer, dimension(MaxEQsources)  :: Magrel
      integer, dimension(MaxEQsources)  :: sourceID,OutArray
 
      character (len=20)     :: dum
      character (len=60)     :: Message
      character (len=256)    :: string

      CHARACTER (LEN=80),DIMENSION(80)  :: DUMREAD
      
      real :: defD1,defD2,defDIP,defRlw
      real :: defdelD1,defdelD2,defdeldepth,defdelDIP
      real :: defdelRlw
      real :: MLow, MHigh, M0
      real :: realDUM,relsum,Rlw,xM0,x1
      
      real, dimension(MaxMagreL)     :: alfrel,blfrel
      real, dimension(MaxMagrel)     :: sigrel,wtrel
      real, dimension(MaxEQsources)  :: RlwFlt


!*******************************************************************************
!.....Open afile as a .txt format.
!     Default name:  sources.txt
      if (afile .eq. '   ')  then
          afile = 'sources.txt'
      end if
      string = afile
      open (unit=unit1,file=afile,status="old",iostat=IERR)
      read (unit1,*)  dum
      read (unit1,*)  dum
      !

!*******************************************************************************
      !     Default values
      !Aspect ratio Rlwflt
      defRlw = 0.5
!*************************** SOURCES NODES *************************************      
!.....Number of sources
      read (unit1,*)  Ns1
      read (unit1,*)  dum
      read (unit1,*)  dum
      
!.....Read Nodes ID, Long and Lat, plus some info
      nerror = 0
      do i = 1, MaxNodesource
          call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
          if (DUMREAD(1) .eq. 'END')  then
              exit
          else
              read (DUMREAD(1),*,ERR=100) nodeID(i)
              go to 150
100           string = 'nodeID'
              write (*,*) 'ERROR reading',string,' Node;',i
              nerror = nerror + 1
              
150           read (DUMREAD(2),*,ERR=200) snlong(i)
              go to 250
200           string = 'Longitude'
              write (*,*) 'ERROR reading',string,' Node;',i
              nerror = nerror + 1
              
250           read (DUMREAD(3),*,ERR=300) snlat(i)
              go to 350
300           string = 'Latitude'
              write (*,*) 'ERROR reading',string,' Node;',i
              nerror = nerror + 1
350           continue
          end if
          
      end do
      if (nerror .gt. 0)  then
          write (*,*) nerror,' in coordinates of sources nodes'
          write (*,*) 'STOP Execution. Please check Nodes file'
          STOP 
      end if
      Nodesource = i-1
!
!      
!************************** SOURCES GEOMETRY **********************************
      read (unit1,*) dum
      read (unit1,*) dum
      
!.....Default values for AREA sources
      !defdeldepth = default depth increment for volume sampling
      read (unit1,*)  defdeldepth
      read (unit1,*) dum
      
!.....Default values for FAULT sources
      !DIP    = fault dip
      !D1     = minimum depth of top of fault
      !D2     = maximum depth of bottom of rupture
      !Rlw    = aspect ratio of fault Width/Lenth of rupture
      !delxxx = maximum +/- variation in parameters
      defDIP = 0.; defD1 = 0.; defD2 = 0.; Rlw = 0.
      defdelDIP = 0.; defdelD1 = 0.; defdelD2 = 0.; defdelRlw = 0.
      
      read (unit1,*) defDIP,defD1,defD2,Rlw
      read (unit1,*) defdelDIP,defdelD1,defdelD2,defdelRlw

      
!     Read the description of the Magnitude-Fault Length relationships
!      nrel = number of relationships
!      		maximum value for nrel is mlrelat [parameter (mtrelat=10)]

!      i = 1:
!      Magnitude-Length relationship:
!      wtrel(i) = weigth of i-th M-L relationship
!      alfrel(i), blfrel(i), sigrel(i) = parameters for the i-th
!      		M-L relationship of the type:
!      			log10 (length-km) = alfrel(i)  + blfrel(i)*magnitude
!      	    isimrel(i) = index specifying whether the default M-L 
!      	    relationship is used (isimrel(i) = 0) or a relationship is 
!           selected at random from the set of nrel relationships. One
!      	    selection for each simulation.

!     i = 2:
!     Magnitude-Area relationship:
!           M-L relation: param.  xh, xh, sigma 
!           Area(km**2) = M*xf + xh + epsilon [N(0,sigma)]

           read (unit1,*) nrel
           relsum = 0.
           do  i = 1, nrel
               read (unit1,*) IDrel,wtrel(i),alfrel(i),blfrel(i),sigrel(i)
               relsum = relsum + wtrel(i)
           end do
           !Normalize the weights of the M-L relationships
           do  i = 1,nrel
               if (relsum .gt. 0.) then
                   wtrel(i) = wtrel(i) / relsum
               else
                   wtrel(i) = 1. / nrel
               end if
           end do
	    
	    
!.....Start reading source specific geometry data
      read (unit1,*) dum
      read (unit1,*) dum
      do i = 1, Ns1
      
          !READ (SourceID, 'AREA'/'FAULT', SourceName)
          !^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          read (unit1,*) SourceID(i),DUM,SourceName(i)
          
          IF (DUM .EQ.'AREA')  then
              SourceType(i) = 1
            elseif (DUM .eq. 'FAULT')  then
              SourceType(i) = 2
            else
              !Error in source type, stop execution
              write (*,*) 'ERROR in Source Type. Source:',  &
              i,SourceName(i)
              STOP
          end if
          
          SELECT CASE (SourceType(i))
          
              CASE (1)
                
!.............Read AREA sources description

              !Area (flat), and volumes (cylinder) sources
              !Read number of nodes, min and max depth, sampling depth 
              !interval, and flag for reading first and last node
              !n3 > 0 read all the nodes, number of nodes is n3
              !n3 > 0 read first and last, number of nodes is -n3
              !areaD1() = top surface of volume
              !areaD2() = bottom of volume
              !Depth axis is positive downward
              !deldepth() = interval of sampling depth, deafaulot is 0.1km
              deldepth(i) = 0.1
              
              !READ (nodes,areaD1,areaD2,DelDepth)
              !^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
              call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
              if (Nstring .lt. 3)  then
                  write (*,*) 'ERROR reading source',i
                  write (*,*)  &
                  '....Number of nodes, Depth 1, Depth 2, Deldepth'
                  write (*,*) '....Needs at least 3 entries'
                  write (*,*) '....Stop Execution'
              end if
              read (DUMREAD(1),*) n3
              read (DUMREAD(2),*) areaD1(i)
              read (DUMREAD(3),*) areaD2(i)
              deldepth(i) = defdeldepth
              if (Nstring .eq. 4)  then
                  read (DUMREAD(4),*) deldepth(i)
              end if
              if (n3 .eq. 0)  then
                  write (*,*) 'ERROR nodes number = 0',i,SourceName(i)
              end if 
!
              !Nodes IDs are nor necessarily sequential but they must
              !follow the periphery of the area sequentially, and 
              !if the IDs are sequential (n3<0), then enter only 
              !first and last node ID.
              if (n3 .gt. 0)  then
                  read (unit1,*) (scrnodes(i,j),j=1,n3)
                else
                  n3 = -n3
                  read (unit1,*)  scrnodes(i,1),nlast
                  do j = 2, n3
                      scrnodes(i,j) = scrnodes(i,j-1) + 1
                  end do
                  if (scrnodes(i,n3) .ne. nlast)  then
                      write (*,*) 'ERROR Source :',i,  &
                      'Wrong Nodes numbers'
                  end if
              end if
              scrnodnum(i) = n3
 
              CASE (2)
              
!.............Read FAULT geometry description

              !READ (scrnodnum, nestrips RealDUM, Magrel)
              !^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
              !Read number of nodes, number of W-strips
              call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
              if (Nstring .lt. 3)  then
                  write (*,*) 'ERROR reading source',i
                  write (*,*)  &
                  '....Number of nodes, Number of W-strips'
                  write (*,*) '....Needs at least 3 entries'
                  write (*,*) '....Stop Execution'
              end if 
        
              read (DUMREAD(1),*) n3
              read (DUMREAD(2),*) nwstrips(i)
              read (DUMREAD(3),*) RealDUM
              !Mag-Area, or length relationship
              Magrel(i) = 1   !default
              if (DUMREAD(4).ne.'  ')  then
                  read (DUMREAD(4),*) Magrel(i)
              end if
              !             
              if (n3 .eq. 0)  then
                  write (*,*) 'ERROR nodes number = 0',i,SourceName(i)
              end if               
              !Nodes IDs are not necessarily sequential but they must
              !follow the periphery of the area sequentially, and 
              !if the IDs are sequential (n3<0), then enter only 
              !first and last node ID.
              if (n3 .gt. 0)  then
                  read (unit1,*) (scrnodes(i,j),j=1,n3)
                else
                  n3 = -n3
                  read (unit1,*)  scrnodes(i,1),nlast
                  do j = 2, n3
                      scrnodes(i,j) = scrnodes(i,j-1) + 1
                  end do
                  if (scrnodes(i,n3) .ne. nlast)  then
                      write (*,*) 'ERROR Source :',i,  &
                      'Wrong Nodes numbers'
                  end if
              end if
              scrnodnum(i) = n3
              nsegFault(i) = n3-1
              
              !READ (Wtop (nstrips) )
              !^^^^^^^^^^^^^^^^^^^^^^
              !Read Depth of top and bottom of W-strips
              call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
              if (Nstring .lt. (nwstrips(i)+1))  then
                  write (*,*) 'ERROR reading W-strips depths source',i
                  write (*,*) '....Need (Nstrips+1) values'
                  write (*,*) '....Stop Execution'
              end if 
              
              read (DUMREAD(1),*) Wtop(i,1)
              do j = 2,nwstrips(i)
                  read (DUMREAD(j),*) Wtop(i,j)
                  Wbot(i,j-1) = Wtop(i,j)
              end do            
              read (DUMREAD(nwstrips(i)+1),*) Wbot(i,nwstrips(i))
!              read (DUMREAD(Nstring),*) Wbot(i,nwstrips(i))


              !Read geometry of each W-strip
              do j = 1, nwstrips(i)
              
                  !DIP-Best estimate
                  call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
                  Nexpected = nsegFault(i)
                  Message = 'DIP'
                  do k = 1,nsegFault(i)
                      DIPRd(i,j,k) = defDIP
                      if (DUMREAD(k) .ne. '  ')  then
                          read (DUMREAD(k),*,ERR=500) DIPRd(i,j,k)
                          go to 550
500                       call EQinERROR (DUMREAD,i,Message, &
                          Nexpected,Nstring,SourceName(i))
550                   end if
                  end do                  
                  !Minimum DIP
                  call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
                  Message = 'Minimum DIP'
                  do k = 1,nsegFault(i)
                      DIP1Rd(i,j,k) = defDIP - defdelDIP
                      if (DUMREAD(k) .ne. '  ')  then
                          read (DUMREAD(k),*,ERR=600) DIP1Rd(i,j,k)
                          go to 650
600                       call EQinERROR (DUMREAD,i,Message, &
                          Nexpected,Nstring,SourceName(i))
650                   end if 
                  end do
                  !Maximum DIP
                  call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
                  Message = 'Maximum DIP'
                  do k = 1,nsegFault(i)
                      DIP2Rd(i,j,k) = defDIP + defdelDIP
                      if (DUMREAD(k) .ne. '  ')  then
                          read (DUMREAD(k),*,ERR=700) DIP2Rd(i,j,k)
                          go to 750
700                       call EQinERROR (DUMREAD,i,Message, &
                          Nexpected,Nstring,SourceName(i))
750                   end if 
                  end do

                  !Rake-Best estimate
                  call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
                  Message = 'Rake'
                  do k = 1,nsegFault(i)
                      RakeRd(i,j,k) = 180.  !Default Rake = SS
                      if (DUMREAD(k) .ne. '  ')  then
                          read (DUMREAD(k),*,ERR=760) RakeRd(i,j,k)
                          go to 770
760                       call EQinERROR (DUMREAD,i,Message, &
                          Nexpected,Nstring,SourceName(i))
770                   end if 
                  end do

                  !Rake-Minimum
                  call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
                  Message = 'Rake'
                  do k = 1,nsegFault(i)
                      Rake1Rd(i,j,k) = 180.  !Default Rake = SS
                      if (DUMREAD(k) .ne. '  ')  then
                          read (DUMREAD(k),*,ERR=761) Rake1Rd(i,j,k)
                          go to 771
761                       call EQinERROR (DUMREAD,i,Message, &
                          Nexpected,Nstring,SourceName(i))
771                   end if 
                  end do

                  !Rake-Maximum
                  call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
                  Message = 'Rake'
                  do k = 1,nsegFault(i)
                      Rake2Rd(i,j,k) = 180.  !Default Rake = SS
                      if (DUMREAD(k) .ne. '  ')  then
                          read (DUMREAD(k),*,ERR=762) Rake2Rd(i,j,k)
                          go to 772
762                       call EQinERROR (DUMREAD,i,Message, &
                          Nexpected,Nstring,SourceName(i))
772                   end if 
                  end do
                 
              end do
              
              !Aspect Ratio of simulated ruptures for the entire Fault
               call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
               Nexpected = 1
               Message   = 'Aspect Ratio Rlw'
               read (DUMREAD(1),*,ERR=800) Rlwflt(i)
               go to 850
800            call EQinERROR (DUMREAD,i,Message, &
               Nexpected,Nstring,SourceName(i))                                                            
850            if (Rlwflt(i) .eq. 0.)  then
                   RlwFlt(i) = defRlw
               end if
              CASE DEFAULT
                    
          END SELECT
      end do
!
!
!*************************** SEISMICITY ****************************************

!.....Read seismicity rates information
      read (unit1,*) DUM
      
!     Read info to define working array of Magnitudes 
!     and number of recurrence simulations
!     Mlow and MHigh are the values used to define the full array of magnitude
!     M0 is the generic value used for the start of "integration"
!     Mstart and Mend are used either to narrow down the range of Magnitude
!     values possible for a specific source, or to limit the range of 
!     Magnitudes used in the plotting of faults
!     Magnitude array:
!     ****************
!         MLow  = lowest magnitude considered in the run
!         MHigh = largest considered
!         M0    = Lowest Magnitude for magnitude integration
!         Mstart= Lowest magnitude for a specific source
!         Mend  = Largest Magnitude for a specific source
!     Reccurrence simulations:
!     ************************
!     nsimrec = number of simulations of reccurrence to model ureccurrence 
!               epistemic uncertainty
!   
 
      Meqmax = 0.
      
      !Read info on working magnitude array
      !READ (MLow,MHigh,delM)
      !^^^^^^^^^^^^^^^^^^^^^^
      call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
      read (DUMREAD(1),*,ERR=854) MLow
      read (DUMREAD(2),*,ERR=854) MHigh
      read (DUMREAD(3),*,ERR=854) delM
      go to 855
854   write (*,*) 'ERROR working Magnitude array, MLow,MHigh,DelM'
      Write (*,*) 'Values read:'
      do j = 1, Nstring
          write (*,"(a80)") DUMREAD(j)
      end do
      write (*,*) '..........Stop Execution'
      STOP
      
      !Read number of reccurrence simulations. Set to default if zero
      !read (NSIMREC)
      !^^^^^^^^^^^^^^
855   call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
      read (DUMREAD(1),*,ERR=856) nsimrec
      go to 857
856   write (*,*) 'ERROR in number of reccurrence simulations: nsimrec'
      Write (*,*) 'Values read:'
      do j = 1, Nstring
          write (*,"(a80)") DUMREAD(j)
      end do
      write (*,*) '..........Stop Execution'
      STOP
857   if (nsimrec .eq. 0)  then
          nsimrec = defnsimrec
      end if
            
!.....Construct working array of Magnitudes
      nmag = 1
      xMagLBnd(1) = MLow
      VMagBnd(1)  = xMagLBnd(1)
      xMagHBnd(1) = xMagLBnd(1) + delM
      do j = 2, MaxMagBins
         xMagLBnd(j) = xMagLBnd(j-1) + delM
         VMagBnd(j)  = xMagLBnd(j)
         xMagHBnd(j) = xMagLBnd(j) + delM
         if (XmagLBnd(j) .gt. MHigh)  EXIT
         nmag = nmag + 1
      end do
      VMagBnd(MaxMagBins+1)  = xMagHBnd(MaxMagBins)
      if (nmag .gt. Maxmag)  then
         !Number of magnitude bins greater than dimensions
         write (*,*) 'ERROR too many magnitude bins:'
         write (*,*) 'Number of bins =',nmag,' Max allowed:',Maxmag
         write (*,*) '..........STOP EXECUTON'
         STOP
      end if
      nmagbins = nmag 
      !Center bin Magnitude array
      do j = 1, nMagbins
          xMagbins(j) = (xMagLBnd(j+1) + XMagLBnd(j))/ 2.
      end do
     
!.....Read minimum Mag of integration M0, if =0, set to default (M0=4.5)
      !READ (Xm0)
      !^^^^^^^^^^
      read (unit1,*) xM0
      M0 = xM0
      if (xM0 .eq. 0.)  then
          M0 = defM0
      end if
!
!.....Do we want to plot the reccurrence curves?
      !Iplotrec = 0  No plot 
      !         = 1  Plot as requested for specific sources (iplotrec1(i))
      !         = 2  Plot for ALL sources regardless of specific request
      read (unit1,*) DUM
      read (unit1,*) iplotrec
     
!.....Read seismicity parameters for each seismic source
      do i = 1, Ns1
         !Source ID, Type of recurrence relationship, and assignment of
         !Ground Motion prediction set
         !READ ( IDdum, SeisRecType, iSourceGM, DUM )
         !^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
         Nexpected = 4
         Message = 'Source ID, Recurrence Type'
         if (Nexpected .ne. Nstring)  then
             call EQinERROR (DUMREAD,i,Message, Nexpected, &
             Nstring, SourceName(i))
         end if

         nerror = 0
         read (DUMREAD(1),*,ERR=865) j
         read (DUMREAD(2),*,ERR=865) SeisRectype(i)
         read (DUMREAD(3),*,ERR=865) iSourceGM(i)
         go to 867
865      nerror = nerror + 1
         if (nerror .ne. 0)  then
             write (*,*) 'ERROR in Source: ',SourceName(i)
             write (*,*) 'Source ID, Recurrence Type'
             write (*,*) '..........STOP Execution'
         end if 
         
867      read (DUMREAD(4),*) DUM
         if (DUM .ne. SourceName(i))  then
             write (*,*) 'Source Name mismatch. Seismicity ID:',j
             write (*,*) 'Source Seismicity Name:',dum
             write (*,*) 'Source geometry name  :',SourceName(i)
         end if  
         
         !Request for plotting?
         call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
         if (Nstring .gt. 1)  then
             Write (*,*) 'WARNING Reccurrence plot request for:',  &
             SourceName(i)
             write (*,*) 'Request Flag=',DUMREAD(1)
         end if
         iplotrec1(i) = 0
         read (DUMREAD(1),*) k
         if  ((k.eq.1) .or. (iplotrec.eq.2))  then
         iplotrec1(i) = 1
         end if
                 
      
         select case (SeisRecType(i))
         
             case (1)
!............Truncated Exponential: Input G-R parameters and more
!            (on G-R is on Natural Log)

             !Input main variables: (REAL)
             !---------------------
             !Arec(i,1)= MLam Magnitude for which Lambda is given 
             !Arec(i,2)= MG (Center of Gravity of the seis data, if known, or 0)
             !Arec(i,3)= Lambda at MLam (annual rate of ocurrence)
             !Arec(i,4)= Beta (Slope of G-R in natural log)
             !Arec(i,5)= Std dev on log10 of Lambda
             !Arec(i,6)= Std dev on Beta
             !Arec(i,7)= Best Estimate of Max Magnitude for the source
             !Arec(i,8)= Lower bound MaxMag. If 0, use  + or - delM
             !Arec(i,9)= Upper bound MaxMag. If 0, use  + or - delM
             !Arec(i,10)= delM, uncertainty on Magmax

             !Variables for uncertainty description: (REAL)
             !--------------------------------------
             !Brec(i,j)= Type of probability distribution
             !           1.: Uniform between MmaxLower and MmaxUpper
             !           2.: Triangular. 0 at Mlower, Mode at Mbest, 0 at Mupper
             !           3.: Trapeze. Uniform on [Mlower-Mbest], 0 at Mupper
             !           4.: Trapeze. 0 at Mlow, uniform on [Mbest-Mupper]
             !           5.: Gaussian, limited by +/- 1-sigma (70%)
             !           6.: Gaussian, limited by +/- 2-sigma (90%)
             !           7.: Gaussian, limited by +/- 3-sigma (98%)
             !          -n.: Discrete, where n is the number of points (n<=10)
             !           ----------------------------------------------------
             !           j = 1 for Max Magnitude
             !               2 for slip 
!
             !Read parameters to construct G-R curve
             call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
             Nexpected = 6
             n6 = Nexpected
             Message = 'MLam,MG,Lambda(Mlam),Beta,delLambda,delBeta'
             if (Nexpected .ne. Nstring)  then
                 call EQinERROR (DUMREAD,i,Message, Nexpected, &
                 Nstring, SourceName(i))
             end if
             nerror = 0
             do j = 1, Nstring
                 read (DUMREAD(j),*,ERR=870) ArecRd(i,j)
                 go to 875
870              nerror = nerror + 1
875          end do             
             if (nerror .ne. 0)  then
                 write (*,*) 'ERROR in Source: ',SourceName(i)
                 write (*,*) 'MLam,MG,Lambda,Beta,delLambda,delBeta'
                 write (*,*) '..........STOP Execution'
             end if
             
             !Read Max Mag 
             call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
             Nexpected = 5
             Message = 'MmaxBE,MmaxLower,MmaxUpper,delMmax,Distype'
             if (Nexpected .ne. Nstring)  then
                 call EQinERROR (DUMREAD,i,Message, Nexpected, &
                 Nstring, SourceName(i))
             end if
             nerror = 0
             do j = 1,4
                 read (DUMREAD(j),*,ERR=880) ArecRd(i,j+n6)
                 go to 885
880           nerror = nerror + 1
885          end do
             read (DUMREAD(5),*,ERR=890) BrecRd(i,1)
             go to 895
890          nerror = nerror + 1             
895          if (nerror .ne. 0)  then
                 write (*,*) 'ERROR in Source: ',SourceName(i)
                 write (*,*) 'MmaxBE,MmaxLower,MmaxUpper,delMmax,Distype'
                 write (*,*) '..........STOP Execution'
             end if 

             !Read amount of Slip for the fault (3 Arec values, 1 Brec)
             call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
             Nexpected = 4
             n6 = 10
             Message = 'Min Slip, B.E. Slip, Max Slip,Distrib.type'
             if (Nexpected .ne. Nstring)  then
                 call EQinERROR (DUMREAD,i,Message, Nexpected, &
                 Nstring, SourceName(i))
             end if
             nerror = 0
             do j = 1,3
                 !Read Slip rates in mm/yr
                 read (DUMREAD(j),*,ERR=896) x1
                 !Transform Slip rates into m/sec
                 ArecRd(i,j+n6) = x1 /(3600.*24.*365.*1000.)
                 go to 897
896              nerror = nerror + 1
897          end do
             read (DUMREAD(4),*,ERR=898) ix1
             BrecRd(i,2) = ix1
             go to 899
898          nerror = nerror + 1             
899          if (nerror .ne. 0)  then
                 write (*,*) 'ERROR in Source: ',SourceName(i)
                 write (*,*) 'Min Slip, B.E. Slip, Max Slip,Distrib.type'
                 write (*,*) '..........STOP Execution'
             end if 


!............Interval of Magnitudes of interest for this source
!             MagStart(i) = M0
!             MagStop(i)  = Arec(i,8)  ! the maximum maximurum Mag
             !Index of the array of Magnitude bins containing these values
             iMagStart(i)  = 1 
             iMagStop(i)   = 1 
             do j = 1,Maxmagbins
             if (xMagLBnd(j).gt. M0)  exit
                 iMagStart(i) = j
             end do
             do j = 1,Maxmagbins
                 if (xMagLBnd(j).gt. (ArecRd(i,8)+ArecRd(i,10)))  exit
                 iMagStop(i) = j - 1
             end do
             
             if (Meqmax .lt. ArecRd(i,9))  then
                Meqmax = ArecRd(i,9)
             end if
             
             
             case (2)
             !Characteristic Earthquake:Input Youngs & Coppersmith parameters
             
             case (3)
             !Segmented rupture model: Foxall model parameters
             
             case (4)
             !Values entered for each value of Magnitude
             
             case default
             
         end select
      
     
      end do
     
     
      
!
!************************** ZONATION MAPS **************************************

!.....Read description of zonation maps

      !Rewind unit1 and position to read info on zonation maps
      rewind (unit1)
      do j = 1,100000
          read (unit1,*) DUM
          if (DUM .eq. 'Zonation-Maps') exit
      end do
      
      call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
      
      !Number of zonation maps
      read (DUMREAD(1),*,ERR=900) Nmaps
      go to 950
900   write (*,*) 'ERROR in reading Number of Maps'
      If (Nstring .lt. 1)  then
          write (*,*) 'Number of expected values: 1'
          write (*,*) 'Number values read:',Nstring
          write (*,*) 'Values read:'
          do i = 1, Nstring
              write (*,"(a80)") DUMREAD(i)
          end do
          write (*,*) '..........STOP Execution' 
          STOP
      end if    
        
950   do i = 1, Nmaps
          call DUMREADER (DUMREAD,Ndumread,Nstring,unit1)
          !Map ID number
          read (DUMREAD(1),*,ERR=1000) MapID
          !Number of sources in map i
          read (DUMREAD(2),*,ERR=1000) nzMap(MapID)
          !Weight of map i
          read (DUMREAD(3),*,ERR=1000) wMap(MapID)
          go to 1050
1000      write (*,*) 'ERROR in reading Map ID and/or Map weight'
          Write (*,*) 'Values read:'
          do j = 1, Nstring
              write (*,"(a80)") DUMREAD(j)
          end do
          write (*,*) '..........Stop Execution'
          STOP
          
1050      n4 = nzMap(i)
          call  ReadIntegArray  (MaxEQsources,n4,n5,OutArray,unit1) 
          If (n4 .ne. n5) then
              write (*,*) 'ERROR for zonation map:',i
              write (*,*) 'Expected number:',n4,' sources'
              write (*,*) 'Number found   :',n5,' sources'
              write (*,*) '..........STOP EXECUTION'
              STOP
          end if       
          do j = 1, n4
              MapZid(i,j) = OutArray(j)
          end do
      end do
          !
          !Normalize Maps weights
          realDUM = 0.
          do j = 1, nMaps
              realDUM = realDUM + wMap(j)
          end do
          do j = 1, nMaps
              wMap(j) = wMap(j) / realDUM
          end do
          
             
      
          
      close (unit1)  
      return
        
      end subroutine RdinEQsources
