!
!******************************************************************************
        subroutine DUMREADER  (Alpha,Nchar,Nstring,unit)
!******************************************************************************
!
!   This routine reads a generically formatted record of 80 characters 
!   (InAlpha), spits out the separated strings of continuous non-blank
!   characters, and spits out those strings, their number , and the number 
!   of characters in each string.

!   INPUT:
!       unit = read unit number
!       The input is what is read by reading unit as a 80 character record 
!       in the first executable statement of the subroutine
!

!
!   OUTPUT:
!       Alpha(i) = array of characters in STRING
!       Nchar(i) = integer array of number of caharacters in Alpha(i)
!       Nstring  = Number of strings read in the record
!

    IMPLICIT NONE
    
    INTEGER :: i,j, n1, n2, Nstring, unit
    
    INTEGER, DIMENSION(80) :: Nchar, nstart
    INTEGER, DIMENSION(81) :: nar, nsw
    
    CHARACTER (LEN=1)  :: a1
    CHARACTER (LEN=80), DIMENSION(80) :: ALPHA
    CHARACTER (LEN=1),  DIMENSION(81) :: ALPHA1
    
      a1         = ' '
      ALPHA1(81) = a1
      nar(81)    = 0
      ALPHA = ' '
      
!First executable statement:
      read (unit,"(80a1)") (ALPHA1(i),i=1,80)
 !
 !Places occupied    
      do i = 1, 80
          if (ALPHA1(i) .ne. a1)  then
              nar(i) = 1
           else
              nar(i) = 0
          end if
      end do

!Switches
      n1   = 0
      do i = 1,81
      nsw(i) = 0
          if (nar(i) .ne. n1) then
          nsw(i) = 1
          n1 = nar(i)
          end if
      end do
      
!Find strings beggining, end and number of characters
      Nstring = 0
      n1 = -1
      do i = 1,80
          if (nsw(i) .eq. 1)   then
              if (n1 .eq. -1)  then
                  Nstring = Nstring + 1
                  nstart(Nstring) = i
                  n1 = -n1
                else
                  Nchar(Nstring) = i - nstart(Nstring)
                  n1 = -n1
              end if
          end if
      end do
      
!Create Alpha() strings
      do i = 1, Nstring
          n1 = nstart(i)
          n2 = n1 + Nchar(i)
          write (Alpha(i),"(80a1)") (Alpha1(j),j=n1,n2)
      end do
      
      RETURN
      
      END SUBROUTINE DUMREADER
