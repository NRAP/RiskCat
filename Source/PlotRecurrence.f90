#ifdef USE_DISLIN
!
! This routine plots the recurrence models used in the run. 

!      Called by routine:  EQsourcesinfo and by WeightCat

!      Uses DISLIN graphic library.
!
  !****************************************************************************    
      SUBROUTINE PlotRecurrence (CSTR1,CSTR2,CTIT1,CTIT2,jwdt,LegendTIT,  &
                 Maxmag,N,NameEXT,NameModels,NameRoot,Nsource,SOURCE,     &
                 Nc,Nportrait,Nscreen,NVIEW,NWIDTH1, pagenumber,X,Y)
!******************************************************************************
!
!   Plots Recurrence curves used in run. Standard recurrence relationships 
!   (mean,+- 1 standardd dev.), and Foxall results of simulations for 
!   segmented faults.
!   The X-axis is Magnitude, (linear)
!   The Y-axis is Annual rate of ocurrence
!
!       Plot is either displayed on screen or stored in file PLOTFILENAME
!       PLOTFILENAME must include the extension (i.e .pdf, .png, etc.)
!       Nview   = 1 Display on screen
!                 2 Stores graphics on .PNG, .PDF, .BMP, .WMF or .PS file
!       NOTE on stored files:
!       *********************
!       DISLIN allows creation of new pages only for PDF and PS formats
!       Therefore, for all other formats (PNG, BMP and WMF), a new file
!       is created for each new figure. The name of the file is then the
!       original Root name followed by the figure number, and the extension
                       
!       CSTR1,  = For the identification title at top of the page 
!       CSTR2     which includes:
!                    "string CSTR1"/Date and Time of the Run/"String CSTR2"      
!       jwdt    = Selection of Date and Time page header, and page number
!               = 1, YES
!               = 0, NO Date and time page header, and NO page number
!       LegendTIT   = Legend Title
!       Maxmag  = Max number of magnitude points on X axis
!       N       = Number of points in each curve to be plotted (same for all)
!       NameModels  = Name of the models plotted
!       Nc      = Number of curves
!       Nportrait   = 1 plot in portrait layout
!                     2 plot in landscape layout
!       Nscreen = For Nview=1 only (Screen Display), counts number of frames 
!                 displayed. This number runs through the entire application
!       Nwidth1 = Width of the major lines (Nwidth=15 default)
!       Nwidth2 = Width of other lines (good number is Nwidth2=10)
!       Nview   = Selects the screen or file format (See above note)
!       pagenumber  = For (Nview.NE.1 (storing on file'NamePlotRec'), keeps
!                 a counter of number of pages.
!       SOURCE  = Name of Seismic SOURCE
!       TEXTREC = Root name for the Recurrence plot file (to which we add
!                 the selected extension, .png, .pdf, ...)
!       XMAG1   = Second part of the second line in the figure title
!       X(I,J)  = Array of max dimension (Nf,NcMax) of X values for Nc curves
!       Y(i,j)  = Array of max dimension (Nf,NcMax) of Y values for Nc curves     
!                                      
      USE DISLIN
      IMPLICIT NONE
      
      integer :: jwdt,k,Maxmag
      integer :: N1,N11,N12,N2,N21,N22,N3,N31,N32,nalpha1,nalpha2 
      integer :: Nc,NC1,NC2,NC3,Np
      integer :: newfile 
      integer :: Nscreen,Nsource
      integer :: pagenumber !Counter for the pages being plotted
      
      CHARACTER (LEN=1)  :: a1
      CHARACTER (LEN=Nc*20) :: CBUF
      CHARACTER (LEN=41) :: CSTR1
      CHARACTER (LEN=20) :: CSTR2
      CHARACTER (LEN=60) :: CTIT1,CTIT3
      CHARACTER (LEN=27) :: CTIT2
      CHARACTER (LEN=20) :: LegendTIT
      character (len=256):: message
      CHARACTER (LEN=5)  :: NameEXT
      CHARACTER (LEN=80) :: Nameplotrec
      CHARACTER (LEN=14) :: NamePlotRec1
      CHARACTER (LEN=80) :: NameRoot
      CHARACTER (LEN=256):: SOURCE
      CHARACTER (LEN=256):: STRING
      CHARACTER (LEN=9)  :: TitleX
      CHARACTER (LEN=25) :: TitleY
      CHARACTER (lEN=1),  DIMENSION(256):: ALPHA
      CHARACTER (LEN=1),  DIMENSION(256):: Alpha1,ALPHA2,Alpha3
      CHARACTER (LEN=5),  DIMENSION(3)  :: CLAB = (/'LOG  ','FLOAT','ELOG '/)
      CHARACTER (LEN=1),  DIMENSION(10) :: Cview2
      CHARACTER (LEN=7),  DIMENSION(8)  :: KOLOR
      CHARACTER (LEN=20), DIMENSION(6)  :: NameModels
      
      INTEGER :: I,j,N,Nportrait,Nview,NWIDTH1
      INTEGER :: NX,NYA,NYLABEL
      
      REAL    :: Xmax,Xmax1,Xmin,Xmin1,XP,Ymax,Ymax1,Ymin,Ymin1
      REAL, DIMENSION(Maxmag)   :: XA,YA
      REAL, DIMENSION(Nc,Maxmag) :: X,Y
!
!INITIALIZATIONS:
!
      a1        = ' '
      Cview2(1) = '.'
      Cview2(2) = 'p'
      Cview2(3) = 'n'
      Cview2(4) = 'g'
      Cview2(5) = 'd'
      Cview2(6) = 'f'
      Cview2(7) = 'b'
      Cview2(8) = 'm'
      Cview2(9) = 'w'
      Cview2(10)= 's'
      KOLOR(1)  = 'RED'
      KOLOR(2)  = 'GREEN'
      KOLOR(3)  = 'BLUE'
      KOLOR(4)  = 'CYAN'
      KOLOR(5)  = 'ORANGE'
      KOLOR(6)  = 'MAGENTA'
      KOLOR(7)  = 'WHITE'
      KOLOR(8)  = 'YELLOW'
      message   = 'INSUFFICIENT NUMBER OF SIMULATIONS'
!      NameModels(1) = 'Mean Truncated Exp'
!      NameModels(2) = 'Mean + 1.Sig'
!      NameModels(4) = 'Gutemberg-Richter'
      TitleY = 'Annual rate of events > M'
      TitleX = 'Magnitude'

!Create the figure TITLE lines 
      write (STRING,"(i4)")  Nsource
      call AlphaN  (ALPHA,NC3,nalpha1,nalpha2,STRING)     
      write (STRING,"(a27)") CTIT2
      call AlphaN  (Alpha3,NC1,nalpha1,nalpha2,STRING)
      write (STRING,"(a80)") SOURCE
      call AlphaN  (Alpha2,NC2,nalpha1,nalpha2,STRING)
      
      write (CTIT3,*) (Alpha3(i),i=1,NC1),a1,(ALPHA(k),k=1,NC3),  &
                      a1,(Alpha2(j),j=1,NC2) 
                      
!Set file creation code
      newfile = 0
      if (NameEXT .eq. 'PNG')  then
          newfile = 1
      end if
      if (NameEXT .eq. 'BMP')  then
          newfile = 1
      end if
      if (NameEXT .eq. 'WMF')  then
          newfile = 1
      end if      
      
!Create the full name of the plot file, including the extension
      if (Nview.gt.1) then
          write (STRING,*) NameRoot
          call AlphaN (Alpha1,N1,N11,N12,STRING)
          write (STRING,*) NameEXT
          call AlphaN (Alpha2,N2,N21,N22,STRING)
          if (newfile.eq. 0)  then
              Write (NamePlotREC,"(256a1)")      &
              (Alpha1(i),i=1,N1),'.',(Alpha2(j),j=1,N2)
          else
              write (STRING,*) pagenumber
              call AlphaN (Alpha3,N3,N31,N32,STRING)
              Write (NamePlotREC,"(256a1)")      &
              (Alpha1(i),i=1,N1),(Alpha3(k),k=1,N3),  &
              '.',(Alpha2(j),j=1,N2)
          end if

      call RemoveBlankSpaces ( NamePlotRec, NamePlotRec1 )
      
      endif

      
! Define X and Y Axes
!     Determine the min and max of each axis
!     X-axis is LINear, Y-axis is  LOGarithmic:
      Xmin = 1.e10
      Xmax = 0.
      Ymin = 1.
      Ymax = 0.
      do i = 1,Nc
        do j = 1,N
            if (Y(i,j).gt. 1.e-12)  then
                Xmin = AMIN1 (Xmin,X(i,j))
                Xmax = AMAX1 (Xmax,X(i,j))
                Ymin = AMIN1 (Ymin,Y(i,j))
                Ymax = AMAX1 (Ymax,Y(i,j))
            end if
        end do
      end do
      Xmin = INT (Xmin)
      Ymin = ALOG10 (Ymin)
      Ymax = ALOG10 (Ymax)      
      Xmin1= INT (Xmin)
      Xmax1= INT (Xmax)
      Ymin1= INT (Ymin)
      Ymax1= INT (Ymax)
    
      if (Ymin1 .ne. Ymin) then
        Ymin = Ymin1 - 1.
      end if      
      if (Xmax1 .ne. Xmax) then
        Xmax = Xmax1 + 1.
      end if      
      if (Ymax1 .ne. Ymax) then
        if (Ymax.lt.0.) then
            Ymax = Ymax1
        else
            Ymax = Ymax1 + 1.
        end if
      end if
!--------------------------------------------------------------------------
!Plotting initializations:
      
      if (Nview .eq. 1) then    
          !Display on screen
          CALL METAFL(NameEXT) 
          CALL SCRMOD('REVERS')
          CALL SETPAG('DA4L')       !Landscape
          if (Nportrait.lt.2) THEN
              CALL SETPAG('DA4P')   !Portrait
          END IF      
          CALL DISINI()             !Opens the plot screen
          CALL PAGERA
          call COMPLX
      else
          if ((pagenumber.eq.1) .or. (newfile.eq.1))   then
             !Create plot file with extension 
              CALL METAFL(NameEXT)
              !Create file in desired format, including extension  
              CALL SETFIL (NamePlotRec1)  !Name includes extension (ex: .pdf)
              CALL SCRMOD('REVERS')
              CALL SETPAG('DA4L')        !Landscape
              if (Nportrait.lt.2) THEN
                  CALL SETPAG('DA4P')     !Portrait
                  CALL DISINI()
                  CALL PAGERA
                  CALL COMPLX
              END IF  
          else
              CALL NEWPAG()
          endif       
      endif
     
!      CALL CLRMOD ('FULL')
      CALL SETCLR (255)
      CALL PAGERA()
      CALL HWFONT()

      CALL COLOR('FORE')

      CALL AXSLEN(1400,1700)
      CALL NAME(TitleX,'X')
      CALL NAME(TitleY,'Y')
      CALL AXSSCL('LIN','X')
      CALL AXSSCL('LOG','Y')

      CALL TITLIN(CTIT1,2)
      CALL TITLIN(CTIT3,4)
      CALL HEIGHT (40)

      NYA=2400
      NYLABEL = 1000
      CALL LABDIG(-1,'XY')

      CALL AXSPOS(500,NYA)
      CALL LABELS(CLAB(1),'XY')
      CALL TICPOS ('REVERS','XY')
      CALL GRAF(Xmin,Xmax,Xmin,1.,Ymin,Ymax,Ymin,1.)
      CALL GRID (2,2)

      CALL HEIGHT(60)
      CALL TITLE()
      CALL HEIGHT(30)
      if (jwdt.eq.1)  then
          CALL PAGHDR (CSTR1,CSTR2,4,0)
      end if
          
 !If no curves  due to insufficient number of simulations, plot message
      if (Nc .eq. 0) then
      CALL MESSAG (MESSAGE,700,1500)
      END IF
 
 !Plot Nc Recurrence Curves
        do j = 1, Nc
            do i = 1,N
                 if (Y(j,i).lt.1.e-12) exit 
                 XA(i) = X(j,i)
                 YA(i) = Y(j,i)
                 Np = i
            end do
                k = 1 + modulo(j-1,8)
                CALL COLOR (KOLOR(k))
                CALL LINWID (NWIDTH1)
            if (j.GT.8) then 
                CALL DASHL    
            endif
            CALL CURVE (XA,YA,Np)
        end do
        CALL SOLID ()
        
!Legend statements
        CALL HEIGHT (35)
        CALL COLOR ('FORE')
        CALL LEGINI(CBUF,Nc,20)  
        CALL LINWID (10)    !line width
!        NX = 1269
        NX = NXPOSN(XMIN)
        CALL LEGPOS(NX,NYPOSN(10**((Ymax+3*Ymin)/4.)))
        Do j = 1, Nc
            CALL LEGLIN (CBUF,NameModels(j),j)
        end do
        CALL LEGTIT (LegendTIT)
        CALL LEGEND(CBUF,3)
        CALL LINWID (1)
        
!Add a page number
        CALL HEIGHT (30)
        if (jwdt.eq.1)  then
           CALL MESSAG('Page: ',1800,2800)
           if (Nview .eq. 1)  then
                XP = FLOAT(Nscreen)
                CALL NUMBER(XP,-1,1900,2800)
                Nscreen = Nscreen + 1
            else
                XP = FLOAT(pagenumber)
                CALL NUMBER(XP,-1,1900,2800)
                pagenumber = pagenumber + 1
            end if
        end if
        
      CALL ENDGRF()
             
      if ((Nview.eq.1) .or. (newfile.eq.1))  then
        CALL DISFIN()
      endif

      RETURN
    
      END SUBROUTINE PlotRecurrence
#endif
