! Calculates Uniform Hazard Spectra from set of several frequency hazard curves 
!
!*******************************************************************************
!
        SUBROUTINE UHSpec (gm, haz, nf, ngm, RP, UHS)
!
!*******************************************************************************
!
!       Calculates the Uniform Hazard Spectra from a set of nf hazard curves,
!       one for each frequency.
!
! Input:
! ******    gm(j)    = real array of ngm ground-motion parameter values
!           haz(i,j) = real array of ngm hazard values for nf hazard curves
!           nf       = number of frequencies
!           ngm      = number of ground-motion values
!           RP       = Return Period for which the UHS is calculated
!
! Output:
! *******   UHS(i)   = Array of ngm ground-motion values

!           i = 1, ngm    index of ground-motion values
!           j = 1, nf     index  of frequency values

!           Interpolations are done in Log-Log space
!
!*******************************************************************************
!
        IMPLICIT NONE

        INTEGER :: i, j, mflag1, mflag2, nf, ngm

        REAL    :: dy1, dy2, p, p1, RP, x1, x2, x3, y1, y2

        REAL, dimension(ngm)    :: GM
        REAL, dimension(nf)     :: UHS
        REAL, dimension(ngm,nf) :: haz

        p = 1./ RP
 

 !       print *, '  '
  !      print *, 'Hazard curves in UHSectra'
   !     do i = 1, nf, 9
    !    print *, ' GM        HAZ(GM)   N-Frequency=',i
     !       do j = 1, ngm
      !         print *, GM(j), haz(j,i)
       !     end do
        !end do

        do i = 1, nf
            
            !Find if all hazard values for this frequency are  >p or < p
            mflag1 = 0
            mflag2 = 0

            do j = 1, ngm
                if (haz(j,i) .ge. p)  then
                    mflag1 = mflag1 + 1
                end if
            end do
            if (mflag1 .le. 1)    then
                UHS(i) = GM(1)
                go to 100
            end if
            if (mflag1 .eq. ngm)  then
                UHS(i) = GM(ngm)
                go to 100
            end if

            do j = 1, ngm
                if (haz(j,i) .lt. p)  then
                    mflag2 = mflag2 + 1
                end if
            end do

            if (mflag2 .eq. 0)    then
                UHS(i) = GM(ngm)
                go to 100
            end if
            if (mflag2 .eq. ngm)  then
                UHS(i) = GM(1)
                go to 100
            end if
            mflag2 = ngm - mflag2 + 1

            j = mflag1                  
            !Interpolation in Log-Log space
            x1 = alog10 (GM(j))
            x2 = alog10 (GM(j+1))
            y1 = -12.
            y2 = -12.
            if (haz(j,i) .gt. 0.)  then
                y1 = alog10 (haz(j,i))
            end if
            if (haz(j+1,i) .gt. 0.)  then
                y2 = alog10 (haz(j+1,i))
            end if
            p1 = alog10 (p)
            dy1 = (y1 - y2)
            dy2 = (y1 - p1)
            if (dy1 .ge. 1.e-10)  then
                x3 = x1 + (x2-x1)*(dy2/dy1)
                UHS(i) = 10. ** x3
            else
                x3 = GM(j)
            end if

100     continue

        end do

        RETURN

        END subroutine UHSpec
