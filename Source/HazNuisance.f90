!
!                      This routine is called by simulperil
!
!******************************************************************************
        subroutine HazNuisance  (A,Beta,Dmg,GM,Haz,HazNuis,nGM)
!******************************************************************************
!
!   This routine calculates the probability of exceedance of a given level
!   of non-acceptance of a GM level.
!   Non-acceptance of a given GM level is qualified as a fail
!   Non acceptance is modeled by a fragility curve that gives a probability 
!   of failure as a function of GM values.
!   Low GM values have low failure probability
!   and very high values of GM have fail probability closer to 1.0

!   INPUT:
!       A     = median value of GM which fails with probability 0.5
!       Beta  = Slope of the fragility curve
!       GM    = vector of nGM values of GM for which hazard is calculated
!               working array is GMlog =  Log(GM)
!       Haz   = vector of nGM hazard values, calculated in SIMRISK
!
!   OUTPUT:
!       Dmg      = vector of N values of probability of failure
!       HazNuis  = vector of N values of hazard of failure levels
!                  Non-Acceptance-Probability levels (NAP).

!

    IMPLICIT NONE
    
    INTEGER                :: i, j, h1, h2, N, nGM
    INTEGER, dimension(nGM):: icase

    REAL                   :: A, Beta, p1, p2, p3, t, x1, x2, x3
    REAL, dimension(nGM+1) :: GM
    REAL, dimension(nGM)   :: Dmg,Haz, HazNuis, Y

!*******************************************************************************
!
!The probability of a given level of nuisance (a probability of damage) to be 
!exceeded, is also the probability that the corresponding level ground-motion
!is exceeded.

!     1. Create a vector of nuisance levels (probabilities): Dmg        
!     2. Calculate the ground motion values GM1 that gives that nuisance: p1
!        Use standard fragility model. Dmg(GM)= PHI[ (ln(GM/A) / Beta)) ]
!               Dmg  = damage level, a probability
!               A    = GM value for 50% damage, median on a log scale
!               Beta = slope of fragility function (aleatory uncertainty)
!     3. Calculate probability of p1 being exceeded as the probability of GM1
!        being exceeded, from the Hazard function Haz(i), using interpolation
!        on log of Hazard values.
!    

!*******************************************************************************
!
      N = nGM
      !Array of nuisance values :
      !--------------------------
      Dmg(N) = 1.
      HazNuis(N) = Haz(nGM)
      do i = 1, N-1
          Dmg(i) = Float(i) / Float(N)
      end do

      !Special case of Dmg being outside of Nuisance range for range of GM
      x1 = GM(1)
      x2 = GM(nGM)
      t  = alog (x1/A) / Beta
      call GaussNorm (t, p1)
      t  = alog (x2/A) / Beta
      call GaussNorm (t, p2)
      do i = 1, nGM
          icase(i) = 3
          if (Dmg(i) .le. p1)      then
              icase(i) = 1
          elseif (Dmg(i) .ge. p2)  then
              icase(i) = 2
          end if
      end do

!     Find ground-motion corresponding to Dmg(i):
!     -------------------------------------------
      do i = 1, N-1

          SELECT CASE ( icase(i) )
   
          CASE (1)
              !Dmg(i) is lower than lowest GM value Nuisance
              HazNuis(i) = Haz(1)

          Case (2)
              !Dmg(i) is greater than highest GM value Nuisance
              HazNuis(i) = Haz(nGM)

          CASE (3)
              !Dmg (i) is within range of values for GM range
              !Find the GM interval that bounds Dmg(i): [h1, h1+1]
              h1 = 1
              do j = 1, nGM
                  x1 = GM(j)
                  t  = alog (x1/A) / Beta
                  call GaussNorm (t, p1)
                  if (p1 .ge. Dmg(i))  then
                      h1 = j - 1
                      exit
                  end if
              end do

              x1  = GM(h1)
              x2  = GM(h1+1)
              t  = alog (x1/A) / Beta
              call GaussNorm (t, p1)
              t  = alog (x2/A) / Beta
              call GaussNorm (t, p2)

              do j = 1, 100
                  !Interpolate
                  x3 = x1 + (x2-x1)*(Dmg(i)-p1)/(p2-p1)
                  t  = alog (x3/A) / Beta
                  call GaussNorm (t, p3)
                  if (ABS((p3-Dmg(i))) .le. 1.e-6)  then
                      exit
                  else
                      if (p3 .gt. Dmg(i))  then
                          x2 = x3
                          t  = alog (x2/A) / Beta
                          call GaussNorm (t, p2) 
                      else
                          x1 = x3
                          t  = alog (x1/A) / Beta
                          call GaussNorm (t, p1) 
                      end if                                    
                  end if
              end do
              Y(i) = x3

          CASE DEFAULT

          END SELECT
      end do

!-----Probability of exceedance of Dmg(i) is Haz[Y(i)]. 
!     Interpolate on log scale of Haz()
      do i = 1, N
          if (icase(i) .eq. 3)  then
              !GM interval: GM() values of index h1 and h2
              do j = 1, nGM
                  if (GM(j) .ge. Y(i))  then
                      h1 = j - 1
                      exit
                  end if
              end do
              if (h1 .lt. 1)  then
                  h1 = 1
              end if
              h2 = h1 + 1
              !x1 =  Alog ( Haz(h1) )
              !x2 =  Alog ( Haz(h2) )
              x1 = Haz(h1)
              x2 = Haz(h2)
              t  =  (Y(i)-GM(h1)) / (GM(h2)-GM(h1))
              x3 = x1 + t*(x2-x1)
              !HazNuis(i) = exp( x3 )
              HazNuis(i) = x3
          endif 
          !print *,'i,icase,Y,j,x1,x2,x3,HazNuis:',   &
          !i,icase(i),Y(i),h1,GM(h1),GM(h2),Haz(h1),Haz(h2),HazNuis(i)        
      end do

      !print debugging results
      !print *,'  '
      !print *, '  Nuisance results:'
      !print *, '      Index   Nuisance        Prob of'
      !print *, '               level         exceedance'
      !do i = 1, nGM
       !   print *,i,  Dmg(i), HazNuis(i)
      !end do

      
      RETURN
      
      END SUBROUTINE HazNuisance

