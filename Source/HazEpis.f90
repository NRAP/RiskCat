!*******************************************************************************

        SUBROUTINE HazEpis (CSTR1, CSTR2, iepis, itp, nCritRD, nGMbins, NMspl , &
                           Nscreen, pagenumber1, PlotHazEpis, TrackNumber, Years) 

!*******************************************************************************
!Calculate the annual hazard curve for iepis-th Epistemic simulation 
!
!       Uses NMspl Histograms,  
!           - Sums up Histograms
!           - For a specific epistemic cycle: iepis
!
!           Input:
!           ******
!           - iepis, index of the iepis-th Epistemic simulation
!           - itp, index of the time window of length 'years'
!           - Years is the timewindow considered for the hazard (in years)
!
!           Output:
!           *******
!           - Hazard curve of GM for the iepis-th epistemic simulation
!           - Plot the hazard curve  (PDF)
!------------------------------------------------------------------------------

        use ModuleEQ                   
      
        IMPLICIT NONE
      
        INTEGER :: i,icrt,iepis,iGM,iplot,ispl,itp
        integer :: ix,IxSCL,Iy1,j
        INTEGER :: nCritRD,nGMbins,NMspl,pagenumber1,PlotHazEpis,TrackNumber    
        
        REAL    :: sum,TotalEvents,w2,Years
        
        REAL, dimension(nGMbins)  :: work1
  
        
        !Declarations for plotting
        CHARACTER (LEN=41) :: CSTR1
        CHARACTER (LEN=20) :: CSTR2
        CHARACTER (LEN=13) :: CTIT, CTIT3 
        CHARACTER (LEN=27) :: CTIT2        
        CHARACTER (LEN=5)  :: NameEXT
        CHARACTER (LEN=12) :: NameRoot
        
        CHARACTER (len=16),DIMENSION(NcMax) :: NameLegends
        
        INTEGER :: jwdt,N,Nc,Nf,Nportrait,Nscreen
        INTEGER :: Nview,Nwidth1
        
        REAL, dimension(MaxGMbins)  :: X
        
        REAL, dimension(MaxGMbins,NcMax) :: Y
        
!*******************************************************************************
!
!       Total number of Magnitude events greater than M0, per time-window itp
        TotalEvents = 0.        

        do ispl = 1, NMspl
            if (istd_Dieterich .eq. 1)  then
                TotalEvents = TotalEvents + (IendEq(ispl,itp) -  &
                IstartEq(ispl,itp)+1)

            else            
               Totalevents = Totalevents + nEqtot(ispl,itp)
            end if
        end do

        !Average number of events per catalog, per time-window, per year
        TotalEvents = TotalEvents / (Years*Float(NMspl))

    
        NameEXT    = 'CONS'
        Nview      = 1

    do iplot = 1,2 
    !iplot = 1 for screen, 2 for save on file)

        do icrt = 1, NcritRD
            
            !Histogram: Number of GM values in each GM bin of GM, per year
            sum   = 0.
            work1 = 0.
            do iGM = 1, nGMbins
                work1(iGM) = work4(icrt,iGM) 
                sum = sum + work1(iGM)
            end do
            
            !CCDF function of GM for this site and Epis loop for 1 Year
            Haz1(icrt,nGMbins,iepis,itp) = 0.
            if (work1(nGMbins) .gt. 0.)  then
                Haz1(icrt,nGMbins,iepis,itp) = work1(nGMbins)/sum
            end if
            do iGM = nGMbins-1, 1, -1
                w2 = Haz1(icrt,iGM+1,iepis,itp) + work1(iGM)/sum
                if (w2 .lt. 0.)  then
                    w2 = 0.
                end if
                Haz1(icrt,iGM,iepis,itp) = w2
            end do

            !Annual rate of occurence of events greater than iGM
            do iGM = 1, nGMbins
               w2 = Haz1(icrt,iGM,iepis,itp)
                if (w2 .gt. 1.0)  then
                   Haz1(icrt,iGM,iepis,itp) =  1.0
                endif
                !print *,'Haz1(.,',iGM,',iepis,itp)=',Haz1(icrt,iGM,iepis,itp)
            end do


            !Annualized Hazard assuming each year is same
            !xLamda is the probability that at least 1 event greater than the
            !minimum GM0 value will occur.
            !Assuming a Poisson arrival within a single year:
            !   xLamda = exp (- annual rate for events .ge. GM0)
            !annual rate = (Number of events in time window >= GM0) / Years
            !            = (Annual # events) * P(gm> GM0), GM0=GMarray(1)
            !P(gm > GM0) = Haz(icrt,iGM=1,iepis,itp) = 1. - exp(xLambdaGM)
 
          !  do iGM = 1, nGMbins
           !     xLamdaGM = Haz1(icrt,iGM,iepis,itp)
            !    w2 = 1. - exp ( -xLamdaGM )
             !   if (w2 .gt. 0.)  then
              !      Haz1(icrt,iGM,iepis,itp)= w2
               ! else
                !    Haz1(icrt,iGM,iepis,itp)= 0.
                !end if
            !enddo


!******************************************************************************
            
        !Are plots requested ?
        !*********************
#ifdef USE_DISLIN

        if ((PlotHazEpis .gt.0) .and. (itp.eq.1))  then
        
            !Plot Hazard Curves for this site and Epistemic cycle
            CTIT       = 'Annual Hazard'
            write (CTIT2,"(a8,i3)") 'Site No.', icrt
            write (CTIT3,"(a8,i3)") '- Epis #', iepis
            ix         = 1
            IxSCL      = 0  ! = 1, X axis is linear, 0 for logarithmic
            Iy1        = 1
            N          = nGMbins - 1
            jwdt       = 1
            Namelegends(1)  = 'Mean'

            if (Tracknumber .lt. 10)      then
                if (iepis .lt. 10)        then
                    write (Nameroot,"('PL00',i1,'B-00',i1)") Tracknumber,iepis
                elseif (iepis .lt. 100)   then
                    write (Nameroot,"('PL00',i1,'B-0',i2)")  Tracknumber,iepis
                else
                    write (Nameroot,"('PL00',i1,'B-',i3)")   Tracknumber,iepis
                end if

            elseif (Tracknumber .lt. 100)  then
                if (iepis .lt. 10)        then
                    write (Nameroot,"('PL0',i2,'B-00',i1)") Tracknumber,iepis
                elseif (iepis .lt. 100)   then
                    write (Nameroot,"('PL0',i2,'B-0',i2)")  Tracknumber,iepis
                else
                    write (Nameroot,"('PL0',i2,'B-',i3)")   Tracknumber,iepis
                end if

            else
                if (iepis .lt. 10)        then
                    write (Nameroot,"('PL',i3,'B-00',i1)") Tracknumber,iepis
                elseif (iepis .lt. 100)   then
                    write (Nameroot,"('PL',i3,'B-0',i2)")  Tracknumber,iepis
                else
                    write (Nameroot,"('PL',i3,'B-',i3)")   Tracknumber,iepis
                end if

            end if
 
            Nc         = 1
            Nf         = nGMbins-1
            Nportrait  = 1
            NWIDTH1    = 15
              
            !Load arrays
            X(1)   = GMarray(1)
            do j = 1,Nc
               Y(1,j) = 1.
            end do
            do i = 2, nGMbins-1
                X(i) = GMarray(i)
                do j = 1, Nc
                    Y(i,j) = Haz1(icrt,i,iepis,itp)
                    print *, 'i=',i,'  X(i)=',X(i),'  Y(i)=',Y(i,j)
                end do
            end do
                    
!         Plot Hazard curves for epistemic loop only if ONE time window
!         -------------------------------------------------------------
          if ((itp.eq.1) .and. (PlotScreen.eq."Display-Screen")   &
          .and. (PlotHazepis.gt.0))  then
              call PlotHazCurves (CSTR1,CSTR2,CTIT,CTIT2,CTIT3, &
              Ix,IxSCL,Iy1,jwdt,N,NameEXT,NameLegends,NameRoot, &
              Nc,NcMax,MaxGMbins,Nportrait,Nscreen,NVIEW,              &
              NWIDTH1,pagenumber1,X,Y)
          end if
                      
        end if            
#endif            
    end do
      
#ifdef USE_DISLIN  
        if ((iplot.eq.2) .and. (icrt.ge.NcritRD)) then
            if ((itp.eq.1).and. (PlotHazEpis.gt.0))  CALL DISFIN()
        end if
#endif
        NameEXT = 'PDF'
        NVIEW = 2
        
    end do !loop iplot for plotting 

!******************************************************************************

        return
        end subroutine HazEpis
