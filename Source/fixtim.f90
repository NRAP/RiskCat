      subroutine fixtim(traz,trar,trat,np,npmax,dt,tim0,tim1,Maxegf)

      IMPLICIT NONE

      INTEGER :: i, jj, np, npmax, Maxegf,np1
      REAL    :: dt, tim0, tim1
      REAL, dimension(Maxegf) :: traz,trar,trat
!
!     reset traces so that first data point is at tim1

      np1 = ifix(tim1/dt) - ifix(tim0/dt) + 1
      jj = 0
      do i = 1, np
          jj = jj + 1
          if ((jj.gt.npmax) .or. (jj+np1.gt.np))  then
              np = jj - 1
          else
              traz(jj) = traz(jj + np1)
              trar(jj) = trar(jj + np1)
              trat(jj) = trat(jj + np1)
          endif
      end do

      return 
      end
