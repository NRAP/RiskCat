!       This routine is called by routine simulperil
!
! This routine calculates the hazargd curves for one epistemic simulation
! when using SYNHAZ to estimate the ground motion

!       It takes all the catalogs of earthquakes generated in an epistemic
!       simulation (NMisp catalogs).
!       The hazard from each catalog is calculated and combined over all 
!       catalogs as a weighted sum of hazard curves.
!
!       The consequences, in terms of loss or other metric, are calculated
!       for each catalog and weighted accordingly.

!Once the source has been selected, we need to
!select the parameters (M,Dip, Depth, etc,) associated with the
!parameters used to calculate the probability of occurrence of 
!events >= M0. 
!these parameters are stored in Arec(i,*)which is updated after 
!an event

!Input:
!       iepis   = index of current epistemic cycle
!       NcritRD = number of sites for which to calculate hazard
!       Neq2    = Number of earthquakes in the RSQSim catalog file
!       nGMbins = number of bins of GM valuesSfaultband
!       nMspl   = number of catalogs simulated associated with each 
!                 magnitude calibration bin
!       rateM0  = occurrence rate at M0
!       tlife   = array of time since starting. Defines hazard timewindows
!       WgEpis  = Weight of the current Epistemic simulation cycle
!       Years   = Length of catalogs in years.
!
!******************************************************************************
!

        subroutine  GMHR2                                                    &
                    (Az1, Az2, BadMinetFlag, cigar, CSTR1,CSTR2,                           &
                    damp, D1, D2, DipPoint1, DipPoint2, freq, GMoutMax,      &
                    GMoutMin, iATN_Larry, iepis, INgm, MaxEqINPUT, maxnodl,  &
                    maxnodw, Mxstrp,                                         &
                    nbinMagSpl,NcritRD,Nepis, nfreq, nGMbins, nMspl, Nscreen,&
                    OUTgm, pagenumber1, PlotHazEpis, Rake1Point, Rake2Point, &
                    rigidity, StrMax, StrMin, synflag, synflagRS,            &
                    TrackNumber, VsMax, VsMin, xLmax, xM0, Years)
                     
!
!******************************************************************************

        !Internal custom modules
        use CritFacilities
        use CritNodes
        use GENparam
        use ModuleEQ 
        use ModuleRSQSim  
        use ModuleSYNHAZ                
      
        IMPLICIT NONE
        

        CHARACTER (LEN=40):: faultname
        CHARACTER (LEN=5) :: Typecalc
        CHARACTER (LEN=3) :: Typecalc2
        CHARACTER (LEN=9) :: Xevent

        INTEGER :: BadMinetFlag
        INTEGER :: i,i1,i2,iATN_Larry,icrt,idum,iEND,iepis
        INTEGER :: IERR,IERR1
        INTEGER :: ievprint2,ievprint3,igm,ii,iii,iii1,inev,inev1,inev2
        INTEGER :: INgm,iSimsource,ispl
        INTEGER :: istrip,isub1,itp,ittp,ixM0
        INTEGER :: j,jcrit
        INTEGER :: k,kgm,ks,MaxEqINPUT,maxnodl,maxnodw
        INTEGER :: mgridyes, Mxstrp
        INTEGER :: nbinMagSpl,n1,NcritRD,Nd,Nepis,Nev,Nev1,nfreq
        INTEGER :: nLHbins,nLHSamples,nsub,nGMbins,nl,Nscreen
        INTEGER :: nsimul
        INTEGER :: nMspl,Nodes,npltf,OUTgm
        INTEGER :: pagenumber1,PlotHazEpis,synflag,synflagRS,tracenodes,TrackNumber 

        INTEGER, dimension(10) :: ievprint
          
        REAL    :: Az1, Az2     
        REAL    :: damp, DipPoint1, DipPoint2, D1, D2, GMout1,GMoutMax,GMoutMin
        REAL    :: mopass,mopass1
        REAL    :: RADtheta, Rake1Point, Rake2Point, rigidity,rnunf
        REAL    :: rq1,rq4,rq5,rq6,rq7,rq8
        REAL    :: sigma,simdepth1,Simlat1,SimLong1,SimMag1, STRMax, StrMin
        REAL    :: timewindow
        REAL    :: VsMax, VsMin, wgmt,wmspl
        REAL    :: xc,xcretime,xcretM0,xDip,xDip1,xDip2, xi
        REAL    :: xLmax,xMag1, xMagPoint, xM0,xshift1,xvs
        REAL    :: xWeight,xWmax,xxx
        REAL    :: x1
        REAL    :: yc,years, yi, yshift1,yyy,y1
        REAL    :: zzk2,z1
        
        REAL, dimension(Mxstrp)       :: depth2
        REAL, dimension(nfreq)        :: freq, PARS
        real, dimension(NsimEMP)      :: GMaleatory
        real, dimension( MaxEq)       :: GMout
        
        REAL, dimension(Maxseg,Mxstrp)      :: dips
        REAL, dimension(Maxnodl,3)          :: trace
      
!Variable set for plotting
        INTEGER :: ifault,ipfault,iplotnum
        REAL    :: cigar, radeg, rearth, rdegree, rw
        REAL    :: xf,xh,vux,vuy,vuz,xmpltf1,xmpltf2

!VAriables for testing GM. (May be removed later)
        integer :: imaxbin, iminbin, Nbin, Ndelta
        integer :: Histo(102)
        integer, dimension(MaxEq) :: iGMout
        real    :: delta, xbin0, xbinup, xmaxbin, xminbin
        real, dimension(MaxEq)    :: GMout2

!  Declarations for plotting response spectra
      CHARACTER (LEN=41) :: CSTR1
      CHARACTER (LEN=20) :: CSTR2
      CHARACTER (LEN=13) :: CTIT        
      CHARACTER (len=16),DIMENSION(8) :: NameLegends
      INTEGER :: ix,ixSCL,iy1,Nc
      INTEGER :: Nview,Nwidth1
      REAL, dimension(31)   :: Xplot    
      REAL, dimension(31,8) :: Yplot
      CTIT       = '  SPECTRA    '
      ix         = 1
      IxSCL      = 0  ! = 1, X axis is linear, 0 for logarithmic
      Iy1        = 1
      Namelegends(1)  = 'Acc. (cm/s/s)'
      Nc         = 1
      Nview      = 1
      NWIDTH1    = 15
!

      
!*******************************************************************************  
!Constants and misc. initializations:
      rearth  = 6371200.                   ! Earth radius in meters
      rdegree = rearth * 6.28318531 / 360. ! Length of 1 degre at equator
      radeg   = 57.2957795                 ! Conversion degree-radian
      RADtheta = pp_theta / radeg          ! Convert pp_theta to radians

!*******************************************************************************

      INgm  = 2
      OUTgm = 2

!     There are NMspl catalogs for ONE epistemic simulation.

!     Reset values in work4() 
      do i = 1, NGMbins
          do j = 1, NcritRD
              work4(j,i) = 0.
          end do
      end do
      !
      !BadMinetFlag is a flag to identify bad Catalogs.
      !One case is when the GMout1 value out of synhaz is absolute zero.
      !     In this case the remaining events in the catalog are disregarded
      !     and the code goes on to processing the next catalog.
      BadMinetFlag = 1    !This for a good catalog. Will be changed to 0 if bad

!Loop over the number of Hazard time windows:
!--------------------------------------------
      Time_Window: Do itp = 1, Ntp


        !Skip itp cycle if zero events
        ittp = Nonzerotp1(itp)
        Skip_itp_cycle: if (ittp .gt. 0)  then 
        !go through itp cycle

            !Assignment of weights for each epistemic set:
            !---------------------------------------------
            xWeight  = Weightepi(iepis) 
            if (iStd_Dieterich .eq. 3)  then
                xWeight = .1
            end if    
 
            !Loop over the number of aleatory simulations:
            !--------------------------------------------  
            Loop2_Sampling: Do ispl = 1, NMspl

                !iEND is the index of the last event processed
                !   = last event in last time window
                iEND = IendEq(ispl,Ntp)

                if (iStd_Dieterich .eq. 2)  then
                   ii = (iepis-1)*NMspl + ispl
                   !open(unit=3,file='CatRSQ',status="UNKNOWN",   &
                   !iostat=IERR)  
                   open(unit=3,file='CatRSQ',status="UNKNOWN",   &
                   form="unformatted",iostat=IERR)                       
                   !Re-set event counter within catalog 
                   ixM0 = 0  
                end if

!               Number of events to be processed in this catalog            
                Nev = NCatEvents(ispl)
                ievprint3 = INT (Nev/10)
                do i= 1, 9
                    ievprint(i) = i *  ievprint3
                end do
                ievprint(10) =  Nev
                ievprint2    =    1

                !Duration of time window for which Hazard is calculated (yrs)
                timewindow = tlife(1)
                if (itp .gt.1)  then
                   timewindow = tlife(itp) - tlife(itp-1)
                endif

                !Loop over the number of Events in the catalog:
                !----------------------------------------------   
                i1 = IstartEq(ispl,itp)
                i2 = IendEq(ispl,itp)  
         
                Loop3_Number_of_Events_in_Catalog: Do inev = i1, i2
                    inev1 = inev - i1 + 1 
                    inev2 = i2 - i1 + 1

                    ! Event Magnitude
                    SimMag1   = CatEqMag(ispl,inev)
                    xMagPoint = SimMag1
                    !Event seismic moment
                    mopass  = CatEqM0(ispl,inev)    
         
 
                    SELECT CASE (iStd_Dieterich)  

                        CASE (1)  ! Standard (PSHA) simulation of sources

                        !Process only events with magnitude M > xM0, 
                        !and event index < MaxEqINPUT
                        if (SimMag1 .gt. xM0)  then
                            ixM0 = ixM0 + 1
                            if (ixM0.gt.MaxEqINPUT) then
                                exit
                            end if
                        end if

             
!                       1.Retrieve Source and its parameters.
                        ! iSimsource = Source ID   
                        iSimsource = iCateQ(ispl,inev)

!                       2.Retrieve Type of source (Area/Fault/...)
                        ! faults properties are determined below in Faultband
                        if ((SourceType(iSimsource)) .eq. 1)  then
                            ! Areas, Select EQ location
                            nodes = scrnodnum(iSimsource)
                            call AreaEQselect(iSimsource,nodes, &
                            simdepth1,Simlat1,SimLong1)
                        end if                                

!                       3.Calculate ground motion parameter at critical points  
                        !Here we use SYNHAZ to calculate the ground motion.
                        !First we simulate a rupture using 'faultband'
                        !create the input to run SYNHAZ, and we run it
                      
                        SELECT CASE (SourceType(iSimSource))
                      
                          CASE (1)      !Area Sources and Point-Sources

                            !This case is that of area sources for which there
                            !azimuthal direction of rupture for each source,
                            !and for point sources as in the empirical and/or
                            !simulated catalogs (ex. Corrinne's simulated Cats)
                            !Since in this routine Larry's synhaz code is used,
                            !the following parameters needed :
                            !   Overall Magnitude (all sub-elements combined)
                            !   Point of initiation
                            !   general Dip
                            !   general azimuth
                            !   Coordinates of the center of sub-elements
                            !   Seimic moment of each sub-element
                            !   Rise-time
                            !   Stress Drop
                            !   Slip vector, etc...
                            !
                            !- -With M0 we derive surface area of rupture
                            !-- Assuming 1/1 L/W ratio construct quadrangle
                            !   with dimensions L x W
                            !-- Call faultband to generate sub-elements,
                            !-- Use Brune source model for Rise-time
                            !-- Use one epistemic sample of each uncertain param
                            !       stress-drop: uniform in range 30bars to 100 bars
                            !       Vs COV= 10%

                            Call faultPoint  (Az1, Az2, cigar, D1, D2,   &
                                DipPoint1,DipPoint2, Nsub, Rake1Point,   &
                                Rake2Point,rigidity, StrMax, StrMin,     &
                                VsMax, VsMin, Xi, xLmax, xMagPoint, Yi)  
     
 
                          CASE (2)       !Case of Fault Sources

                            !Default parameters for SYNHAZ
                            !-----------------------------------
                            !SynStrike = 91.2060 !in degrees
                            !SynDip    = 90.0    !in degrees
                            SynSV      = 29.3    !in degrees
                            SynStdp    = 100.    !Stress Drop 
                            faultname = SourceName(iSimsource) 
                            !Only one strip, set depth to 15.km          
                            depth2(1) = 15. !Was=eqdepth(iSimsource) unreallistic     
                            xwmax = 20.
                            !Number of nodes
                            tracenodes = scrnodnum(iSimsource)
                            !Number of segments
                            nl = tracenodes - 1
                            !Number of strips (nd=1)
                            nd = 1 
                            nsimul = 1                 
                            !Load 'trace' vector of trace coordinates
                            do jcrit = 1, tracenodes
                                n1  = scrnodes(iSimsource,jcrit)
                                trace(jcrit,1) = snLong(n1)
                                trace(jcrit,2) = snLat(n1)
                                !Depth of top of fault
                                trace(jcrit,3) = Wtop(iSimsource,1)  
!                                dips(jcrit,1)  = 0.        !Vertical                 
                            end do
                            !Depth of bottom of strips in segments
                            do istrip = 1, nd
                                depth2(istrip) = Wbot(iSimsource,istrip)
                            end do

                            !Generate a random Dip for each flap of this fault
                            do i = 1, nl
                                do jcrit = 1, nd
                                    xxx   = rnunf ()
                                    xDip1 = DIP1RD(iSimsource,jcrit,i)
                                    xDip2 = DIP2RD(iSimsource,jcrit,i)
                                    xDip  = xDip1 + xxx*(xDip2-xDip1)
                                    !At this point Dip is in Standard Math 
                                    !definition, which is the angle from vertical
                                    ! counter-clockwise on orthogonal system
                                    !Needs to transform to Geologists' definition
                                    !This is done in faultband for use in synhaz
                                    Dips(i,jcrit) = xDip
                                end do
                            end do

                            !Info for plotting simulated rupture surfaces
                            ifault   = iSimsource
                            ipfault  = 0
                            iplotnum = 0
                            mgridyes = 0
                            rw = 0.5
                            cigar = 0.5
                            xf = 0.5
                            xh = 0.125
                            vux = 0.
                            vuy = 0.
                            vuz = 0.
                            xshift1 = 0.
                            yshift1 = 0.
                            xmpltf1 = 4.5
                            xmpltf2 = 9.
                            !Generate the input file for SYNHAZ       
                            call faultband (cigar,dips,depth2,   &
                            iATN_Larry,iSimsource,ipfault,     &
                            maxnodl,maxnodw,Mxstrp,  &
                            NcritRD,nl,nd,npltf,nsimul,nsub,rw,  &
                            trace,xf,xh,SimMag1,  &
                            xLmax,xmpltf1,xmpltf2,xwmax)
                        
                       
                          CASE DEFAULT

                        END SELECT  !Type of seismic source


                      
                    CASE (2) !Case of RSQSim simulated faults


!added jbs 2-8-18
!if (inev .gt. 5)  then
!stop
!end if


                        !Default parameters for SYNHAZ
                        !-----------------------------------
                        !SynStrike = 91.2060 !in degrees
                        !SynDip    = 90.0    !in degrees
                        SynSV      = 29.3    !in degrees
                        SynStdp    = 100.    !Stress Drop
!
                        !Read sub-element specific information
                        !-------------------------------------
                        !Limit on number of sub-elements for SYNHAZ
                        nsub = SubEltNumber(inev) 
                        NSynSubElt = nsub
                        if (nsub .gt. MaxSubElt)  then
                             print *, 'iepi,ispl:', iepis,ispl
                             print *, 'NsubElt too large =', nsub
                             print *, 'xcretMag  =', xMag1
                             stop 'STOP - Too many subelements for Synhaz'
                        end if
                       

!********************************************************************************

                        !Seismic moment in MKS (J) in RSQSim
                        !M0 needed in dyne.cm (erg) for synhaz
                        !CAtalog M0 are read in MPa from RSQSim
                        !xMag1  = .667*alog10(xcretM0) - 6.067 (M0 in Joules)

                        xMag1  = CatEqMag(ispl,inev)  

                        !Process only events with magnitude M > xM0, 
                        !and event index < MaxEqINPUT
                        if (xMag1 .gt. xM0)  then
                            ixM0 = ixM0 + 1
                            if (ixM0.gt.MaxEqINPUT) then
                                exit
                            end if

                        !Load sub-events properties for use in SYNHAZ
                            Load_RSQS: do iii = 1, nsub
                            !Sub-event index count, sub-elt index,time (sec),
                               !slip (m), rise-time (sec), Stress-drop (MPa)   - rq7
                               !iii   = index of the iii-th small rupture.     - iiith rupture
                               !isub1 = index of the sub-element on which this - Elem Index
                               !        iii-th rupture occurs
                               !There can be multiple rupture of the same subelt
                               !so that nsub can in general be much greater then
                               !Max number of small fault elements.
                               !rise time (rupt duration) - rq4
                               !Stress drop               - rq6
                               !slip (m)                  - rq7
                               !rupture time (sec)        - rq8
                               
                            !read (3,*,iostat=IERR) idum,isub1,   &
                             !    rq8,rq4,rq7,rq6
                               read (3) idum,isub1,rq8,rq4,rq7,rq6

!added jbs 2-8-18
print *, 'idum,isub1,rupt. time,risetim,slip,Strd',&
idum,isub1,rq8,rq4,rq7,rq6

                            !Mapping of Sub-event << Sub-Elt # in fault file
                            SynEqSubMap(iii) = isub1
                            iii1             = isub1
!                           1. Sub-element initiation time (sec)
                            SynSubTim(iii) = rq8

!                           2. Calculate Seismic Moment of Sub-element M0 in dyne-.cm
                            !Calculate M0 in Joules here, as M0 = mu*S*D
                            !Mu - rigidity:     in dynes/cm^2 
                            !S  - rq5 -Area:    m^2 (*10000 to get in cm^2)
                            !D  - rq4 -Displacement: m (*100 for cm)
                            !M0 of sub-element in dynes.cm
                            rq5      = SynAreaSub(isub1)
                            xcretM0  = rigidity * (rq4*100.) * (rq5*10000)
                            xxx = xcretM0 

                            !Scale by calibration scaling factor
                            xxx = xxx * scaleM0(iepis)
                            SynSubM0(iii) = 1.0e21
                            if  (ABS(xxx) .gt. 1.e-10)  then
                                SynSubM0(iii) = xxx
                            end if

!                           3. Center of sub-element X-Longitude 
                            x1 = XYZsub(isub1,1)
                            !SynXYZsub(iii,1) =  x1

!                           4. Center of sub-element Y-Latitude 
                            y1 = XYZsub(isub1,2)
                            !SynXYZsub(iii,2) =  y1 

!                           5. Center of sub-element Depth (km)
                            z1 = XYZsub(isub1,3)
                            !SynXYZsub(iii,3) =  z1
                            !Set coordinates of overall initiation point
                            if (iii .eq. 1)  then
                                SynXYZinit(1) = x1
                                SynXYZinit(2) = y1
                                SynXYZinit(3) = z1
                            end if
                            !Find which fault the subelement belongs to
                            call Faultidentify (iSimsource, x1, y1, z1) 
  
!                           6. Direction-x slip vector
                            !RSQS6(iii) = rq1

!                           7. Direction-y slip vector
                            !RSQS7(iii) = rq2

!                           8. Direction-z slip vector
                            !RSQS8(iii) = rq3

                            !Strike of the Subelement
                            !SynSubstk(iii) read in riskcat from "Element" file

                            !Dip of the Sub-element
                            !Read in riskcat from "Elements" file as Dip_usual
                                
!                           9. slip amount rq4 given in m
                            !Transform into cm for synhaz
                            xxx = rq4 * 100.
                            SynSubSlip(iii) = 1.e-20
                            if  (ABS(xxx) .gt. 1.e-20)  then
                                SynSubSlip(iii) = xxx
                            end if
                            !Slip direction of rake in subelement plane
                            !SynRsv(iii): Read from "Elements" file in degrees.

!                           10. area of subelements in km^2: rela(i)
                            !SynAreaSub()
                            !calculated in riskcat from x-y dimensions in file
                            !when fault file is loaded

!                           12. Stress-Drop 
                            !(given from RSQSIM in MPa (10^6 N/m^2)
                            !synhaz needs it in bars (10^5 Pa)
                            ! 1.MPa = 10 bars
                            !Conversion is made in synhaz.
                            SynStdp(iii) = 100.
                            xxx  = rq6 * 1.
                            if  (ABS(xxx) .gt. 1.e-10)  then
                                SynStdp(iii) = xxx
                            end if

!                           11. rise-time
                            !ristm(i) :different for each sub-elt. (sec)
                            xvs = rq7
                            if (rq7 .le. 1.e-10)  then
                                !RSQSim assumes a 1m/s rise velocity (Vr)
                                !Thus rise-time = Slip / Vr = Slip
                                xvs = rq4
                            end if
                            Synrise(iii) = xvs

                            !Rigidity - Read in riscat in Pa,
                            !immediately converted, in riscat,to dynes/cm2
                            !for use in synhaz by: rigidity = rigidity * 10.

                        end do Load_RSQS


                        !Read Main event parameters
                        !read (3,*)  Xevent,idum,idum,rq7,rq1,rq8
                        read (3) Xevent,idum,idum,rq7,rq1,rq8
                        xcretime = rq7
                        !Reset sub-event initiation time to relative to main
                        !counted from initiation of overall event
                        !Sliptime (sec): slptim (i) time of initiation of elt 
                        !minus the time of initiation of the first element of 
                        !of the overall earthquake
                        SynSubtim(1) = 0.

                      end if


                                                    
                    CASE (3) !Case of empirical or simulated input catalogs
                            !This case is that of events in simple catalogs
                            !with a dominant azimuthal direction of rupture 
                            !(ex. Corrinne's simulated Cats)
                            !Since in this routine Larry's synhaz code is used,
                            !the following parameters are needed :
                            !   Overall Magnitude (all sub-elements combined)
                            !   Point of initiation
                            !   general Dip
                            !   general azimuth
                            !   Coordinates of the center of sub-elements
                            !   Seimic moment of each sub-element
                            !   Rise-time
                            !   Stress Drop
                            !   Slip vector, etc...
                            !
                            !- -With M0 we derive surface area of rupture
                            !-- Assuming 1/1 L/W ratio construct quadrangle
                            !   with dimensions L x W
                            !-- Call faultPoint to generate sub-elements,
                            !-- Use Brune source model for Rise-time
                            !-- Use one epistemic sample of each uncertain param
                            !       stress-drop: uniform in range 30bars to 100 bars
                            !       Vs in range VsMin, VsmAX
                            Xi = Xrsq(ispl,inev)
                            Yi = Yrsq(ispl,inev)

                            Call faultPoint  (Az1, Az2, cigar, D1, D2,  &
                               DipPoint1, DipPoint2, Nsub, Rake1Point,  &
                               Rake2Point,rigidity, StrMax, StrMin,     &
                               VsMax, VsMin,Xi, xLmax, xMagPoint, Yi)  
     
     

                    CASE DEFAULT

                    END SELECT


!******************************************************************************         
                  
!                       Calculate the ground motion for the current earthquake

                        !Input Variables to SynHaz passed in ModuleSynhaz
                        !--------------------------------------------------
                        !Values read from Menuin.csv
                        !hazparam  = selection of hazard parameter 
                        !            ("PGA", "PGV", "THY", etc...)
                        !NsimEMP   = Number of aleatory simulations of GM
                        !xc, yc    = coordinates of observation point
                        
                        !Values read from Menuin.csv 
                        !xVh       = ratio of healing to rupture velocity
                        !            Vh = Vr * xVh,  applies to all faults
                        !xVr       = ratio of rupture to shear wave velocity
                        !            Vr = Vs * xVr ,applies to all faults
                        !            (Long, Lat, in dec. degs)
                                                
                        !Values read from sources input file for each fault:
                        !dipsyn    = dip of the fault in dec. deg. 
                        !            Horz.=0 deg., Vert.=90 deg.
                        !SynStrike = fault strike. Dec. deg. from North. 
                        !            positive east (clockwise)
                        !Slipsyn   = Fault slip vector directions in dec. deg.
                        !            SS       =  0 deg.
                        !            Normal   = 90 degs.
                        !            Reverse = -90 degs.
                        
                        ! Output to SynHaz:
                        !------------------
                        !GMout     = calculated ground motion
                        !            For the time being GMout is a single 
                        !            value (PGA)
                                                                       
!                       Do loop on observation points
!                       -----------------------------
                        Do icrt = 1, NcritRD
                            !Station name and location
                            synStation  =  SiteName(icrt)
                            SynStationX =  CritXY(icrt,1)  !Longitude
                            SynStationY =  CritXY(icrt,2)  !Latitude    
                            xc = CritXY(icrt,1)
                            yc = CritXY(icrt,2)  
!
                            !Set flag to FULL synhaz calculation
                            Typecalc  = ' FULL'
                            Typecalc2 =   'no '
                            if ((SimMag1 .le. Magmaxpoint) .and.  &
                                (pointsource .eq. 'yes'))       then
                                !We want point-source approx. Reset Typecalc2
                                Typecalc  = 'POINT'
                                Typecalc2 =   'yes'
                            end if
                            !Allocate memory for synhaz
                            call synhazAllocation    &
                            (1,maxdim,maxegf,maxel,maxev,ipfl)

                            !------------
                            !Call SYNHAZ:
                            !------------    
                            !Scale moments per calibration Moment budget scaling
                            xxx = scaleM0(iepis)
                         
                            mopass1 = mopass * xxx
                            xxx = SynNp(1)
                            call synhaz (damp,freq,INgm,mopass1,nfreq,OUTgm,PARS, &
                                        rigidity,synflag,synflagRS,GMout1,Typecalc2) 
      !print *, '  '
      !print *, ' Frequency     Spectral Value  AFTER SYNHAZ'
      !do ifreq = 1, nfreq
      !    print *, freq(ifreq), PARS(ifreq)
      !end do

                            
                            !Deallocate memory
                            call synhazAllocation    &
                                 (2,maxdim,maxegf,maxel,maxev,ipfl)

                            if (GMout1 .gt. GMoutMax) then
                               GMoutMax = GMout1
                            endif
                            if (GMout1 .lt. GMoutMin)  then
                               GMoutMin = GMout1
                            endif

                            if (synflag .eq. 1)  then

                                write (*,732) 'iEpis/Nepis:',iepis,&
                                Nepis, Catfile(((iEpis-1)*NMspl)+ispl)
                                write (*,722)                      &
                                'ispl/NMspl:',ispl,'/',NMspl,      &
                                'Tp=',itp,'/',Ntp,inev1,'/',inev2, &
                                'Cumul=',inev,'/',iEND,            &
                                'M=',SimMag1,                      &
                                'M0=',mopass,                      &
                                'ScaleM0=',scaleM0(iepis),         &
                                'NSubElt=',nsub,                   &
                                Typecalc,                          &
                                'PGA(cm/s/s)=',GMout1

                            elseif (synflag .eq. 2)  then
                                write (*,732) 'iEpis/Nepis:',iepis,&
                                Nepis,Catfile(((iEpis-1)*NMspl)+ispl)
                                write (*,722 )                     &
                                'ispl/NMspl:',ispl,'/',NMspl,      &
                                'Tp=',itp,'/',Ntp,inev1,'/',inev2, &
                                'Cumul=',inev,'/',iEND,            &
                                'M=',SimMag1,                      &
                                'M0=',mopass,                      &
                                'ScaleM0=',scaleM0(iepis),         &
                                'NSubElt=',nsub,                   &
                                Typecalc,                          &
                                'PGV(cm/s)=',GMout1
                            else
                                write (*,732) 'iEpis/Nepis:',iepis,&
                                Nepis,Catfile((iEpis-1)+ispl)
                                write (*,722 )                     &
                                'ispl/NMspl:',ispl,'/',NMspl,      &
                                'Tp=',itp,'/',Ntp,inev1,'/',inev2, &
                                'Cumul=',inev,'/',iEND,            &
                                'M=',SimMag1,                      &
                                'M0=',mopass,                      &
                                'ScaleM0=',scaleM0(iepis),         &
                                'NSubElt=',nsub,                   &
                                Typecalc,                          &
                                'PGD(cm)=',GMout1
                            end if
                            print *,   &
                            'GMoutMin=',GMoutMin,' GMoutMax= ',GMoutMax

                            !If GMout1=0 there is a problem with the catalog
                            !Set BadMinetFlag to 0
                            if (GMout1 .le. 1.e-15)  then
                               BadMinetFlag = 0
                               !Skip all remaining events of the catalog. Exit both
                               !Aleatory and epistemic loop for this catalog
                               exit
                            end if

                            
                            GMaleatory(1) = Gmout1
                            GMout(inev) = GMout1

#ifdef USE_DISLIN
      !Plot the response spectra
      !-------------------------
    if (synflagRS .gt. 0)  then       
      !Load arrays
      do i = 1, nfreq
          do j = 1, Nc
              Xplot(i)   = freq(i)
              Yplot(i,j) = PARS(i)
              !print *, 'i=',i,'  Xplot(i)=',Xplot(i),'  Yplot(i)=',Yplot(i,j)
          end do
      end do
                    
      !call PlotRespSpec (CTIT,Ix,IxSCL,Iy1,NameLegends,Nc,nfreq,Xplot,Yplot)
    end if
                                 
#endif 

!*******************************************************************************
!                           SYNHAZ Progress and Check:
!                           --------------------------
                            if ((mopass.eq.0.) .or. (GMout1.eq.0.))  then
                               if (inev .eq. ievprint(ievprint2)) then
                                   ievprint2 = ievprint2 + 1
                                   print *,'SYNHAZ: Event',inev,'of',Nev,  &
                                   '---Site:',icrt
                                   print *,'M0=',mopass,'  Magnitude=',   &
                                   SimMag1,'---GMout1 =',GMout1
                                   print *, ' '
                               end if
                            end if
!*******************************************************************************
!                           Cumul GM value 
                            !-------------
                            !Aleatory simulation of SYNHAZ input (TBD)
                            !Case of single parameter GM definition 

                            ! 1. Retrieve magnitude bin sampling weight
                            !Which bin does xMSc fall into?
                            do i = 1, nbinMagSpl
                                if (Smag(i) .lt. SimMag1+0.001)  then
                                    ks = i
                                else
                                    exit
                                end if
                            end do 
                            wmspl = wMagbin(ks,itp)
                                                                
                            ! 2. Latin-HyperCube sampling the distribution of GM
                            !----------------------------------------------- 
                            !with nLHbins and using nLHSamples in each bin
                            !Limit the excursion to nSIGMA
                            !(Read from input sources file)
!                           xnSIGMA       = Max number of sigmas for sampling
!                                           read from sources file (source.txt)
                            !               Stored in module ModuleEq
                            !               xnSigma is what is used with GMPEs
!                           xnSigmaSynhaz = Same as xnSIGMA, but for use in synhaz
                            !               read as input from menu file
                            nLHbins    = 10   !L_H number of sampled bins
                            nLHSamples =  5   !L-H number of samples per bin
                            xnsigma    = xnSigmaSynhaz 
                            sigma      = SigmaSynhaz
                            zzk2 = ALOG (GMaleatory(1))

                            call LHCGM ( icrt, nGMbins, nLHbins, nLHSamples,  &
                            sigma, wmspl, xWeight, zzk2)

                            !do ii = 1, NsimEMP
 !                               zzk2 = ALOG (GMaleatory(1))
  !                              !if zzk2 .gt. last GM value add to last bin
   !                             !Weight and cumulate this value
    !                            do iGM = 1, nGMbins
     !                               kGM = iGM                              
      !                              if (GMarrayLog(iGM) .gt. zzk2)  exit
       !                         end do
        !                        !for the time being xgmt = 1.
         !                       wgmt = 1.
          !                      !xWeight - Calibration bin weight
           !                     work4(icrt,kGM) =   work4(icrt,kGM)   &
            !                                      + xWeight*wgmt 
                            !end do    ! end loop on ii SYNHAZ aleatory sampling

! Part for spectra needs to be updated as above******************************************
                            if (synflagRS .gt. 0)  then
                                !Case of Spectra
                                if (Nfreq .gt. 1)  then
                                    do i = 1, nfreq
                                        zzk2 = ALOG (PARS(i))
                                        !if (zzk2.gt.last GM) add to last bin
                                        !Weight and cumulate this value
                                        do iGM = 1, nGMbins
                                            kGM = iGM                              
                                            if (GMarrayLog(iGM) .gt. zzk2)  exit
                                        end do
                                        !for the time being xgmt = 1.
                                        wgmt = 1.
                                        !xWeight - Calibration bin weight
                                        work4SPEC(icrt,kGM,i) =      &
                                        work4SPEC(icrt,kGM,i) + xWeight*wgmt 
                                    end do
                                end if
                            end if
!*******************************************************************************

                         end do        ! end loop on icrt site index
                         if (BadMinetFlag .eq. 0)  then
                            exit
                         end if
                    
                End Do Loop3_Number_of_Events_in_Catalog    ! inev
                if (BadMinetFlag .eq. 0)  then
                   exit
                end if
                
            End Do Loop2_Sampling                           ! ispl
            if (BadMinetFlag .eq. 0)  then
                exit
            end if        
         
        !Hazard for this set of catalogs and this hazard time window
        !-----------------------------------------------------------
        !Case of single value parameter definition
        CALL HazEpis (CSTR1,CSTR2,iepis,itp,nCritRD,nGMbins,NMspl,Nscreen,    &
                      pagenumber1,PlotHazEpis,TrackNumber,timewindow)
        !Case of Spectra
        if ((Nfreq.gt.1) .and. (synflagRS.gt.0))  then
            CALL HazSpecEpis (iepis,itp,nCritRD,nfreq,nGMbins,NMspl,    &
                  Nscreen,pagenumber1,PlotHazEpis,TrackNumber,timewindow,Years)            
        end if

     END IF Skip_itp_cycle

     End Do Time_Window    !Ends loop on number of Hazard time windows


! COMBINATION of ALL THE HAZARD CURVES RESULTING FROM THE NEPIS EPISTEMIC 
! SIMULATIONS IS DONE IN MAIN:
!       Use the weight of each epistemic realization for linear combination
!       of the curves.
!       Obtain the histogram of hazard for each GM bin
!       Calculate peercentile values of Hazard for each GM bin
!       Plot resulting percentile curves

!SKIP Check
         go to 1234
!JBS718+++++++++++++++++++++++Test for GM distribution+++++++++++++++++++++++
!Open file for storing GM test values
        open(unit=8,file='TestGM.csv',status="UNKNOWN",iostat=IERR1)
!Test for distribution of GM
         
         ispl    = NMspl
         Nev1    = IendEq(ispl,Ntp)
         xmaxbin = -1.e6
         xminbin =  1.e6
         print *,'Nev1, Ntp =', Nev1,Ntp
         

         do ii = 1, Nev1
             zzk2 = GMout(ii)
             if (zzk2 .gt. 0.)  then
                 zzk2 = alog10 (zzk2)
             else
                 zzk2 = 1.e-6
             end if
             if (xminbin .gt. zzk2) xminbin = zzk2
             if (xmaxbin .lt. zzk2) xmaxbin = zzk2
         end do
         write (*,"(10e8.2)") (GMout(ii),ii=1,Nev1)
         !Sort the array of GM values
         do ii = 1, Nev1
             iGMout(ii) = ii
             GMout2(ii) = GMout(ii)
             print *, 'ii,GMout, iGMout:',ii, GMout(ii), iGMout(ii)
         end do
         call SortJBS ( iGMout, Nev1, GMout,GMout2)
         do ii = 1, Nev1
             j = iGMout(ii)
             !print *, 'ii,V , ir, V1:',ii, GMout(j), iGMout(ii), GMout(j)
             write (8,*) 'ii,V , ir, V1:',ii, GMout(j), iGMout(ii), GMout(j)
         end do

         !Set parameters for the Histogram steps
         imaxbin = INT ( xmaxbin )
         iminbin = INT ( xminbin )
         if (FLOAT(iminbin) .gt. xminbin+1.e-10)  then
             iminbin = iminbin - 1
         end if

         xbin0   = float (iminbin )
         Ndelta  = 10  !Number of steps in one log cycle
         Nbin    = (imaxbin - iminbin) * Ndelta
         delta   = 1. / Float(Ndelta) !step increment in one log cycle

         !Calculate the histogram

         !GMout is already sorted by increasing values 
         Histo  = 0
         do ii  = 1, Nev1
             k = iGMout(ii)
             if (zzk2 .gt. 0.)  then
                 zzk2 = alog10 ( GMout(k) )
             else
                 zzk2 = 1.e-6
             end if
             do j = 1, Nbin
                 xbinup = Float(iminbin) + delta*Float(j)
                 if (zzk2 .lt. xbinup)  then
                 Histo(j) = Histo(j) + 1
                 exit
                 end if
             end do
         end do

         !Check
         j = 0
         do ii = 1, Nbin
             j = j + Histo(ii)
         end do
         print *, 'Sum of Histo =', j
 
         !Write the histogram on file "TestGM.csv"
         do ii = 1, Nbin
              write (8,712) Histo(ii)
 !             print *, 'Histo(',ii,')=',Histo(ii)
         end do


        close (8)
1234    continue

!End Check**********************************************************************

712      format (i10)
722      format (a11,i2,a1,i1,2x,a3,i2,a1,i1,i4,a1,i5,2x,a6,i5,a1,i5,2x,a2, &
                f5.2,1x,a3,e9.2,1x,a8,e8.1,1x,a8,i6,1x,a5,1x,a11,e10.3)
732      format (a12,i3,'/',i3,'    ',a14)

        return

        end subroutine GMHR2
