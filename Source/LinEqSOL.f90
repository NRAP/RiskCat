!Linear solver solves the A.X = B equation using LU decomposition
!developed by J-P Moreau, 2011.
!******************************************************************************
        subroutine LinEqSOL (AM1, BM1, NM, XM)
        
        USE LU
        
        implicit none
        
        !Declarations for Moreau's linear solver
        integer :: d, i, j, rc, NM 
        
        REAL (KIND=8), dimension(NM,NM) :: AM, AM1 !real matrix (n x n)
        REAL (KIND=8), dimension(NM)    :: BM, BM1 !real vector (n)
        REAL (KIND=8), dimension(NM)    :: XM      !real vector (n)
        INTEGER,       dimension(NM)    :: INDX    !integer vector (n)
                
        
!**********************************************************************
do i = 1, NM
    BM(i) = BM1(i)
    do j = 1, NM
        AM(i,j) = AM1(i,j)
    end do
end do

!call LU decomposition routine
  call LUDCMP(AM,NM,INDX,D,rc)

!call appropriate solver if previous return code is ok
  if (rc.eq.0) then
      call LUBKSB(AM,NM,INDX,BM)
  endif

!print results or error message
  if (rc.eq.1) then
      write(*,*) ' The system matrix is singular, no solution !'
  else
!      write(*,*) ' System solution:'
      do i=1, NM
          XM(i) = BM(i)
 !         write(*,*) i,BM(i)
      end do
   end if        
        end subroutine LinEqSOL



