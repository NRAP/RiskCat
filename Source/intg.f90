      subroutine intg(tz,k,dt,maxdim)
 
      IMPLICIT NONE

      INTEGER :: i, j1, j3, k, k2, maxdim
      REAL    :: dt, pi2, w
      COMPLEX, dimension(maxdim) :: tz

      pi2 = 6.28318530717958
      j1 = ifix(float(k)/2. + 1.001)
      do  i = 2,j1
         w = pi2 * float(i-1)/(float(k)*dt)
         tz(i) = tz(i)/cmplx(0.0,w)
      end do
!-LJH 02/17/00, replace      tz(1) = cmplx(0.0,0.0)
      tz(1) = tz(2)
      k2 = k + 1
      j3 = j1 - 1
      do  i = 2,j3
         k2 = k2 - 1
         tz(k2) = conjg(tz(i))
      end do

      return
      end
