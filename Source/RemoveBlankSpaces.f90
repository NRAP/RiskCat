!
!******************************************************************************
        subroutine RemoveBlankSpaces  (NameIn,NameOut)
!******************************************************************************
!
!   This routine reads a generically formatted record of 80 characters 
!   (InAlpha), creates the separate strings of continuous non-blank
!   characters, and concatenates those strings into a single value without 
!   spaces in it.

!   INPUT:
!       NameIn  = character variable that contains blank spaces
!       The input is  a 80 character record maximum.
!       The first executable statement of the subroutine is to load it into a
!       character array of dimension 80.
!

!
!   OUTPUT:
!       NameOut = character variable with blank spaces removed

    IMPLICIT NONE
    
    INTEGER :: i,j

    CHARACTER (LEN=1)  :: a1
    CHARACTER (LEN=14) :: NameIn
    CHARACTER (LEN=14) :: NameOut 
    CHARACTER (LEN=1), DIMENSION(80) :: ALPHA
    CHARACTER (LEN=1), DIMENSION(80) :: ALPHA1
    
      a1      = ' '
      ALPHA   = ' '
      ALPHA1  = ' '      
      NameOut = ' '

!First executable statement:
      read (NameIn,"(80a1)") (ALPHA1(i),i=1,80)

!Test for blanks and write non blanks into ALPHA
      j = 0
      do i = 1, 80
          if (ALPHA1(i) .ne. a1)  then
              j = j + 1
              ALPHA(j) = ALPHA1(i)
          end if
      end do
      
!Write ALPHA into NameOut
!      open (unit=9,file="working",status="unknown",iostat=IERR)
!      write (9, "(80a1)") (ALPHA(i), i=1,j)
!      rewind 9
!      write (*,*)  j
!      write (*,*)  ALPHA
      if (j .eq. 9)    write (NameOut,"(9a)")  (ALPHA(i),i=1,j)
      if (j .eq. 10)   write (NameOut,"(10a)") (ALPHA(i),i=1,j)
      if (j .eq. 14)   write (NameOut,"(14a)") (ALPHA(i),i=1,j)
!      close (9)
!      write (*,*) j,NameOut
!      write (*,*) (ALPHA(i), i=1,80) 
     
      RETURN
      
      END SUBROUTINE RemoveBlankSpaces
