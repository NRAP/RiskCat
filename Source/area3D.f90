!cssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
        subroutine  area3D (p,area)
!
!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
!       this routine calculates the planar area of flat quadrilateral
!		in a 3-D space.

!		Input:
!         *****
!		p(i,j) =
!			The quadrilateral is defined by two segments: AB and CD
!			where AB and CD are parallel.
!			p(j,1) =longitude, in decimal degrees
!			p(j,2) = latitude, decimal degrees, and
!			p(j,3) = depth,  km
!			coordinates of the corners, stored in matrix p(4,3).
!
!		Output:
!         ******
!		area in (km**2)
!

        implicit none
        
        integer :: k
        real :: area,q1q2,q3q4 
        real :: p(4,3), q1(3), q2(3), q3(3), q4(3)

        do k = 1,3
                q1(k) = (p(1,k) + p(2,k)) / 2.
                q2(k) = (p(3,k) + p(4,k)) / 2.
                q3(k) = (p(1,k) + p(4,k)) / 2.
                q4(k) = (p(2,k) + p(3,k)) / 2.
        end do

        call dist (q1(1), q1(2), q2(1), q2(2), q1q2)
        call dist (q3(1), q3(2), q4(1), q4(2), q3q4)

        q1q2 = sqrt (q1q2*q1q2 + (q1(3)-q2(3))**2)
        q3q4 = sqrt (q3q4*q3q4 + (q3(3)-q4(3))**2)

        area = q1q2 * q3q4

        return
	
        end subroutine area3D
