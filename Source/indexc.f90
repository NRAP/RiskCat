! Lawrence Hutchings 04/19/2010, modified from Ryan Cary 2006
! Purpose: This function returns the index of the first empty
! character occurence in strIn. If none is found, the length
! of the string is returned.
! idem is the allowed character length 
! Variables: 
! strIn - The charactor array that is blank(' ') index positon is in question.
! idem  - maximum length of strIn in question.
! indexOut - actual lenght of strIn, returned by the subroutine.

        SUBROUTINE indexc(strIn,indexOut,idem)

        IMPLICIT NONE

        INTEGER                            ::  idem, indexOut, j
        CHARACTER (LEN=idem)               :: strIn
        CHARACTER (LEN=1), dimension(idem) :: a


        read(strIn,'(120a1)' )(a(j), j = 1,idem)

        do j = 1, idem
            if(a(j).eq.' ')then
                indexOut = j - 1
                return 
            endif
        enddo

        indexOut = idem

        return
 
        end subroutine INDEXC
