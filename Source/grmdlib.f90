!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
         subroutine  grmdlib  (attn,MaxAttnCoeff,xm,r,xi,zzk,   &
                               icat,ierrorgm,model)
!
!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss

!       This  routine is called by:  GMHR
!
!	This subroutine generates the natural log of median GM, zz(k) 
!       for a number of ground motion models. 
!	
! Input is:
!	model = model index, stored in attn(19)
!	xm    = magnitude value
!	xi    = intensity value
!       r     = actual arithmetic distance value
!	attn  = 1D- Array containing the coefficients of the ground
!               motion model with index as above: "model"
!	icat  = parameter used in the Trifunac accel. model (emodel)
!	zzk   = the output Neperian (base-e) logarithm of the 
!		calculated ground motion.
!
        
        implicit real (a-h,o-z)

        INTEGER :: icat, ierrorgm, imodel, iparam, iwarn
        INTEGER :: MaxAttnCoeff, model
        REAL    :: r, sigma, xi, xm, zzk
		
        real, dimension(MaxAttnCoeff) :: attn


        ierrorgm = 0

        Ground_Motion_models: select case  ( model )

            CASE (1)
               zzk= amodel (MaxAttnCoeff,xm,r,attn)

            CASE (2)
               zzk= bmodel (MaxAttnCoeff,xm,r,attn)

            CASE (3)
               zzk= cmodel (MaxAttnCoeff,xm,r,attn)

            CASE (4)
               zzk= dmodel (MaxAttnCoeff,xm,r,attn)

            case (5)
               zzk= emodel (MaxAttnCoeff,xi,r,attn,icat,xm)

            case (6)
               zzk= fmodel (MaxAttnCoeff,xm,r,attn,l)

            case (7)
               zzk= gmodel (MaxAttnCoeff,xm,r,attn)

            case (8)
               zzk= hmodel (MaxAttnCoeff,xm,r,attn)

            case (9)
               zzk= ximod (MaxAttnCoeff,xm,r,attn,l)

            case (10)
               zzk= xmod (MaxAttnCoeff,xm,r,attn)

            case (11)
               zzk= xjb82 (MaxAttnCoeff,xm,r,attn)

            case (13)
               zzk= xidrs (MaxAttnCoeff,xm,r,attn)

            case (14)
               zzk= xdabc (MaxAttnCoeff,xm,r,attn,l)

            case (15)
               zzk= xmod15 (MaxAttnCoeff,xm,r,attn)
               
            case (16)
               zzk= xtip98 (MaxAttnCoeff,xm,r,attn)

            case (17)
               zzk= xtoro97 (MaxAttnCoeff,xm,r,attn)

            case (18)
               zzk= xsab87 (MaxAttnCoeff,xm,r,attn)

            case (19)
               zzk= xrom (MaxAttnCoeff,xm,r,attn)

            case (20)
               !Abrahamson Silva, SRL 1997
               zzk= xabsilva(MaxAttnCoeff,xm,r,attn)

            case (21)
               !Sadigh et al. Rock,SRL 1997
               zzk= xsadighrock(MaxAttnCoeff,xm,r,attn)

            case (22)
               !Sadigh et al. Soil,SRL 1997
               zzk= xsadighsoil(MaxAttnCoeff,xm,r,attn)

            case (23)
               !Campbell, SRL 1997
               zzk= xcampbell97(MaxAttnCoeff,xm,r,attn)

            case (24)
               !Youngs Subduct.,SRL 1997
               zzk= xyoungs97(MaxAttnCoeff,xm,r,attn)

            case (25)
               !Campbell-Bozorgna(2007-NGA)
               zzk= xCB_NGA(MaxAttnCoeff,xm,r,attn)

            case (26)
               !Douglas, BSSA 2013
               imodel = INT( attn(1) )
               iparam = INT( attn(2) )
               ir     = INT( attn(3) )
               iwarn  = INT( attn(4) )
               call DOUGLAS_2013 (xm, imodel, iparam, ir, iwarn, r,sigma, zzk)
                

            case default; ierrorgm = 1

        end select Ground_Motion_models

        return

        end  subroutine  grmdlib
	
!******************************************************************************
!                 Library of Ground Motion Models
!******************************************************************************
!     model a (model index=1)
!     boore-atkinson rv-model
!  if model c(19)=1.55 new 11/4/92 Boore's model fit

      real function amodel (MaxAttnCoeff,xment,d,c)
!     function amodel (xment,d,c)
!
      implicit real (a-h,o-z)
      dimension c(MaxAttnCoeff)
      xm=xment
      pi=3.1416
! truncation for new Boore model c(19)=1.55
      if(c(19).gt.1.54999.and.c(19).lt.1.55001)then
      if(xm.gt.7.5)xm=7.5
      endif
! truncation for new herrmann 11/11/92 model c(19)=1.45
      if(c(19).gt.1.44999.and.c(19).lt.1.45001)then
      if(xm.gt.7.5)xm=7.5
      endif
      r=sqrt(d**2+c(20)**2)
      xlr=log10(r)
      xm2=xm**2
      xm3=xm*xm2
      l=0
      if (r.gt.100.) l=10
      if(c(19).gt.1.9.and.xm.gt.6.4)go to 45
   30 continue
      v=c(1+l)+c(2+l)*xm+c(3+l)*r+c(4+l)*xm2+c(5+l)*xm3+c(6+l)*xm*xlr  &
        +c(7+l)*xm2*xlr+c(8+l)*xm3*xlr
      if (r.gt.100.) go to 50
      v=v-xlr
      if(c(19).gt.1.5)v=v+c(10)*cos(pi*(xm-3.75))
!  change added for fit to Boore's 11/4/92 model
      if(c(19).eq.1.55)then
         jc10= INT(c(20))
         yc10=c(20)- REAL(jc10)
         v=v+yc10*cos(2.*pi*(xlr-2.375))
      endif
!  end of change
      go to 60
   45 if(r.gt.100.)go to 30
      v=-3.48+1.349*xm-.15636*xm2+.010277*xm3-.0383*xm*xlr+.00907*xm2*  &
         xlr-.000537*xm3*xlr-.001463*r-xlr
      go to 60
   50 v=v+c(9)*xlr
       if(c(19).lt.1.4)v=v+c(10)*cos(2.*pi*(xlr-2.375))
!
!
!    next card corrects error in logic  added  11/16/91
!
!    most cases ok because c(20) was a number like 15.000 km
!    but if c(20) = XX.X then a problem exists  in that xc10 .ne. 0.
!    original coding overlooked this point
!
       if(c(19).lt.1.5)go to 60
!
!   since coef of cos term is always small inclue in c(20) when
!   needed        c19=1.1 std model if c10 not zero then with
!   c10cos(2pi(logr-2.375)) term in ff
!   for cases when both ff and nf branches are involved  the coef for the
!   ff branch is carried as the dec part of the depth term c20 term
!             c19=1.6  with cos(pi(m-3.75)) in near field branch
!              c19=1.8 cos(pi(m-3.75)) in both nf & ff branches
!             c19=1.85 cos(pi(m-3.75)) in nf branch & cos(2pi(logr-2.375)) in ff
!   c19=1.91  aki's hardwired model for charleston large mag fit nf
!
      if(c(19).gt.1.7)then
      if(c(19).gt.1.9)go to 60
      else
      ic10= INT(c(20))
      xc10=c(20)- REAL(ic10)
      if(c(19).lt.1.84)then
      v=v+xc10*cos(pi*(xm-3.75))
      elseif(c(19).gt.1.84)then
      v=v+xc10*cos(2.*pi*(xlr-2.375))
      endif
      endif
   60 amodel=v*2.3026

      return
      end function amodel
      
!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

!     model b  (model index=2)
!  torro-mcguire rv-model

      real function bmodel (MaxAttnCoeff,xm,d,c)                        

      implicit real (a-h,o-z)                                         

      dimension c(MaxAttnCoeff)

      r=sqrt(d**2+c(20)**2)                                            
      sr=sqrt(r)                                                       
      xlr=log10(r)                                                     
      xm2=xm**2
      xm3=xm2*xm
      if (r.gt.100.) go to 100
      v=c(1)+c(2)*xm+c(3)*r-xlr+c(5)*xm2+c(6)*xm3+c(7)*xm*r   &
        +c(4)*(xm-4.25)*(xm-8.)*sin(3.1415926535898*(xm-4.5))           
      go to 200
  100 v=c(11)+c(12)*xm+c(13)*r+c(14)*xlr+c(15)*xm2+c(16)*xm3+c(17)*xm*r   &
        +c(18)*xm*xlr+(c(8)+c(9)*xm2+c(10)*xm3)/sr
  200 bmodel=v*2.3026

      return
      end function bmodel
      
!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

!     model c (index=3)

!     expert 2's rv-accel model

      real function cmodel (MaxAttnCoeff,xm,d,c)                     

      implicit real (a-h,o-z)                                         

      dimension c(MaxAttnCoeff)

      xm2=xm**2
      xm3=xm2*xm
      l=0
      h=2.5*(xm-1.)
      if (xm.lt.5.) h=5.*(xm-3.)
      r=sqrt(d**2+h**2)                                               
      xlr=log10(r)                                                    
      if (r.gt.100.) go to 100
      if (xm.le.4.5) l=10
      v=c(1+l)+c(2+l)*xm+c(3+l)*r-xlr+c(4+l)*xm2+c(5+l)*xm*r   &
        +c(6+l)*xm2*r +c(7+l)*xm*xlr+c(8+l)*xm3*xlr
      go to 200
  100 v=2.772+.248*xm-.00119*r-3.432*xlr+.000135*xm*r   &
        +.501*xm*xlr-.0288*xm2*xlr+.00208*cos(8.378*(xlr-2.))         
  200 cmodel=v*2.3026

      return
      end function cmodel
      
!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

!     d model  (index=4)

!     expert 3's rv-5a accel model

      real function dmodel (MaxAttnCoeff,xm,d,c)                    

      implicit real (a-h,o-z)                                         

      dimension c(MaxAttnCoeff)

      xm2=xm**2
      xm3=xm2*xm
      r=sqrt(d*d+64.)                                                  
      xlr=log10(r)                                                     
      if (r.gt.100.) go to 100
      v=c(1)+c(2)*xm+c(3)*r-xlr+c(4)*xm2+c(5)*xm2*r+c(6)*xm*xlr   &
        +c(7)*xm2*xlr+c(8)*xm3*xlr
      go to 200
  100 v=c(11)+c(12)*xm+c(13)*r+c(14)*xlr+c(15)*xm2+c(16)*xm3+c(17)*xm*r   &
        +c(18)*xm3*xlr-.0197*cos(8.378*(xlr-2.))                        
  200 dmodel=v*2.3026

      return
      end function dmodel
      
!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

!       model e  (model index = 5)
!       *******
!     trifunac-anderson  accel model
!
      real function emodel(MaxAttnCoeff,xi,d,c,icat,xm)              

      implicit real (a-h,o-z)                                         
      dimension  c(MaxAttnCoeff)
      r=d
      if(r.lt.c(9))r=c(9)
      if(c(19).lt.5.7)then
      alr=log(r)                                                      
      else
      alr=log10(r)                                                     
      endif
      xsit=0.
      if(icat.eq.1)xsit=2.
      if(icat.eq.2.or.icat.eq.3)xsit=1.
      if(icat.eq.6.or.icat.eq.7)xsit=1.
      if(c(19).gt.5.2)go to 20
      emodel=c(1)+c(2)*xi+c(3)*r+c(4)*alr + c(5)*xsit
      go to 30
 20   xi0=c(11)+c(12)*xm+c(13)*xm*xm
      if(xi0.ge.12) xi0=12
      xis=c(1)+c(2)*xi0+c(3)*r+c(4)*alr
      emodel=c(6)+c(7)*xis+c(5)*xsit
       if(c(19).gt.5.7)emodel=emodel*2.30259
  30  continue
        return
        end function emodel
        
!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

!       model f (model index = 6)
!     model f (model index = 6)
!     *******

      real function fmodel (MaxAttnCoeff,xm,d,c,l)                    

      implicit real (a-h,o-z)                                         

      dimension c(MaxAttnCoeff)
!
!*************************** added 7/30/91 ************
      if(c(10).gt.3.)then
      if(xm.gt.c(10))l=10
      endif
! ***************************************************************

      if (c(5).gt.1.) go to 100
      if (xm.lt.5.) h=5.*(xm-3.)
      if (xm.ge.5.) h=2.5*(xm-1.)
      go to 101
  100 h=c(5)
  101 r=sqrt(d**2+h**2)                                                
      fmodel=c(1+l)+c(2+l)*xm+c(3+l)*log(r)+c(4+l)*r                   

      return
      end function fmodel
      
!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

!     model g  (model index=7)

!     expert 2's rv spectral model

      real function gmodel (MaxAttnCoeff,xm,d,c)                      

      implicit real (a-h,o-z)                                         

      dimension c(MaxAttnCoeff)

      xm2=xm**2
      xm3=xm*xm2
      h=2.5*(xm-1.)
      r=sqrt(d*d+h*h)                                                  
      xlr=log10(r)                                                     
      if (r.gt.100.) go to 100
      v=c(1)+c(2)*xm+c(3)*r-xlr+c(4)*xm2+c(5)*xm*r+c(6)*xm2*r +   &
        c(7)*xm*xlr+c(8)*xm3*xlr+c(20)*sin(1.57*(xm-6.5))               
      go to 200
  100 v=c(11)+c(12)*xm+c(13)*r+c(14)*xlr+c(15)*xm2+c(16)*xm*r +   &
        c(17)*xm*xlr+c(18)*xm2*xlr+c(9)*cos(8.378*(xlr-2.)) +     &     
        c(10)*cos(1.57*(xm-5.))                                         
  200 gmodel=v*2.3026

      return
      end function gmodel

!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

!      model  h   (index = 8 )

!     expert 3's rv-5sv model for spectra

      real function hmodel (MaxAttnCoeff,xm,d,c)                       

      implicit real (a-h,o-z)                                         

      dimension c(MaxAttnCoeff)

      xm2=xm**2
      xm3=xm2*xm
      r=sqrt(d*d+c(20)*c(20))                                          
      xlr=log10(r)                                                     

      if (r .gt. 100.)  then
         sr=1./sqrt(r)                                                    
         v=c(11)+c(12)*r+c(13)*xlr +c(14)*xm2 + c(15)*xm3 + c(16)*sr
         if (c(19).eq.8.1) v=v + .000202*xm*r -.0225*cos(8.378*(xlr-2.14))
         if (c(19).eq.8.2.or.c(19).eq.8.3)                    &
            v=v + c(17)*xm3*r + c(18)*xm*xlr + c(10)*xm3*xlr
         if (c(19).lt.8.4) then
            hmodel=v*2.303
            return
         end if
         cs=0.
         if (c(19).eq.8.5) cs=1.
         v= v + (c(17)*xm + c(18)*xm2 +c(10)*xm3)*xlr         &
              - cs*.0337*cos(3.1415926535898*(xm-5.))
      else
         cs = 0.
         cc = 1.
         if ((c(19)-8.4) .gt. 0.)  then
            cs = 1.
            cc = 0.
         end if
         v=c(1)+c(2)*r-xlr+c(3)*xm2+c(4)*xm3 +c(5)*xm*r +     &
           (c(6)*xm +c(7)*xm2 + c(8)*xm3)*xlr  +              &
           c(9)*(cs*sin(2.098*(xm-5.))+cc*cos(1.047*(xm-5.)))             
      end if
   
      hmodel=v*2.303

      return
      end function hmodel
      
!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss

      subroutine numark2 (MaxAttnCoeff,xm,r,attn,z,iflagav)

!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss

!     this routine is used only when dealing with newmark spectra.
!     for a (any) frequency, the sv value is calculated in two ways.
!          1. the spectrum considered to be anchored on a velocity
!             attenuaion curve
!          2. the spectrum considered to be anchored on an acceleration
!             attenuation curve.
!     the value of sv used is taken as the smaller of the two above
!     values.
!     the velocity part of the eqn is given by the first 8 coefficients
!     the acceleration part is given by coeficients 11 to 18 inclusive.

!     the variable iflagav is a flag which indicates if we are in
!     the velocity/acceleration regime or in the acceleration regime.
!     iflagav = 1 vel/acc regime , we calculate both
!               2 acc regime only. calculate only acceleration.

      implicit real (a-h,o-z)                                         

      dimension attn(MaxAttnCoeff),mod(2),x(2)

      mod(1) = INT(attn(19))
      mod(2) = INT(attn(20))
      m2 = 3 - iflagav

!     loop over the two parts of the equation
      x(2) = 0.
      do  i = 1,m2
         l = 20 - (i*10)
         m = mod(i)

         if (m .eq. 6)  then
            x(i)=fmodel(MaxAttnCoeff,xm,r,attn,l)
         end if
         if (m .eq. 9)  then
            x(i)=ximod(MaxAttnCoeff,xm,r,attn,l)
         end if
      end do

      z = x(1)
      if (iflagav .ne. 2) then

         if (z .ge. x(2)) then
            z = x(2)
            return
         else
            iflagav = 2
         end if     
      end if

      return

      end subroutine numark2

!sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
        subroutine trifun  (MaxAttnCoeff,xi,r,alr,vl1,attn,icat,p)
!
!sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
!       this routine returns the probability of the spectral velocity
!       being smaller than vl for a site located at distance r from the
!       source of intensity xi.
!       the site correction factor is given by its natural log., corsite.
!       the equations are taken from trifunac and anderson report ce 77-03
!       and trif. & lee  ce 85-04
!       of usc.
!

      implicit real (a-h,o-z)                                         

        dimension  attn(MaxAttnCoeff)
!   convert  trifunac & lee to cm/sec
      attn3=attn(3)+.405
!
      xsit=0.
      if(icat.eq.1)xsit=2.
      if(icat.eq.2.or.icat.eq.3)xsit=1.
      if(icat.eq.6.or.icat.eq.7)xsit=1.
!
!       calculate the attenuated intensity via modified gupta-nuttli eq.
        xis = attn(4) + xi + attn(5)*r + attn(6)*alr
!
!       calculate the pl
        pl = ((vl1/2.30259) - attn(2)*xis - attn3 - attn(14)*xsit )   &
            / attn(1)
!
!       calculate the pa
        z = exp(attn(7)*pl + attn(8))                                  
        p = 1. - exp(-z)                                               
        if (attn(11) .gt. 1.)  p = p**attn(11)
        return
        end subroutine trifun

!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
!
!     Model  xabsilva    index  =  20
!
!     added 4/24/2001
!     Abrahamson and Silva model: Seismological Research Letters
!     January/February 1997
!
        !
        real function xabsilva  (MaxAttnCoeff,xmm,d,c)

          implicit real (a-h,o-z)
          dimension c(MaxAttnCoeff)

          r=sqrt(d*d+c(8)**2)
          if (xmm .le. c(7))  then
             xc = c(2)
          else
             xc = c(4)
          end if
!
!         Fault type factor
!         c(15) = F  = 1.0 for Reverse
!                      0.5 for reverse/oblique
          !                      0.0 for others
          xf = c(5)
          if ((xmm.gt.5.8) .and. (xmm.le.c(7)))  then
             xf = c(5) + ((c(6)-c(5))/(c(7)-5.8))*(xmm-5.8)
          endif
          !
          if (xmm .gt. c(7)) then
             xf = c(6)
          endif
          !
          y = c(1) + xc*(xmm-c(7)) + c(12)*(8.5-xmm)*(8.5-xmm) +    &
               (c(3) + c(13)*(xmm-c(7)))* log(r) +xf*c(15)
!
!         Site properties factor
!         c(16) = S  =  1 for Rock and  Shallow soil
!                       0 for Deep Soil
!         In the SRL equation, the unit of y is g's. Since we have
!         transformed everything in cm/s/s, need to go back to g's
          !         in following statement.
          xs = exp (y) / 981.
          xs = c(16) * (c(10) + c(11)*(log(xs+c(14))))

          xabsilva = y + xs 
!         xabsilva = y + xs + 0.16187 !with correction for HW at 7.5km and M6

          return
        end function xabsilva

!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

!   model  xcampbell97    index  =  23
!
!   added 5/2/2001
!   Campbell, SRL, Jan/Feb. 1997 model
!
      !
      real function xcampbell97 (MaxAttnCoeff,xm,d,c) 

        implicit real (a-h,o-z)
        dimension c(MaxAttnCoeff)
!
        !
        f   = c(11) ! Style of faulting =0 for SS, =1 for reverse)
        ssr = c(12) ! Local site conditions (ssr=shr=0) for alluv/firm soil
        shr = c(13) ! (ssr=1,shr=0)/soft rock and (ssr=0,shr=1)/hard rock
        xd  = c(14) ! Depth to basement (=0 for Rock site)
        xs  = c(15) ! Depth to the seismogenic zone. By definition, the
        ! distace REIS has to be greater than th edistance to the
        ! seismigenic zone (d >+ xs)
        if (d .lt. xs)  then
           d = xs
        end if
        xd = 5.0
!
!	Calculate the natural log of Ah expressed in g's 
        !	(per SLR 97, page 164)
        if (xm .le. 0.) then
           test = 0.
        end if
        y = -3.512 + 0.904*xm - 0.664*log(d*d+(.0222*exp(1.294*xm)))  &
             + (1.125 - 0.112*log(d) - 0.0957*xm) * f                 &
             + (0.440 - 0.171*log(d)) * ssr                           &
             + (0.405 - 0.222*log(d))* shr
!
!	Calculate the Spectral ordinates, in g's
        !	(per SLR 97, page 164)
        yy = 0.
        if ( abs((c(1)+c(2)+c(3)+c(4)+c(5)+c(6))) .gt. 1.e-6)  then
           fsa = 0.
           if (xd .lt. 1.)  then
              fsa = c(6)*(1.-shr) * (1.-xd) + 0.5*c(6)*(1.-xd)*ssr
           end if
           yy = c(1) + c(2)*tanh(c(3)*(xm-4.7))                      &
                + (c(4)+c(5)*xm)*d + 0.5*c(6)*ssr + c(6)*shr         &
                + c(7)*tanh(c(8)*xd) * (1.-shr) + fsa
        end if
        y = y + yy
!
        !      convert from units of g's to cm/s/s
        xcampbell97 = y + 6.889
        !
        return
      end function xcampbell97
      
! FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
!     Campbell and Bozorgnia (2007-NGA) model
!
!     THIS IS MODEL NUMBER : 25

      FUNCTION xCB_NGA (MaxAttnCoeff,xm,d,c) 

        implicit none
        integer :: MaxAttnCoeff
        real  ::  xCB_NGA
        real, intent(in) :: d,xm
        real  ::  Delta,Fdis,Fflt,ffltZ,Fmag,Fhng,FhngD,FhngM,FhngR
        real  ::  Fnm,Frv,Rjb,ZTOR,FhngZ
        rEAL, dimension(MaxAttnCoeff) :: c
	
	
!       Setting some parameters for the specific case of the ANPP
!Considering that the most important faults are nearby (SaF and NWF)
!and that we do not know the rake for NWF and Saf is SS, then
        Frv =   0.
        !Same considerations for Fnm
        Fnm =   0.
        !The most damaging EQ from SaF or NWF would be for M>=6, for which
        !the rupture plane would probably cut to the surface. then ZTOR = 0.
        ZTOR =  0.
        ffltZ = 0.
        if (ZTOR .ge. 1.)  then
            ffltZ = 1.
        end if
        !Delta i sht edip of the rupture plane, in degrees.
        Delta = 65.
        !Condidering that the faults are mostly SS vertical, Rjb = Rrup
        Rjb  = d

!Fmag------------------------------------------------------
        Fmag = c(1) + c(2)*xm
        if (xm .gt. 5.5)  then
            Fmag = Fmag + c(3)*(xm-5.5)
            if (xm .gt.6.5)  then
                Fmag = Fmag + c(4)*(xm-6.5)
            end if
        end if
        
!Fdis------------------------------------------------------
        Fdis = (c(5)+c(6)*xm) * 0.5*alog(d*d+c(7)*c(7))
        
!Fflt------------------------------------------------------
        Fflt = c(8)*Frv*ffltZ + c(9)*Fnm
        
!Fhng------------------------------------------------------
        fhngR   = 1.
        if (Rjb .gt. 0.)  then
            fhngR = max(d,(sqrt(Rjb*Rjb + 1.)-Rjb)) /  &
                    max(d,sqrt(Rjb*Rjb + 1.))
          else
            fhngR = (d - Rjb) / d
        end if
  
        fhngM  = .0
        if (xm .gt. 6.)  then
            fhngM  = 2. * (xm-6.0)
        end if
        if (xm .gt. 6.5)  then
            fhngM  = 1.
        end if
        
        fhngZ  = 0.
        if (ZTOR .lt. 20.) then       
            fhngZ  = (20. - ZTOR) / 20.
        end if
        
        fhngD  = 1.
        if (Delta .gt. 70.)  then
            fhngD  = (90. - Delta) / 20.
        end if
        
        Fhng   = c(10) * fhngR * fhngM * fhngZ * fhngD
        
!Fsite and Fsed are not considered since site specific studies will be done
!for ANPP.

        !Natural Log of the ground motion
        xCB_NGA = Fmag + Fdis + Fflt + Fhng
        !Convert from G's to cm/s/s
        xCB_NGA = XCB_NGA + 6.889
        
        return
        
        end function xCB_NGA
        
!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

!  model xdabc   index=14
!
!  model in PG&E LTSP rpt   more generally sadigh's model
!
!    c(8)   plays the role of fault type
!
!   c(20) plays the role of a dummy depth to compare to J&B model
!
!
      real function xdabc(MaxAttnCoeff,xm,d,c,l)                   

      implicit real (a-h,o-z)                                         

      dimension c(MaxAttnCoeff)
!****************************************************************
!
!      added next 2 lines   7/30 /91
      if(xm.gt.c(10))l=10
      r=sqrt(d*d+c(20)*c(20))                                          
!
!******************* removed next 2 lines 7/30/91 **************
!      r=d
!      if(d.le.15.)r=dsqrt(d*d+25.)                                     
!
!****************************************************************
      xdabc=c(1+l)+c(2+l)*xm+c(3+l)*(8.5-xm)**c(4+l) +c(5+l)*log(r+  &   
            c(6+l)*exp(c(7+l)*xm)) + c(8)                                    
      return
      end function xdabc

!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

!    model xidrs   index= 13
!
!  added 5/21/90
! idriss's 1987 model  fron j&b 1988 summary paper
!
!
      real function xidrs(MaxAttnCoeff,xm,d,c)                       

      implicit real (a-h,o-z)                                         

      dimension c(MaxAttnCoeff)
      r=d
!
!    note next line added to account for area zone around the site
!    more generally may want to delete this line
      if(d.le.15.)r=sqrt(d*d+25.)                                      
      xm2=xm*xm
      xm3=xm2*xm
      xm4=xm3*xm
!
      xlna=c(1)+c(2)*xm+c(3)*xm2 + c(4)*xm3 + c(5)*xm4
      d=c(11) + c(12)*xm + c(13)*xm2 + c(14)*xm3 + c(15)*xm4
      xidrs=xlna + d*log(r+20.)                                        
      return
      end function xidrs
      
!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

!     model xi  (index = 9 )

!     expert 2's n-h spectral model using his rv-5a, v models

      real function ximod (MaxAttnCoeff,xm,d,c,l)                   

      implicit real (a-h,o-z)                                         

      dimension c(MaxAttnCoeff)

      if (l.eq.10) go to 100
      h=2.5*(xm-1.)
      xm2=xm**2
      xm3=xm2*xm

!  vel part
      r=sqrt(d**2+h**2)                                                
      xlr=log10(r)                                                     
      if (r.gt.100.) go to 50
      v=-3.169 + 1.024*xm - .0245*r - xlr -.0206*xm2 + .00587*xm*r -  &
         .000348*xm2*r -.00087*xm2*xlr -.0162*sin(1.57*(xm-6.5))         
      go to 75
   50 v=-5.398 +1.998*xm-.00026*r -1.663*xlr -.129*xm2 +.0198*xm2*xlr &
        +.0144*cos(1.57*(xm-5.))                                        

!  note the .22 is the log of the amp fact for n-h velto sv
   75 ximod=(v+.22)*2.303
      return

  100 continue

!   accel part
      v=cmodel(MaxAttnCoeff,xm,d,c)

!   note v has been converted to log base e in cmodel
      ximod=v-c(9)

      return
      end function ximod
      
!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

!  model  xjb82    index  =  11
!
!    added 5/21/90
!   joyner and boore's 1982 model
!
!   c(5)  plays the role of the h parameter in their model
!
      real function xjb82(MaxAttnCoeff,xmm,d,c)

      implicit real (a-h,o-z)                                         
      dimension c(MaxAttnCoeff)

      r=sqrt(d*d+c(5)**2)                                              
!
!  if c(11) .ne. 0. then using atkinson's model  so must convert xm to
!  moment magintude
      xm=xmm
      if(c(11).gt.0.)then
      xm=c(11)+c(12)*xm+c(13)*xm*xm
      endif
!
      xjb82=c(1)+c(2)*(xm-6.)+c(3)*(xm-6.)**2 +c(4)*log(r)+c(6)*r
!
!     Case for very small Earthquakes scaling for M<6
      if (xmm .lt. 4.5)  then
          xjb82 = c(1) - c(2)*1.5 + c(4)*log(r)
!         Magnitude scaling  
          xjb82 = xjb82 - 1.7*(4.5 - xmm)
      end if  
!
!     At this point the model is for use parameters with NATURAL LOG
!	 convert to base e
!      xjb82=xjb82*2.3026
      return
      end function xjb82
      
!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

!     added 5/21 /90
      !
      real function xmod(MaxAttnCoeff,xm,d,c)                       

        implicit real (a-h,o-z)
        dimension  c(MaxAttnCoeff)
!
! ken campbell's 1989  models
!
!   c(8) plays the role of fault type
!   c(16)  is depth to bedrock
!   c(20) plays the role of a dummy depth to compare to the J&B model
!   c(17) plays the role of rupture direction  in some of campbell's
!   earlier models    c(18) = coef of rupture direction
!      r=dsqrt(d*d+c(20)*c(20))
      r=d
!
!    note next line added to account for area zone around the site
!    more generally may want to delete this line
      if(d.le.15.)then
         r=sqrt(d*d+25.)
      end if
      xmod=c(1)+c(2)*xm+c(3)*log(r+c(4)*exp(c(5)*xm))+c(6)*r+c(7)*c(8)  &
           +c(17)*c(18)
      if(c(11).gt.0)then
         xmod=xmod+c(11)*tanh(c(12)*(xm+c(13)))+c(14)*tanh(c(15)*c(16))
      endif
   return
    end function xmod
      
!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
!
!  model  xmod15    index=15
!   generic model of the form
!  ln(a)=c1+c2*m+c3*m**2 +c4*ln(r) +c5*r
!
!   in particu;ar form used for my fit to WCC model for the NPR at INEL
      !
      real function xmod15(MaxAttnCoeff,xm,d,c)
        
        implicit real (a-h,o-z)
        dimension c(MaxAttnCoeff)
      r=d
!
!     this line added to take care of adding a depth term for the two
!      area zones  in which the INEL NPR site sits
!      for more general use remove
      !
      if(r.le.15.)then
         r=sqrt(r*r+25.)
      end if
      xmod15=c(1)+c(2)*xm+c(3)*xm**2 + c(4)*log(r) +c(5)*r
      return
    end function xmod15

!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
      !
      real  function xrom (MaxAttnCoeff,xm,d,c)
!
!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
!
!	Model type:  Romanian study, 1997     
!	Index = 19
!	For use in dealing with the Vrancea zone.
!
!	Ln(y) = c1 + c2.M + c3.Ln(Rh+c4)
!	Natural logs
!	cm/s/s, Local/Ms magnitudes, Hypocentral distance km
   !
   implicit real (a-h,o-z)
   dimension c(MaxAttnCoeff)
 rh = sqrt (d**2 + c(5) **2)
 xrom = c(1) + c(2)*xm + c(3)*log(rh+c(4))
 return
end function xrom

!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
 !
 real  function xsab87 (MaxAttnCoeff,xm,d,c)
!
!	Model type: Sabetta and Pugliese, 1987
!	Index = 18
!	For use in generic environment. From Italian data
   !
   implicit real (a-h,o-z)
   dimension c(MaxAttnCoeff)
 r = d**2 + c(3) **2
 r = log (r) / 2.
 xsab87 = c(1) + c(2)*xm - r
 return
end function xsab87
	
!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
!
!     Model  xSadighRock    index  =  21
!
!     added 5/1/2001
!	Sadigh, Youngs et al. Rock model: Seismological Research Letters
!	January/February 1997
!
 !
 real function xsadighrock  (MaxAttnCoeff,xm,d,c) 

        implicit real (a-h,o-z)
        dimension c(MaxAttnCoeff)
        !
        ! Shift in attn indeces for m>6.5
        k = 0
        if (xm .gt. 6.5)  then
           k = 10
        end if
       !
       xkm = 8.5 - xm
       if (xm .gt. 8.5)  then
          xkm = 0.
       end if
       y = c(1+k) + (c(2+k)*xm) + c(3+k)*xkm**2.5 +               &
           c(4+k)*log(d+exp(c(5+k)+c(6+k)*xm)) + c(7+k)*log(d+2.)
!
 !	Transform g's into cm/s/s
        xsadighrock = y + 6.889
        return
end function xsadighrock
      
!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
!
!     Model  xSadighSoil    index  =  22
!
!     added 5/1/2001
!	Sadigh, Youngs et al. Soil model: Seismological Research Letters
!	January/February 1997
!
!
      real function xsadighsoil  (MaxAttnCoeff,xm,d,c)
        implicit real (a-h,o-z)
        dimension c(MaxAttnCoeff)
!
 x4 = 2.1863
 x5 = 0.32
 if (xm .gt. 6.5)  then
    x4=0.3825; x5=0.5882
 end if
 !
 xkm = 8.5 - xm
 if (xkm .lt. 0.)  then
    xkm = 0.
 end if
 y = c(1) + c(2)*xm - c(3)*log(d+ (x4*exp(x5*xm))) + c(6) +   &
      c(7)*xkm**2.5 
 !
 !	Transform g's into cm/s/s
        xsadighsoil = y + 6.889
        return
   end function xsadighsoil
   
!fffffffffffffffffffffffffffffffffffffffffffffffffffffffff
!
!       model xtip98  index = 16
!
!       added july 28,1998, by JBS
!
!       Model developed for the TIP project, by N. Abrahamson
!	c(19) is the model type (=16)
!	c(10) is the magnitude threshold (6.25)
!	c(11), c(12) and c(13) are for the Mblg to Mw
!	      conversion
!	Values of PGA are in cm/s/s
!	Values of PGV are in cm/s
!	Log are Natural Logs
   !
   real function  xtip98 (MaxAttnCoeff,xmm,d,c)
   implicit real (a-h,o-z)
   dimension c(MaxAttnCoeff)
 !
 r = sqrt (d*d + c(15)*c(15))
 xm = xmm
 !	convert Mblg to Mw if c(11) non-zero
 if (c(11) .gt. 0.)  xm=c(11)+xm*c(12)+c(13)*xm*xm
 ccm = c(2)
!	ccm is the coeff of (M-m1). Change for xm above 
 !	magnitude threshold
 if (xm .gt. c(10))  ccm = c(14)
 !
 xmc = xm - c(10)
 xtip98 = c(1) + ccm*xmc + c(3)*((8.5-xm)**2) +   &
      (c(4) + c(5)*xmc)*log(r) +c(6)
return
end function xtip98
	
!
 !ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
 real  function  xtoro97 (MaxAttnCoeff,xm,d,c)
!
!	Mode; type Tore 97 for Midcontinents, Moment Magnitude
!	Index = 17
   !
   implicit  real (a-h,o-z)
   dimension  c(MaxAttnCoeff)
 !
 xm6 = xm - 6.
 rm = sqrt (d*d + c(7)**2)
 rx = log (rm)
 ry = log (rm/100.)
 if (ry .le. 0.) ry = 0.
 xtoro97 = c(1) + (c(2) + c(3)*xm6)*xm6 - c(4)*rx   &
      - (c(4)-c(5))*ry - c(6)*rm
 return
end function xtoro97

!ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        real  function  xyoungs97 (MaxAttnCoeff,xm,d,c)
!
!	Model type Youngs SRL 1997 for Subduction zones
!		xm = M = Moment Magnitude
!		d  = distance to rupture (km)
!		c  = coefficients
!		c(6) = H = dominant depth
!		c(7) = Zt = 0 for Interface
!					1 for Intraslab
!		
!	Model Index = 24
          !
          implicit  real (a-h,o-z)
          dimension  c(MaxAttnCoeff)
 !
        xH = c(6)
        Zt = c(7)
        xyoungs97 = c(8) + c(11)*xm + c(1) + c(2)*((10.-xm)**3)
        xyoungs97 = xyoungs97 + c(3)*log(d+c(12)*exp(c(13)*xm))
        xyoungs97 = xyoungs97 + c(14)*xH + c(15)*Zt
        return
        end function xyoungs97
