!
!*******************************************************************************
!
        SUBROUTINE CreateFileName (Ftype,Fcase,Ntrack,NameExt,OutName)
!
!*******************************************************************************
!
!       Generates a filename, mostly for creating a plot file name
!      
!       Inputs:
!       *******
!       Ftype    : Type of info in the file.             (Must be 3 characters)
!                  Exemples: 'REC','HAZ','NUIS' for 
!                  Hazard results
!                  Reccurence curves
!                  Nuisance curves
!                  etc, as desired
!       Fcase      Some label that identifies the project (9 characters max)
!                  Exemple: 'Salah','NRAP", etc
!       Ntrack     Tracking number of the run  (number between 1 and 999)
!       NameExt    Extension. 'PDF', 'CSV' etc.
!
!       Ftype, Fcase and Ext are read from input menu file (default MenuIn.csv)
!
!       Output:
!       *******
!       OutName    Created Full name of the file, including extension
!                  as follows:
!                  Outfile = Fcase-Ftype-Ntrack.NameExt
!
!*******************************************************************************
!    
        implicit none

        CHARACTER (LEN=3)   :: Atrack, Ftype 
        CHARACTER (LEN=9)   :: Fcase
        CHARACTER (LEN=3)   :: NameExt
        CHARACTER (LEN=22)  :: OutName
        CHARACTER (LEN=256) :: STRING

        CHARACTER (LEN=1), DIMENSION(256)  :: Alpha1,Alpha2,Alpha3,Alpha4

        INTEGER :: i, Ntrack
        INTEGER :: N1,N11,N12,N2,N21,N22,N3,N31,N32,N4,N41,N42

        REAL    :: track
!
!*******************************************************************************

        write (STRING,*) Fcase
        call AlphaN (Alpha1,N1,N11,N12,STRING)
        write (STRING,*) Ftype
        call AlphaN (Alpha2,N2,N21,N22,STRING)
  
        track = Float (Ntrack)
        i = INT ( Alog10(track) + 1.)
        SELECT CASE (i)
            CASE (1)    !Ntrack is 1 to 9
                write (Atrack,"(a2,i1)")  '00',Ntrack
            CASE (2)    !Ntrack is  10 to 99
                write (Atrack,"(a1,i2)")   '0',Ntrack
            CASE (3)    !Ntrack is  100 to 999
                write (Atrack,"(i3)")          Ntrack
            CASE DEFAULT
        END SELECT
        write (STRING,*) Atrack
        call AlphaN (Alpha3,N3,N31,N32,STRING)

        write (STRING,*) NameExt
        call AlphaN (Alpha4,N4,N41,N42,STRING)

        write (Outname,"(256a1)")        &
              (Alpha3(i),i=1,N3),        &
              (Alpha1(i),i=1,N1),'-',    &
              (Alpha2(i),i=1,N2),        &
              '.',                       &
              (Alpha4(i),i=1,N4)

        return
        end SUBROUTINE CreateFileName
