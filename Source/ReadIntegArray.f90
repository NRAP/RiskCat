!
!.....This routine reads a record, max 80 characters containing integer values.
!     Finds the integer values and load them sequentially in array "OutArray"
!     Values must be separated by blanks or "-" (dash)
!     Loaded sequentially when separated by blanks
!     Generated from first to last, when separated by "-"
!     Ex:
!       if Record read:  1 3 9    5-8
!       Array OUtArray is : 1,2,9,5,6,7,8
!Input:
!******
!     N         = minimum Number of values to be read, 
!                 possibly from several record lines
!
!Output:
!*******
!     m         = number of integers actually found in the records
!     OutArray()= Array of dimension m, loaded with m integer values read

!Note: 
!     The values stored in file (unit1) can be on several lines of records
!     The number of values read (m) can be different from the minimum number 
!     of values expected (N)
!
!Subroutines needed:
!*******************
!       DUMREADER ()
!
!-------------------------------------------------------------------------------
!
        subroutine  ReadIntegArray  (MaxEQsources,N,m,OutArray,unit1)
        
        implicit none
        
        integer :: dash,i,j,k,loop,m,MaxEQsources
        integer :: N,n1,n2,Nfirst,Nlast,Nstring,unit1
        
        integer, dimension(80)            :: Nchar
        integer, dimension (MaxEQsources) :: OutArray
        
        character (len=1) ::  char1
        character (len=80)::  String2
        
        character (len=1),  dimension(80) :: char
        CHARACTER (LEN=80), DIMENSION(80) :: DUMREAD
        
!*******************************************************************************

        char1 = '-'
        m     = 0
        
        Read_Lines: do loop = 1,100
        if (m .ge. N) EXIT
        
        !Read the record and load  Nstrings strings in DUMREAD
        call DUMREADER (DUMREAD,Nchar,Nstring,unit1)
        
        !Take each string and find if it has '-' in it
        do i =1, Nstring
        char  = ' '
        n1 = Nchar(i)
        read (DUMREAD(i),"(80a1)") (char(j),j=1,n1)
            dash = 0
            do k = 1,80
                if (char(k) .eq. '-')  then
                    dash = 1
                end if
            end do
            if (dash .eq. 0)  then
                !There is no dash: Only one integer number
                m = m + 1
                read (DUMREAD(i),*) OutArray(m)
            else
                
                !There is a dash: Generate sequence
                !Find and load the first integer number into OutArray
                n1 = 0
                do k = 1, 80
                    if (char(k) .eq. '-')  then
                        exit
                      else
                        n1 = n1 + 1
                    end if
                end do
                m = m + 1
                write (string2,"(80a1)") (char(k),k=1,n1)
                read(string2,*) Nfirst
                OutArray(m) = Nfirst
                !
                !Find last integer in sequence and load sequence into OutArray
                n1     = 0
                do k = 1, 80
                    if (char(k) .eq. '-')  then
                        exit
                    else
                        n1 = n1 +1
                    end if
                end do
                n2 = n1 + 1
                do k = n1+2,80
                    if (char(k) .eq. ' ')  then
                        exit
                    else
                        n2 = n2 + 1
                    end if
                end do
                write (string2,"(80a1)") (char(k),k=n1+2,n2)
                read(string2,*) Nlast
                do k = Nfirst+1,Nlast
                    m = m + 1
                    OutArray(m) = k
                end do
            end if
        
        end do
        
        end do Read_Lines

        return
        
        end subroutine ReadIntegArray
