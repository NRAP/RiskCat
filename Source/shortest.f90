!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
        subroutine  shortest  (sx,sy,apex,dd)
!
!ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
!
!       this routine calculates the shortest distance between a point
!		on the surface of the earth and a quadrilateral in the 
!		earth crust,in a 3-D space.
!		The shortest distance is obtained by scanning through a digitized
!		version of the quadrilateral.

!		Input:
!         *****
!			Sx, Sy = coordinates of the site at the surface. 
!					 Longitude, Latitude in decimal degrees.
!			apex(4,3) = coordinates of the 4 corners of the quadrilateral
!					 Longitude, Latitude in decimal degrees, and depth
!					 in km.
!		Output:
!         ******
!			dd = shortest distance in km
!

        implicit none 
        integer :: iline,i1,k,n,nline
        real :: alfax,alfay,alfaz,dd,delta,d1,d2,d3,rx,ry,sum,sx,sy
        real :: Tx,Ty,Tz,xp,xt 
        real :: apex(4,3), q(4,3), p(2,3)

        dd = 1.e10
        ry = (6371.2 * 6.2831853/360)  ! length of 1 degree latitude
        rx = ry * cos(sy/57.2957795)   ! length of 1 degree longitude at S
!
!		Scan through lines that are apart 1/100-th of the width of the 
!		quadrilateral.
!		Calculate the shortest distance from each line Q1-Q2 to the site 
        nline = 100

    scanlines:  do iline = 1,nline+1
        i1 = iline - 1
        do  k = 1,3
            q(1,k) = apex(1,k) + (i1*(apex(2,k) - apex(1,k))/ nline)
            q(2,k) = apex(4,k) + (i1*(apex(3,k) - apex(4,k))/ nline)
        end do

!	transform to coordinates from Global Long-Lat-Depth into 
!       X,Y,Z system centered in S
        do  n = 1,2
            p(n,1) = (q(n,1)-sx) * rx
            p(n,2) = (q(n,2)-sy) * ry
            p(n,3) = q(n,3)
        end do

!	locate the point T,: intersection of line P1-P2 with plane perpandicular
!	to P1-P2 and passing througth S.
      !	coefficients directeurs
        alfax = p(2,1) - p(1,1)
        alfay = p(2,2) - p(1,2)
        alfaz = p(2,3) - p(1,3)
        !normalize them
        sum = sqrt (alfax*alfax + alfay*alfay + alfaz*alfaz)
        alfax = alfax / sum
        alfay = alfay / sum
        alfaz = alfaz / sum
!       parametric value of point T on line P1-p2
        xt = - (alfax*p(1,1) + alfay*p(1,2) + alfaz*p(1,3))
!       coordinates of T
        Tx = p(1,1) + alfax*xt
        Ty = p(1,2) + alfay*xt
        Tz = p(1,3) + alfaz*xt

!	scalar product of vectors TA.TB to test whether T is between P1 and P2
        xp = (p(1,1) - Tx) * (p(2,1) - Tx)  &
        + (p(1,2) - Ty) * (p(2,2) - Ty) &
        + (p(1,3) - Tz) * (p(2,3) - Tz)

!	is T between P1 and P2 ?
        if (xp .gt. 0.)  then
       !T is outside of P1-P2. The shortest distance is Min(SA,SB)
!			call  dist  (q(1,1),q(1,2),sx,sy,d1)
!			d1 = sqrt (d1*d1 + q(1,3)*q(1,3))
!			call  dist  (q(2,1),q(2,2),sx,sy,d2)
!			d2 = sqrt (d2*d2 + q(2,3)*q(2,3))
            d1 = 0.
            d2 = 0.
            do k = 1,3;   d1 = d1 + p(1,k)*p(1,k);   end do
            d1 = sqrt (d1)
            do k = 1,3;   d2 = d2 + p(2,k)*p(2,k);   end do
            d2 = sqrt (d2)
            d3 = d1
            if (d3 .gt. d2)  d3 = d2
        else
!T is between P1 and P2, the shortest distance is ST
!		transform coordinates back to Global Long-Lat-Depth system
!		Tx = (Tx/rx) + sx
!		Ty = (Ty/ry) + sy
!		call  dist  (Tx,Ty,sx,sy,d3)
!		d3 = sqrt (d3*d3 + Tz*Tz)
            d3 = sqrt (Tx*Tx + Ty*Ty + Tz*Tz)
        end if
!
!		I believe that the variation of the distance from S to the surface
!		of the element is always monotonous. Therefore, if d3 increases we 
!		have already found the shortest distance. 
!		Get out of the scanning loop
        delta = d3 - dd
        dd = d3
        if (delta .ge. 0.)  return

    end do scanlines

    return

    end subroutine shortest
