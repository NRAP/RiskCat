!
!*******************************************************************************
!
        subroutine PeakRandom ( np, pkacc, theta, trar, trat )
!
!*******************************************************************************
!
!      This routine Calculates the max.horiz. g.m. vector for a direction theta
!
!       Input:
!       ******
!       np    = number of points in time histories
!       traz  = vertical component of acceleration is not used
!       trar  =  horizontal x direction
!       trat  =  horizontal y direction
!       theta = direction of final vector in horizontal plane
!
!       Output:
!       *******
!       pkacc = peak vector acceleration in theta direction
!
!*******************************************************************************
!
        implicit none
!
        integer :: i, np
        real    :: cosx, GMvector, pkacc, sinx, theta, xgm, ygm
        real, dimension(np) :: trar, trat


!******************************************************************************    
!
        cosx = cos (theta)
        sinx = sin (theta)
        pkacc = -1.e20
        !Horizontal GM vector GMvector, peak
        do i = 1, np
            xgm =   trar(i)*cosx + trat(i)*sinx
            ygm = - trar(i)*sinx + trat(i)*cosx
            GMvector = sqrt (xgm*xgm + ygm*ygm)
            if (GMvector .gt. pkacc)  then
                pkacc = GMvector
            end if
        end do  

       return

       end subroutine PeakRandom

