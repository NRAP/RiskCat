      subroutine filter(tz,k,dt,filow,filhi,Maxdim)

      IMPLICIT NONE

      INTEGER :: i, j1, j3, k, k2, Maxdim
      REAL    :: as, bs, dds, ds, dt, es, filhi, filow, fs, pi, t, ws
      COMPLEX, dimension(Maxdim) :: tz

!      parameter (maxdim=80000)
 !     dimension tz(maxdim)
  !    complex tz
      pi = 3.14159265
      j1 = ifix(float(k)/2. + 1.001)
      t = 30.0
      do 55 i = 1,j1
      ws = float(i-1)/(float(k)*dt)
      if(ws.ge.filow.and.ws.le.filhi) go to 55
      if(ws.lt.filow) as = 2.0 * t * (filow - ws)
      if(ws.gt.filhi) as = 2.0 * t * (ws - filhi)
      bs = sin(pi*(1.0 + as))/(pi*(1.0 + as))
      dds = pi * (1.0 - as)
      if(dds.ne.0.0) ds = sin(dds)/dds
      if(dds.eq.0.0) ds = 1.0
      es = 0.5 * (bs + ds)
      fs = sin(pi*as)/(pi * as) + es
      tz(i) = tz(i)*cmplx(fs,0.0)
   55 continue
      k2 = k + 1
      j3 = j1 - 1
      do 65 i = 2,j3
      k2 = k2 - 1
      tz(k2) = conjg(tz(i))
   65 continue
      return
      end
