#ifndef IN_STRUCT_H
#define IN_STRUCT_H

#define MAX_EQ_ENTRIES 10000
//#define MAX_FAULTS 10

typedef struct ITagged {
  //----------------------------
  //PARAMETERS OF THE SIMULATION
  //----------------------------
  double AMin, AMax;        /* rate coeff (dimensionless) */
  double fA;                /* Reduction factor for A during rupture propagation (A' = fA*A) */
  double BMin, BMax;        /* state coeff (dimensionless) */
  double DcMin, DcMax;      /* critical slip distance (m) */
  double mu0Min, mu0Max;    /* steady-state friction at ddot = ddotStar */
  double ddotStar1;         /* normalizing constant in constitutive law */
  double ddotAB1;           /* 2a -> 2b transition slip speed */
  double alphaMin, alphaMax;    /*  */
  double theta0Min, theta0Max;  /* inital value of state variable (s) */
  double tau0Min, tau0Max;  /* initial value of shear stress (MPa) */
  double sigmaMin, sigmaMax;/* normal stress (MPa) */
  double sigmaZGrad;        /* vertical gradient of normal stress (MPa/m) */
  double sigmaFracPin;      /* if the normal stress on a patch drops to this times the
                               initial normal stress, the patch will be pinned */
  double maxThetaPin;       /* if the state variable theta on any patch exceeds this value + 1e3*t, the
                               patch will be locked */
  double ddotEQ1;           /* earthquake sliding speed (m/s) if constant for the whole model */
  double stressOvershootFactor; /* should be > 0 and < 1 (and closer to zero than 1),
                                   Elements will stay in state 2 until
                                   the shear stress drops to tauSS(ddotEQ) - sOF*(tau0 - tauSS(ddotEQ) */
  double lameLambda, lameMu;    /* Lame parameters (MPa) */
  int slowSlip;             /* if all patches are regular, this should be 0. If all patches are slow-slip
                               this should be 1.  Otherwise a slipSlipFname will need to be specified
                               (and if so, this param is ignored) */
  int nEq;                  /* for how many events to run the simulation */
  double maxT;              /* maximum amount of simulated time to run (s) */
  double tStart;            /* start time of simulation (mostly for use when
                               restarting to continue previous runs) */

  //----------------------------
  //FAULT GENERATION
  //----------------------------
//  int nFaults;           /* number of faults in the simulation */
//  int nLength[MAX_FAULTS]; /* how many cells along each fault's length */
//  int nHeight[MAX_FAULTS]; /* how many cells along each fault's height */
  double faultDepth;            /* depth of the center of the fault */
  double faultLength;           /* length of the fault */
  double faultHeight;           /* height of the fault */
  double cellSize;         /* size of a fault path */
  double strikeAngle;      /* strike angle of the fault in degrees */
  double dipAngle;         /* dip angle of the fault in degrees */
  double rakeAngle;        /* rake angle of the fault in degrees */
  double ddot;             /* earthquake sliding speed (m/s) */

} ICommonStruct;

typedef struct OTagged {
	double outEQ_t0[MAX_EQ_ENTRIES];		//0: time (s)
	double outEQ_M0[MAX_EQ_ENTRIES];		//1: energy (J)
	double outEQ_logM0[MAX_EQ_ENTRIES];		//2: magnitude (log(J))
	double outEQ_center0[MAX_EQ_ENTRIES];	//3: x (m)
	double outEQ_center1[MAX_EQ_ENTRIES];	//4: y (m)
	double outEQ_center2[MAX_EQ_ENTRIES];	//5: z (m)
	double outEQ_u0[MAX_EQ_ENTRIES];		//6: ux
	double outEQ_u1[MAX_EQ_ENTRIES];		//7: uy
	double outEQ_u2[MAX_EQ_ENTRIES];		//8: uz
	double outEQ_slip[MAX_EQ_ENTRIES];		//9: slip = M0/(mu*area), since mu*area*slip = M0 (m)
	double outEQ_lameMu[MAX_EQ_ENTRIES];	//10: lame' mu (Pa)
	double outEQ_area[MAX_EQ_ENTRIES];		//11: area (m^2)
	double outEQ_dt[MAX_EQ_ENTRIES];		//12: rise time (s)
	int outEQ_hypocenter[MAX_EQ_ENTRIES];//13: patch index
	int outEQ_index;						//14: index of current EQ
} OCommonStruct;
#endif
