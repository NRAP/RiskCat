subroutine Xnordf (x, p)

! Normal Gaussian of x

implicit none

real (kind = 8) erfc2, p, sqrt2, x , x1
sqrt2 = SQRT ( 2. )

x1 = x / sqrt2
p = 1. - (0.5*erfc2( x1 ))

return
end subroutine Xnordf