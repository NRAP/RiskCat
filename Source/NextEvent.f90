!Select one of the Earthquake source,time of occurrence in the time step
! and magnitude events
!
!   Input:
!       Sources properties, passed in ModuleEQ
!       dt      =  Time window where EQ can occur. It is the remianing time
!                  within a simulation time step, left for other eqs to occur.
!       xM0     = Starting Magnitude values for PSHA. Here probably 2 or less
!       Parameter of uncertainty on the number of events as a function of M
!       xFactor =  variability (Aleatory) in number of events in a mag bin
!                  used to calculate a multiplicating factor that varies as 
!                  a function of M. Zero for m = 0, and = Factor for M = Mmax.
!                  
!   
!   Output:
!       Nsc  = ID of selected Source
!       xMsc = Magnitude selected
!       Time = Time of occurrence since begining of time step 
!

!*******************************************************************************
        SUBROUTINE NextEvent (dt,M0,MSc,NSc,Time,xFactor)
                             
          use ModuleEQ
          use GENparam                   
          
          IMPLICIT NONE
          
          INTEGER :: i,is,j,imag,Nsc        
          
          REAL :: alfa,beta,Lambda
          REAL :: dt,M0,M1,M2,Mc,MSc,Ninbin,PM0
          REAL :: r1,rnunf,sum
          REAL :: testEQ,Time,TotalNeq,xmax
          REAL :: x1,x2,xFactor,xFactorMc
          
          REAL, dimension(nMagbins) :: CDFMagbin
          
                    
!          REAL, ALLOCATABLE,dimension(:,:,:) :: binrates
          
!*******************************************************************************
        NSc = 0
        
!......1. First determine if any Eq > M0 is generated in this time window
        testEQ = 1.
        Do i = 1, Ns
         SELECT CASE (SeisRectype(i))                   
           !Truncated exponential model
           CASE (1)
           !Mmax. 
           xMax = ArecUpdt(i,7)
           !beta.
           beta = ArecUpdt(i,4)
           !alfa.
           Lambda = ArecUpdt(i,3) 
           alfa = alog(Lambda) + beta*ArecUpdt(i,1)
           !alfa is for 1 year. We need it for the time step duration dt
           alfa = alfa + alog(dt)
           !Truncated Exponential: Input G-R parameters
                   M1 = XMagLBnd(1)
                   M2 = XMagHBnd(1)
                   MSc = (M1 + M2) / 2.
                   xFactorMc = xFactor * (MSc/xMax)
                   call  EQTruncRec (alfa,beta,dt,M0,xMax,M1,M2,Ninbin,PM0)
                   !Pm0 is the probability of experiencing an event .ge. to M0
                   !for duration dt(in years):
                   ! (1.-Pm0) is prob of NO events
                   testEQ = testEQ*(1.-Pm0)
                                   
           !Characteristic model (Youngs-Coppersmith)
           CASE (2)                   
           !Segmented fault model (Foxall Model)
           CASE (3)
           !Empirical model manually entered
           CASE (4)
           CASE DEFAULT
           END SELECT
        end do
        
        !Prob of at least ONE event in region
        testEQ = 1. - testEQ
        !Simulate if any EQ occurs:
        r1 = rnunf()
        if (r1 .gt. testEQ) then
        !There is NO EVENTS this step, return a NSc = 0
        return
        end if
        
        
!......2. THERE IS AT LEAST ONE EVENT generated this step:
!       First select the magnitude bin
!       Second, select the source that produces it

! First:
!       For each source, simulate number of events in each magnitude bin
!       and cumulate in each bin, over all sources
        CDFMagbin = 0.
        CDFbin    = 0.
        do is = 1, Ns
           i = Mapzid(kZMap,is)
           SELECT CASE (SeisRectype(i))                   
           !Truncated exponential model
           CASE (1)
           !Mmax. 
           xMax = ArecUpdt(i,7)
           !beta.
           beta = ArecUpdt(i,4)
           !alfa.
           Lambda = ArecUpdt(i,3) 
           alfa = alog(Lambda) + beta*ArecUpdt(i,1)
           !alfa is for 1 year. We need it for the time step duration dt
           alfa = alfa + alog(dt)
           !Truncated Exponential: Input G-R parameters
               do j = 1,NMagbins
                   M1 = XMagLBnd(j)
                   M2 = XMagHBnd(j)
                   Mc = (M1 + M2) / 2.
                   xFactorMc = xFactor * (Mc/xMax)
                   call  EQTruncRec (alfa,beta,dt,M0,xMax,M1,M2,Ninbin,PM0)
                   !Ninbin is the number of events expected in the magnitude
                   !bin [M1 - M2], for this source. 
                   !Some Aleatory variability in occurrence rates
                   !Simulate mult factor: 
                   !Equal Zero at M=0 and FactorMc at xMax
                   r1 = rnunf()
                   x1 = alog (xFactorMc)
                   x2 = exp (x1*(2*r1-1))
                   !Cumul of events in each mag bin, over all sources
                   CDFMagbin(j) = CDFMagbin(j) + Ninbin*x2
               end do 
                                   
           !Characteristic model (Youngs-Coppersmith)
           CASE (2)                   
           !Segmented fault model (Foxall Model)
           CASE (3)
           !Empirical model manually entered
           CASE (4)
           CASE DEFAULT
           END SELECT
        end do    
!      
!       Normalize the distribution of events in mag bins: CDFMagbin
        TotalNeq = 0.
        do  j = 1, nMagbins
            TotalNeq     = TotalNeq + CDFMagbin(j)
            CDFMagbin(j) = TotalNeq
        end do 
        do j = 1, nMagbins;  CDFMagbin(j)=CDFMagbin(j)/TotalNeq;  enddo

       !Debug...................................
       !print *,'NextEvent line 152: CDFMagbin():'
       !print *,'********************************'
       !do j = 1, nMagbins
       !print *, CDFMagbin(j)
       !end do

!       Select the magnitude bin from the distribution
        !Random U[0-1],  r1
        r1 = rnunf()
        do j = 1, nMagbins
            if (r1 .lt. CDFMagbin(j))  then
                iMag = j
                exit
            end if
        end do
        
! Second:
!       Select the source that has the next event
        NSc = 1
!       Build distribution of sources for the selected magnitude bin
        CDFbin = 0.
        M1 = xMagLBnd(imag)
        M2 = xMagHBnd(imag)
        Mc = (M1 + M2) / 2.
        MSc = Mc
        
        do j = 1,Ns
            xFactorMc = xFactor * (Mc/xMax)
            call  EQTruncRec (alfa,beta,dt,M0,xMax,M1,M2,Ninbin,PM0)
            !Simulate number of event in bin for each source
            r1 = rnunf()
            x1 = alog (xFactorMc)
            x2 = exp (x1*(2*r1-1))
            !Cumul of events in selected bin over all sources
            CDFbin(j) = CDFbin(j) + Ninbin*x2        
        end do
        
        !Create CDF
        sum = 0.
        do j = 1,Ns
            sum      = sum + CDFbin(j)
            CDFbin(j)= sum
        end do
        
        !Normalize
        do j = 1, Ns; CDFbin(j) = CDFbin(j)/sum;  enddo
        
        !Select the Source
        r1 = rnunf()
        do i = 1, Ns
            if (r1 .lt. CDFbin(i))  then
               NSc = i
               exit
            end if
        end do
!
! Third: Simulate the time of occurrence, assuming Poisson distribution
!       of interarrival time of events greater thwn M0
        !Mean rate = Lamda = dt / TotalNeq , per year
        !CDF = 1 - exp[-t/Lamda] 
        !For random r1: r1 = 1 - exp[-Time/Lamda]
        r1 = rnunf()
        Time = (dt/TotalNeq)*3. !Use at most 3 sigmas from mean
        if (r1 .lt. 1.0)  then
            Time = -(dt/TotalNeq) * alog(1.-r1)
        end if
           
!          DEALLOCATE (binrates, STAT=IERR)
    !      if (ierr /= 0) then
   !           !Could not deallocate
  !            write (6,*) 'Could not deallocate array binrates, STOP'
 !             STOP
!          end if                        
          
          
        return
        end subroutine NextEvent
